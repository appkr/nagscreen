## Caveat
Vagrant provisioning scripts. DO NOT USE THIS AT PRODUCTION ENVIRONMENT.

## How to connect to Vagrant VM from a same sub-net for test purpose.
http://your_ip_address:8080

**Note** Before connecting to the VM, please make sure you have set your VM's `/etc/apache2/sites-available/000-default.conf` file.
```
ServerName vm_host_name
DocumentRoot /var/www/public


<Directory /var/www/public>
     Options -Indexes +FollowSymLinks
     AllowOverride All
     Require all granted
</Directory>
```
