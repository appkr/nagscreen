#!/usr/bin/env bash

# VARIABLES
DOCUMENT_ROOT=/vagrant

echo "--- Good morning, master. Let's get to work. Installing now. ---"

echo "--- Updating packages list ---"
sudo apt-get update

echo "--- MySQL time ---"
sudo debconf-set-selections <<< 'mysql-server mysql-server/root_password password root'
sudo debconf-set-selections <<< 'mysql-server mysql-server/root_password_again password root'

echo "--- Installing base packages ---"
sudo apt-get install -y vim curl python-software-properties

echo "--- We want the bleeding edge of PHP, right master? ---"
sudo add-apt-repository -y ppa:ondrej/php5

echo "--- Updating packages list ---"
sudo apt-get update

echo "--- Installing PHP-specific packages ---"
sudo apt-get install -y php5 apache2 memcached sendmail libapache2-mod-php5 php5-curl php5-gd php5-mcrypt mysql-server-5.5 php5-mysql git-core php5-readline php5-memcache php5-sqlite php5-dev gearman libgearman-dev php5-gearman libsqlite3-dev

echo "--- Installing and configuring Xdebug ---"
sudo apt-get install -y php5-xdebug

cat << EOF | sudo tee -a /etc/php5/mods-available/xdebug.ini
xdebug.scream=1
xdebug.cli_color=1
xdebug.show_local_vars=1
EOF

echo "--- Enabling mod-rewrite ---"
sudo a2enmod rewrite

echo "--- Setting document root ---"
sudo rm -rf /var/www
sudo ln -fs $DOCUMENT_ROOT /var/www


echo "--- What developer codes without errors turned on? Not you, master. ---"
sed -i "s/error_reporting = .*/error_reporting = E_ALL/" /etc/php5/apache2/php.ini
sed -i "s/display_errors = .*/display_errors = On/" /etc/php5/apache2/php.ini

sed -i 's/AllowOverride None/AllowOverride All/' /etc/apache2/apache2.conf

echo "--- Restarting Apache ---"
sudo service apache2 restart

echo "--- Composer is the future. But you knew that, did you master? Nice job. ---"
curl -sS https://getcomposer.org/installer | php
sudo mv composer.phar /usr/local/bin/composer

# echo "--- Set timezone to KST. ---"
# sudo ln -sf /usr/share/zoneinfo/Asia/Seoul /etc/localtime

echo "--- Install mysql server 5.6 ---"
sudo wget -O mysql-5.6.deb http://dev.mysql.com/get/Downloads/MySQL-5.6/mysql-5.6.17-debian6.0-i686.deb
# sudo wget -O mysql-5.6.deb http://dev.mysql.com/get/mysql-apt-config_0.3.1-1ubuntu12.04_all.deb
sudo dpkg -i mysql-5.6.deb
sudo apt-get -y install libaio1
sudo apt-get -y remove mysql-common mysql-server-5.5 mysql-server-core-5.5 mysql-client-5.5 mysql-client-core-5.5
sudo apt-get -y autoremove
sudo cp /opt/mysql/server-5.6/support-files/mysql.server /etc/init.d/mysql.server
# cp /etc/mysql/my.cnf /etc/my.cnf
sudo update-rc.d -f mysql remove
sudo update-rc.d mysql.server defaults

echo "--- Mysql server 5.6 installed ---"
sudo sed -i "s/PATH = .*/PATH = /usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/opt/mysql/server-5.6/bin/" /etc/environment
# /home/ubuntu/.composer/vendor/bin
source /etc/environment
which mysql

echo "--- Mysql server setting ---"
sudo sed -i "s/basedir = .*/basedir = /opt/mysql/server-5.6" /etc/mysql/my.cnf
sudo sed -i "s/lc-messages-dir = .*/lc-messages-dir = /opt/mysql/server-5.6/share" /etc/mysql/my.cnf
sudo sed -i "s/table_cache/table_open_cache" /etc/mysql/my.cnf

echo "--- Restart and update mysql server ---"
sudo service mysql.server start
sudo mysql_upgrade -u root -p root

# Laravel stuff here, if you want

echo "--- All set to go! Would you like to play a game? ---"
