(function(){
    var ua = navigator.userAgent.toLocaleLowerCase(),
        playstoreUrl = "market://details?id=" + airplug.package,
        pcUrl = "https://play.google.com/store/apps/details?id=" + airplug.package,
        openUrl = "custom_url_scheme://activity?query=value";

    $('.open-hook').click(function () {
        var frame_id = '__check_app__',
            $iframe = $('#' + frame_id),
            clickedAt = +new Date;

        if (!$iframe[0])
            $iframe = $('<iframe id="' + frame_id + '" />').hide().appendTo('body');

        setTimeout(function () {
            if (+new Date - clickedAt < 2000) {
                if (/android/.test(ua)) $iframe.attr('src', playstoreUrl);
//                else if (/iphone|ipad|ipod/.test(ua))
//                {
//                    $iframe.attr('src', appstoreUrl);
//                    version = 5;
//                }
                else $iframe.attr('src', pcUrl);
            }
        }, 500);

        $iframe.attr('src', openUrl);
    });
})();
