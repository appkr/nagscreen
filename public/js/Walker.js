var $iframe = $("#myFrame"),
    $spinnerHolder = $(".spinner-holder"),
    $loadingIcon = $("<i></i>", {
      "class": "fa fa-spinner fa-spin text-center"
    }),
    $startButton = $("button.start-test"),
    $stopButton = $("button.stop-test"),
    $counter = $(".counter"),
    $siteUrl = $(".site-url"),
    $loadingTime = $(".loading-time"),
    $avgLogdingTime = $(".avg-loading-time"),
    $testStartTime = $(".test-start-time"),
    $stopFlag = false,
    networkErrorCount = 0,
    errorCount404 = 0,
    errorCount422 = 0,
    timer;


var Report = function(startTime) {
  this.startTime = startTime;
  this.loadingStart = 0;
};


var Walker = {

  config: {
    sleep: 3000,
    seed: "http://google.com",
    apiEndPoint: "http://promote.airplug.com/test/get-random-url"
  },

  counter: 0,

  previousUrl: "",

  currentUrl: "",

  reports: [],

  report: new Report(new Date().getTime()),

  init: function (config) {
    $testStartTime.text(new Date(this.report.startTime));

    $.extend(this.config, config);
    this.registerEventHandler();
    this.updateButton();
    this.runTest(); // initial test
  },

  registerEventHandler: function() {
    var self = Walker;

    $startButton.on("click", self.handleTestStarted);
    $stopButton.on("click", self.handleTestStopped);
  },

  handleTestStarted: function(){
    var self = Walker;

    $("div.js-flash-message").remove();
    $stopFlag = false;
    self.updateButton();
    self.runNext(); // second test after
  },

  handleTestStopped: function(){
    var self = Walker;

    $stopFlag = true;
    self.flashMessage("Test stopped by user interruption! Press START TEST button to resume.", "info", 10000);
    self.updateButton();
    $loadingIcon.remove();
  },

  handleIframeLoadingDone: function() {
    var self = Walker;

    var iframeLoadingTime = new Date().getTime() - self.report.loadingStart;

    $loadingIcon.remove();
    self.reports.push(iframeLoadingTime);
    $loadingTime.text((iframeLoadingTime / 1000).toFixed(1));
    $avgLogdingTime.text((self.getAvgLoadingTime() / 1000).toFixed(1));

    self.runNext();
  },

  getRandomWebpageUrl: function() {
    var self = Walker;

    return $.ajax({
      url: self.config.apiEndPoint,
      type: "GET",
      data: {
        seed: self.currentUrl || self.config.seed
      },
      crossDomain: true,
      timeout: 30000,
      cache: false,
      beforeSend: function () {
        $spinnerHolder.append($loadingIcon);
      },
      statusCode: {
        404: self.handle404,
        422: self.handle422
      }
    });
  },

  successCallback: function(response, textStatus, jqXHR) {
    var self = Walker;

    networkErrorCount = 0;
    errorCount404 = 0;
    errorCount422 = 0;

    if (response) {
      self.previousUrl = self.currentUrl;
      self.currentUrl = response.siteUrl;
      self.loadIframe();
    } else {
      self.currentUrl = self.previousUrl;
      self.runNext();
    }
  },

  errorCallback: function(jqXHR, textStatus, errorThrown) {
    var self = Walker;

    networkErrorCount++;

    // if load error occurs, return back to previous page and restart

    if (networkErrorCount <= 5){
      self.flashMessage("Consecutive network error: " + networkErrorCount + "/5", "warning", self.config.sleep);
      self.runNext();
    } else {
      networkErrorCount = 0;
      self.flashMessage("More than 5 consecutive network errors! Reloading test page in 60 seconds.", "danger", 60000);
      window.setTimeout(function() {
        self.runNext();
      }, 60000);
    }
  },

  handle404: function() {
    var self = Walker;

    errorCount404++;

    if (errorCount404 <= 2) {
      //self.currentUrl = self.previousUrl;
      self.flashMessage("Request url not found. Fallback to previous url.", "warning", self.config.sleep);
    } else {
      errorCount404 = 0;
      self.currentUrl = self.config.seed;
      self.flashMessage("Two 404 errors in a raw. Fallback to first url.", "warning", self.config.sleep);
    }

    self.runNext();
  },

  handle422: function() {
    var self = Walker;

    errorCount422++;

    if (errorCount422 <= 2) {
      //self.currentUrl = self.previousUrl;
      self.flashMessage("This url does not have links. Fallback to previous url.", "warning", self.config.sleep);
    } else {
      errorCount422 = 0;
      self.currentUrl = self.config.seed;
      self.flashMessage("Two 422 errors in a raw. Fallback to first url.", "warning", self.config.sleep);
    }

    self.runNext();
  },

  runNext: function() {
    var self = Walker;

    if (timer) {
      clearTimeout(timer);
    }

    timer = window.setTimeout(function() {
      self.runTest();
    }, self.config.sleep);
  },

  runTest: function () {
    var self = Walker;

    if ($stopFlag == true) {
      return;
    }

    self.getRandomWebpageUrl()
      .success(self.successCallback)
      .error(self.errorCallback);
  },

  loadIframe: function() {
    var self = Walker;

    self.counter ++;
    $counter.text("[" + self.counter + "]");
    $siteUrl.text(self.currentUrl);

    self.report.loadingStart = new Date().getTime();
    $iframe
      .attr("src", self.currentUrl)
      .load(self.handleIframeLoadingDone);
  },

  updateButton: function() {
    if ($stopFlag == false) {
      $startButton.attr("disabled", "disabled");
      $stopButton.removeAttr("disabled");
    } else {
      $stopButton.attr("disabled", "disabled");
      $startButton.removeAttr("disabled");
    }
  },

  getTimestamp: function () {
    return (!Date.now)
      ? Date.now
      : new Date().getTime();
  },

  numberFormat: function(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  },

  flashMessage: function(message, type, delay) {
    var flashObj = $("div.js-flash-message");
    if (flashObj) flashObj.remove();

    $("<div></div>", {
      "class": "alert alert-" + type + " alert-dismissible js-flash-message",
      "html": '<button type="button" class="close" data-dismiss="alert">' +
        '<span aria-hidden="true">&times;</span>' +
        '<span class="sr-only">Close</span></button>' +
        message
    }).appendTo($(".container").first());

    $("div.js-flash-message").fadeIn(300).delay(delay).fadeOut(300);
  },

  getAvgLoadingTime: function() {
    var elements = Walker.reports;

    return elements
      .reduce(function(sum, a) {
        return sum + a
      },0)
    /
    (elements.
      length != 0
        ? elements.length
        : 1
    );
  }

};;