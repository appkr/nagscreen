// Global variables
var $indicatorHolder = $(".indicator-holder"),
    $loadingIcon = $("<i></i>", {
      "class": "fa fa-spinner fa-spin text-center"
    }),
    $startButton = $("button.start-test"),
    $stopButton = $("button.stop-test"),
    $image = $("#image"),
    $title = $("#title"),
    $imageSize = $("#image-file-size"),
    //$downloadSpeed = $("#download-speed"),
    $counter = $(".counter"),
    $loadingTime = $(".loading-time"),
    $avgLogdingTime = $(".avg-loading-time"),
    $testStartTime = $(".test-start-time"),
    $stopFlag = false,
    noOfErrors = 0,
    timer;

var Report = function(startTime) {
  this.startTime = startTime;
  this.loadingStart = 0;
};

// DemoImage class
var DemoImage = {

  config: {
    page: 1,
    sleep: 4000
  },

  reports: [],

  report: new Report(new Date().getTime()),

  imageSize: 0,

  init: function (config) {
    $testStartTime.text(new Date(this.report.startTime));

    $.extend(this.config, config);
    this.registerEventHandler();
    this.updateButton(); // disable START-TEST button
    this.runTest(); // do the very first API call
  },

  registerEventHandler: function() {
    var self = this;

    $startButton.on("click", function(){
      $("div.js-flash-message").remove();
      $stopFlag = false;
      self.updateButton();
      self.runNext();
    });

    $stopButton.on("click", function(){
      $stopFlag = true;
      self.flashMessage("Test stopped by user interruption! Press START TEST button to resume.", "info", 10000);
      self.updateButton();
      $loadingIcon.remove();
    });
  },

  handleImageLoadingDone: function() {
    var self = DemoImage;

    var imageLoadingTime = new Date().getTime() - self.report.loadingStart;

    $loadingIcon.remove();
    self.reports.push(imageLoadingTime);
    $loadingTime.text((imageLoadingTime / 1000).toFixed(1));
    $imageSize.text(self.formatFileSize(self.imageSize));
    //$downloadSpeed.text(self.formatSpeed(self.imageSize, imageLoadingTime / 1000));
    $avgLogdingTime.text((self.getAvgLoadingTime() / 1000).toFixed(1));

    self.runNext();
  },

  runTest: function () {
    var self = DemoImage;

    if ($stopFlag == true) {
      return;
    }

    this.fetchDemoImage()
      .success(self.successCallback)
      .error(self.errorCallback);
  },

  fetchDemoImage: function() {
    var self = this;

    return $.ajax({
      url: "http://promote.airplug.com/test/demo-image",
      type: "GET",
      data: {
        page: self.config.page++
      },
      timeout: 5000,
      cache: false,
      beforeSend: function () {
        $indicatorHolder.append($loadingIcon);
      }
    });
  },

  successCallback: function(response, textStatus, jqXHR) {
    var self = DemoImage;

    noOfErrors = 0;

    if (response && response.data[0].image) {
      self.updateCounter(response.current_page, response.last_page);
      self.renderPage(response.data[0]);
    } else {
      self.runNext();
    }
  },

  errorCallback: function(jqXHR, textStatus, errorThrown) {
    var self = DemoImage;

    noOfErrors++;

    if (noOfErrors <= 5){
      self.flashMessage("Consecutive network error: " + noOfErrors + "/5", "warning", self.config.sleep);
      self.runNext();
    } else {
      noOfErrors = 0;
      self.flashMessage("More than 5 consecutive network errors! Reloading test page in 60 seconds.", "danger", 60000);
      $stopFlag = true;
      window.setTimeout(function() {
        window.location.reload();
      }, 60000);
    }
  },

  runNext: function() {
    var self = DemoImage;

    if (timer) {
      clearTimeout(timer);
    }

    timer = window.setTimeout(function() {
      self.runTest();
    }, self.config.sleep);
  },

  renderPage: function(image) {
    var self = DemoImage;

    $title.text(image.title);

    self.imageSize = image.image_size;
    self.report.loadingStart = new Date().getTime();
    $image
      .attr("src", image.image + "?t=" + self.getTimestamp())
      .load(self.handleImageLoadingDone);
  },

  updateCounter: function (current, total) {
    $counter.html(
      '<strong>' +
      this.numberFormat(current) +
      "</strong> <small>out of</small> " +
      this.numberFormat(total) +
      " <small>images</small>"
    );
  },

  updateButton: function() {
    if ($stopFlag == false) {
      $startButton.attr("disabled", "disabled");
      $stopButton.removeAttr("disabled");
    } else {
      $stopButton.attr("disabled", "disabled");
      $startButton.removeAttr("disabled");
    }
  },

  getTimestamp: function () {
    return (!Date.now)
      ? Date.now
      : new Date().getTime();
  },

  numberFormat: function(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  },

  flashMessage: function(message, type, delay) {
    var flashObj = $("div.js-flash-message");
    if (flashObj) flashObj.remove();

    $("<div></div>", {
      "class": "alert alert-" + type + " alert-dismissible js-flash-message",
      "html": '<button type="button" class="close" data-dismiss="alert">' +
        '<span aria-hidden="true">&times;</span>' +
        '<span class="sr-only">Close</span></button>' +
        message
    }).appendTo($(".container").first());

    $("div.js-flash-message").fadeIn(300).delay(delay).fadeOut(300);
  },

  getAvgLoadingTime: function() {
    var elements = DemoImage.reports;

    return elements
      .reduce(function(sum, a) {
        return sum + a
      },0)
    /
    (elements.
      length != 0
        ? elements.length
        : 1
    );
  },

  formatFileSize: function(size) {
    if (isNaN(size)) return 'NaN';

    var decr   = 1024;
    var step   = 0;
    var suffix = ['Bytes', 'KBytes', 'MBytes', 'GBytes'];

    while ((size / decr) > 0.9) {
      size = size / decr;
      step ++;
    }

    return Math.round(size) + suffix[step];
  },

  formatSpeed: function(byte, time) {
    if (isNaN(byte)) return 'NaN';
    if (isNaN(time)) return 'NaN';
    if (time == 0) return 'Div/0';

    var denominator = 1024;
    var step = 0;
    var suffix = ['bps', 'Kbps', 'Mbps', 'Gbps'];

    var numerator = byte * 8;

    while ((numerator / denominator) > 0.9) {
      var byteFormatted = numerator / denominator;
      step ++;
      console.log(byteFormatted);
      console.log(suffix[step]);
    }

    return Math.round(byteFormatted / time) + suffix[step];
  }

};;