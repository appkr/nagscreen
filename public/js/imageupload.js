(function ($) {
    // Create CK Editor instance
    var editor = CKEDITOR.replace('content', {height: "30em"});

    // Override Dropzone default options
    Dropzone.options.myAwesomeDropzone = {
        maxFilesize: 2, // MB
        acceptedFiles: "image/*",

        init: function () {
            var imagePath = "{{Config::get('app.url')}}/{{Config::get('setting.image.webPath')}}/";

            // Success event handler
            this.on("success", function (file, data) {
                var imgString = '<img src="' + imagePath + data.filename + '">',
                    formString = '<input type="hidden" name="images[]" value="' + data.id + '">';

                // Insert img tag at CK Editor's content area
                editor.insertHtml(imgString);

                // Append hidden form to update post_id of the already uploaded images
                $("#my-meta-form").prepend(formString);
            });

            // Delete event handler
            this.on("addedfile", function (file) {
                var removeButton = Dropzone.createElement("<button>Remove file</button>"),
                    _this = this;

                removeButton.addEventListener("click", function (e) {
                    e.preventDefault();
                    e.stopPropagation();

                    _this.removeFile(file);

                    // Ajax request to delete teh image at server
                    if (confirm("Are you sure to delete an image?")) {
                        $.ajax({
                            url: "/attachment/" + file.name,
                            type: "POST",
                            data: {
                                _token: "{{{ csrf_token() }}}",
                                _method: "DELETE"
                            },
                            dataType: "json"
                        }).done(function (response) {
                                // console.log(response);
                                if (response.code == 200) {
                                    // visual feedback here
                                } else {
                                    // visual feedback here
                                }
                            });
                    }
                });

                // Add the button to the file preview element.
                file.previewElement.appendChild(removeButton);
            });

        }
    };

    $.fn.insertAtCaret = function (appendString) {
        appendString = appendString.trim();
        CKEDITOR.instances[$(this).attr("id")].insertText(appendString);
    };
})(jQuery);