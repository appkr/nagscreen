var utility = {
    fetch: function(url, data) {
        return $.ajax({
            url: url,
            type: "POST",
            data: data,
            dataType: "json"
        })
    },

    flash: function(title, text, type) {
        new PNotify({
            title: title,
            text: text,
            type: type
        });
    }
};