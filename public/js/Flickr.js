// Global variables
var $indicatorHolder = $(".indicator-holder"),
    $loadingIcon = $("<i></i>", {
      "class": "fa fa-spinner fa-spin text-center"
    }),
    $startButton = $("button.start-test"),
    $stopButton = $("button.stop-test"),
    $image = $("#image"),
    $title = $("#title"),
    //$ownername = $("#ownername"),
    //$dateadded = $("#dateadded"),
    //$description = $("#description"),
    $counter = $(".counter"),
    $loadingTime = $(".loading-time"),
    $avgLogdingTime = $(".avg-loading-time"),
    $testStartTime = $(".test-start-time"),
    $stopFlag = false,
    noOfErrors = 0,
    timer;

var Report = function(startTime) {
  this.startTime = startTime;
  this.loadingStart = 0;
};

// Flickr class
var Flickr = {

  config: {
    page: 1,
    sleep: 4000,
    method: "flickr.groups.pools.getPhotos",
    group_id: "83823859@N00",
    extras: "url_l,description",
    api_key: "d401046f072dae687da49de8cb79cf04"
  },

  reports: [],

  report: new Report(new Date().getTime()),

  init: function (config) {
    $testStartTime.text(new Date(this.report.startTime));

    $.extend(this.config, config);
    this.registerEventHandler();
    this.updateButton(); // disable START-TEST button
    this.runTest(); // do the very first Flickr API call
  },

  registerEventHandler: function() {
    var self = this;

    $startButton.on("click", function(){
      $("div.js-flash-message").remove();
      $stopFlag = false;
      self.updateButton();
      self.runNext();
    });

    $stopButton.on("click", function(){
      $stopFlag = true;
      self.flashMessage("Test stopped by user interruption! Press START TEST button to resume.", "info", 10000);
      self.updateButton();
      $loadingIcon.remove();
    });
  },

  handleImageLoadingDone: function() {
    var self = Flickr;

    var imageLoadingTime = new Date().getTime() - self.report.loadingStart;

    $loadingIcon.remove();
    self.reports.push(imageLoadingTime);
    $loadingTime.text((imageLoadingTime / 1000).toFixed(1));
    $avgLogdingTime.text((self.getAvgLoadingTime() / 1000).toFixed(1));

    self.runNext();
  },

  runTest: function () {
    if ($stopFlag == true) {
      return;
    }

    this.fetchFlickrPhoto();
  },

  fetchFlickrPhoto: function() {
    var self = this;

    $.ajax({
      url: "https://api.flickr.com/services/rest/",
      type: "GET",
      data: {
        method: self.config.method,
        group_id: self.config.group_id,
        extras: self.config.extras,
        api_key: self.config.api_key,
        per_page: 1,
        page: self.config.page++,
        format: "json",
        jsoncallback: "Flickr.flickrCallback"
      },
      jsonp: false,
      timeout: 5000,
      cache: false,
      beforeSend: function () {
        $indicatorHolder.append($loadingIcon);
      },
      success: self.successCallback,
      error: self.errorCallback
    });
  },

  successCallback: function(data, textStatus, jqXHR) {
    noOfErrors = 0;
  },

  errorCallback: function(jqXHR, textStatus, errorThrown) {
    var self = Flickr;

    noOfErrors++;

    if (noOfErrors <= 5){
      self.flashMessage("Consecutive network error: " + noOfErrors + "/5", "warning", self.config.sleep);
      self.runNext();
    } else {
      noOfErrors = 0;
      self.flashMessage("More than 5 consecutive network errors! Reloading test page in 60 seconds.", "danger", 60000);
      $stopFlag = true;
      window.setTimeout(function() {
        window.location.reload();
      }, 60000);
    }
  },

  flickrCallback: function(response) {
    var self = Flickr;

    if (response && response.photos.photo[0].url_l) {
      self.updateCounter(response.photos.page, response.photos.pages);
      self.renderPage(response.photos.photo[0]);
    } else {
      self.runNext();
    }

  },

  runNext: function() {
    var self = Flickr;

    if (timer) {
      clearTimeout(timer);
    }

    timer = window.setTimeout(function() {
      self.runTest();
    }, self.config.sleep);
  },

  renderPage: function(photo) {
    var self = Flickr;

    $title.text(photo.title);
    //$ownername.text(photo.ownername);
    //$dateadded.text(new Date(photo.dateadded * 1000));
    //$description.text(photo.description._content.substring(0, 200));

    self.report.loadingStart = new Date().getTime();
    $image
      .attr("src", photo.url_l.replace("https", "http") + "?t=" + self.getTimestamp())
      .load(self.handleImageLoadingDone);
  },

  updateCounter: function (current, total) {
    $counter.html(
      '<strong>' +
      this.numberFormat(current) +
      "</strong> <small>out of</small> " +
      this.numberFormat(total) +
      " <small>images</small>"
    );
  },

  updateButton: function() {
    if ($stopFlag == false) {
      $startButton.attr("disabled", "disabled");
      $stopButton.removeAttr("disabled");
    } else {
      $stopButton.attr("disabled", "disabled");
      $startButton.removeAttr("disabled");
    }
  },

  getTimestamp: function () {
    return (!Date.now)
      ? Date.now
      : new Date().getTime();
  },

  numberFormat: function(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  },

  flashMessage: function(message, type, delay) {
    var flashObj = $("div.js-flash-message");
    if (flashObj) flashObj.remove();

    $("<div></div>", {
      "class": "alert alert-" + type + " alert-dismissible js-flash-message",
      "html": '<button type="button" class="close" data-dismiss="alert">' +
        '<span aria-hidden="true">&times;</span>' +
        '<span class="sr-only">Close</span></button>' +
        message
    }).appendTo($(".container").first());

    $("div.js-flash-message").fadeIn(300).delay(delay).fadeOut(300);
  },

  getAvgLoadingTime: function() {
    var elements = Flickr.reports;

    return elements
      .reduce(function(sum, a) {
        return sum + a
      },0)
    /
    (elements.
      length != 0
        ? elements.length
        : 1
    );
  },

  // This function will not work
  // because of CORS restriction
  injectImageFileSize: function(photo) {
    var self = Flickr;

    $.ajax({
      url: photo.url_l,
      type: "HEAD",
      success: function(data, textStatus, jqXHR) {
        console.log(jqXHR.getResponseHeader("content-length"));
      }
    });
  }

};;