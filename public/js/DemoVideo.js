var myVideo = document.getElementById("myVideo"),
    $videoTag = $("video").first(),
    $linkTag = $(".video-link"),
    $durationTag = $("span.duration"),
    $sizeTag = $("span.size"),
    $qualityTag = $("span.quality"),
    //$descriptionTag = $("p#description"),
    $startButton = $("button.start-test"),
    $stopButton = $("button.stop-test"),
    $spinnerHolder = $(".spinner-holder"),
    $loadingIcon = $("<i></i>", {
      "class": "fa fa-spinner fa-spin"
    }),
    $counter = $(".counter"),
    $bufferingTime = $(".buffering-time"),
    $bufferingCount = $(".buffering-count"),
    $accumulatedBufferingTime = $(".accumulated-buffering-time"),
    $totalBufferingTime = $(".total-buffering-time"),
    $totalBufferingCount = $(".total-buffering-count"),
    $testStartTime = $(".test-start-time"),
    $stopFlag = false,
    noOfErrors = 0,
    timer;

var Report = function(videoId, startTime) {
  this.videoId = videoId;
  this.startTime = startTime;
  this.bufferingTime = 0;
  this.accumulatedBufferingTime = 0;
  this.bufferingCount = 0;
};

var Stat = function(startTime) {
  this.startTime = startTime;
  this.totalBufferingTime = 0;
  this.totalBufferingCount = 0;
};

var DemoVideo = {

  config: {
    url: "",
    sleep: 1000,
    page: 1,
    videoIndex: 0
  },

  videoId: "",

  stat: new Stat(new Date().getTime()),

  report: {},

  init: function (config) {
    $testStartTime.text(new Date(this.stat.startTime));

    $.extend(this.config, config);

    this.registerEventHandler();
    this.updateButton();
    this.runTest();
  },

  registerEventHandler: function() {
    var self = this;

    $startButton.on("click", function(){
      $("div.js-flash-message").remove();
      $stopFlag = false;
      self.updateButton();
      self.runTest();
    });

    $stopButton.on("click", function(){
      myVideo.pause();
      $stopFlag = true;
      self.flashMessage(
        "Test stopped by user interruption! Press START TEST button to resume.",
        "info",
        10000
      );
      self.updateButton();
      $loadingIcon.remove();
    });

    myVideo.addEventListener("loadstart", self.handleVideoStarted);

    myVideo.addEventListener("waiting", self.handleBufferingStarted);

    myVideo.addEventListener("playing", self.handleVideoPlaying);

    myVideo.addEventListener("ended", self.handleVideoEnded);

    myVideo.addEventListener("error", self.runNext);

    //myVideo.addEventListener("abort", self.runNext);

    myVideo.addEventListener("click", function() {
      myVideo.paused
        ? myVideo.play()
        : myVideo.pause();
    });
  },

  handleVideoStarted: function() {
    var self = DemoVideo;

    $spinnerHolder.append($loadingIcon);

    self.report = new Report(self.videoId, new Date().getTime());
    self.report.bufferingStart = new Date().getTime();

    $bufferingTime.text((self.report.bufferingTime/1000).toFixed(1));
    $bufferingCount.text(self.report.bufferingCount);
    $accumulatedBufferingTime.text((self.report.accumulatedBufferingTime/1000).toFixed(1));
    $totalBufferingTime.text((self.stat.totalBufferingTime/1000).toFixed(1));
    $totalBufferingCount.text(self.stat.totalBufferingCount);
  },

  handleVideoPlaying: function() {
    var self = DemoVideo;

    $loadingIcon.remove();

    if (self.report.bufferingStart) {
      self.report.bufferingTime = parseInt(new Date().getTime() - self.report.bufferingStart);
      self.report.accumulatedBufferingTime += self.report.bufferingTime;
      self.stat.totalBufferingTime += self.report.bufferingTime;
    }

    $bufferingTime.text((self.report.bufferingTime/1000).toFixed(1));
    $accumulatedBufferingTime.text((self.report.accumulatedBufferingTime/1000).toFixed(1));
    $totalBufferingTime.text((self.stat.totalBufferingTime/1000).toFixed(1));
  },

  handleBufferingStarted: function() {
    var self = DemoVideo;

    $spinnerHolder.append($loadingIcon);

    self.report.bufferingStart = new Date().getTime();
    self.report.bufferingCount += 1;
    self.stat.totalBufferingCount += 1;

    $bufferingCount.text(self.report.bufferingCount);
    $totalBufferingCount.text(self.stat.totalBufferingCount);
  },

  handleVideoEnded: function() {
    var self = DemoVideo;
    self.report = {};
    self.runNext();
  },

  runTest: function () {
    if ($stopFlag == true) {
      return;
    }

    this.fetchDemoVideo();
  },

  fetchDemoVideo: function() {
    var self = this;

    return $.ajax({
      url: self.config.url,
      type: "GET",
      data: {
        page: self.config.page++,
        callback: "DemoVideo.ajaxCallback"
      },
      jsonp: false,
      timeout: 5000,
      cache: false,
      beforeSend: function () {
        $spinnerHolder.append($loadingIcon);
      },
      success: self.successCallback,
      error: self.errorCallback
    });
  },

  successCallback: function(data, textStatus, jqXHR) {
    noOfErrors = 0;
  },

  errorCallback: function(jqXHR, textStatus, errorThrown) {
    noOfErrors++;
  },

  ajaxCallback: function(response) {
    var self = DemoVideo;

    if (response) {
      if (response.current_page > response.last_page) {
        $stopFlag = true;
        alert("No more video. Refresh page to test again. ");
        return;
      }

      self.updateCounter(response.current_page, response.last_page);
      // Start playing video
      self.playVideo(response.data[0]);
    } else {
      // Moving to next video
      self.runNext();
    }
  },

  runNext: function() {
    var self = DemoVideo;

    if (timer) {
      clearTimeout(timer);
    }

    timer = window.setTimeout(function() {
      self.runTest();
    }, self.config.sleep);
  },

  playVideo: function(video) {
    var self = DemoVideo;

    self.videoId = video.airplug_id;

    myVideo.play();

    $videoTag.attr("poster", self.getYtThumUrl(video.youtube_id))
      .attr("src", self.getApVideoUrl(video.airplug_id));
    $linkTag.attr("href", self.getApVideoUrl(video.airplug_id))
      .text(video.title);
    $durationTag.text(video.duration + "sec");
    $sizeTag.text(self.formatFileSize(video.size));
    $qualityTag.text(self.getBitRate(video.size, video.duration) + "bps");
    //$descriptionTag.text(video.description.substring(0, 200));
  },

  updateCounter: function (current, total) {
    $counter.html(
      '<strong class="text-danger">' +
      this.numberFormat(current) +
      "</strong> <small>out of " +
      this.numberFormat(total) +
      " videos</small>");
  },

  updateButton: function() {
    if ($stopFlag == false) {
      $startButton.attr("disabled", "disabled");
      $stopButton.removeAttr("disabled");
    } else {
      $stopButton.attr("disabled", "disabled");
      $startButton.removeAttr("disabled");
    }
  },

  getTimestamp: function () {
    return (!Date.now)
      ? Date.now
      : new Date().getTime();
  },

  getYtThumUrl: function(ytId) {
    return "http://i.ytimg.com/vi/" + ytId + "/0.jpg?t=" + this.getTimestamp();
  },

  getApVideoUrl: function(apId) {
    return "http://tfrontg1.airplug.co.kr/media/" + apId + ".mp4?t=" + this.getTimestamp();
  },

  getBitRate: function(size, duration) {
    return this.numberFormat(Math.round((size * 8) / duration));
  },

  numberFormat: function(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  },

  formatFileSize: function(size) {
    if (isNaN(size)) return 'NaN';

    decr   = 1024;
    step   = 0;
    suffix = ['Bytes', 'KBytes', 'MBytes', 'GBytes'];

    while ((size / decr) > 0.9) {
      size = size / decr;
      step ++;
    }

    return Math.round(size) + suffix[step];
  },

  flashMessage: function(message, type, delay) {
    var flashObj = $("div.js-flash-message");
    if (flashObj) flashObj.remove();

    $("<div></div>", {
      "class": "alert alert-" + type + " alert-dismissible js-flash-message",
      "html": '<button type="button" class="close" data-dismiss="alert">' +
      '<span aria-hidden="true">&times;</span>' +
      '<span class="sr-only">Close</span></button>' +
      message
    }).appendTo($(".container").first());

    $("div.js-flash-message").fadeIn(300).delay(delay).fadeOut(300);
  }

};