/**
 * @license Copyright (c) 2003-2014, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.html or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function (config) {
    // Define changes to default configuration here.
    // For the complete reference:
    // http://docs.ckeditor.com/#!/api/CKEDITOR.config

    config.skin = 'bootstrapck';
    config.language = 'en';
    config.extraPlugins = 'justify';
    config.extraPlugins = 'pbckcode';
    // config.basicEntities = false; //Whether to escape basic HTML entities in the document

    // The toolbar groups arrangement, optimized for two toolbar rows.
    config.toolbarGroups = [
        { name: 'document', groups: [ 'mode', 'document', 'doctools' ] },
        { name: 'clipboard', groups: [ 'clipboard', 'undo' ] },
        { name: 'links' },
        { name: 'insert' },
        { name: 'tools' },
        { name: 'others' },
        '/',
        { name: 'styles' },
        { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
        { name: 'paragraph', groups: [ 'align', 'list', 'indent', 'blocks' ] },
        { name: 'colors' },
        { name: 'pbckcode' }
    ];

    // Remove some buttons, provided by the standard plugins, which we don't
    // need to have in the Standard(s) toolbar.
    // config.removeButtons = 'Underline,Subscript,Superscript';
    config.removeButtons = 'Subscript,Superscript';

    // Set the most common block elements.
    config.format_tags = 'p;h1;h2;h3;pre;div';

    // Make dialogs simpler.
    config.removeDialogTabs = 'image:advanced;link:advanced';

    // "Basic" styling. Useful for comment form
    config.toolbar_Basic = [
        [ 'Bold', 'Italic', 'Strike' ]
    ];
};

// Allow arbitrary html tags
CKEDITOR.config.allowedContent = true;

CKEDITOR.config.extraAllowedContent = 'small';

// Allow empty span tag, such as bootstrap glyphicon
CKEDITOR.dtd.$removeEmpty.span = false;

// PBCKCODE CUSTOMIZATION
CKEDITOR.config.pbckcode = {
    // An optional class to your pre tag.
    cls: '',

    // The syntax highlighter you will use in the output view
    highlighter: 'prettyprint',

    // An array of the available modes for you plugin.
    // The key corresponds to the string shown in the select tag.
    // The value correspond to the loaded file for ACE Editor.
    modes: [
        ['CSS', 'css'],
        ['HTML', 'html'],
        ['Java', 'java'],
        ['JS', 'javascript'],
        ['JSON', 'json'],
        ['Markdown', 'markdown'],
        ['PHP', 'php'],
        ['Python', 'python'],
        ['SQL', 'sql'],
        ['XML', 'xml'],
        ['YAML', 'yaml']
    ],

    // The theme of the ACE Editor of the plugin.
    theme: 'textmate',

    // Tab indentation (in spaces)
    tab_size: '4'
};
