var app = {

    /** Default config */
    config: {
        $logLet: $(".log-let"),
        $urlLet: $(".url-let"),
        $url: '//promote.airplug.com/test/response/get?max=3&json=1',
        $counter: 0,
        $sleep: 1000,
        $stopFlag: false,
        $requestTimestamp: 0,
        $finishedTimestamp: 0
    },


    /** Constructor */
    init: function (config) {
        $.extend(this.config, config);
        this.registerEventHandler();
    },


    registerEventHandler: function() {
        var self = this;

        $("button.start-test").on("click", function(){
            self.config.$stopFlag = false;
            self.runTest();
        });

        $("button.stop-test").on("click", function(){
            self.config.$stopFlag = true;
        });

        self.updateUrl();
    },


    runTest: function() {
        var self = this,
            localCounter = self.config.$counter++;

        if (self.config.$stopFlag == true) return;

        self.config.$requestTimestamp = self.getTimestamp();

        self.fetch(self.config.$url, {
            "timestamp": self.config.$requestTimestamp
        }).done(function (response) {
            self.log(localCounter, response)

            setTimeout(function () {
                self.runTest();
            }, self.config.$sleep);
        });
    },


    /** Logging Facility */
    log: function (counter, response) {
        var self = this,
            logMessage = '';

        self.config.$finishedTimestamp = self.getTimestamp();

        if (response.error) {
            logMessage = response.error.message;
            logCode = response.error.code;
        } else {
            logMessage = response.data.message;
            logCode = response.data.code;
        }

        console.log(counter + " / " + logMessage);
        console.log(response.data.received + "-" + self.config.$requestTimestamp);

        $("<p></p>", {
            "class": "text-muted",
            "text": "Counter : " 
                + counter 
                + " / Round trip time : " 
                + (self.config.$finishedTimestamp - self.config.$requestTimestamp)
                + " ms ("
                + (response.data.respond - response.data.received)
                + " ms of server execution time with "
                + response.data.sleep
                + " ms deliberate sleep) / Server respond : " 
                + logCode 
                + " " 
                + logMessage
        }).prependTo(this.config.$logLet);
    },


    /** Make current timestamp with millisecond */
    getTimestamp: function() {
        return (! Date.now) ? Date.now : new Date().getTime();
    },


    /** aJax request */
    fetch: function(url, data, method) {
        return $.ajax({
            method: method || "GET",
            url: url,
            data: data,
            dataType: "json"
        })
    },

    /** update Url */
    updateUrl: function() {
        this.config.$urlLet.html(this.config.$url);
    }

};