<?php

$feed = file_get_contents("http://feeds.feedburner.com/railscasts");
$data = simplexml_load_string($feed);

for($i = 0; $i < 346; $i++) {
  $path = (string) $data->channel->item[$i]->enclosure->attributes()['url'];
  $parts = pathinfo($path);

  set_time_limit(0);
  $fp = fopen (dirname(__FILE__) . "/{$parts['basename']}", 'w+');
  $ch = curl_init(str_replace(" " , "%20", $path));
  curl_setopt($ch, CURLOPT_TIMEOUT, 0);
  curl_setopt($ch, CURLOPT_FILE, $fp);
  curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
  curl_exec($ch);
  curl_close($ch);
  fclose($fp);

  echo "Downloading " . $parts['basename'] . " done" . PHP_EOL;
}