<?php

class PostController extends \BaseController
{

    /**
     * @var Illuminate\Database\Eloquent\Model
     */
    private $model;


    /**
     * @param Post $model
     */
    public function __construct(Post $model)
    {
        parent::__construct();

        $this->beforeFilter('csrf', ['on' => ['post', 'put', 'patch', 'delete']]);
        $this->beforeFilter('auth', ['except' => ['getView']]);

        $this->model = $model;
    }


    /**
     * Display a listing of the resource.
     *
     * @param null $email
     * @return View
     */
    public function index($email = null)
    {
        $user  = null;
        $input = prepare_input_for_post(Input::all());

        if (! empty($email)) $user = User::where('email', '=', $email)->first();

        Input::flash();

        $query = (is_memcached())
            ? $this->model->with(['user', 'comments', 'package', 'category'])->remember(60)->cacheTags('posts')
            : $this->model->with(['user', 'comments', 'package', 'category']);

        if (! empty($user)) $query->whereUserId($user->id);

        if (! empty($input['filter']) && ! empty($input['question'])) $query->filter($input['filter'], $input['question']);

        $posts = $query->orderBy($input['sort'], $input['order'])->paginate($input['limit']);

        return View::make('post.index')->with('posts', $posts)->with('user', $user);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return  View
     */
    public function create()
    {
        return View::make('post.create');
    }


    /**
     * Store a newly created resource in storage.
     *
     * @return  Response
     */
    public function store()
    {
        $input = Input::all();
        $input = append_user_id($input);
        $input = transform_checkbox_value($input);

        $validation = Validator::make($input, $this->model->rules);

        if ($validation->fails()) return Redirect::route('post.create')->withErrors($validation)->withInput();

        $post = $this->model->create($input);

        if (isset($input['images']) && ! empty($input['images'])) {
            Attachment::setAttachmentPostIdKey($input['images'], $post->id);
        }

        Attachment::deleteOrphanedImages();

        if (! $post)
            return Redirect::route('post.create')->with('error', 'Something went wrong! Please try again.')->withInput();

        Event::fire('history.track', [$post]);
        Event::fire('cache.flush', ['posts']);

        return Redirect::route('post.index')->with('success', 'Your post was saved!');
    }


    /**
     * Display the specified resource.
     *
     * @param null $id
     * @return  View
     */
    public function show($id = null)
    {
        return View::make('post.show')
                   ->with('post', $this->model->with('user', 'comments', 'package', 'category')->findOrFail($id));
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param $id
     * @return  Veiw
     */
    public function edit($id)
    {
        $post = $this->model->findOrFail($id);

        if ((! $post->isOwner()) && ! (is_admin())) return Redirect::route('post.index')->with('error', 'The post does not belong to you !');

        return View::make('post.edit')->with('post', $post);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param null $id
     * @return  Response(=Redirect)
     */
    public function update($id = null)
    {
        $input = Input::all();
        $input = append_user_id($input);
        $input = transform_checkbox_value($input);

        $validation = Validator::make($input, $this->model->rules);

        if ($validation->fails()) return Redirect::route('post.show', [$input['id']])->withErrors($validation)->withInput();

        $post = $old = $this->model->findOrFail($id);

        $affectedRows = $post->update($input);

        if (isset($input['images']) && ! empty($input['images'])) Attachment::setAttachmentPostIdKey($input['images'], $post->id);

        Attachment::deleteOrphanedImages();

        if (! $affectedRows) 
        {
            return Redirect::route('post.show', [$input['id']])
                           ->with('error', 'Something went wrong! Please try again.')->withInput();
        }

        Event::fire('history.track', [$old]);
        Event::fire('cache.flush', ['posts']);

        return Redirect::route('post.show', $id)->with('success', 'Your post has been updated !');
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param null $id
     * @return  Response(=Redirect)
     */
    public function destroy($id = null)
    {
        $post = $old = $this->model->find($id);

        if (! $post) return $this->setStatusCode(404)->respondWithError('Not Found');

        $post->comments()->delete();
        $post->attachments()->delete();
        $affectedRow = $post->delete();

        Event::fire('history.track', [$old]);
        Event::fire('cache.flush', [['posts', 'comments']]);

        return $this->respond(['data' => (boolean) true]);
    }


    /**
     * Controller for post/toggle post request
     *
     * @param null $id
     * @return object Response
     */
    public function toggle($id = null)
    {
        $input = Input::only('published');

        $post = $old = $this->model->findOrFail($id);

        if (! $post) return $this->setStatusCode(404)->respondWithError('Not Found');

        $post->published = isset($input['published']) && $input['published'] == 1 ? 1 : null;

        if ($post->save()) {
//            if ($input['published'] == 1) Event::fire('history.track', [$old]);
            Event::fire('cache.flush', ['posts']);
        }

        return $this->respond(['data' => ['message' => ($input['published'] == 1) ? 'PUBLISHED !' : 'UNPUBLISHED !']]);
    }


    /**
     * Controller for single/{id} get request
     *
     * @param null $id
     * @return object View
     */
    public function getView($id = null)
    {
        return ($post = $this->model->findOrFail($id)) ? View::make('post.view')->with('post', $post) : App::abort(404);
    }

}