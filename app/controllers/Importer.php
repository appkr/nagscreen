<?php

/**
 * Class Importer
 *
 * Import BBVideo model by reading filesystem and youtube data api
 */
class Importer {

    /**
     * @var BBVideo
     */
    protected $model;

    /**
     * @param BBVideo $model
     */
    public function __construct(BBVideo $model) {
        $this->model = $model;
    }

    /**
     * Import BBVideo model by reading filesystem and youtube data api
     */
    public function run() {
        $cnt = 1;
        // $model = [];
        $files = File::files(base_path('demo-videos'));

        foreach ($files as $file) {
            $filename = pathinfo($file, PATHINFO_FILENAME);

            if ($filename == 'import') {
                continue;
            }

            echo "[{$cnt}] {$filename}... ";

            $feeds = json_decode(file_get_contents("http://gdata.youtube.com/feeds/api/videos/{$filename}?alt=json&v=2"), true);

            $model = [
                'title'       => $feeds['entry']['title']['$t'],
                'author'      => $feeds['entry']['author'][0]['name']['$t'],
                'description' => str_limit(strip_tags($feeds['entry']['media$group']['media$description']['$t']), 200),
                'airplug_id'  => $filename,
                'youtube_id'  => $filename,
                'duration'    => (int) $feeds['entry']['media$group']['yt$duration']['seconds'],
                'size'        => (int) File::size($file),
                'locale'      => 'en',
                'activated'   => 1
            ];

            echo "Done" . PHP_EOL;
            $cnt ++;

            $this->model->create($model);

            sleep(1);
        }

        //return File::put(base_path() . '/bb_videos/import.php', '<?php return ' . var_export($model, true) . ';');

        return true;
    }

    /**
     * Import BBVideo model from a file formatted as php array
     */
    public function importFromFile() {
        $cnt = 1;
        $data = include(base_path() . '/bb_videos/import.php');

        foreach($data as $model) {
            echo "[{$cnt}] {$model['airplug_id']}... ";

            $this->model->create($model);

            echo "Done" . PHP_EOL;
            $cnt ++;
        }

        return true;
    }

    public function extractVideoPageUrlsByUsername($username = "movieclipsTRAILERS") {
        $urls = [];
        $query = [
            "orderby" => "published",
            "alt" => "json",
            "v" => 2,
            "start-index" => 1,
            "max-results" => 50
        ];

        foreach(range(1,5) as $index) {
            $query['start-index'] = (int) $query['start-index'] + 50;
            $feeds = json_decode(file_get_contents("http://gdata.youtube.com/feeds/mobile/users/{$username}/uploads?" . http_build_query($query)), true);

            foreach($feeds['feed']['entry'] as $entry) {
                $urls[] = $entry['media$group']['media$player']['url'];
            }
        }

        File::put(storage_path("youtube-video-urls-by-{$username}.txt"), implode("\n", $urls));
    }

}