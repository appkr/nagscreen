<?php

use Carbon\Carbon;
use airplug\Transformers\PostTransformer;

class PostApiController extends \ApiController
{

    /**
     * @var airplug\Transformers\PostTransformer
     */
    private $postTransformer;


    /**
     * @var Illuminate\Database\Eloquent\Model
     */
    private $model;


    /**
     * Constructor
     *
     * @param Post            $model
     * @param PostTransformer $postTransformer
     * @internal param \Post $post
     */
    public function __construct(Post $model, PostTransformer $postTransformer)
    {
        parent::__construct();

        $this->beforeFilter('force.ssl', ['on' => ['post', 'put', 'patch', 'delete']]);
        $this->beforeFilter('apicsrf', ['on' => ['post', 'put', 'patch', 'delete']]);

        $this->model = $model;

        $this->postTransformer = $postTransformer;
    }


    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $input = prepare_input_for_post_api_call(Input::all());

        $route = Route::currentRouteName();

        $query = null;

        if ($route == 'api.post.index' || $route == 'api.post.popup') {
            $query = (is_memcached())
                ? $this->model->with('user', 'package', 'category')->remember(60)->cacheTags('posts')->published()
                : $this->model->with('user', 'package', 'category')->published();
        }
        else if ($route == 'api.post.latest') {
            $query = (is_memcached())
                ? $this->model->remember(60)->cacheTags('posts')->published()->where('created_at', '>', Carbon::now()->subDays($input['interval']))
                : $this->model->published()->where('created_at', '>', Carbon::now()->subDays($input['interval']));
        }
        else {
            $query = (is_memcached())
                ? $this->model->remember(60)->cacheTags('posts')->published()
                : $this->model->published();
        }

        // Conditions
        if ($route == 'api.post.popup') $query->popup();

        $query->apiFilter($input);

        // TODO research more...
        // $query->orWhere('package','=','global');

        if ($route == 'api.post.index') {
            if (! $posts = $query->orderBy('id', $input['order'])->paginate($input['limit']))
                return $this->respondNotFound();

            if ($posts->count() < 1) return $this->respondNotFound();

            $response = $this->respondWithPagination($posts,
                ['data' => $this->postTransformer->transformCollection($posts->toArray()['data'])]);
        }
        else if ($route == 'api.post.popup') {
            if (! $post = $query->where('created_at', '>', Carbon::now()->subDays($input['interval']))->orderBy('id', 'desc')->first())
                return $this->respondNotFound();

            $response = $this->respond(['data' => $this->postTransformer->transform($post->toArray())]);
        }
        else if ($route == 'api.post.etag') {
            if (! $timestring = $query->max('created_at')) return $this->respondNotFound();

            $response = $this->respond(['data' => [
                'timestring' => $timestring,
                'timestamp'  => (int) strtotime($timestring)
            ]]);
        }
        else if ($route == 'api.post.latest') {
            if (! $post = $query->orderBy('id', 'desc')->lists('id')) return $this->respondNotFound();

            $response = $this->respond(['data' => $post]);
        }

        return (isset($input['callback'])) ? $response->setCallback($input['callback']) : $response;
    }


    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        if (! $post = $this->model->with('user', 'package', 'category')->published()->find($id))
            return $this->respondNotFound();

        $response = $this->respond(['data' => $this->postTransformer->transform($post->toArray())]);

        return (isset($input['callback'])) ? $response->setCallback($input['callback']) : $response;
    }


    /**
     * Controller for api/popup get request
     */
    public function popup()
    {
        return $this->index();
    }


    /**
     * Controller for api/etag get request
     */
    public function etag()
    {
        return $this->index();
    }


    /**
     * Controller for api/latest get request
     */
    public function latest()
    {
        return $this->index();
    }

}