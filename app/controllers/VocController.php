<?php

class VocController extends \BaseController
{

    /**
     * @var Illuminate\Database\Eloquent\Model
     */
    private $model;

    /**
     * @param Voc $model
     */
    public function __construct(Voc $model)
    {
        parent::__construct();

        $this->beforeFilter('csrf', ['on' => ['post', 'put', 'patch', 'delete']]);
        $this->beforeFilter('auth');

        $this->model = $model;
    }


    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $input = Input::all();

        // Sort implementation
        $fieldsList  = ['id', 'title', 'created_at', 'updated_at'];
        $orderMethod = ['asc', 'desc', 'ASC', 'DESC'];

        $order = (! empty($input['order']) && in_array($input['order'], $orderMethod)) ? $input['order'] : 'desc';
        $sort  = (! empty($input['sort']) && in_array($input['sort'], $fieldsList)) ? $input['sort'] : 'id';

        // Search and Filter implementation
        $filter   = (! empty($input['filter'])) ? $input['filter'] : '';
        $question = (! empty($input['question'])) ? $input['question'] : '';

        Input::flash();

        $query = (is_memcached())
            ? $this->model->with('package', 'feedbacks')->remember(60)->cacheTags('vocs')
            : $this->model->with('package', 'feedbacks');

        if (! empty($filter) && ! empty($question)) $query->filter($filter, $question);

        $vocs = $query->orderBy($sort, $order)->paginate(20);

        return View::make('voc.index')->with('vocs', $vocs);
    }


    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id = null)
    {
        $voc = $this->model->findOrFail($id);

        return View::make('voc.show')->with('voc', $voc);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id = null)
    {
            $voc = $this->model->findOrFail($id);

            $res = $voc->feedbacks()->delete();

            if (! $voc->delete()) return $this->setStatusCode(500)->respondWithError('Internal Server Error');

            Event::fire('history.track', [$voc]);
            Event::fire('cache.flush', ['vocs']);

            return $this->respond(['data' => true]);
    }

}