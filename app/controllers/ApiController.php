<?php

use Illuminate\Pagination\Paginator;

class ApiController extends Controller
{

    /**
     * @var integer
     */
    private $statusCode = 200;


    public function __construct()
    {
        $this->beforeFilter('auth.basic', ['on' => ['post', 'put', 'patch', 'delete']]);
    }


    /**
     * CSRF token
     * 
     * @return \Illuminate\Http\Response
     */
    public function token()
    {
        return $this->respond(['data' => ['_token' => csrf_token()]]);
    }


    /**
     * Listing of category
     *
     * @return \Illuminate\Http\Response
     */
    public function category()
    {
        $categories = (is_memcached())
            ? Category::remember(60)->cacheTags('categories')->lists('name')
            : Category::lists('name');

        return $this->respond(['data' => $categories]);
    }


    /**
     * @return \Illuminate\Http\Response
     */
    public function package()
    {
        $packages = (is_memcached())
            ? Package::remember(60)->cacheTags('packages')->lists('package')
            : Package::lists('package');

        return $this->respond(['data' => $packages]);
    }


    /**
     * @return \Illuminate\Http\Response
     */
    public function language()
    {
        return $this->respond(['data' => array_keys(Config::get('setting.languages'))]);
    }


    /**
     * Generic response
     *
     * @param  array $data
     * @param  array $headers
     * @return Illuminate\Http\Response
     */
    public function respond(array $data, $headers = [])
    {
        return Response::json($data, $this->getStatusCode(), $headers);
    }


    /**
     * Respond collection of data with pagination
     *
     * @param  Paginator $items
     * @param  array     $data
     * @return $this
     */
    public function respondWithPagination(Paginator $items, $data)
    {
        $data = array_merge([
            'total_count'  => $items->getTotal(),
            'total_page'   => ceil($items->getTotal() / $items->getPerPage()),
            'current_page' => $items->getCurrentPage(),
            'last_page'    => $items->getLastPage(),
            'limit'        => $items->getPerPage()
        ], $data);

        return $this->respond($data);
    }


    /**
     * Respond 201
     *
     * @param  array $data
     * @internal param array $headers
     * @return $this
     */
    public function respondCreated($data)
    {
        return $this->setStatusCode(201)->respond($data);
    }


    /**
     * Respond 204
     *
     * @return \Illuminate\Http\Response
     */
    public function respondNoContent()
    {
        return Response::make(null, 204);
    }


    /**
     * Generic error response
     *
     * @param  string $message
     * @return $this
     */
    public function respondWithError($message)
    {
        return $this->respond([
            'error' => [
                'code'    => $this->getStatusCode(),
                'message' => $message
            ]
        ]);
    }


    /**
     * Respond 403
     *
     * @param  string $message
     * @return Illuminate\Http\Response
     */
    public function respondForbidden($message = 'Forbidden')
    {
        return $this->setStatusCode(403)->respondWithError($message);
    }


    /**
     * Respond 404
     *
     * @param  string $message
     * @return Illuminate\Http\Response
     */
    public function respondNotFound($message = 'Not Found')
    {
        return $this->setStatusCode(404)->respondWithError($message);
    }


    /**
     * Respond 500
     *
     * @param  string $message
     * @return Illuminate\Http\Response
     */
    public function respondInternalError($message = 'Internal Error')
    {
        return $this->setStatusCode(500)->respondWithError($message);
    }


    /**
     * Respond 422
     *
     * @param  string $message
     * @return Illuminate\Http\Response
     */
    public function respondUnprocessableError($message = 'Unprocessable Entity')
    {
        return $this->setStatusCode(422)->respondWithError($message);
    }


    /**
     * @return mixed
     */
    public function getStatusCode()
    {
        return $this->statusCode;
    }


    /**
     * @param mixed $statusCode
     * @return $this
     */
    public function setStatusCode($statusCode)
    {
        $this->statusCode = $statusCode;

        return $this;
    }

}