<?php

use Guzzle\Service\Client as Client;
use Carbon\Carbon;

class FetchGooglePlayStatisticsController extends \Controller {

    /**
     * AppAnnie account id. Get it from http://appannie.zendesk.com/entries/23224068-1-Account-Connections-List
     */
    const ACCOUNT_ID = '155439';

    /**
     * Market platform
     */
    const PLATFORM = 'google-play';

    /**
     * Vertical market apps|books
     */
    const VERTICAL = 'apps';

    /**
     * Category of the apps. Get it from http://support.appannie.com/entries/23744774-2-Category-List-
     */
    const CATEGORY = 'OVERALL > APPLICATION > MEDIA_AND_VIDEO';

    /**
     * Name of the chart. free|top100...
     */
    const FEED = 'free';

    protected $client;

    protected $stats;

    protected $reviews;

    protected $today;

    protected $yesterday;

    /**
     * Constructor
     *
     * @param GooglePlayStat              $stats
     * @param GooglePlayReview            $reviews
     */
    public function __construct(
        GooglePlayStat $stats,
        GooglePlayReview $reviews
    ) {
        $this->stats = $stats;
        $this->reviews = $reviews;

        $base_date = Carbon::createFromFormat('Y-m-d', $this->stats->max('date'));

        $this->yesterday = $base_date->toDateString();
        $this->today = $base_date->addDay()->toDateString();

        $this->client = new Client('https://api.appannie.com');
        $this->client->setDefaultHeaders([
            'Authorization' => 'Bearer edf836e177ad235504339ef310d8d4491969ad39',
            'Accept'        => 'application/json'
        ]);
    }

    /**
     * Fetch Google Play daily_user_installs and rank and save to the database.
     *
     * @param $package Package name (e.g. com.airplug.mao.agent)
     * @return string
     */
    public function fetchStats($package) {
        if ($this->stats->wherePackageId($this->getPackageId($package))->count() < 1) {
            $this->today = '2010-06-21';
        }

        $request = $this->client->createRequest(
            'GET',
            '/v1/accounts/' . self::ACCOUNT_ID . '/apps/' . $package . '/sales?break_down=date&start_date=' . $this->today
        );
        $response = $this->client->send($request);
        $res = $response->json();

        $sales = [];

        if (! empty($res['sales_list'])) {
            foreach ($res['sales_list'] as $data) {
                if ($data['date'] < $this->today) {
                    continue;
                }

                $sales = $this->stats->firstOrCreate([
                    'package_id'          => $this->getPackageId($package),
                    'date'                => $data['date'],
                    'daily_user_installs' => ($dailyInstallCount = $data['units']['app']['downloads']) ?: 0
                ]);
            }
        }

        self::fetchRanks($package);

        Log::info(__METHOD__ . "('{$package}') done", $sales ? $sales->toArray() : []);
        Event::fire('cache.flush', ['googleplaystats']);
        Cache::forget('googleplaysummary');

        return 'done';
    }

    /**
     * Fetch Google Play reviews and save to database
     *
     * @param $package
     * @return string
     */
    public function fetchReviews($package) {
        if ($this->reviews->wherePackageId($this->getPackageId($package))->count() < 1) $this->today = '2010-06-21';

        $request = $this->client->createRequest(
            'GET',
            '/v1/accounts/' . self::ACCOUNT_ID . '/' . self::VERTICAL . '/' . $package . '/reviews?start_date=' . $this->today
        );
        $response = $this->client->send($request);
        $res = $response->json();

        $review = [];

        if (! empty($res['review_list'])) {
            foreach ($res['review_list'] as $data) {
                if ($data['date'] < $this->today) continue;

                $review = $this->reviews->firstOrCreate([
                    'package_id' => $this->getPackageId($package),
                    'rating'     => $data['rating'],
                    'language'   => $data['language'],
                    'title'      => $data['title'],
                    'content'    => $data['text'],
                    'device'     => $data['device'],
                    'version'    => $data['version'],
                    'date'       => $data['date'],
                    'reviewer'   => $data['reviewer']
                ]);
            }
        }

        Log::info(__METHOD__ . "('{$package}') done", $review ? $review->toArray() : []);
        Event::fire('cache.flush', ['googleplayreviews']);

        return 'done';
    }

    /**
     * Fetch Google Play rank
     *
     * @param $package
     */
    private function fetchRanks($package) {
        $request = $this->client->createRequest(
            'GET',
            '/v1/' . self::VERTICAL . '/' . self::PLATFORM . '/app/' . $package . '/ranks?category=' . self::CATEGORY . '&feed=' . self::FEED
        );
        $response = $this->client->send($request);
        $res = $response->json();

        if (! empty($res['app_ranks'])) {
            foreach ($res['app_ranks'] as $data) {
                if ($data['country'] != 'KR') {
                    continue;
                }

                foreach ($data['ranks'] as $date => $rank) {
                    if (! $date) {
                        continue;
                    }

                    if ($date < $this->today) {
                        continue;
                    }

                    $stat = $this->stats
                        ->wherePackageId($this->getPackageId($package))
                        ->where('date', $date);

                    if ($stat) {
                        $rank = $stat->update(['rank_kr' => $rank]);
                    }
                }
            }
        }
    }

    /**
     * Calculate package_id with the passed package_name
     *
     * @param $package
     * @return int
     */
    private function getPackageId($package) {
        $packages = (is_memcached())
            ? Package::remember(60)->cacheTags('packages')->lists('id', 'package')
            : Package::lists('id', 'package');

        return (int) $packages[$package];
    }

    /**
     * Import data from Google Play Dev Console's csv downloads
     *
     * @param $package
     * @param $path
     * @return string
     */
    public function importStatsFromCsv($package, $path) {
        $fh = fopen($path, 'r');
        $data = fgetcsv($fh);

        $packages = get_active_google_play_packages(true);

        foreach (array_chunk($data, 5) as $line) {
            GooglePlayStat::create([
                'package_id'              => (int) $packages[$package],
                'date'                    => $line[0],
                'daily_device_installs'   => (int) $line[1],
                'daily_device_uninstalls' => (int) $line[2],
                'daily_user_installs'     => (int) $line[3],
                'daily_user_uninstalls'   => (int) $line[4]
            ]);
        }

        return 'done';
    }

}