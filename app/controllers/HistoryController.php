<?php

class HistoryController extends \BaseController
{

    /**
     * @var Illuminate\Database\Eloquent\Model
     */
    private $model;

    public function __construct(History $model)
    {
        parent::__construct();

        $this->beforeFilter('auth');

        $this->model = $model;
    }


    /**
     * @return mixed
     */
    public function index()
    {
        $input = prepare_input_for_history(Input::all());

        Input::flash();

        $query = $query = (is_memcached())
            ? $this->model->with('user')->remember(60)->cacheTags('histories')
            : $this->model->with('user');

        if (! empty($input['keyword'])) $query->search($input['keyword']);

        $histories = $query->orderBy($input['sort'], $input['order'])->paginate($input['limit']);

        return View::make('history.index')->with('histories', $histories);
    }

}