<?php

use airplug\Transformers\PushUserTransformer;

class PushUserApiController extends \ApiController
{

    /**
     * @var airplug\Transformers\PushUserTransformer
     */
    private $pushUserTransformer;


    /**
     * @var Illuminate\Database\Eloquent\Model
     */
    private $model;


    /**
     * Constructor
     *
     * @param PushUser                                 $model
     * @param airplug\Transformers\PushUserTransformer $pushUserTransformer
     */
    public function __construct(PushUser $model, PushUserTransformer $pushUserTransformer)
    {
        parent::__construct();

        $this->beforeFilter('force.ssl', ['on' => ['post', 'put', 'patch', 'delete']]);
        $this->beforeFilter('apicsrf', ['on' => ['post', 'put', 'patch', 'delete']]);

        $this->pushUserTransformer = $pushUserTransformer;

        $this->model = $model;
    }


    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @param int  $statusCode
     * @return Response
     */
    public function show($id = null, $statusCode = 200)
    {
        if (! $model = $this->model->with('package')->find($id)) return $this->respondNotFound();

        return $this->setStatusCode($statusCode)->respond(['data' => $this->pushUserTransformer->transform($model->toArray())]);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $input = Input::all();

        if (isset($input['package']))
        {
            if (! $package = Package::wherePackage($input['package'])->first()) return $this->respondNotFound();
            $input = exchange_array_item($input, ['package_id' => $package->id], 'package');
        }

        if (isset($input['uuid'])) $input = lpad_uuid($input);

        $validation = Validator::make($input, $this->model->rules);

        $returnedUser = null;

        if (isset($input['package_id']) && isset($input['uuid'])) {
            $returnedUser = PushUser::onlyTrashed()->wherePackageId($input['package_id'])->whereUuid($input['uuid'])->first();
        }

        if ($returnedUser)
        {
            $returnedUser->restore();

            $returnedUser->registration_id = $input['registration_id'];
            $returnedUser->save();

            Event::fire('cache.flush', ['pushusers']);

            return $this->show($returnedUser->id, 201);
        }
        else
        {
            if ($validation->fails()) return $this->respondUnprocessableError($validation->errors()->toArray());

            if (! $newUser = $this->model->create($input)) return $this->respondInternalError();

            Event::fire('cache.flush', ['pushusers']);

            return $this->show($newUser->id, 201);
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param null $registration_id
     * @return Response
     */
    public function destroy($registration_id = null)
    {
        if (! $targetUser = $this->model->whereRegistrationId($registration_id)->first())
            return $this->respondNotFound();

        if (! $targetUser->delete()) return $this->respondInternalError();

        Event::fire('cache.flush', ['pushusers']);

        return $this->respond(['data' => (boolean) true]);
    }


    /**
     * @return Response
     */
    public function legacy()
    {
        return (Input::get('query') == 'register') ? $this->store() : $this->destroy();
    }

}