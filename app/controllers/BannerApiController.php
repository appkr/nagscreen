<?php

use airplug\Transformers\BannerTransformer;

class BannerApiController extends \ApiController
{

    /**
     * @var airplug\Transformers\BannerTransformer
     */
    private $bannerTransformer;


    /**
     * @var Illuminate\Database\Eloquent\Model
     */
    private $model;


    /**
     * Constructor
     *
     * @param Banner            $model
     * @param BannerTransformer $bannerTransformer
     */
    public function __construct(Banner $model, BannerTransformer $bannerTransformer)
    {
        parent::__construct();

        $this->beforeFilter('force.ssl', ['on' => ['post', 'put', 'patch', 'delete']]);
        $this->beforeFilter('apicsrf', ['on' => ['post', 'put', 'patch', 'delete']]);

        $this->model = $model;

        $this->bannerTransformer = $bannerTransformer;
    }


    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id = null)
    {
        if (! $banner = $this->model->with('package')->find($id)) return $this->respondNotFound();

        $response = $this->respond([
            'data' => $this->bannerTransformer->transform($banner->toArray())
        ]);

        return (isset($input['callback'])) ? $response->setCallback($input['callback']) : $response;
    }


    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $banners = $this->fetch();

        if ($banners->count() < 1) return $this->respondNotFound();

        $response = $this->respondWithPagination($banners, ['data' => $this->bannerTransformer->transformCollection($banners->toArray()['data'])]);

        return (isset($input['callback'])) ? $response->setCallback($input['callback']) : $response;
    }


    /**
     * @return mixed
     */
    private function fetch()
    {
        $input = prepare_input_for_banner(Input::all());

        $query = (is_memcached())
            ? $this->model->with('package')->remember(60)->cacheTags('banners')
            : $this->model->with('package');

        $banners = $query->orderBy('id', $input['order'])->paginate($input['limit']);

        return $banners;
    }

}