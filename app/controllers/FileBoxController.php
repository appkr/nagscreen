<?php

class FileBoxController extends \BaseController
{

    /**
     * @var Illuminate\Database\Eloquent\Model
     */
    private $model;


    /**
     * @param \FileBox $model
     */
    public function __construct(FileBox $model)
    {
        parent::__construct();

        $this->beforeFilter('auth');

        $this->model = $model;
    }


    /**
     * @return mixed
     */
    public function index()
    {
        $input = prepare_input_for_filebox(Input::all());

        $query = $query = (is_memcached())
            ? $this->model->with('user')->remember(60)->cacheTags('filebox')
            : $this->model->with('user');

        if (! empty($input['question'])) $query->where('filename','like','%'.$input['question'].'%')->orWhere('created_by','like','%'.$input['question'].'%');

        $filebox = $query->orderBy($input['sort'], $input['order'])->paginate(25);

        return View::make('filebox.index')->withFilebox($filebox);
    }


    /**
     * @return Response
     */
    public function store()
    {
        $file = Input::file('file');

        if (! Input::hasFile('file'))
            return Response::json(['error'=>'File too big.'], 400);

        $name = $file->getClientOriginalName();
        $path = public_path().'/filebox';
        $size = $file->getSize();

        if (File::exists($path.DIRECTORY_SEPARATOR.$name))
            return Response::json(['error'=>'Duplicate file name.'], 422);

        if (! $file->move($path, $name))
            return Response::json(['error'=>'Internal Error. The most possible cause is file write permission.'], 500);

        if (! $file = $this->model->create([
            'user_id'  => Auth::user()->id,
            'filename' => $name,
            'size'     => $size
        ]))
            return Response::json(['error'=>'Internal Error.'], 500);

        Event::fire('cache.flush', ['filebox']);
        return $this->setStatusCode(201)->respond(['data' => $file->toArray()]);
    }


    /**
     * @param null $id
     * @return Response
     */
    public function destroy($id = null)
    {
        if (! $filebox = $this->model->find($id))
            return $this->setStatusCode(404)->respondWithError('Not Found');

        File::delete(public_path().'/filebox/'.$filebox->filename);
        OnetimeFileboxUrl::whereFilename($filebox->filename)->delete();

        if (! $filebox->delete())
            return $this->setStatusCode(500)->respondWithError('Internal Error. File delete failed');

        Event::fire('cache.flush', ['filebox']);
        return $this->respond(['data' => (boolean) true]);
    }


    /**
     * @param $filename
     * @return mixed
     */
    public function download($filename)
    {
        return Response::download(public_path().'/filebox/'.base64_decode($filename), $filename, $this->getContentTypeHeader($filename));
    }

}