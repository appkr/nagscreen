<?php

class BaseController extends Controller {

    /**
     * @var integer
     */
    private $statusCode = 200;

    /**
     * Constructor
     */
    public function __construct() {
        $this->beforeFilter(function () {
            if (is_memcached()) {
                $packages = Package::remember(60)->cacheTags('packages')->lists('name', 'id');
                $packages_long = Package::remember(60)->cacheTags('packages')->lists('name', 'package');
                $categories = Category::remember(60)->cacheTags('categories')->lists('name', 'id');
                $comments = History::with('user')->remember(60)->cacheTags('histories')
                                   ->where('trackable_type', 'Comment')->orWhere('trackable_type', 'Feedback')
                                   ->orderBy('created_at', 'desc')->take(5)->get();
                $documents = Apidoc::remember(60)->cacheTags('apidocs')->orderBy('id', 'asc')->lists('title', 'id');
            } else {
                $packages = Package::lists('name', 'id');
                $packages_long = Package::lists('name', 'package');
                $categories = Category::lists('name', 'id');
                $comments = History::with('user')->where('trackable_type', 'Comment')
                                   ->orWhere('trackable_type', 'Feedback')->orderBy('created_at', 'desc')->take(5)->get();
                $documents = Apidoc::orderBy('title', 'asc')->lists('title', 'id');
            }

            View::share('packages', $packages);
            View::share('packages_long', $packages_long);
            View::share('categories', $categories);
            View::share('recentComments', $comments);
            View::share('documents', $documents);
            View::share('languages', Config::get('setting.languages'));
            View::share('project', Config::get('setting.project'));
            View::share('order', Input::get('order'));
            View::share('sort', Input::get('sort'));
            View::share('filter', Input::get('filter'));
            View::share('question', Input::get('question'));
            View::share('keyword', Input::get('keyword'));
        });
    }

    /**
     * Setup the layout used by the controller.
     *
     * @return void
     */
    protected function setupLayout() {
        if (! is_null($this->layout)) {
            $this->layout = View::make($this->layout);
        }
    }

    /**
     * Generic response
     *
     * @param  array|string $data
     * @param  array        $headers
     * @param bool          $html
     * @return Illuminate\Http\Response
     */
    public function respond($data, $headers = [], $html = false) {
        $type = $html ? 'make' : 'json';

        return Response::$type($data, $this->getStatusCode(), $headers);
    }

    /**
     * Generic error response
     *
     * @param  string $message
     * @param bool    $html
     * @return $this
     */
    public function respondWithError($message, $html = false) {
        if ($html) return $this->respond($message, [], true);

        return $this->respond([
            'error' => [
                'code'    => $this->getStatusCode(),
                'message' => $message
            ]
        ]);
    }

    /**
     * Respond 204
     *
     * @return \Illuminate\Http\Response
     */
    public function respondNoContent() {
        return $this->setStatusCode(204)->respond(null, [], $html);
    }

    /**
     * @return mixed
     */
    public function getStatusCode() {
        return $this->statusCode;
    }

    /**
     * @param mixed $statusCode
     * @return $this
     */
    public function setStatusCode($statusCode) {
        $this->statusCode = $statusCode;

        return $this;
    }

    /**
     * Calculate Content-type header based on file extension
     *
     * @param $path
     * @return array
     */
    protected function getContentTypeHeader($path) {
        $extension = pathinfo($path, PATHINFO_EXTENSION);
        $contentType = Config::get("mime.{$extension}") ?: 'application/octet-stream';
        return ['Content-Type' => $contentType];
    }

}