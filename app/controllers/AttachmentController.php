<?php

class AttachmentController extends \BaseController
{

    /**
     * @var Illuminate\Database\Eloquent\Model
     */
    private $model;

    /**
     * @param Attachment $model
     */
    public function __construct(Attachment $model)
    {
        parent::__construct();

//        $this->beforeFilter('csrf', ['on' => ['post', 'put', 'patch', 'delete']]);

        $this->beforeFilter('auth');

        $this->model = $model;
    }


    /**
     * Controller for attachment/store post request
     *
     * @return object Response
     */
    public function store()
    {
        $file = Input::file('file');

        if (! Input::hasFile('file')) return $this->setStatusCode(500)->respondWithError('Internal Error');

        if (! $data = save_image($file, Config::get('setting.image.systemPath'), 'img', Config::get('setting.image.width')))
        {
            return $this->setStatusCode(500)->respondWithError('Internal Error. The most possible cause is file write permission.');
        }

        $attachment = $this->model->create(['filename' => $data['name'], 'size' => $data['size']]);

        if (! $attachment)
        {
            return $this->setStatusCode(500)->respondWithError('Internal Error.');
        }

        return $this->setStatusCode(201)->respond(['data' => $attachment->toArray()]);
    }


    /**
     * Controller for attachment/destroy  post request
     *
     * @param null $id
     * @return object Response
     */
    public function destroy($id = null)
    {
        $attachment = $this->model->findOrFail($id);
        $savePath   = Config::get('setting.image.systemPath');

        if (! $attachment)
        {
            return $this->setStatusCode(404)->respondWithError('Not Found');
        }

        File::delete($savePath . DIRECTORY_SEPARATOR . $attachment->filename);

        if (! $attachment->delete())
        {
            return $this->setStatusCode(500)->respondWithError('Internal Error. File delete failed');
        }

        return $this->respond(['data' => (boolean) true]);
    }

}