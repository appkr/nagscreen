<?php

use airplug\Transformers\EulaUserTransformer;
use airplug\Transformers\PostTransformer;

class EulaUserApiController extends \ApiController
{

    /**
     * @var airplug\Transformers\EulaUserTransformer
     */
    private $eulaUserTransformer;


    /**
     * @var airplug\Transformers\PostTransformer
     */
    private $postTransformer;


    /**
     * @var Illuminate\Database\Eloquent\Model
     */
    private $model;


    /**
     * Constructor
     *
     * @param EulaUser                                 $model
     * @param airplug\Transformers\EulaUserTransformer $eulaUserTransformer
     * @param airplug\Transformers\PostTransformer     $postTransformer
     */
    public function __construct(EulaUser $model, EulaUserTransformer $eulaUserTransformer, PostTransformer $postTransformer)
    {
        parent::__construct();

        $this->beforeFilter('apicsrf', ['on' => ['post', 'put', 'patch', 'delete']]);

        $this->model = $model;

        $this->eulaUserTransformer = $eulaUserTransformer;

        $this->postTransformer = $postTransformer;
    }


    /**
     * Display the specified resource.
     *
     * @param int  $id
     * @param int  $statusCode
     * @return Response
     */
    public function show($id, $statusCode = 200)
    {
        if (! $eula = $this->model->with('package')->find($id)) return $this->respondNotFound();

        $response = $this->setStatusCode($statusCode)->respond(['data' => $this->eulaUserTransformer->transform($eula->toArray())]);

        return (Input::get('callback')) ? $response->setCallback(Input::get('callback')) : $response;
    }


    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $input = prepare_input_for_eula(Input::all());
        $input = lpad_uuid($input);

        $validation = Validator::make($input, $this->model->rules);

        if ($validation->fails()) return $this->respondUnprocessableError($validation->errors()->toArray());

        $input['ip']            = Request::server('REMOTE_ADDR');
        $input['eula_language'] = (isset($input['eula_language'])) ? $input['eula_language'] : 'ko';

        // In case eula_version was not given, fallback to most recent version
        if (! isset($input['eula_version']) || empty($input['eula_version']))
        {
            $input['eula_version'] = (is_memcached())
                ? Post::eula()->remember(60)->cacheTags('posts')->wherePackageId($input['package_id'])->whereLanguage($input['eula_language'])->max('id')
                : Post::eula()->wherePackageId($input['package_id'])->whereLanguage($input['eula_language'])->max('id');

            if (is_null($input['eula_version'])) $input['eula_version'] = 1;
        }

        // If there is a previous record, update it
        if ($returningUser = $this->model->wherePackageId($input['package_id'])->where('androidID', $input['androidID'])->first())
        {
            if (! $returningUser->update($input)) return $this->respondInternalError();

            return $this->show($returningUser->id, 201);
        }

        if (! $eula = $this->model->create($input)) return $this->respondInternalError();

        return $this->show($eula->id, 201);
    }


    /**
     * @return \Illuminate\Http\Response
     */
    public function etag()
    {
        $input = prepare_input_for_eula(Input::all());

        $query = (is_memcached())
            ? Post::eula()->remember(60)->cacheTags('posts')
            : Post::eula();

        if (! empty($input['package_id'])) $query->wherePackageId($input['package_id']);
        if (! empty($input['language'])) $query->whereLanguage($input['language']);

        if (! $timestring = $query->max('created_at')) return $this->respondNotFound();

        $response = $this->respond(['data' => [
            'timestring' => $timestring,
            'timestamp'  => (int) strtotime($timestring)
        ]]);

        return (isset($input['callback'])) ? $response->setCallback($input['callback']) : $response;
    }


    public function view()
    {
        $input = prepare_input_for_eula(Input::all());

        $query = (is_memcached())
            ? Post::with('user', 'package', 'category')->remember(60)->cacheTags('posts')->published()->eula()
            : Post::eula()->published()->eula();

        if (! empty($input['package_id'])) $query->wherePackageId($input['package_id']);
        if (! empty($input['language'])) $query->whereLanguage($input['language']);

        if (! $eula = $query->orderBy('id', 'desc')->first()) return $this->respondNotFound();

        $response = $this->respond(['data' => $this->postTransformer->transform($eula->toArray())]);

        return (isset($input['callback'])) ? $response->setCallback($input['callback']) : $response;
    }

}