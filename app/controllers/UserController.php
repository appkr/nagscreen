<?php

class UserController extends \BaseController
{

    /**
     * @var Illuminate\Database\Eloquent\Model
     */
    private $model;


    /**
     * @param \User $model
     */
    public function __construct(User $model)
    {
        parent::__construct();

        $this->beforeFilter('csrf', ['on' => 'post', 'except' => 'postUpdate']);
        $this->beforeFilter('admin', ['on' => 'index']);
        $this->beforeFilter('auth', ['only' => ['profile']]);
        $this->beforeFilter('guest', ['only' => ['getSignin', 'getSignup']]);

        $this->model = $model;
    }


    /**
     * @return mixed
     */
    public function index()
    {
        $input = prepare_input_for_user(Input::all());

        $apiUsersToBeExemptedAtListing = (is_memcached())
            ? Package::remember(60)->cacheTags('packages')->lists('package')
            : Package::lists('package');

        $query = $this->model->with('posts', 'comments', 'feedbacks', 'pushmessages');
        $users = (is_memcached())
            ? $query->remember(60)->cacheTags('users')->whereNotIn('email', $apiUsersToBeExemptedAtListing)->orderBy($input['sort'], $input['order'])->get()
            : $query->whereNotIn('email', $apiUsersToBeExemptedAtListing)->orderBy($input['sort'], $input['order'])->get();

        return View::make('user.index')->withUsers($users);
    }


    /**
     * Controller for user/signin get request
     *
     * @return object View
     */
    public function getSignin()
    {
        return View::make('user.signin');
    }


    /**
     * Controller for user/signin post request
     *
     * @return object Redirect
     */
    public function postSignin()
    {
        // build a user to login
        $user = [
            'email'    => Input::get('email'),
            'password' => Input::get('password')
        ];

        if (Auth::once($user))
        {
            $redirect = get_location_after_signin(Input::get('url'));

            // If user was not activated...
            if (! Auth::user()->activated)
            {
                Auth::logout();

                return Redirect::to($redirect)
                    ->with('error', 'Your account has not been activated. Please try again later.')
                    ->withInput();
            }

            // Auth::loginUsingId(Auth::user()->id, Input::has('remember'));
            Auth::loginUsingId(Auth::user()->id, true);

            // Event fire for last login time save
            Event::fire('auth.login', [Auth::user()]);
            Event::fire('cache.flush', ['users']);

            return Redirect::intended($redirect)->with('info', 'You are now logged in !');
        }
        else
        {
            return Redirect::route('session.create')
                ->with('error', 'Your email/password combination was incorrect! Please try again.')
                ->withInput();
        }
    }


    /**
     * Controller for user/signout get request
     *
     * @return object Redirect
     */
    public function getSignout()
    {
        if (Auth::check())
        {
            Auth::logout();

            return Redirect::home()->with('info', 'Your are now logged out !');
        }
        else
        {
            return Redirect::home();
        }
    }


    /**
     * Controller for user/signup get request
     *
     * @return object View
     */
    public function getSignup()
    {
        return View::make('user.signup');
    }


    /**
     * Controller for user/signup post request
     *
     * @return object Redirect
     */
    public function postSignup()
    {
        $validation = Validator::make(Input::all(), $this->model->rules);

        if ($validation->passes())
        {
            $redirect = get_location_after_signin(Input::get('url'));

            // Code for activation
            $code = str_random(60);

            $user = $this->model->create([
                'email'    => e(Input::get('email')),
                'password' => Hash::make(Input::get('password')),
                'code'     => $code,
            ]);

            // if insert operation was not successful
            if (! $user)
                return Redirect::route('user.create')->with('error', 'Something went wrong! Please try again.')->withInput();

            // Auth::login($user);

            Event::fire('auth.signup', [$user]);
            Event::fire('cache.flush', ['users']);

            return Redirect::to($redirect)
                ->with('success', 'Your account registration was successful! Check your email account to activate the account.');
        }
        else
        {
            return Redirect::route('user.create')->withErrors($validation)->withInput();
        }
    }


    /**
     * Activate user account
     *
     * @param  string $code
     * @return Redirect
     */
    public function activate($code)
    {
        $user = $this->model->whereCode($code)->whereActivated(0)->first();

        if ($user)
        {
            $user->activated = 1;
            $user->code      = null;

            Event::fire('cache.flush', ['users']);

            if ($user->save()) return Redirect::route('session.create')->with('info', 'Account activated! Please signin.');
        }

        return Redirect::home()->with('error', 'Something went wrong! Please try again later.');
    }


    /**
     * Controller for user/remind get request
     *
     * @return object View
     */
    public function getRemind()
    {
        return View::make('user.remind');
    }


    /**
     * Controller for user/remind post request
     *
     * @return object Redirect
     */
    public function postRemind()
    {
        $response = Password::remind(Input::only('email'), function ($message) {
            $message->subject(trans('reminders.subject'));
        });

        switch ($response)
        {
            case Password::INVALID_USER:
                return Redirect::back()->with('error', Lang::get($response));

            case Password::REMINDER_SENT:
                return Redirect::home()->with('info', Lang::get($response));
        }
    }


    /**
     * Controller for user/reset get request
     *
     * @param null $token
     * @return object View
     */
    public function getReset($token = null)
    {
        if (is_null($token)) App::abort(404);

        return View::make('user.reset')->with('token', $token);
    }


    /**
     * Controller for user/reset post request
     *
     * @return object Redirect
     */
    public function postReset()
    {
        $credentials = Input::only('email', 'password', 'password_confirmation', 'token');

        $response = Password::reset($credentials, function ($user, $password) {
            $user->password = Hash::make($password);
            $user->save();
        });

        switch ($response)
        {
            case Password::INVALID_PASSWORD:
            case Password::INVALID_TOKEN:
            case Password::INVALID_USER:
                return Redirect::back()->with('error', Lang::get($response));

            case Password::PASSWORD_RESET:
                return Redirect::route('session.create')->with('info', 'Password changed!');
        }
    }


    /**
     * Controller for user/profile get request
     *
     * @param null $id
     * @return View
     */
    public function profile($id = null)
    {
        $user = ($id)
            ? $this->model->with('histories')->findOrFail($id)
            : $this->model->with('histories')->findOrFail(Auth::user()->id);

        return ($user)
            ? View::make('user.profile')->with('user', $user)
            : Redirect::back()->with('error', 'User data not exist !');
    }

}