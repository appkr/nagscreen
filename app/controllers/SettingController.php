<?php

class SettingController extends \BaseController
{

    /**
     * Constructor
     */
    public function __construct()
    {
        parent::__construct();

        $this->beforeFilter('csrf', ['on' => 'post']);

        $this->beforeFilter('admin');
    }


    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $setting = array_stringify(unserialize(Setting::first()->value));

        return View::make('setting.index')->with('setting', $setting);
    }


    /**
     * Update the specified resource in storage.
     *
     * @return Response
     */
    public function update()
    {
        $data = [];

        foreach (Input::except('_token', '_method') as $key => $value)
        {
            array_restore($data, $key, $value);
        }

        $setting = Setting::first();

        if ($setting)
        {
            $setting->value = serialize($data);
            $result         = $setting->update();
        }
        else
        {
            $result = Setting::create(['value' => serialize($data)]);
        }

        Cache::forget('setting');

        return ($result)
            ? Redirect::back()->with('success', 'Setting updated !')
            : Redirect::back()->with('error', 'Something went wrong. Try again later !');
    }

}