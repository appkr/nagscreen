<?php

class TestLogsController extends \BaseController
{

    /**
     * @var TestLogs
     */
    private $model;

    /**
     * @param TestLogs $model
     */
    function __construct(TestLogs $model)
    {
        // $this->beforeFilter('csrf', ['on' => ['post', 'put', 'patch', 'delete']]);
        // $this->beforeFilter('auth.basic', ['only' => ['store']]);
        $this->beforeFilter('auth', ['only' => ['index', 'show', 'downloadSpreadSheet', 'destroy']]);

        $this->model = $model;

        parent::__construct();
    }

    /**
     * Display a listing of the resource.
     * GET /test/logs
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $modules = $this->model->modules();

        $query = $this->model->with('user');

        if (Input::get('filter') && Input::get('question'))
        {
            $query->where(Input::get('filter'), 'like', '%' . urldecode(Input::get('question')) . '%');
        }

        if (Input::get('sort') && Input::get('order'))
        {
            $query->orderBy(Input::get('sort'), Input::get('order'));
        }

        $testLogs = $query->latest()->paginate(50);

        return View::make('tester.logs.index', compact('testLogs', 'modules'));
    }

    /**
     * Display the specified resource.
     * GET /test/logs/{id}
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $log = $this->model->with('user')->findOrFail($id);

        return View::make('tester.logs.show')->with('testLog', $this->makeArrayFlatten($log->toArray()));
    }

    /**
     * Store a newly created resource in storage.
     * POST /test/logs/{testName}
     *
     * @param $testName
     * @return \Illuminate\Http\Response
     */
    public function store($testName)
    {
        if (Request::isJson())
        {
            $request = Request::instance();
            $payload = json_decode($request->getContent(), true);

            // debug
            // $filename = storage_path() . '/' . 'log-' . time() . '.txt';
            // File::put($filename, var_export($payload, true));

            foreach($payload as $log) {
                $this->saveLog($testName, $log);
            };
        }
        else {
            $this->saveLog($testName, Input::except('_method', '_token'));
        }

        return $this->respond(['data' => true]);
    }

    /**
     * Download/export log records belongs to the given $module
     *
     * @param $module
     * @return \Illuminate\Http\RedirectResponse
     */
    public function downloadSpreadSheet($module)
    {
        set_time_limit(180);

        $data = $this->extractLogByModule($module);

        Excel::create($module . '-' . str_replace(' ', '-', \Carbon\Carbon::now()->toDateTimeString()), function ($excel) use ($module, $data) {
            $excel->sheet($module, function($sheet) use ($data) {
                $sheet->fromArray($data);
            });
        })->export('xlsx');

        return Redirect::back();
    }

    /**
     * Xxport log records to CSV format
     *
     * @param $module
     * @return \Illuminate\Http\RedirectResponse
     */
    public function downloadCsv($module)
    {
        set_time_limit(360);

        $data     = $this->extractLogByModule($module);
        $filename = $module . '-' . str_replace(' ', '-', \Carbon\Carbon::now()->toDateTimeString()) . '.csv';

        header('Content-Type: text/csv; charset=utf-8');
        header('Content-Disposition: attachment;filename=' . $filename);
        header('Content-Transfer-Encoding: binary');

        ob_start();
        $fp = fopen('php://output', 'w');

        fputcsv($fp, array_keys($data[0]), ',', '"');

        foreach ($data as $line)
        {
            fputcsv($fp, $line, ',', '"');
        }

        fclose($fp);
        return ob_get_clean();
    }

    /**
     * Delete logs which belongs to given $testName
     *
     * @param string $testName
     * @return $this|\Illuminate\Http\Response
     */
    public function destroy($testName)
    {
        if (! $logs = $this->model->whereModule($testName))
        {
            return $this->setStatusCode(404)->respondWithError("Given test module {$testName} not found");
        }

        $logs->delete();

        return $this->respond(['data' => true]);
    }

    /**
     * Save log data
     *
     * @param string $testName
     * @param array  $data
     * @return \Illuminate\Database\Eloquent\Model|static
     */
    private function saveLog($testName, array $data)
    {
        return $this->model->create([
            'module'  => $testName,
            'log'     => is_array($data) ? json_encode($data) : $data,
            'user_id' => Auth::check() ? Auth::user()->id : null,
            'ip'      => Request::server('REMOTE_ADDR') ? : null,
        ]);
    }

    /**
     * @param  string $module name of the test module to extract
     * @return array
     */
    private function extractLogByModule($module)
    {
        $data = [];

        if (! $logs = $this->model->with('user')->whereModule($module)->latest()->get()->toArray())
        {
            return Redirect::back()->withError('Seems the given module is not valid or it does not have any record !');
        }

        foreach (array_chunk($logs, 100) as $chunked)
        {
            foreach($chunked as $log)
            {
                $newLogArray = $this->makeArrayFlatten($log);
                $data[]      = $newLogArray;
            }
        }

        return $data;
    }

    /**
     * @param array $log
     * @return array
     */
    private function makeArrayFlatten(array $log)
    {
        if (isset($log['user']) && is_array($log['user']))
        {
            $log = array_merge(array_except($log, ['user']), $log['user']);
        }

        $logBody = json_decode($log['log'], true);
        ksort($logBody);

        $log = array_merge(array_except($log, ['log']), $logBody);

        if (isset($log['timestamp']) && is_array($log['timestamp']))
        {
            $log = array_merge(array_except($log, ['timestamp']), $log['timestamp']);
        }

        if (isset($log['timestamp'])) {
          $log['timestamp'] = \Carbon\Carbon::createFromTimeStamp(round($log['timestamp']/1000))->toDateTimeString();
        }

        return array_except($log, ['user_id', 'timezone', 'timezone_type']);
    }

}