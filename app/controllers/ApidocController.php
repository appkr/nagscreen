<?php

class ApidocController extends \BaseController
{

    /**
     * @var Illuminate\Database\Eloquent\Model
     */
    private $model;


    public function __construct(Apidoc $model)
    {
        parent::__construct();

        $this->beforeFilter('auth');
        $this->beforeFilter('csrf', ['on' => ['post', 'put', 'patch', 'delete']]);

        $this->model = $model;
    }


    /**
     * Display a listing of the resource.
     *
     * @param null $id
     * @return Response
     */
    public function index($id = null)
    {
        // TODO show revision history
        if (is_null($id)) $id = 1;

        $apidoc = $this->model->find($id);

        $search = ($key = Input::get('keyword'))
            ? $this->model->search($key)->get()
            : null;

        return View::make('apidoc.index')->withApidoc($apidoc)->withSearch(highlight_search_match($search, $key));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return  View
     */
    public function create()
    {
        return View::make('apidoc.create');
    }


    /**
     * Store a newly created resource in storage.
     *
     * @return  Response
     */
    public function store()
    {
        $validation = Validator::make(Input::all(), $this->model->rules);

        if ($validation->fails()) return Redirect::back()->withInput()->withErrors($validation);

        $apidoc = $this->model->create(Input::all());

        Event::fire('history.track', [$apidoc]);
        Event::fire('cache.flush', ['apidocs']);

        return Redirect::route('api.index', ['id' => $apidoc->id])->with('success', 'Created !');
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param $id
     * @return  Veiw
     */
    public function edit($id = null)
    {
        return View::make('apidoc.edit')->withApidoc($this->model->findOrFail($id));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param null $id
     * @return  Response(=Redirect)
     */
    public function update($id = null)
    {
        if (! $apidoc = $this->model->findOrFail($id)) return Redirect::back()->with('error', 'Not Found');

        $validation = Validator::make(Input::all(), $this->model->rules);

        if ($validation->fails()) return Redirect::back()->withInput()->withErrors($validation);

        $apidoc->update(Input::all());

        Event::fire('history.track', [$apidoc]);
        Event::fire('cache.flush', ['apidocs']);

        return Redirect::route('api.index', ['id' => $apidoc->id])->with('success', 'Modified !');
    }

}