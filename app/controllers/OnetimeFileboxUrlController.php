<?php

use Carbon\Carbon;

class OnetimeFileboxUrlController extends \BaseController
{

    /**
     * @var Illuminate\Database\Eloquent\Model
     */
    private $onetimeurl;


    /**
     * @var Illuminate\Database\Eloquent\Model
     */
    private $filebox;


    /**
     * @param OnetimeFileboxUrl $onetimeurl
     * @param FileBox           $filebox
     * @internal param \FileBox $model
     */
    public function __construct(OnetimeFileboxUrl $onetimeurl, FileBox $filebox)
    {
        parent::__construct();

        $this->beforeFilter('auth', ['on' => ['make']]);
        $this->beforeFilter('csrf', ['on' => ['post', 'put', 'patch']]);

        $this->onetimeurl = $onetimeurl;
        $this->filebox    = $filebox;
    }


    /**
     * Fetch one time url that matches to given token
     *
     * @param $token
     * @return bool
     */
    private function fetchUrl($token)
    {
        return $this->onetimeurl->whereToken($token)->first();
    }


    /**
     * Response to file download request
     *
     * @param $token
     * @throws airplug\Exceptions\ResourceExpiredException
     * @return Illuminate\Http\Response
     */
    public function download($token)
    {
        if (! $onetimeurl = $this->fetchUrl($token)) App::abort(404);
        if (Carbon::now() > $onetimeurl->expires_at) throw new airplug\Exceptions\ResourceExpiredException();

        $path = public_path().'/filebox/'.$onetimeurl->filename;

        if (! File::exists($path)) App::abort(404);

        return Response::download($path, $onetimeurl->filename, $this->getContentTypeHeader($onetimeurl->filename));
    }


    /**
     * Shows upload form in response to one time upload url hit
     *
     * @param $token
     * @throws airplug\Exceptions\ResourceExpiredException
     * @return \Illuminate\View\View
     */
    public function upload($token)
    {
        if (! $onetimeurl = $this->fetchUrl($token)) App::abort(404);
        if (Carbon::now() > $onetimeurl->expires_at) throw new airplug\Exceptions\ResourceExpiredException();

        return View::make('filebox.upload')->withData($onetimeurl);
    }


    /**
     * Create one time download|upload url
     *
     * @return \Illuminate\Http\Response
     */
    public function make()
    {
        $expires_at = Carbon::now()->addDays(Input::get('period', 7));

        $token = str_random(40);

        if (! $onetimeurl = $this->onetimeurl->create([
            'user_id'    => Auth::user()->id,
            'token'      => $token,
            'filename'   => Input::get('filename', null),
            'expires_at' => $expires_at
        ]))
            return Response::json(['error'=>'Internal Error.'], 500);

        return $this->setStatusCode(201)->respond(['data'=>$onetimeurl->toArray()]);
    }


    /**
     * Upload file in response to upload request
     *
     * @return \Illuminate\Http\Response
     */
    public function save()
    {
        $file = Input::file('file');

        $validation = Validator::make(Input::all(), ['email' => 'required|email']);
        if ($validation->fails()) return Response::json(['error'=>$validation->messages()->all()], 422);

        if (! Input::hasFile('file')) return Response::json(['error'=>'File too big.'], 400);

        $name = $file->getClientOriginalName();
        $path = public_path().'/filebox';
        $size = $file->getSize();

        if (File::exists($path.'/'.$name)) return Response::json(['error'=>'Duplicate file name.'], 422);

        if (! $file->move($path, $name)) return Response::json(['error'=>'Internal Error.'], 500);

        if (! $file = $this->filebox->create([
            'user_id'    => Input::get('user_id', 1),
            'created_by' => Input::get('email'),
            'filename'   => $name,
            'size'       => $size
        ]))
            return Response::json(['error'=>'Internal Error.'], 500);

        Event::fire('cache.flush', ['filebox']);
        Event::fire('onetimefileboxurl.consumed', [Input::get('user_id'), $file]);

        return $this->setStatusCode(201)->respond(['data' => $file->toArray()]);
    }


    /**
     * Delete a record
     *
     * @return string
     */
    public function destroy()
    {
        if (! $count = $this->onetimeurl->where('expires_at' < Carbon::now())->count()) return 'Nothing to delete';
        else
        {
            if (! $this->onetimeurl->where('expires_at' < Carbon::now())->delete()) return 'Internal Error. File delete failed';
            return $count.' entries has been removed';
        }
    }

}