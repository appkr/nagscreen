<?php

use Guzzle\Service\Client;

class GcuController extends \BaseController
{

    /**
     * @var string
     */
    private $baseUrl = 'modumtv.appspot.com/api/';

    /**
     * @var Guzzle\Service\Client
     */
    private $client;

    /**
     * @param Client $client
     */
    public function __construct(Client $client)
    {
        $this->beforeFilter('auth');
        $this->beforeFilter('csrf', ['on' => ['post', 'put', 'patch', 'delete']]);

        $this->client = $client;
        parent::__construct();
    }


    public function index()
    {
        if (! Cache::has('extractor'))
        {
            $request = $this->client->createRequest('GET', $this->getScheme() . $this->baseUrl . 'extractors');
            $request->setHeader('Content-type', 'application/json');
            $request->setHeader('Access-Control-Allow-Origin', '*');

            $response = $this->client->send($request);
            $response = $response->json();

            $services = [];
            foreach ($response['extractors'] as $entry) $services[] = $entry['name'];
            asort($services);
            $extractors = implode(', ', $services);

            Cache::forever('extractors', $extractors);
        }
        else
        {
            $extractors = Cache::get('extractors');
        }

        return View::make('tester.gcu.index')->withExtractors($extractors);
    }


    /**
     * Fire api call, get the result, and return to the view
     *
     * @return \Illuminate\Http\Response
     */
    public function extract()
    {
        $url     = Input::get('url');
        $flatten = Input::get('flatten');

        if (! Cache::has($url))
        {
            $request = $this->client->createRequest('GET', $this->getScheme() . $this->baseUrl . 'info?' . $this->buildQuery($url, $flatten));
            $request->setHeader('Content-type', 'application/json');
            $request->setHeader('Access-Control-Allow-Origin', '*');

            $response = $this->client->send($request);
            $response = $response->json();

            Cache::put($url, $response, \Carbon\Carbon::now()->addMinutes(60 * 12));
        }
        else
        {
            $response = Cache::get($url);
        }

        if (! $response) $this->setStatusCode(500)->respondWithError('Extraction failed !');

        return $this->respond(['data' => $response]);
    }


    /**
     * Find url scheme
     *
     * @return string
     */
    private function getScheme()
    {
        return Request::secure() ? 'https://' : 'http://';
    }


    /**
     * Build url encoded query string
     *
     * @param string $url
     * @param bool   $flatten
     * @return string
     */
    private function buildQuery($url, $flatten)
    {
        return http_build_query([
            'url'     => $url,
            'flatten' => $flatten ? 'True' : 'False'
        ]);
    }

}