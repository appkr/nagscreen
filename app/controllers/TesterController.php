<?php

class TesterController extends \BaseController {

    public function __construct() {
        $this->beforeFilter('auth', ['only' => ['video', 'amsXTester']]);

        parent::__construct();
    }

    /**
     * Index page of AirPlug Test Portal
     * GET /test
     *
     * @return \Illuminate\View\View
     */
    public function index() {
        $tools = [
            /*[
                'type'        => 'tool',
                'title'       => 'DEMO Videos',
                'url'         => route('test.bbdemo'),
                'description' => 'List of videos provisioned by AirPlug for demo purpose.',
            ],*/
            [
                'type'        => 'tool',
                'title'       => 'Test Videos',
                'url'         => route('test.bbdemo2'),
                'description' => 'Infinite video-show for Aging Test',
            ],
            /*[
                'type'        => 'tool',
                'title'       => 'DEMO Images',
                'url'         => route('test.xdemo'),
                'description' => 'List of images provided by "Open Source Photography" from Flickr',
            ],
            [
                'type'        => 'tool',
                'title'       => 'DEMO Images 2',
                'url'         => route('test.xdemo2'),
                'description' => 'List of images hosted by AirPlug for "Japanese Device Capability" test',
            ],*/
            [
                'type'        => 'tool',
                'title'       => 'Test Images',
                'url'         => route('test.xdemo3'),
                'description' => 'Infinite image-show for Aging Test',
            ],
            [
                'type'        => 'tool',
                'title'       => 'Web Walker',
                'url'         => route('test.web'),
                'description' => 'Infinite web-loading for Aging Test',
            ],
            /*[
                'type'        => 'tool',
                'title'       => 'DEMO Images - https',
                'url'         => route('test.xdemo.https'),
                'description' => 'List of HTTPS images hosted by Flickr for Aging Test',
            ],*/
            [
                'type'        => 'tool',
                'title'       => 'Download Test',
                'url'         => route('test.download'),
                'description' => 'List of various types of test contents, to see SmartDownloader or AirPlayer/StreamingPlayer is invokable and workable.',
            ],
            [
                'type'        => 'tool',
                'title'       => 'Gcu',
                'url'         => route('test.gcu.index'),
                'description' => 'Video url extractor. Over 400+ video sites.',
            ],
            [
                'type'        => 'tool',
                'title'       => 'Intent Test',
                'url'         => route('test.intent'),
                'description' => 'List of various types of test contents, to see SmartDownloader or Streaming Player receives the thrown intent correctly. The contents are dummy.',
            ],
            /*[
                'type'        => 'tool',
                'title'       => 'Webhook Test',
                'url'         => '/webhook',
                'description' => 'Feasibility study project to redirect html5 web-video to AirPlayer/StreamingPlayer using custom url scheme.',
            ],
            [
                'type'        => 'tool',
                'title'       => 'Infinite Request Test',
                'url'         => '/infinite',
                'description' => 'Ams-x test tool, under continuously varying network environment. To achieve that, Like ddos attack, it sends an ajax request infinitely after 1 second from successfully receiving response.',
            ],*/
            [
                'type'        => 'tool',
                'title'       => 'Log Viewer',
                'url'         => route('test.logs.index'),
                'description' => 'Now a device can send an api request to the server to save its test result. This tool helps developers look over the test results and download them as an excel file.',
            ],
            /*[
                'type'        => 'url',
                'title'       => 'POST Test Url',
                'url'         => route('test.response', 'post'),
                'secure'      => secure_url('/test/response/post'),
                'description' => 'HTTP POST pinging test url. Test applications, like proxy-setting tool, can send a POST request to this url to see POST reqeust is working correctly under current network configurations.',
            ],
            [
                'type'        => 'url',
                'title'       => 'GET Test Url',
                'url'         => route('test.response', 'get'),
                'secure'      => secure_url('/test/response/get'),
                'description' => 'HTTP GET pinging test url. Test applications, like proxy-setting tool, can send a GET request to this url to see GET reqeust is working correctly under current network configurations.',
            ]*/
        ];

        return View::make('tester.index', compact('tools'));
    }

    /**
     * Intent Test page
     * GET /test/intent
     *
     * @return \Illuminate\View\View
     */
    public function intent() {
        $extensions = unserialize(file_get_contents(public_path() . '/tester/extensions.php'));

        return View::make('tester.intent')->withExtensions($extensions);
    }

    /**
     * Download/Play Test page
     * GET /test/download
     *
     * @return \Illuminate\View\View
     */
    public function download() {
        $vectors = unserialize(file_get_contents(public_path() . '/tester/vectors.php'));
        $timestamp = time();

        $simulation = [
            'smallfile(100MB)' => 'tfrontg1.airplug.co.kr/media/test-dummy-100MB.dat?t='.$timestamp,
            'bigfile(1GB)' => 'tfrontg1.airplug.co.kr/media/smalldown.dat?t='.$timestamp,
            'hls'          => 'tfrontg1.airplug.co.kr/sb_hls/base.m3u8',
            'olleh market' => 'mcsdown.ollehmarket.com:46090/public/get.asp?c=3300&phone=V0mLl%2B3DCxr3c0qTrb%2BH%2FA%3D%3D&device=302&apm=KTFWIPIAPM&init=14072210:11:37&date=14072210:11:38&cook=&applicationid=8100A076&phonemodel=LG-F160K&cid=51200004020310&bin_no=51200004020316&osver=410&prunit=A&evtype=PU&evmethod=3&fsize=6839530&b_mode=Y&app_type=00&sess_key=866ae4c259ba45c78d78aa2dcc88a84c&com_type=01&binary_type=01&app_version=02.00.04&is_update=N',
            '10sec delay'  => route('test.delayed', 10),
            '30sec delay'  => route('test.delayed', 30)
        ];

        return View::make('tester.download', compact('vectors', 'simulation'));
    }

    /**
     * Transfer-encoding: binary Test
     * GET /test/binary
     *
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function binary() {
        return Response::download(public_path() . '/tester/binary/chuncked-encoding-test-vector.mp4');
    }

    /**
     * Simulating delayed server response
     * GET /test/delayed{$tic}
     *
     * @param int $tic
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delayed($tic) {
        if ($tic > 60) $tic = 60;

        sleep($tic);

        return Redirect::to('/tester/binary/chuncked-encoding-test-vector.mp4');
    }

    /**
     * BB Video page
     * GET /test/video
     *
     * @return \Illuminate\View\View
     */
    public function video() {
        $videos = (is_memcached())
            ? BBVideo::orderBy('airplug_id', 'asc')->remember(60 * 24)->cacheTags('videos')->paginate(3)
            : BBVideo::orderBy('airplug_id', 'asc')->paginate(3);

        return View::make('tester.video', compact('videos'));
    }

    /**
     * BB Video for X-connect test
     * GET /test/bbdemo
     *
     * @return \Illuminate\View\View
     */
    public function bbdemo() {
        $query = (is_memcached())
            ? BBVideo::remember(60 * 24)->cacheTags('videos')->whereActivated(1)
            : BBVideo::whereActivated(1);

        if (! is_korean_browser()) $query->whereLocale('en');

        $videos = $query->orderBy('id', 'desc')->paginate(10);
        $timestamp = time();

        return View::make('tester.bbdemo', compact('videos', 'timestamp'));
    }

    /**
     * BB Video for X-connect test
     * GET /test/bbdemo2
     *
     * @return \Illuminate\View\View
     */
    public function bbdemo2() {
        return View::make('tester.bbdemo2');
    }

    public function fetchDemoVideos() {
        $videos = BBVideo::remember(60 * 24)->cacheTags('videos')->
            whereActivated(1)->whereLocale('en')
            ->orderBy('id', 'desc')->paginate(1);

        return Response::json($videos->toArray())->setCallback(Input::get('callback'));
    }

    /**
     * BB Video for X-connect test
     * GET /test/bbdemo2
     *
     * @return \Illuminate\View\View
     */
    public function bbdemo2Angular() {
        $query = (is_memcached())
            ? BBVideo::remember(60 * 24)->cacheTags('videos')->whereActivated(1)
            : BBVideo::whereActivated(1);

        if (! is_korean_browser()) $query->whereLocale('en');

        $videos = $query->orderBy('id', 'desc')->get()->toJson();
        $timestamp = time();

        return View::make('tester.bbdemo2-angular', compact('videos', 'timestamp'));
    }

    /**
     * Give flickr image api call delegatation for Ams-x functioning demo
     * GET /test/xdemo
     *
     * @return \Illuminate\View\View
     */
    public function xdemo() {
        $page      = Input::get('page') ?: 1;
        $perPage   = Input::get('perPage') ?: 100;
        $timestamp = time();
        $pager     = Input::get('pager');
        $query     = http_build_query(Input::except('page'));

        Flickering::handshake();

        $results = Flickering::getResultsOf('groups.pools.getPhotos', [
            'group_id' => '83823859@N00',
            'page'     => $page,
            'per_page' => $perPage,
            'extras'   => 'url_m, description'
        ]);

        $photos = Paginator::make($results->obtain()['photo'], $results->total, $results->perpage);

        return View::make('tester.xdemo', compact('photos', 'timestamp', 'pager', 'query'));
    }

    /**
     * Specially designed page for "Japanese Device Capability" test
     * GET /test/xdemo2
     *
     * @return \Illuminate\View\View
     */
    public function xdemo2() {
        $timestamp = time();
        $baseUrl = '//tfrontg1.airplug.co.kr/images/';

        $photos = FlickrImages::get();

        return View::make('tester.xdemo2', compact('baseUrl', 'photos', 'timestamp'));
    }

    /**
     * Infinite slide show
     * GET /test/xdemo3
     *
     * @return \Illuminate\View\View
     */
    public function xdemo3() {
        return View::make('tester.xdemo3');
    }

    public function fetchDemoImages() {
        $perPage = Input::get('perPage') ?: 1;

        $original = FlickrImages::latest()->paginate($perPage);

        $current = array_map(function($image) {
            return [
                'id' => (int) $image['id'],
                'flickr_id' => $image['flickr_id'],
                'owner' => $image['owner'],
                'title' => $image['title'],
                'description' => $image['description'],
                'thumbnail' => '//tfrontg1.airplug.co.kr/images/' . $image['thumbnail'],
                'image' => '//tfrontg1.airplug.co.kr/images/' . $image['image'],
                'thumbnail_size' => (int) $image['thumbnail_size'],
                'image_size' => (int) $image['image_size']
            ];
        }, $original->toArray()['data']);

        return $this->respond([
            'total' => $original->getTotal(),
            'per_page' => $original->getPerPage(),
            'current_page' => $original->getCurrentPage(),
            'last_page' => $original->getLastPage(),
            'data' => $current
        ]);
    }

    public function fetchFlickrImages() {
        $page    = Input::get('page') ?: 1;
        $perPage = Input::get('perPage') ?: 1;

        Flickering::handshake();

        $results = Flickering::getResultsOf('groups.pools.getPhotos', [
            'group_id' => '83823859@N00',
            'page'     => $page,
            'per_page' => $perPage,
            'extras'   => 'url_sq, url_t, url_s, url_q, url_m, url_n, url_z, url_c, url_l, url_o, description'
        ]);

        $photos = Paginator::make(
            $this->transformFlickrImagesUrlToHttpUrl($results->obtain()['photo']),
            $results->total,
            $results->perpage
        );

        return $this->respond($photos->toArray());
    }

    /**
     * Respond to
     * GET /test/web
     *
     * @return mixed
     */
    public function web() {
        return View::make('tester.web');
    }

    /**
     * Find random webpage url with the given $seed
     *
     * @return \Illuminate\Http\Response
     */
    public function getRandomWebpageUrl() {
        $seed = Input::get('seed');

        try {
            $markup = file_get_contents($seed);
        } catch (ErrorException $e) {
            return $this->setStatusCode(404)->respondWithError($e->getMessage());
        }

        if (! $url = $this->getRandomUrl($markup)) {
            return $this->setStatusCode(422)->respondWithError('Unprocessible Entity');
        }

        return $this->respond([
            'siteUrl' => $url
        ]);
    }

    /**
     * Calculate random url from $markup
     *
     * @param $markup
     * @return bool
     */
    private function getRandomUrl($markup) {
        $pattern = '/<a.*href="(?P<url>\b(([\w-]+:\/\/?|www[.])[^\s()<>]+(?:\([\w\d]+\)|([^[:punct:]\s]|\/))))"/';

        if (preg_match_all($pattern, $markup, $matches)) {
            $len = count($matches['url']) - 1;
            foreach(range(0, $len) as $index) {
                $url = $matches['url'][rand(0, $len)];
                if (starts_with($url, 'http://')) {
                    return $url;
                }
            }
        }

        return false;
    }

    /**
     * Custom Url Scheme Test
     * GET /test/cus
     *
     * @return \Illuminate\View\View
     */
    public function cus() {
        return View::make('tester.cus');
    }

    /**
     * Respond to test request
     *
     * @param string|null $method
     * @return string
     */
    public function makeResponse($method = null) {
        $sleep = 0;
        $method = strtoupper($method ?: Request::server('REQUEST_METHOD'));
        $html = Input::get('json') ? false : true;

        if (Input::get('min') || Input::get('max')) {
            $min = (! Input::get('min') || Input::get('min') < 0) ? 0 : Input::get('min');
            $max = (! Input::get('max') || Input::get('max') > 10) ? 10 : Input::get('max');

            if ($min >= $max) {
                return $this->setStatusCode(422)->respondWithError('Invalid min/max value', $html);
            }

            $sleep = rand($min, $max);
            sleep($sleep);
        }

        $response = $html
            ? "Respond to {$method} request. Request arrived at "
            . $GLOBALS['laravel_start']
            . ", server sleep "
            . $sleep * 1000
            . "ms, "
            . " response thrown at "
            . round(microtime(true) * 1000)
            : [
                'data' => [
                    'code'      => 200,
                    'message'   => "Respond to {$method} request",
                    'timestamp' => Input::get('timestamp') ? (int) Input::get('timestamp') : null,
                    'received'  => $GLOBALS['laravel_start'],
                    'sleep'     => $sleep * 1000,
                    'respond'   => round(microtime(true) * 1000)
                ]
            ];

        return ($method != 'HEAD')
            ? $this->respond($response, [], $html)
            : $this->respond(null, [], $html);
    }

    /**
     * Simple implementation of http request
     *
     * @param        $method
     * @param string $url
     * @param array  $body
     * @param array  $headers
     * @return array|bool
     */
    public function httpRequest($method, $url = null, $body = [], $headers = []) {
        // generate url for test when $url argument is not given
        $url = $url ?: route('test.response', strtolower($method)) . '?' . http_build_query(Input::all());

        $parts = parse_url($url);
        list($parts['scheme'], $parts['port']) = $this->buildScheme($parts);

        $request = [
            'header' => $this->buildHeader(strtoupper($method), $parts, $headers),
            'body'   => $this->buildBody($body)
        ];

        $response = null;

        if ($fsp = fsockopen($parts['scheme'] . $parts['host'], $parts['port'], $errno, $errstr, 30)) {
            fwrite($fsp, $request['header']);
            fwrite($fsp, $request['body']);

            while (! feof($fsp)) {
                $response .= fgets($fsp, 1024);
            }

            fclose($fsp);
        } else {
            return false;
        }

        return $this->respond($this->buildResponse($request, $response));

    }

    /**
     * Build HTTP request header string
     *
     * @param string $method
     * @param array  $parts
     * @param array  $headers
     * @return string
     */
    private function buildHeader($method, $parts, $headers) {
        $requestHeaders = strtoupper($method) . " {$parts['path']}?" . http_build_query(Input::all()) . " HTTP/1.1" . PHP_EOL;
        $requestHeaders .= "Host: {$parts['host']}" . PHP_EOL;

        if ($headers && is_array($headers)) {
            foreach ($headers as $k => $v) {
                $requestHeaders .= "{$k}: {$v}" . PHP_EOL;
            }
        }

        //$requestHeaders .= "Content-Length: " . strlen($body) . PHP_EOL;
        $requestHeaders .= "Connection: close\r\n\r\n";

        return $requestHeaders;
    }

    /**
     * Build HTTP request body string
     *
     * @param string|array $body
     * @return string
     */
    private function buildBody($body) {
        return (is_array($body)) ? http_build_query($body) : $body;
    }

    /**
     * Build scheme based on passed $parts (which is an output of php parse_url())
     *
     * @param array $parts
     * @return array
     */
    private function buildScheme($parts) {
        switch ($parts['scheme']) {
            case 'https':
                $scheme = 'ssl://';
                $port = 443;
                break;
            case 'http':
            default:
                $scheme = '';
                $port = 80;
        }

        return [$scheme, $port];
    }

    /**
     * Build an array which contains HTTP request and response data.
     *
     * @param array  $request
     * @param string $response
     * @return array
     */
    private function buildResponse($request, $response) {
        $parts = explode("\r\n\r\n", $response, 2);

        return [
            'request'  => $request,
            'response' => [
                'header' => isset($parts[0]) ? $parts[0] : null,
                'body'   => isset($parts[1]) ? $parts[1] : null
            ]
        ];
    }

    /**
     * @param      $file
     * @param null $key
     * @param null $value
     * @return int
     */
    private function updateSerializedData($file, $key = null, $value = null) {
        $path = public_path() . '/tester/' . $file;
        $data = unserialize(file_get_contents($path));

        if (array_key_exists($key, $data)) $data[$key] = $value;

        var_export($data);

        return file_put_contents($path, serialize($data));
    }

    /**
     * Parse and do comparison job between two files,
     * one from ams-X logcat message and the other from Google Chrome Dev Console export,
     *
     * @param string $filename
     * @param string $direction
     * @return \Illuminate\View\View
     */
    public function amsXTester($filename, $direction = 'forward') {
        // The data file should be an Google Chrome exported .har
        $browser_export_parsed = json_decode(file_get_contents('/vagrant/ams-x/' . $filename . '.har'));

        // The data file shold be an url list, deliminated by pipe string (|)
        $agent_logcat_string = file_get_contents('/vagrant/ams-x/' . $filename);

        // @var $urls_from_agent = ['url1','url2',...];
        $urls_from_agent = explode('|', $agent_logcat_string);

        // @var $urls_from_browser = ['url1','url2',...];
        foreach ($browser_export_parsed->log->entries as $entry) $urls_from_browser[] = trim($entry->request->url);

        if ($direction == 'forward') {
            // $direction = 'forward' means
            // check the presence of given browser $url based on agent the logcat messages
            $a = $urls_from_browser;
            $b = $urls_from_agent;
            $title = 'Browser has this request. Does Ams-x also have it?';
        } else {
            // $direction = 'reverse' means
            // check the presence of given agent $url based on the browser console export
            $b = $urls_from_browser;
            $a = $urls_from_agent;
            $title = 'Ams-x has this request. Does Browser also have it?';
        }

        $matched = array_filter($a, function ($url) use ($b) {
            return in_array($url, $b);
        });

        $unmatched = array_filter($a, function ($url) use ($b) {
            return (! in_array($url, $b));
        });

        return View::make('test.ams-x')->with(compact('filename', 'direction', 'title', 'matched', 'unmatched'));
    }

    protected function transformFlickrImagesUrlToHttpUrl($photos) {
        $return = [];

        foreach ($photos as $photo) {
            $imageUrl = isset($photo['url_l']) ? $photo['url_l'] : null;
            //$imageUrl = isset($photo['url_o']) ? $photo['url_o'] : null;

            //if (! $imageUrl && isset($photo['url_l'])) {
            //    $imageUrl = $photo['url_l'];
            //}

            if (! $imageUrl && isset($photo['url_c'])) {
                $imageUrl = $photo['url_c'];
            }

            if (! $imageUrl && isset($photo['url_z'])) {
                $imageUrl = $photo['url_z'];
            }

            if (! $imageUrl && isset($photo['url_n'])) {
                $imageUrl = $photo['url_n'];
            }

            if (! $imageUrl && isset($photo['url_m'])) {
                $imageUrl = $photo['url_m'];
            }

            if (! $imageUrl && isset($photo['url_q'])) {
                $imageUrl = $photo['url_q'];
            }

            if (! $imageUrl && isset($photo['url_s'])) {
                $imageUrl = $photo['url_x'];
            }

            if (! $imageUrl && isset($photo['url_t'])) {
                $imageUrl = $photo['url_t'];
            }

            if (! $imageUrl && isset($photo['url_sq'])) {
                $imageUrl = $photo['url_sq'];
            }

            $photo['url_l'] = str_replace("https://", "http://", $imageUrl);
            $photo['url_sq'] = str_replace("https://", "http://", $photo['url_sq']);
            $return[] = $photo;
        }
        return $return;
    }

}