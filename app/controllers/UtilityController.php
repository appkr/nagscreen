<?php

class UtilityController extends \ApiController
{

    /**
     * Display a listing of the resource.
     *
     * @param string $package
     * @return Response
     */
    public function redirectToPlayStore($package = 'com.airplug.mao.agent')
    {
        // https://play.google.com/store/apps/developer?id=AirPlug+Inc.
        $play_store_link = (is_android())
            ? 'market://details?id=' . $package
            : 'https://play.google.com/store/apps/details?id=' . $package;

        return Redirect::to($play_store_link);
    }


    public function getLinkToSNS()
    {
        $link  = Input::get('link') ? Input::get('link') : route('home');
        $text  = Input::get('text') ? Input::get('text') : 'NagScreen';
        $media = Input::get('media') ? Input::get('media') : null;
        $res = Share::load($link, $text, $media)->services('delicious', 'digg', 'evernote', 'facebook', 'gmail', 'gplus', 'linkedin', 'pinterest', 'reddit', 'scoopit', 'springpad', 'tumblr', 'twitter', 'viadeo', 'vk');

        return Response::json(['data' => $res], 200);
    }

}