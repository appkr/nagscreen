<?php

use Carbon\Carbon;

class EulaController extends \BaseController
{

    /**
     * @var Illuminate\Database\Eloquent\Model
     */
    private $model;


    /**
     * Constructor
     *
     * @param \Post $model
     */
    public function __construct(Post $model)
    {
        parent::__construct();

        $this->model = $model;
    }

    /**
     * Controller for eula get request
     *
     * @return Response
     */
    public function getView()
    {
        /**
         * Request params from client:
         *
         * language=[ko/en]
         * packagename=[com.airplug.mao.agent]
         *
         * Response from server @HTTP header
         * Last-modified: RFC2822 datatime
         * Set-cookie: eula_version=xx
         * Set-cookie: eula_language=[ko/en]
         *
         * <html>...</html>
         */

        $input = prepare_input_for_eula(Input::all());

        $query = (is_memcached())
            ? $this->model->eula()->remember(60)->cacheTags('posts')
            : $this->model->eula();

        if (! empty($input['package_id'])) $query->wherePackageId($input['package_id']);
        if (! empty($input['language'])) $query->whereLanguage($input['language']);

        if (! $eula = $query->orderBy('id', 'desc')->first()) return App::abort(404);

        if ($eula)
        {
            $epoc = Carbon::createFromTimeStamp(0)->toRFC2822String();

            return Response::view('eula.view', ['eula' => $eula])
                           ->header('Last-modified', Carbon::createFromTimeStamp(strtotime($eula->created_at))->toRFC2822String())
                           ->header('Set-Cookie', "eula_version={$eula->id}; expires={$epoc}; path=/", false)
                           ->header('Set-Cookie', "eula_language={$eula->language}; expires={$epoc}; path=/", false);
        }

        return Response::view('eula.view', ['eula' => null]);
    }

}