<?php

class PackageController extends \BaseController
{

    /**
     * @var Illuminate\Database\Eloquent\Model
     */
    private $model;


    /**
     * @param Package $model
     */
    public function __construct(Package $model)
    {
        parent::__construct();

        $this->beforeFilter('admin');
        $this->beforeFilter('csrf', ['on' => ['post', 'put', 'patch', 'delete']]);

        $this->model = $model;
    }


    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $packages = (is_memcached())
            ? $this->model->with('banner')->remember(60)->cacheTags('packages')->get()
            : $this->model->with('banner')->get();

        return View::make('package.index')->with('packages', $packages);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $input = Input::all();

        // Duplicate check
        if ($package = $this->model->wherePackage($input['package'])->whereName($input['name'])->first()) {
            return Redirect::back()->with('error', 'Already exist !');
        }

        // Validation
        $validation = Validator::make($input, $this->model->rules);

        if ($validation->fails())
        {
            return Redirect::back()->withInput()->withErrors($validation);
        }

        // Resource creation
        if (! $package = $this->model->create($input))
        {
            return Redirect::back()->withInput()->with('error', 'Failed to create a package');
        }

        // Create authentication info for API user
        $apikey = Hash::make($input['package']);

        if (! $user = User::create(['email' => $input['package'], 'password' => $apikey, 'activated' => 1]))
        {
            return Redirect::back()->withInput()->with('error', 'Failed to create a api user with the given package');
        }

        Event::fire('cache.flush', [['posts', 'pushusers', 'vocs', 'packages', 'banners', 'users']]);

        return Redirect::route('package.index')->with('success', 'Created !');
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id = null)
    {
        if (! $package = $this->model->findOrFail($id)->first()) return Redirect::back()->with('error', 'Not Found !');

        // Delete post and related items
        $post_ids = make_id_array_from_eloqent_collection($package->posts());
        $package->posts()->delete();
        Comment::whereIn('post_id', $post_ids)->delete();
        Attachment::whereIn('post_id', $post_ids)->delete();

        // Delete pushuser
        $package->pushUsers()->delete();

        // Delete voc and related items
        $package->vocs()->delete();
        Feedback::whereIn('voc_id', make_id_array_from_eloqent_collection($package->vocs()))->delete();

        // Delete banners
        $package->banner()->delete();

        $package->delete();

        if ($user = User::whereEmail($package->package)->first()) $user->delete();

        Event::fire('cache.flush', [['posts', 'pushusers', 'vocs', 'packages', 'banners', 'users']]);

        return Redirect::back()->with('success', 'Deleted !');
    }

}