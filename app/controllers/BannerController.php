<?php

class BannerController extends \BaseController
{
    /**
     * @var Illuminate\Database\Eloquent\Model
     */
    private $model;


    /**
     * @param \Banner $model
     */
    public function __construct(Banner $model)
    {
        parent::__construct();

        $this->beforeFilter('admin', ['except' => ['view']]);
        $this->beforeFilter('csrf', ['on' => ['post', 'put', 'patch', 'delete']]);

        $this->model = $model;
    }


    /**
     * Display the html view of Banners from Package Model
     *
     * @return Response
     */
    // public function view()
    // {
    //     $banners = $this->fetch();

    //     if ($banners->count() < 1) return App::abort(404);

    //     return View::make('banner.view')->withBanners($banners);
    // }


    /**
     * Show the form for creating a new resource.
     *
     * @param null $package_id
     * @return  View
     */
    public function create($package_id = null)
    {
        $package = Package::with('banner')->find($package_id);

        if (! is_null($package->banner))
        {
            return $this->edit($package->banner->id);
        }

        return View::make('banner.create')->withPackage($package);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $input  = Input::all();

        // Validation
        $validation = Validator::make($input, $this->model->rules);

        if ($validation->fails())
        {
            return Redirect::back()->withInput()->withErrors($validation);
        }

        // File processing and upload
        $images = [
            'launcher_image' => Input::file('launcher_image'),
            'banner_image'   => Input::file('banner_image')
        ];

        if (! $banner = $this->model->create(array_merge($input, $this->processMultipleImages($images))))
        {
            return Redirect::back()->withInput()->with('error', 'Failed to create a banner');
        }

        Event::fire('cache.flush', [['packages', 'banners']]);

        return Redirect::route('package.index')->with('success', 'Created !');
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param $id
     * @return  Veiw
     */
    public function edit($id = null)
    {
        $banner = $this->model->with('package')->findOrFail($id);

        return View::make('banner.edit')->withBanner($banner);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param null $id
     * @return  Response(=Redirect)
     */
    public function update($id = null)
    {
        $input  = abandon_null_input(Input::all());
        $banner = $this->model->with('package')->findOrFail($id);

        // Validation rule override for update call
        unset($this->model->rules['package_id']);

        // Validation
        $validation = Validator::make($input, $this->model->rules);

        if ($validation->fails())
        {
            return Redirect::back()->withInput()->withErrors($validation);
        }

        // Delete existing files
        if (Input::hasFile('launcher_image') && $banner->launcher_image && File::exists($path = public_path().'/banners/'.$banner->launcher_image)) File::delete($path);
        if (Input::hasFile('banner_image') && $banner->banner_image && File::exists($path = public_path().'/banners/'.$banner->banner_image)) File::delete($path);

        // File processing and upload
        $images = [
            'launcher_image' => Input::file('launcher_image'),
            'banner_image'   => Input::file('banner_image')
        ];

        if (! $banner = $banner->update(array_merge($input, $this->processMultipleImages($images))))
        {
            return Redirect::back()->withInput()->with('error', 'Failed to update the banner');
        }

        Event::fire('cache.flush', [['packages', 'banners']]);

        return Redirect::route('package.index')->with('success', 'Updated !');
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param null $id
     * @return  Response(=Redirect)
     */
    public function destroy($id = null)
    {
        $banner = $old = $this->model->find($id);

        if (! $banner) return $this->setStatusCode(404)->respondWithError('Not Found');

        if ($banner->launcher_image) File::delete(public_path().'/banners/'.$banner->launcher_image);
        if ($banner->banner_image) File::delete(public_path().'/banners/'.$banner->banner_image);

        $affectedRow = $banner->delete();

        Event::fire('cache.flush', [['packages', 'banners']]);

        return $this->respond(['data' => (boolean) true]);
    }


    /**
     * @return mixed
     */
    private function fetch()
    {
        $input = prepare_input_for_banner(Input::all());

        $query = (is_memcached())
            ? $this->model->with('package')->remember(60)->cacheTags('banners')
            : $this->model->with('package');

        $banners = $query->orderBy('id', $input['order'])->paginate($input['limit']);

        return $banners;
    }


    /**
     * @param $images
     * @return array
     */
    private function processMultipleImages($images)
    {
        $result = [];

        foreach($images as $key => $image)
        {
            if (! is_null($image))
            {
                if (! Input::hasFile($key))
                {
                    return Redirect::back()->withInput()->with('error', 'Image file not uploaded');
                }

                if (! $data = save_image($image, public_path().'/banners', $key))
                {
                    return Redirect::back()->withInput()->with('error', 'Image file not uploaded. File too big or write permission denied');
                }
                else
                {
                    $result[$key] = $data['name'];
                }
            }
        }

        return $result;
    }

}