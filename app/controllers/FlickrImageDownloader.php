<?php

class FlickrImageDownloader {

    /**
     * Save path for downloaded image
     *
     * @var string
     */
    private $savePath;

    /**
     * Operation counter
     *
     * @var int
     */
    private $counter;

    /**
     * Base http query string for the Flickr API call
     * @var string
     */
    private $query;

    /**
     * Regex pattern to extract filesize from the `wget` system command result
     *
     * @var string
     */
    private $pattern = '/\[(?P<size>\d+)\/(?P<total>\d+)\]/';

    /**
     * FlickrImageDownloader
     *
     * @param FlickrImages $model
     */
    public function __construct(FlickrImages $model) {
        $this->model = $model;
        $this->savePath = storage_path() . '/flickr_images';
        $this->counter = 0;
        $this->query = [
            'format'   => 'php_serial',
            'method'   => 'flickr.groups.pools.getPhotos',
            'group_id' => '83823859@N00',
            'api_key'  => 'd401046f072dae687da49de8cb79cf04',
            'extras'   => 'url_sq,url_l,description',
            'per_page' => 500
        ];
    }

    /**
     * Run the download job
     *
     * @return bool
     */
    public function run() {
        try {
            $start = Input::get('page') ?: 1;
            $this->prepareDirectory();

            foreach(range($start, $start + 3) as $page) {
                $this->report("Processing page {$page}");
                $this->report("-----------------");

                $this->query['page'] = $page;
                $response = unserialize(file_get_contents('https://api.flickr.com/services/rest/?' . http_build_query($this->query)));
                $this->persistModel($response['photos']['photo']);
            }
        } catch (Exception $e) {
            $this->report($e->getMessage());
        }

        return true;
    }

    /**
     * Update image size
     *
     * @return bool
     */
    public function updateImageSize() {
        $photos = $this->model->get();

        foreach($photos as $photo) {
            $imagePath = "{$this->savePath}/{$photo->image}";
            $photo->image_size = File::exists($imagePath)
                ? File::size($imagePath) : 0;
            $photo->save();

            $this->report("[{$this->counter}] {$photo->id}... Done");
            $this->counter ++;
        }

        return true;
    }

    /**
     * Make directory for image save, if not existing
     */
    private function prepareDirectory() {
        if (! File::isDirectory($this->savePath)) {
            File::makeDirectory($this->savePath, 755);
        }
    }

    /**
     * Loop through the $photos array to download images and persist metadata
     *
     * @param array $photos
     * @return bool
     */
    private function persistModel($photos) {
        foreach($photos as $photo) {
            if (! isset($photo['url_l'])) {
                continue;
            }

            $this->downloadImages($photo);

            $thumbnailPath = "{$this->savePath}/{$photo['id']}_s.jpg";
            $imagePath = "{$this->savePath}/{$photo['id']}_l.jpg";

            $model = [
                'flickr_id' => $photo['id'],
                'owner' => $photo['owner'],
                'title' => $photo['title'],
                'description' => $photo['description']['_content'],
                'thumbnail' => isset($photo['url_sq'])
                    ? "{$photo['id']}_s.jpg"
                    : null,
                'image' => "{$photo['id']}_b.jpg",
                'thumbnail_size' => File::exists($thumbnailPath)
                    ? File::size($thumbnailPath)
                    : 0,
                'image_size' => File::exists($imagePath)
                    ? File::size($imagePath)
                    : 0
            ];

            $this->model->create($model);

            $this->report("[{$this->counter}] {$photo['id']}... Done");

            $this->counter ++;
        }

        return true;
    }

    /**
     * Download and save images on the file system
     *
     * @param array $photo
     * @return bool|array of each image filesize
     */
    private function downloadImages($photo) {
        $thumbnailPath = "{$this->savePath}/{$photo['id']}_s.jpg";
        $imagePath = "{$this->savePath}/{$photo['id']}_b.jpg";

        if (isset($photo['url_sq']) && ! File::exists($thumbnailPath)) {
            system("wget {$photo['url_sq']} -O {$thumbnailPath}", $return1);
            //$count1 = preg_match_all($this->pattern, $return1, $matches1);
        }

        if (! File::exists($imagePath)) {
            system("wget {$photo['url_l']} -O {$imagePath}", $return2);
        }

        return true;
    }

    /**
     * Give visual feedback on what is going on.
     *
     * @param string $message
     */
    private function report($message) {
        echo $message . "<br/>" . PHP_EOL;
        flush();
        ob_flush();
    }

}