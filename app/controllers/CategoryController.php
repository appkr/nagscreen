<?php

class CategoryController extends \BaseController
{

    /**
     * @var Illuminate\Database\Eloquent\Model
     */
    private $model;

    /**
     * @param Category $model
     */
    public function __construct(Category $model)
    {
        parent::__construct();

        $this->beforeFilter('admin');

        $this->beforeFilter('csrf', ['on' => ['post', 'put', 'patch', 'delete']]);

        $this->model = $model;
    }


    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $cats = (is_memcached())
            ? $this->model->remember(60)->cacheTags('categories')->get()
            : $this->model->get();

        return View::make('category.index')->with('cats', $cats);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $input = Input::all();

        $category = $this->model->whereName($input['name'])->first();

        if ($category) return Redirect::back()->with('error', 'Already exist !');

        $validation = Validator::make($input, $this->model->rules);

        if ($validation->fails()) return Redirect::back()->withInput()->withErrors($validation);

        $this->model->create($input);

        Event::fire('cache.flush', ['categories']);

        return Redirect::route('category.index')->with('success', 'Created !');
    }

}