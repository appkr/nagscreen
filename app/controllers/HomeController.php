<?php

class HomeController extends BaseController
{

    public function getDashboard()
    {
        $count = [];

        if (is_memcached())
        {
            $count = [
                'post' => Post::remember(60)->cacheTags('posts')->published()->count(),
                'voc' => Voc::remember(60)->cacheTags('vocs')->count(),
                'feedback' => Feedback::remember(60)->cacheTags('feedbacks')->count(),
                'pushuser' => PushUser::remember(60)->cacheTags('pushusers')->whereNotNull('registration_id')->count()
            ];
        }
        else {
            $count = [
                'post' => Post::published()->count(),
                'voc' => Voc::count(),
                'feedback' => Feedback::count(),
                'pushuser' => PushUser::whereNotNull('registration_id')->count()
            ];
        }

        $query = (is_memcached())
            ? Voc::with('package', 'feedbacks')->remember(60)->cacheTags('vocs')
            : Voc::with('package', 'feedbacks');

        $vocs = $query->orderBy('id', 'desc')->take(3)->get();

        return View::make('home.dashboard')->with(compact('count', 'vocs'));
    }

    public function speedtest() {
        return View::make('home.speedtest');
    }

    /**
     * @return mixed
     */
    public function getAbc()
    {
        $data = [
            'likeUrl'       => 'https://apps.facebook.com/abc-agent/',
            'appId'         => '274021236082112',
            'secret'        => '6659f88a1af2eb94ecce913be3eb89ad',
            'playStoreLink' => is_android()
                    ? 'market://details?id=com.airplug.abc.agent'
                    : 'https://play.google.com/store/apps/details?id=com.airplug.abc.agent',
            'title'         => 'ABC-KT',
            'author'        => 'juwonkim@me.com',
            'description'   => '에어플러그의 솔루션(ABC와 AirSocket)은 스마트폰으로 동영상볼 때나 음악들을 때 1) 요금 폭탄의 불안감과 2) 서비스 멈춤/튕김의 불편함 뿐만아니라 3) Lte/Wi-Fi 선택의 피로감을 덜어 주는 솔루션입니다.'
        ];

        Tracker::trackEvent('minipage.abc', 'view');

        return View::make('home.abc')->with('data', $data);
    }


    /**
     * @return mixed
     */
    public function getAirsocket()
    {
        $data = [
            'likeUrl'       => 'https://apps.facebook.com/mao-agent/',
            'appId'         => '652756968080950',
            'secret'        => '4c82d5bc6652dbd2a32735d8b4889817',
            'playStoreLink' => is_android()
                    ? 'market://details?id=com.airplug.mao.agent'
                    : 'https://play.google.com/store/apps/details?id=com.airplug.mao.agent',
            'title'         => 'AirSocket',
            'author'        => 'juwonkim@me.com',
            'description'   => '에어플러그의 솔루션(ABC와 AirSocket)은 스마트폰으로 동영상볼 때나 음악들을 때 1) 요금 폭탄의 불안감과 2) 서비스 멈춤/튕김의 불편함 뿐만아니라 3) Lte/Wi-Fi 선택의 피로감을 덜어 주는 솔루션입니다.'
        ];

        Tracker::trackEvent('minipage.airsocket', 'view');

        if (! is_korean_browser())
        {
            $data = array_merge($data, ['description' => 'English description here']);
            return View::make('home.airsocket-en')->withData($data);
        }

        return View::make('home.airsocket')->withData($data);
    }


    /**
     * @return mixed
     */
    public function getAirsocketForHoppin()
    {
        $data = [
            'likeUrl'       => 'https://apps.facebook.com/airsocket-for-hoppin/',
            'appId'         => '1434674836805071',
            'secret'        => '4de41aae1d1aa45eaf7fe0d61003bafa',
            'playStoreLink' => is_android()
                    ? 'market://details?id=com.airplug.mao.agent'
                    : 'https://play.google.com/store/apps/details?id=com.airplug.mao.agent',
            'title'         => 'AirSocket For Hoppin',
            'author'        => 'juwonkim@me.com',
            'description'   => 'AirSocket 기능을 사용하시면, 지하철, 카페, 언제 어디서나 끊김 없이 동영상을 즐기실 수 있습니다. 대용량 컨텐츠를 빠르게 다운로드 받으실 수 있습니다.'
        ];

        Tracker::trackEvent('minipage.hoppin', 'view');

        return View::make('home.airsocketforhoppin')->with('data', $data);
    }


    /**
     * @return mixed
     */
    public function getVideoplug()
    {
        $data = [
            'likeUrl'       => 'https://apps.facebook.com/videoplug/',
            'appId'         => '1401435326742424',
            'secret'        => 'bb995d82cae3774fbeeb325d7da79f33',
            'playStoreLink' => is_android()
                    ? 'market://details?id=com.airplug.videoplug'
                    : 'https://play.google.com/store/apps/details?id=com.airplug.videoplug',
            'title'         => 'VideoPlug',
            'author'        => 'juwonkim@me.com',
            'description'   => '무선 네트워크 환경에서 미디어 전송 솔루션을 개발하는 (주)에어플러그에서 개발한 비디오 신디케이션/큐레이션 앱입니다. YouTube, Vimeo, PodCast 등 다양한 비디오서비스를 하나의 앱에서 볼 수 있게 해 주죠.'
        ];

        Tracker::trackEvent('minipage.videoplug', 'view');

        if (! is_korean_browser())
        {
            $data = array_merge($data, ['description' => 'English description here']);
            return View::make('home.videoplug-en')->withData($data);
        }

        return View::make('home.videoplug')->withData($data);
    }

}
