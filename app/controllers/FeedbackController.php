<?php

class FeedbackController extends \BaseController
{

    /**
     * @var Illuminate\Database\Eloquent\Model
     */
    private $model;

    public function __construct(Feedback $model)
    {
        parent::__construct();

        $this->beforeFilter('auth');
        $this->beforeFilter('csrf', ['on' => ['post', 'put', 'patch', 'delete']]);

        $this->model = $model;
    }


    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $input = Input::all();
        $input = append_user_id($input);

        $validation = Validator::make($input, $this->model->rules);

        if ($validation->passes())
        {
            $feedback = $this->model->create($input);

            Event::fire('history.track', [$feedback]);

            if ($feedback && isset($input['email']))
            {
                $voc = Voc::with('package')->findOrFail($input['voc_id']);
                Event::fire('feedback', [$voc, $feedback]);
            }

            return Redirect::back()->with('success', 'Created !');
        }

        return Redirect::back()->withInput()->withErrors($validation);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id = null)
    {
        //
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id = null)
    {
        // Event::fire('history.track', [$feedback]);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id = null)
    {
        // Event::fire('history.track', [$feedback]);
    }

}