<?php

use airplug\Transformers\VocTransformer;

class VocApiController extends \ApiController
{

    /**
     * @var airplug\Transformers\VocTransformer
     */
    private $vocTransformer;


    /**
     * @var Illuminate\Database\Eloquent\Model
     */
    private $model;


    /**
     * Constructor
     *
     * @param VocTransformer $vocTransformer
     */
    public function __construct(Voc $model, VocTransformer $vocTransformer)
    {
        parent::__construct();

        $this->beforeFilter('force.ssl', ['on' => ['post', 'put', 'patch', 'delete']]);
        $this->beforeFilter('apicsrf', ['on' => ['post', 'put', 'patch', 'delete']]);

        $this->model = $model;

        $this->vocTransformer = $vocTransformer;
    }


    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $input = prepare_input_for_voc_api_call(Input::all());

        $query = (is_memcached())
            ? $this->model->with('package')->remember(60)->cacheTags('vocs')
            : $this->model->with('package');

        if (isset($input['package_id'])) $query->wherePackageId($input['package_id']);

        $vocs = $query->orderBy('id', $input['order'])->paginate($input['limit']);

        if ($vocs->count() < 1) return $this->respondNotFound();

        $response = $this->respondWithPagination($vocs, ['data' => $this->vocTransformer->transformCollection($vocs->toArray()['data'])]);

        return (isset($input['callback'])) ? $response->setCallback($input['callback']) : $response;
    }


    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $input = Input::all();

        if (isset($input['package']))
        {
            if (! $package = Package::wherePackage($input['package'])->first()) return $this->respondNotFound();
            $input = exchange_array_item($input, ['package_id' => $package->id], 'package');
        }

        $validation = Validator::make($input, $this->model->rules);

        if ($validation->fails()) return $this->respondUnprocessableError($validation->errors()->toArray());

        if (! $voc = $this->model->create($input)) return $this->respondInternalError();

        Event::fire('history.track', [$voc]);
        Event::fire('cache.flush', ['vocs']);

        return $this->show($voc->id, 201);
    }


    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id = null, $statusCode = 200)
    {
        if (! $voc = $this->model->with('package')->find($id)) return $this->respondNotFound();

        $response = $this->setStatusCode($statusCode)->respond([
            'data' => $this->vocTransformer->transform($voc->toArray())
        ]);

        return (isset($input['callback'])) ? $response->setCallback($input['callback']) : $response;
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id = null)
    {
        $input = Input::all();

        // if (isset($input['package']))
        // {
        //     if (! $package = Package::wherePackage($input['package'])->first()) return $this->respondNotFound();
        //     $input = exchange_array_item($input, ['package_id' => $package->id], 'package');
        // }

        if (! $voc = $this->model->find($id)) return $this->respondNotFound();

        if (! $voc->update($input)) return $this->respondInternalError();

        Event::fire('history.track', [$voc]);
        Event::fire('cache.flush', ['vocs']);

        return $this->show($id);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id = null)
    {
        if (! $voc = $this->model->find($id)) return $this->respondNotFound();

        $voc->feedbacks()->delete();
        
        if (! $voc->delete()) return $this->respondInternalError();

        Event::fire('history.track', [$voc]);
        Event::fire('cache.flush', ['vocs']);

        return $this->respond(['data' => (boolean) true]);
    }

}