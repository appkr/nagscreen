<?php

use airplug\Transformers\GooglePlayReviewTransformer;
use airplug\Transformers\GooglePlayStatTransformer;
use airplug\Transformers\NewGooglePlayReviewTransformer;
use airplug\Transformers\NewGooglePlayStatTransformer;
use Carbon\Carbon;

class GooglePlaySummaryController extends \BaseController {

    private $stats;

    private $apps;

    private $app_reviews;

    private $fetcher;

    private $reviews;

    private $statTransformer;

    private $reviewTransformer;

    private $newStatTransformer;

    private $newReviewTransformer;

    /**
     * @param GooglePlayStat                      $stats
     * @param AirSocketPoweredApp                 $apps
     * @param AirSocketPoweredAppReview           $app_reviews
     * @param FetchGooglePlayStatisticsController $fetcher
     * @param GooglePlayReview                    $reviews
     * @param GooglePlayStatTransformer           $statTransformer
     * @param GooglePlayReviewTransformer         $reviewTransformer
     * @param NewGooglePlayStatTransformer        $newStatTransformer
     * @param NewGooglePlayReviewTransformer      $newReviewTransformer
     */
    public function __construct(
        GooglePlayStat $stats,
        AirSocketPoweredApp $apps,
        AirSocketPoweredAppReview $app_reviews,
        FetchGooglePlayStatisticsController $fetcher,
        GooglePlayReview $reviews,
        GooglePlayStatTransformer $statTransformer,
        GooglePlayReviewTransformer $reviewTransformer,
        NewGooglePlayStatTransformer $newStatTransformer,
        NewGooglePlayReviewTransformer $newReviewTransformer
    ) {
        parent::__construct();

        $this->beforeFilter('admin');
        $this->beforeFilter('csrf', ['on' => ['post', 'put', 'patch', 'delete']]);

        $this->stats = $stats;
        $this->apps = $apps;
        $this->app_reviews = $app_reviews;
        $this->fetcher = $fetcher;
        $this->reviews = $reviews;
        $this->statTransformer = $statTransformer;
        $this->reviewTransformer = $reviewTransformer;
        $this->newStatTransformer = $newStatTransformer;
        $this->newReviewTransformer = $newReviewTransformer;
    }

    /**
     * @return mixed
     */
    public function index() {
        return View::make('googleplay.index')
                   ->withHalf($this->stats->with('package')->whereNull('daily_user_uninstalls')->orderBy('package_id', 'asc')->orderBy('date', 'desc')->get())
                   ->withApps($this->apps->lists('name', 'id'))
                   ->withMaxDate($this->stats->max('date'));
    }

    /**
     * Fetch Google Play Statistics and save to the database
     *
     * @return mixed
     */
    public function fetch() {
        $packages = get_active_google_play_packages();

        foreach ($packages as $package) {
            $this->fetcher->fetchStats($package);
            $this->fetcher->fetchReviews($package);
        }

        return Redirect::route('googleplay.index')->with('success', 'Fetching done !');
    }

    /**
     * Create new AirSocketPoweredApp item
     *
     * @return mixed
     */
    public function storeApp() {
        $rules = [
            'package' => 'required|unique:apps|regex:/^[A-z0-9]+\.[A-z0-9]+\.[A-z0-9].+/',
            'name'    => 'required|unique:apps|min:2'
        ];

        $validation = Validator::make(Input::all(), $rules);

        if ($validation->fails()) return Redirect::back()->withInput()->withErrors($validation);

        if (! $this->apps->create(Input::all())) return Redirect::back()->withInput()->with('error', 'Failed to create');

        return Redirect::route('googleplay.index')->with('success', 'Created !');
    }

    /**
     * @return mixed
     */
    public function notify() {
        return View::make('googleplay.notify')
                   ->withSummary($this->extractSummary());
    }

    /**
     * @return mixed
     */
    public function newNotify() {
        return View::make('googleplay.new-notify')
                   ->withSummary($this->newExtractSummary());
    }

    /**
     * Send Google Play Daily Summary email
     *
     * @return mixed
     */
    public function send() {
        $to = (Input::get('to')) ? preg_split('/, ?/', Input::get('to')) : [];
        $body = (Input::get('body')) ?: null;

        Event::fire('googleplay.update', [$this->extractSummary(), $to, $body]);

        return Redirect::route('googleplay.index')->with('success', 'Email has been sent !');
    }

    /**
     * Send Google Play Daily Summary email
     *
     * @return mixed
     */
    public function newSend() {
        $to = (Input::get('to')) ? preg_split('/, ?/', Input::get('to')) : [];
        $body = (Input::get('body')) ?: null;

        Event::fire('googleplay.new.update', [$this->newExtractSummary(), $to, $body]);

        return Redirect::route('googleplay.index')->with('success', 'Email has been sent !');
    }

    /**
     * As AppAnnie public api does not provide daily uninstall head count,
     * administrator has to update corresponding value manually at Google Play Summary console at NagScreen.
     * This class do the job of update missing daily_user_uninstalls
     *
     * @param null $id
     * @return mixed
     */
    public function update($id = null) {
        $validation = Validator::make(Input::all(), ['daily_user_uninstalls' => 'required|integer']);

        if ($validation->fails()) return Redirect::back()->withInput()->withErrors($validation);

        if (! $this->stats->findOrFail($id)->update(Input::all())) {
            return Redirect::back()->withInput()->with('error', 'Failed to update');
        }

        Event::fire('cache.flush', ['googleplaystats']);
        Cache::forget('googleplaysummary');

        return Redirect::route('googleplay.index')->with('success', 'Updated !');
    }

    /**
     * Create AirSocketPoweredAppReview entry
     *
     * @return mixed
     */
    public function storeReview() {
        $validation = Validator::make(Input::all(), ['app_id' => 'required|integer', 'date' => 'required|date', 'rating' => 'integer|between:1,5']);

        if ($validation->fails()) return Redirect::back()->withInput()->withErrors($validation);

        if (! $app_review = $this->app_reviews->create(Input::all())) {
            return Redirect::back()->withInput()->with('error', 'Failed to save a review');
        }

        Event::fire('cache.flush', ['app_reviews']);

        return Redirect::route('googleplay.index')->with('success', 'Saved !');
    }

    /**
     * Respond with the most recent AirSocketPoweredAppReview entry
     * to an $.ajax request from Google Play Summary console
     * to prevent administrators from putting same entry again
     *
     * @param $app_id
     * @return $this|array
     */
    public function getMostRecentReview($app_id) {
        if (! $app_review = $this->app_reviews->with('app')->whereAppId($app_id)->orderBy('date', 'desc')->first()) {
            return $this->setStatusCode(404)->respondWithError('Not Found');
        }

        return ['data' => $app_review->toArray()];
    }

    /**
     * Delete corresponding AirSocketPoweredAppReview entry
     *
     * @param null $id
     * @return mixed
     */
    public function destroyReview($id = null) {
        if (! $app_review = $this->app_reviews->findOrFail($id)->first()) {
            return $this->setStatusCode(404)->respondWithError('Not Found');
        }

        $app_review->delete();

        Event::fire('cache.flush', ['app_reviews']);

        return $this->respond(['data' => (boolean) true]);
    }

    /**
     * extract data for Google Play Daily Summary Update
     *
     * @return array
     */
    public function extractSummary() {
        return Cache::remember('googleplaysummary', 60, function () {
            $packages = get_active_google_play_packages(true);
            $data = [];

            foreach ($packages as $package => $package_id) {
                $max_date = $this->stats->wherePackageId($package_id)->max('date');
                $second_max_date = (new DateTime($max_date))->sub(new DateInterval('P1D'))->format('Y-m-d');

                $data['data'][$package] = [
                    'total_user_installs'   => (int) $this->stats->wherePackageId($package_id)->sum('daily_user_installs'),
                    'total_user_uninstalls' => (int) $this->stats->wherePackageId($package_id)->sum('daily_user_uninstalls'),
                    'total_reviews'         => (int) $this->reviews->wherePackageId($package_id)->count(),
                    'average_rating'        => (float) $this->reviews->wherePackageId($package_id)->avg('rating'),
                    'today'                 => array_merge(
                        $this->statTransformer->transform($this->stats->with('package')->wherePackageId($package_id)->where('date', $max_date)->first()->toArray()),
                        ['reviews' => $this->reviewTransformer->transformCollection($this->reviews->with('package')->wherePackageId($package_id)->where('date', $max_date)->get()->toArray())]
                    ),
                    'yesterday'             => array_merge(
                        $this->statTransformer->transform($this->stats->with('package')->wherePackageId($package_id)->where('date', $second_max_date)->first()->toArray()),
                        ['reviews' => $this->reviewTransformer->transformCollection($this->reviews->with('package')->wherePackageId($package_id)->where('date', $second_max_date)->get()->toArray())]
                    )
                ];
            }

            $apps = AirSocketPoweredApp::all();

            foreach ($apps as $app) {
                $app_reviews = AirSocketPoweredAppReview::whereAppId($app->id)->where('created_at', '>=', Carbon::today()->toDateString())->get();

                if ($app_reviews->count()) {
                    $reviews = [];

                    foreach ($app_reviews as $review) {
                        $reviews[] = [
                            'date'     => $review->date,
                            'reviewer' => $review->reviewer,
                            'rating'   => (int) $review->rating,
                            'title'    => $review->title,
                            'content'  => $review->content
                        ];
                    }

                    $data['related_reviews'][$app['name']] = $reviews;
                }
            }

            return array_merge(['date' => $max_date], $data);
        });
    }

    /**
     * @return mixed
     */
    public function newExtractSummary() {
        return Cache::remember('new-googleplay-summary', 60, function () {
            $packages = get_active_google_play_packages(true);
            $data = [];

            foreach ($packages as $package => $package_id) {
                $today = $this->stats->wherePackageId($package_id)->max('date');

                $todayStat = [
                    $this->stats->wherePackageId($package_id)->where('date', '=', $today)->get()->toArray(),
                    $this->stats->wherePackageId($package_id)->where('date', '=', $this->getDate($today, 'P1D'))->get()->toArray()
                ];

                $todayReview = $this->reviews->wherePackageId($package_id)->where('date', $today)->get()->toArray();

                $last7daysStat = [
                    $this->stats->wherePackageId($package_id)
                        ->where('date', '>=', $this->getDate($today, 'P7D'))->get()->toArray(),
                    $this->stats->wherePackageId($package_id)
                        ->where('date', '<=', $this->getDate($today, 'P8D'))->where('date', '>=', $this->getDate($today, 'P14D'))
                        ->get()->toArray()
                ];

                $last30daysStat = [
                    $this->stats->wherePackageId($package_id)
                        ->where('date', '>=', $this->getDate($today, 'P30D'))
                        ->get()->toArray(),
                    $this->stats->wherePackageId($package_id)
                        ->where('date', '<=', $this->getDate($today, 'P31D'))->where('date', '>=', $this->getDate($today, 'P60D'))
                        ->get()->toArray()
                ];

                $data['data'][$package] = [
                    'package_name'          => Package::wherePackage($package)->pluck('name'),
                    'total_user_installs'   => (int) $this->stats->wherePackageId($package_id)->sum('daily_user_installs'),
                    'total_user_uninstalls' => (int) $this->stats->wherePackageId($package_id)->sum('daily_user_uninstalls'),
                    'total_reviews'         => (int) $this->reviews->wherePackageId($package_id)->count(),
                    'average_rating'        => (float) $this->reviews->wherePackageId($package_id)->avg('rating'),
                    'rank_kr'               => (int) $todayStat[0][0]['rank_kr'],
                    'today'                 => array_merge(
                        $this->newStatTransformer->calculateAndTransformCollection($todayStat),
                        ['reviews' => $this->newReviewTransformer->transformCollection($todayReview)]
                    ),
                    'last_7days'            => $this->newStatTransformer->calculateAndTransformCollection($last7daysStat),
                    'last_30days'           => $this->newStatTransformer->calculateAndTransformCollection($last30daysStat)
                ];
            }

            $apps = AirSocketPoweredApp::all();

            foreach ($apps as $app) {
                $app_reviews = AirSocketPoweredAppReview::whereAppId($app->id)->where('created_at', '>=', Carbon::today()->toDateString())->get();

                if ($app_reviews->count()) {
                    $reviews = [];

                    foreach ($app_reviews as $review) {
                        $reviews[] = [
                            'date'     => $review->date,
                            'reviewer' => $review->reviewer,
                            'rating'   => (int) $review->rating,
                            'title'    => $review->title,
                            'content'  => $review->content
                        ];
                    }

                    $data['related_reviews'][$app['name']] = $reviews;
                }
            }

            return array_merge(['date' => $today], $data);
        });
    }

    /**
     * @param $base
     * @param $diff
     * @return string
     */
    private function getDate($base, $diff) {
        return (new DateTime($base))->sub(new DateInterval($diff))->format('Y-m-d');
    }

}