<?php

class PushMessageController extends \BaseController
{

    /**
     * @var Illuminate\Database\Eloquent\Model
     */
    private $model;

    /**
     * @param \PushMessage $model
     */
    public function __construct(PushMessage $model)
    {
        $this->beforeFilter('csrf', ['on' => ['post', 'put', 'patch', 'delete']]);
        $this->beforeFilter('auth', ['except' => ['countReceivers']]);

        $this->model = $model;

        parent::__construct();
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $input = Input::all();

        // Sort implementation
        $fieldsList  = ['id', 'title', 'created_at', 'updated_at'];
        $orderMethod = ['asc', 'desc', 'ASC', 'DESC'];

        $order = (! empty($input['order']) && in_array($input['order'], $orderMethod)) ? $input['order'] : 'desc';
        $sort  = (! empty($input['sort']) && in_array($input['sort'], $fieldsList)) ? $input['sort'] : 'id';

        // Search and Filter implementation
        $filter   = (! empty($input['filter'])) ? $input['filter'] : '';
        $question = (! empty($input['question'])) ? $input['question'] : '';

        Input::flash();

        $query = (is_memcached())
            ? $this->model->with('user', 'package')->remember(60)->cacheTags('push_messages')
            : $this->model->with('user', 'package');

        if (! empty($filter) && ! empty($question)) $query->filter($filter, $question);

        $push_messages = $query->orderBy($sort, $order)->paginate(20);

        return View::make('push.index')->with('push_messages', $push_messages);
    }


    /**
     * Display the specified resource.
     *
     * @param null $id
     * @return  View
     */
    public function show($id = null)
    {
        $push_message = $this->model->with('user', 'package')->findOrFail($id);

        return View::make('push.show')->with('push_message', $push_message);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return  View
     */
    public function create()
    {
        return View::make('push.create');
    }

    /**
     * Send push message
     *
     * @return Response
     */
    public function send()
    {
        $input = Input::all();
        $input = append_user_id($input);

        $validation = Validator::make($input, $this->model->rules);

        if ($validation->fails()) return Redirect::route('push.index') ->withErrors($validation)->withInput();

        // SEND
        // Queue::push('airplug\Push\GcmAPI@send', ['input'=>$input, 'retry_receivers'=>[]]);
        $success_count = Gcm::send($input);
        $success_message = "Push message successfully sent to {$success_count} users.";

        // RECORD WHAT HAS BEEN SENT AND ITS RESULT
        $input['success'] = $success_count;
        $push_message = $this->model->create($input);

        $push_message->content .= " {$success_message}";
        Event::fire('history.track', [$push_message]);

        Event::fire('cache.flush', ['push_messages']);

        return Redirect::route('push.index')->with('success', $success_message);
    }


    /**
     * get the head count of push message receiver
     */
    public function countReceivers($package_id = null)
    {
        $query = (is_memcached())
            ? PushUser::remember(60)->cacheTags('pushusers')->whereNotNull('registration_id')
            : PushUser::whereNotNull('registration_id');

        return $this->respond(['data' => (int) $query->wherePackageId($package_id)->count()]);
    }

}