<?php

class CommentController extends \BaseController
{

    /**
     * @var Illuminate\Database\Eloquent\Model
     */
    private $model;


    /**
     * @param Comment $model
     */
    public function __construct(Comment $model)
    {
        parent::__construct();

        $this->beforeFilter('csrf', ['on' => 'post']);
        $this->beforeFilter('auth');

        $this->model = $model;
    }


    /**
     * Controller for comment/store post request
     *
     * @return object Response
     */
    public function store()
    {
        $input = Input::all();
        $input = append_user_id($input);

        $validation = Validator::make($input, $this->model->rules);

        if ($validation->fails()) return Redirect::route('post.show', [$input['post_id']])->withErrors($validation)->withInput();

        if (! $comment = $this->model->create($input))
            return Redirect::route('post.show', [$input['post_id']])->with('error', 'Something went wrong! Please try again.')->withInput();

        Event::fire('history.track', [$comment]);

        return Redirect::route('post.show', [$input['post_id']])->with('success', 'Your comment was saved and published!');
    }

}