<?php

use airplug\Transformers\GooglePlayStatTransformer;
use airplug\Transformers\GooglePlayReviewTransformer;
use Carbon\Carbon;

class GooglePlayApiController extends \ApiController {

    /**
     * @var GooglePlayStat
     */
    private $stats;

    /**
     * @var GooglePlayReview
     */
    private $reviews;

    /**
     * @var airplug\Transformers\GooglePlayStatTransformer
     */
    private $statTransformer;

    /**
     * @var airplug\Transformers\GooglePlayStatTransformer
     */
    private $reviewTransformer;

    /**
     * @var FetchGooglePlayStatisticsController
     */
    private $fetcher;

    /**
     * Constructor
     *
     * @param GooglePlayStat              $stats
     * @param GooglePlayReview            $reviews
     * @param GooglePlayStatTransformer   $statTransformer
     * @param GooglePlayReviewTransformer $reviewTransformer
     * @param GooglePlaySummaryController $fetcher
     */
    public function __construct(
        GooglePlayStat $stats,
        GooglePlayReview $reviews,
        GooglePlayStatTransformer $statTransformer,
        GooglePlayReviewTransformer $reviewTransformer,
        GooglePlaySummaryController $fetcher
    ) {
        parent::__construct();

        $this->beforeFilter('force.ssl', ['on' => ['post', 'put', 'patch', 'delete']]);
        $this->beforeFilter('apicsrf', ['on' => ['post', 'put', 'patch', 'delete']]);

        $this->stats = $stats;
        $this->reviews = $reviews;
        $this->statTransformer = $statTransformer;
        $this->reviewTransformer = $reviewTransformer;
        $this->fetcher = $fetcher;
    }

    /**
     * Respond Google Play Summary data
     * (with today, yesterday Google Play stats, and related_apps_reviews)
     *
     * @return array
     */
    public function summary() {
        return $this->fetcher->extractSummary();
    }

    /**
     * Respond Google Play Summary data
     * (with today, yesterday Google Play stats, and related_apps_reviews)
     *
     * @return array
     */
    public function newSummary() {
        return $this->fetcher->newExtractSummary();
    }

    /**
     * Respond single entry of GooglePlayStat
     *
     * @param null $id
     * @return \Illuminate\Http\Response
     */
    public function showStat($id = null) {
        if (! $stat = $this->stats->with('package')->find($id)) return $this->respondNotFound();

        $response = $this->respond([
            'data' => $this->statTransformer->transform($stat->toArray())
        ]);

        return (Input::get('callback')) ? $response->setCallback(Input::get('callback')) : $response;
    }

    /**
     * Respond single entry of GooglePlayReview
     *
     * @param null $id
     * @return \Illuminate\Http\Response
     */
    public function showReview($id = null) {
        if (! $review = $this->reviews->with('package')->find($id)) return $this->respondNotFound();

        $response = $this->respond([
            'data' => $this->reviewTransformer->transform($review->toArray())
        ]);

        return (Input::get('callback')) ? $response->setCallback(Input::get('callback')) : $response;
    }

    /**
     * Respond list of GooglePlayStat
     *
     * @return $this|\Illuminate\Http\Response
     */
    public function indexStat() {
        $stats = $this->fetchStat();

        if ($stats->count() < 1) return $this->respondNotFound();

        $response = $this->respondWithPagination($stats, ['data' => $this->statTransformer->transformCollection($stats->toArray()['data'])]);

        return (isset($input['callback'])) ? $response->setCallback($input['callback']) : $response;
    }

    /**
     * Respond list of GooglePlayReview
     *
     * @return $this|\Illuminate\Http\Response
     */
    public function indexReview() {
        $reviews = $this->fetchReview();

        if ($reviews->count() < 1) return $this->respondNotFound();

        $response = $this->respondWithPagination($reviews, ['data' => $this->reviewTransformer->transformCollection($reviews->toArray()['data'])]);

        return (isset($input['callback'])) ? $response->setCallback($input['callback']) : $response;
    }

    /**
     * @return mixed
     */
    private function fetchStat() {
        $input = prepare_input_for_google_play_api(Input::all());

        $query = (is_memcached())
            ? $this->stats->with('package')->remember(60)->cacheTags('googleplaystats')
            : $this->stats->with('package');

        if (isset($input['package_id'])) $query->wherePackageId($input['package_id']);
        if (isset($input['start_date'])) $query->where('date', '>=', $input['start_date']);

        return $query->orderBy('date', $input['order'])->paginate($input['limit']);
    }

    /**
     * @return mixed
     */
    private function fetchReview() {
        $input = prepare_input_for_google_play_api(Input::all());

        $query = (is_memcached())
            ? $this->reviews->with('package')->remember(60)->cacheTags('googleplayreviews')
            : $this->reviews->with('package');

        if (isset($input['package_id'])) $query->wherePackageId($input['package_id']);
        if (isset($input['start_date'])) $query->where('date', '>=', $input['start_date']);

        return $query->orderBy('date', $input['order'])->paginate($input['limit']);
    }

}