<?php

use Faker\Factory as Faker;

class TestLogsTableSeeder extends Seeder
{

    public function run()
    {
        TestLogs::truncate();
        Eloquent::unguard();

        $faker = Faker::create();

        $user_ids = User::lists('id');

        foreach (range(1, 100) as $index)
        {
            $sleep    = $faker->randomElement([1000, 2000, 3000, 0]);
            $operator = $faker->randomElement(['SKTelecom', 'olleh', 'LG U+']);
            $os       = $faker->randomElement([10, 15, 16, 17, 18, 19]);

            if ($operator == "SKTelecom")
            {
                $ssid         = $faker->randomElement(['T wifi zone', 'T wifi zone_secure', $faker->word]);
                $proxySetting = ($ssid == 'T wifi zone_secure' && $os == 19) ? 0 : 1;
                $securityType = ($ssid == 'T wifi zone_secure' || $ssid == 'T wifi zone')
                    ? 'EAP'
                    : $faker->randomElement(['WEP', 'CHAP', 'TKIP', 'AES', 'WPA', 'OPEN']);
            }
            else
            {
                if ($operator == "olleh")
                {
                    $ssid         = $faker->randomElement(['ollehWiFi', $faker->word]);
                    $proxySetting = ($ssid == 'ollehWiFi' && $os == 19) ? 0 : 1;
                    $securityType = ($ssid == 'ollehWiFi')
                        ? $faker->randomElement(['EAP', 'OPEN'])
                        : $faker->randomElement(['WEP', 'CHAP', 'TKIP', 'AES', 'WPA', 'OPEN']);
                }
                else
                {
                    $ssid         = $faker->randomElement(['U+zone', $faker->word]);
                    $proxySetting = ($ssid == 'U+zone' && $os == 19) ? 0 : 1;
                    $securityType = ($ssid == 'U+zone')
                        ? 'CHAP'
                        : $faker->randomElement(['WEP', 'CHAP', 'TKIP', 'AES', 'WPA', 'Open']);
                }
            }

            $log = [
                'timestamp'     => $faker->unixTime($max = 'now'),
                'method'        => $faker->randomElement(['http-get', 'http-post', 'https-get', 'https-post']),
                'manufacturer'  => $faker->randomElement(['Samsung', 'LG', 'Pantech']),
                'model'         => $faker->word . $index,
                'operator'      => $operator,
                'os'            => $os,
                'ssid'          => $ssid,
                'bssid'         => $faker->macAddress,
                'securityType'  => $securityType,
                'rtt'           => $sleep + rand(250, 2000),
                'sleep'         => $sleep,
                'message'       => null,
                'proxySetting'  => $proxySetting,
                'proxyPresent'  => 1,
                'communication' => 1
            ];

            TestLogs::create([
                'module'  => $faker->randomElement(['testModule1', 'testModule2']),
                'log'     => json_encode($log),
                'user_id' => null,
                // 'user_id' => $faker->randomElement($user_ids),
                'ip'      => $faker->ipv4
            ]);
        }

    }

}