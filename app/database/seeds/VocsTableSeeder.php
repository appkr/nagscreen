<?php

use Faker\Factory as Faker;

class VocsTableSeeder extends Seeder
{

    public function run()
    {
        if (! App::environment('testing')) DB::statement('SET FOREIGN_KEY_CHECKS=0');
        Voc::truncate();
        Eloquent::unguard();

        $faker = Faker::create();
        $package_ids = Package::lists('id');

        foreach (range(1, 10) as $index) {
            Voc::create([
                'package_id' => $faker->randomElement($package_ids),
                'title'      => 'test-' . $faker->sentence(4),
                'content'    => 'test-' . $faker->paragraph(2),
                'email'      => $faker->safeEmail
            ]);
        }

        if (! App::environment('testing')) DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }

}