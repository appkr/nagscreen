<?php

use Faker\Factory as Faker;

class PushMessagesTableSeeder extends Seeder
{

    public function run()
    {
        if (! App::environment('testing')) DB::statement('SET FOREIGN_KEY_CHECKS=0');
        PushMessage::truncate();
        Eloquent::unguard();

        $faker = Faker::create();
        $package_ids = Package::lists('id');
        $user_ids    = User::lists('id');

        foreach (range(1, 10) as $index) {
            PushMessage::create([
                'package_id' => $faker->randomElement($package_ids),
                'user_id'    => $faker->randomElement($user_ids),
                'ticker'     => 'test-' . $faker->sentence(2),
                'title'      => 'test-' . $faker->sentence(4),
                'content'    => 'test-' . $faker->paragraph(2),
                'success'    => $faker->randomDigitNotNull
            ]);
        }

        if (! App::environment('testing')) DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }

}