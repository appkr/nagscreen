<?php

use Faker\Factory as Faker;

class PostsTableSeeder extends Seeder
{

    public function run()
    {
        if (! App::environment('testing')) DB::statement('SET FOREIGN_KEY_CHECKS=0');
        Post::truncate();
        Eloquent::unguard();

        $faker = Faker::create();

        $user_ids     = User::lists('id');
        $category_ids = Category::lists('id');
        $package_ids  = Package::lists('id');
        $languages    = ['ko', 'en'];
        $enum         = [null, 1];

        foreach (range(1, 10) as $index) {
            Post::create([
                'user_id'     => $faker->randomElement($user_ids),
                'category_id' => $faker->randomElement($category_ids),
                'package_id'  => $faker->randomElement($package_ids),
                'language'    => $faker->randomElement($languages),
                'title'       => 'test-' . $faker->sentence(5),
                'content'     => 'test-' . $faker->paragraph(10),
                'popup'       => $faker->randomElement($enum),
                'published'   => $faker->randomElement($enum)
            ]);
        }

        if (! App::environment('testing')) DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }

}