<?php

class CategoriesTableSeeder extends Seeder
{

    public function run()
    {
        if (! App::environment('testing')) DB::statement('SET FOREIGN_KEY_CHECKS=0');
        Category::truncate();
        Eloquent::unguard();

        $categories = [
            ['name' => 'notice'],
            ['name' => 'eula'],
            ['name' => 'faq'],
            ['name' => 'help']
        ];

        foreach ($categories as $category) {
            Category::create($category);
        }

        if (! App::environment('testing')) DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }

}