<?php

use Faker\Factory as Faker;

class ApidocsTableSeeder extends Seeder
{

    public function run()
    {
        if (! App::environment('testing')) DB::statement('SET FOREIGN_KEY_CHECKS=0');
        Apidoc::truncate();
        Eloquent::unguard();

        $faker = Faker::create();

        foreach (range(1, 10) as $index) {
            Apidoc::create([
                'title'   => 'test-' . $faker->sentence(5),
                'content' => 'test-' . $faker->paragraph(10)
            ]);
        }

        if (! App::environment('testing')) DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }

}