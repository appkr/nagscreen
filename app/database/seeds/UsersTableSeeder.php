<?php

use Faker\Factory as Faker;

class UsersTableSeeder extends Seeder
{

    public function run()
    {
        if (! App::environment('testing')) DB::statement('SET FOREIGN_KEY_CHECKS=0');
        User::truncate();
        Eloquent::unguard();

        $faker = Faker::create();

        foreach (range(1, 10) as $index) {
            User::create([
                'email'     => $faker->safeEmail,
                'password'  => Hash::make('password')
            ]);
        }

        if (! App::environment('testing')) DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }

}