<?php

class DatabaseSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Base tables
        $this->call('UsersTableSeeder');
        $this->command->info('User data was successfully seeded!');

        $this->call('PackagesTableSeeder');
        $this->command->info('Package data was successfully seeded!');

        $this->call('CategoriesTableSeeder');
        $this->command->info('Category data was successfully seeded!');

        // Nag Messages
        $this->call('PostsTableSeeder');
        $this->command->info('Post data was successfully seeded!');

        $this->call('CommentsTableSeeder');
        $this->command->info('Comment data was successfully seeded!');

        // Voice of Customers
        $this->call('VocsTableSeeder');
        $this->command->info('VoC data was successfully seeded!');

        $this->call('FeedbacksTableSeeder');
        $this->command->info('Feedback data was successfully seeded!');


        // Push Messages
        $this->call('PushUsersTableSeeder');
        $this->command->info('PushUser data was successfully seeded!');

        $this->call('PushMessagesTableSeeder');
        $this->command->info('Push Message data was successfully seeded!');

        // Api Documentation
        $this->call('ApidocsTableSeeder');
        $this->command->info('Api Document data was successfully seeded!');

        // TODO banner, filebox, onetime-filebox-url, attachment, eula-user
        // TODO history
    }

}
