<?php

use Faker\Factory as Faker;

class PushUsersTableSeeder extends Seeder
{

    public function run()
    {
        if (! App::environment('testing')) DB::statement('SET FOREIGN_KEY_CHECKS=0');
        PushUser::truncate();
        Eloquent::unguard();
        
        $faker = Faker::create();
        $package_ids = Package::lists('id');

        foreach (range(1, 30) as $index) {
            PushUser::create([
                'package_id'      => $faker->randomElement($package_ids),
                'uuid'            => str_random(16),
                'registration_id' => str_random(162)
            ]);
        }

        if (! App::environment('testing')) DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }

}