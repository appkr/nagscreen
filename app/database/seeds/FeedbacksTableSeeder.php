<?php

use Faker\Factory as Faker;

class FeedbacksTableSeeder extends Seeder
{

    public function run()
    {
        if (! App::environment('testing')) DB::statement('SET FOREIGN_KEY_CHECKS=0');
        Feedback::truncate();
        Eloquent::unguard();

        $faker = Faker::create();
        $user_ids = User::lists('id');
        $voc_ids  = Voc::lists('id');

        foreach (range(1, 30) as $index) {
            Feedback::create([
                'user_id' => $faker->randomElement($user_ids),
                'voc_id'  => $faker->randomElement($voc_ids),
                'content' => 'test-' . $faker->paragraph(1),
            ]);
        }

        if (! App::environment('testing')) DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }

}