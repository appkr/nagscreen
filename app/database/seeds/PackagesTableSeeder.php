<?php

class PackagesTableSeeder extends Seeder
{

    public function run()
    {
        if (! App::environment('testing')) DB::statement('SET FOREIGN_KEY_CHECKS=0');
        Package::truncate();
        Eloquent::unguard();

        $packages = [
            ['package' => 'com.airplug.abc.agent', 'name' => 'ABC'],
            ['package' => 'com.airplug.mao.agent', 'name' => 'AirSocket'],
            ['package' => 'com.airplug.sb.agent', 'name' => 'SmartBooster'],
            ['package' => 'com.airplug.videoplug', 'name' => 'VideoPlug']
        ];

        foreach ($packages as $package) {
            Package::create($package);
            User::create([
                'email'    => $package['package'],
                'password' => Hash::make($package['package'])
            ]);
        }

        if (! App::environment('testing')) DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }

}