<?php

use Faker\Factory as Faker;

class CommentsTableSeeder extends Seeder
{

    public function run()
    {
        if (! App::environment('testing')) DB::statement('SET FOREIGN_KEY_CHECKS=0');
        Comment::truncate();
        Eloquent::unguard();

        $faker = Faker::create();
        $user_ids = User::lists('id');
        $post_ids = Post::lists('id');

        Comment::truncate();

        foreach (range(1, 10) as $index) {
            Comment::create([
                'user_id' => $faker->randomElement($user_ids),
                'post_id' => $faker->randomElement($post_ids),
                'title'   => 'test-' . $faker->sentence(3),
                'content' => 'test-' . $faker->paragraph(5)
            ]);
        }

        if (! App::environment('testing')) DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }

}