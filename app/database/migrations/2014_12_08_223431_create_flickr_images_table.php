<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateFlickrImagesTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('flickr_images', function (Blueprint $table) {
            $table->increments('id');
            $table->string('flickr_id');
            $table->string('owner');
            $table->string('title');
            $table->text('description');
            $table->string('thumbnail');
            $table->string('image');
            $table->string('thumbnail_size');
            $table->string('image_size');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('flickr_images');
    }

}
