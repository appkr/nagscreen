<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBbVideosTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bb_videos', function(Blueprint $table)
        {
            $table->increments('id');
			$table->string('title');
			$table->string('author');
			$table->text('description');
			$table->string('airplug_id');
			$table->string('youtube_id');
			$table->integer('duration');
			$table->integer('size');
			$table->timestamps();
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('bb_videos');
    }

}
