<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddIndexOnTestLogsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('test_logs', function(Blueprint $table)
        {
            $table->index('module');
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('test_logs', function(Blueprint $table)
        {
            $table->dropIndex('test_logs_module_index');
        });
    }

}
