<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEulaUsersTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('eula_users', function ($table) {
            $table->increments('id')->unsigned();
            $table->tinyInteger('eula_version');
            $table->string('eula_language');
            $table->integer('package_id')->unsigned()->index();
            $table->string('apppkg')->nullable();
            $table->string('agent_version');
            $table->string('androidID');
            $table->string('eula_accept');
            $table->string('ip')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('eula_users');
    }

}
