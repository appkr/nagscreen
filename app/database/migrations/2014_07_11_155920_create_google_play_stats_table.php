<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateGooglePlayStatsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('google_play_stats', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('package_id')->unsigned();
            $table->date('date');
            $table->integer('daily_user_installs')->nullable();
            $table->integer('daily_user_uninstalls')->nullable();
            $table->integer('rank_kr')->nullable();

            $table->foreign('package_id')->references('id')->on('packages')->onUpdate('cascade');
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('google_play_stats');
    }

}
