<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAppReviewsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('app_reviews', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('app_id')->unsigned();
            $table->string('device')->nullable();
            $table->string('reviewer')->nullable();
            $table->date('date')->nullable();
            $table->string('title')->nullable();
            $table->text('content')->nullable();
            $table->integer('rating')->nullable();
            $table->timestamps();

            $table->foreign('app_id')->references('id')->on('apps')->onUpdate('cascade');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('app_reviews');
	}

}
