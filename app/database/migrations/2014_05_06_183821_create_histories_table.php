<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHistoriesTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('histories', function ($table) {
            $table->increments('id')->unsigned();
            $table->integer('user_id')->unsigned()->index();
            $table->integer('trackable_id')->unsigned()->index();
            $table->string('trackable_type')->index();
            $table->string('channel');
            $table->string('title')->nullable();
            $table->text('content')->nullable();
            $table->string('ip');
            $table->timestamps();
        });

        if (! App::environment('testing', 'local'))
            DB::statement('ALTER TABLE histories ADD FULLTEXT search(title, content)');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('histories');
    }

}
