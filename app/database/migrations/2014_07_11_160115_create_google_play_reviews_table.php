<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateGooglePlayReviewsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('google_play_reviews', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('package_id')->unsigned();
            $table->string('version')->nullable();
            $table->string('device')->nullable();
            $table->string('reviewer')->nullable();
            $table->date('date');
            $table->string('title')->nullable();
            $table->text('content')->nullable();
            $table->string('language')->nullable();
            $table->integer('rating')->nullable();

            $table->foreign('package_id')->references('id')->on('packages')->onUpdate('cascade');
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('google_play_reviews');
    }

}
