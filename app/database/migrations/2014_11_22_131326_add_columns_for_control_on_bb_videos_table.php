<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddColumnsForControlOnBbVideosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('bb_videos', function(Blueprint $table)
		{
			$table->string('locale')->default('en')->nullable();
            $table->boolean('activated')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('bb_videos', function(Blueprint $table)
		{
            $table->dropColumn('locale');
			$table->dropColumn('activated');
		});
	}

}
