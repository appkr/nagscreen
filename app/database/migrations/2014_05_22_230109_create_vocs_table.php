<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateVocsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vocs', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('package_id')->unsigned()->index();
            $table->string('title');
            $table->text('content');
            $table->string('email')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vocs');
    }

}
