<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeyConstraints extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('comments', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('cascade');
            $table->foreign('post_id')->references('id')->on('posts')->onUpdate('cascade');
        });

        Schema::table('attachments', function (Blueprint $table) {
            $table->foreign('post_id')->references('id')->on('posts')->onUpdate('cascade');
        });

        Schema::table('histories', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('cascade');
        });

        Schema::table('push_users', function (Blueprint $table) {
            $table->unique(['package_id', 'uuid']);
            $table->foreign('package_id')->references('id')->on('packages')->onUpdate('cascade');
        });

        Schema::table('eula_users', function (Blueprint $table) {
            $table->foreign('package_id')->references('id')->on('packages')->onUpdate('cascade');
        });

        Schema::table('vocs', function (Blueprint $table) {
            $table->foreign('package_id')->references('id')->on('packages')->onUpdate('cascade');
        });

        Schema::table('feedbacks', function (Blueprint $table) {
            $table->foreign('voc_id')->references('id')->on('vocs')->onUpdate('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('cascade');

        });

        Schema::table('posts', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('cascade');
            $table->foreign('package_id')->references('id')->on('packages')->onUpdate('cascade');
            $table->foreign('category_id')->references('id')->on('categories')->onUpdate('cascade');
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('posts', function (Blueprint $table) {
            $table->dropForeign('posts_user_id_foreign');
        });
    }

}
