<?php

# DEBUG #
Route::get('phpinfo', function(){
    phpinfo();
})->before('admin');

# UTILITY #
Route::get('speedtest', [
    'as' => 'speedtest',
    'uses' => 'HomeController@speedtest'
])->before('admin');

# GLOBAL PATTERNS #
Route::pattern('id', '[0-9]+');
Route::pattern('tic', '[0-9]+');
Route::pattern('package_id', '[0-9]+');
Route::pattern('package', '^[\pL\pN_-]+\.[\pL\pN_-]+\.[\pL\pN_-].+');
Route::pattern('registration_id', '[\pL\pN_-]{162}');

# FOR LEGACY APIs #
Route::get('notice', function(){
    Tracker::trackEvent('legacy', 'notice');
    return Response::json(['error' => 'Not Found'], 404);
});

# Home route
Route::get('/', ['as' => 'home', 'uses' => 'HomeController@getDashboard']);

# APIs #

# API Common
Route::group(['prefix' => 'api'], function () {
    Route::get('token', [
        'as' => 'api.token',
        'uses' => 'ApiController@token'
    ]);
    Route::get('category', [
        'as' => 'api.category',
        'uses' => 'ApiController@category'
    ]);
    Route::get('package', [
        'as' => 'api.package',
        'uses' => 'ApiController@package'
    ]);
    Route::get('language', [
        'as' => 'api.language',
        'uses' => 'ApiController@language'
    ]);
});

# Post API
Route::group(['prefix' => 'post/api'], function () {
    Route::get('/', [
        'as' => 'api.post.index',
        'uses' => 'PostApiController@index'
    ]);
    Route::get('popup', [
        'as' => 'api.post.popup',
        'uses' => 'PostApiController@popup'
    ]);
    Route::get('etag', [
        'as' => 'api.post.etag',
        'uses' => 'PostApiController@etag'
    ]);
    Route::get('latest', [
        'as' => 'api.post.latest',
        'uses' => 'PostApiController@latest'
    ]);
    Route::get('{id}', [
        'as' => 'api.post.show',
        'uses' => 'PostApiController@show'
    ]);
});

# EulaUser API
Route::group(['prefix' => 'eula/api'], function () {
    Route::get('/', [
        'as' => 'api.eula.show',
        'uses' => 'EulaUserApiController@view'
    ]);
    Route::post('/', [
        'as' => 'api.eula.store',
        'uses' => 'EulaUserApiController@store'
    ]);
    Route::get('etag', [
        'as' => 'api.eula.etag',
        'uses' => 'EulaUserApiController@etag'
    ]);
});


#PushUser API
Route::group(['prefix' => 'push/api'], function () {
    Route::post('/', [
        'as' => 'api.push.store',
        'uses' => 'PushUserApiController@store'
    ]);
    Route::delete('/{registration_id}', [
        'as' => 'api.push.destroy',
        'uses' => 'PushUserApiController@destroy'
    ]);
});

# Voc API
Route::group(['prefix' => 'voc/api'], function () {
    Route::get('/', [
        'as' => 'api.voc.index',
        'uses' => 'VocApiController@index'
    ]);
    Route::post('/', [
        'as' => 'api.voc.store',
        'uses' => 'VocApiController@store'
    ]);
    Route::get('{id}', [
        'as' => 'api.voc.show',
        'uses' => 'VocApiController@show'
    ]);
    Route::put('{id}', [
        'as' => 'api.voc.update',
        'uses' => 'VocApiController@update'
    ]);
    Route::delete('{id}', [
        'as' => 'api.voc.destroy',
        'uses' => 'VocApiController@destroy'
    ]);
});

# Banner API
Route::group(['prefix' => 'banner'], function () {
    Route::get('api', [
        'as' => 'api.banner.index',
        'uses' => 'BannerApiController@index'
    ]);
    Route::get('api/{id}', [
        'as' => 'api.banner.show',
        'uses' => 'BannerApiController@show'
    ]);
});

# Google Play Statistics API
Route::group(['prefix' => 'googleplay'], function () {
    Route::get('api', [
        'as' => 'api.googleplay.summary',
        'uses' => 'GooglePlayApiController@summary'
    ]);
    Route::get('new-api', [
        'as' => 'api.new.googleplay.summary',
        'uses' => 'GooglePlayApiController@newSummary'
    ]);
    Route::get('api/stat', [
        'as' => 'api.googleplay.stat.index',
        'uses' => 'GooglePlayApiController@indexStat'
    ]);
    Route::get('api/stat/{id}', [
        'as' => 'api.googleplay.stat.show',
        'uses' => 'GooglePlayApiController@showStat'
    ]);
    Route::get('api/review', [
        'as' => 'api.googleplay.review.index',
        'uses' => 'GooglePlayApiController@indexReview'
    ]);
    Route::get('api/review/{id}', [
        'as' => 'api.googleplay.review.show',
        'uses' => 'GooglePlayApiController@showReview'
    ]);
});

# APPLICATIONS #

# Post Application
Route::get('post/{id}/view', [
    'as' => 'post.view',
    'uses' => 'PostController@getView'
]);
Route::post('post/{id}/toggle', [
    'as' => 'post.toggle',
    'uses' => 'PostController@toggle'
]);
Route::resource('user.post', 'PostController', ['only' => ['index']]);
Route::resource('post', 'PostController');

# Comment Application
Route::post('comment', [
    'as' => 'comment.store',
    'uses' => 'CommentController@store'
]);

# Attachment Application
Route::group(['prefix' => 'attachment'], function () {
    Route::post('/', [
        'as' => 'attachment.store',
        'uses' => 'AttachmentController@store'
    ]);
    Route::delete('{id}', [
        'as' => 'attachment.destroy',
        'uses' => 'AttachmentController@destroy'
    ]);
});

# User Application
Route::group(['prefix' => 'user'], function () {
    Route::get('profile/{id?}', [
        'as' => 'user.profile',
        'uses' => 'UserController@profile'
    ]);
    Route::get('signin', [
        'as' => 'session.create',
        'uses' => 'UserController@getSignin'
    ]);
    Route::post('signin', [
        'as' => 'session.store',
        'uses' => 'UserController@postSignin'
    ]);
    Route::get('signout', [
        'as' => 'session.destroy',
        'uses' => 'UserController@getSignout'
    ]);
    Route::get('signup', [
        'as' => 'user.create',
        'uses' => 'UserController@getSignup'
    ]);
    Route::post('signup', [
        'as' => 'user.store',
        'uses' => 'UserController@postSignup'
    ]);
    Route::get('activate/{code}', [
        'as' => 'user.activate',
        'uses' => 'UserController@activate'
    ]);
    Route::get('remind', [
        'as' => 'user.find',
        'uses' => 'UserController@getRemind'
    ]);
    Route::post('remind', [
        'as' => 'user.remind',
        'uses' => 'UserController@postRemind'
    ]);
    Route::get('reset/{token}', [
        'as' => 'user.reset',
        'uses' => 'UserController@getReset'
    ]);
    Route::post('reset', [
        'as' => 'user.restore',
        'uses' => 'UserController@postReset'
    ]);
});

# Voc Application
Route::group(['prefix' => 'voc'], function () {
    Route::get('/', [
        'as' => 'voc.index',
        'uses' => 'VocController@index'
    ]);
    Route::get('{id}', [
        'as' => 'voc.show',
        'uses' => 'VocController@show'
    ]);
    Route::delete('{id}', [
        'as' => 'voc.destroy',
        'uses' => 'VocController@destroy'
    ]);
});

# Feedback Application
Route::group(['prefix' => 'feedback'], function () {
    Route::post('/', [
        'as' => 'feedback.store',
        'uses' => 'FeedbackController@store'
    ]);
    Route::put('{id}', [
        'as' => 'feedback.update',
        'uses' => 'FeedbackController@update'
    ]);
    Route::delete('{id}', [
        'as' => 'feedback.destroy',
        'uses' => 'FeedbackController@destroy'
    ]);
});

# Search
Route::get('search', [
    'as' => 'history.index',
    'uses' => 'HistoryController@index'
]);

# Admin Application
Route::group(['prefix' => 'admin'], function () {
    # Packages
    Route::get('package', [
        'as' => 'package.index',
        'uses' => 'PackageController@index'
    ]);
    Route::post('package', [
        'as' => 'package.store',
        'uses' => 'PackageController@store'
    ]);
    # Categories
    Route::get('category', [
        'as' => 'category.index',
        'uses' => 'CategoryController@index'
    ]);
    Route::post('category', [
        'as' => 'category.store',
        'uses' => 'CategoryController@store'
    ]);
    # Users
    Route::get('users', [
        'as' => 'user.index',
        'uses' => 'UserController@index'
    ]);
});

# Banner Application
Route::group(['prefix' => 'banner'], function () {
    Route::get('create/{package_id}', [
        'as' => 'banner.create',
        'uses' => 'BannerController@create'
    ]);
    Route::post('/', [
        'as' => 'banner.store',
        'uses' => 'BannerController@store'
    ]);
    Route::get('{id}/edit/', [
        'as' => 'banner.edit',
        'uses' => 'BannerController@edit'
    ]);
    Route::put('{id}', [
        'as' => 'banner.update',
        'uses' => 'BannerController@update'
    ]);
    Route::delete('{id}', [
        'as' => 'banner.destroy',
        'uses' => 'BannerController@destroy'
    ]);
//    Route::get('view', [
//        'as' => 'banner.view',
//        'uses' => 'BannerController@view'
//    ]);
});

# Eula Application
Route::get('eula/view', [
    'as' => 'eula.view',
    'uses' => 'EulaController@getView'
]);
Route::get('eula', [
    'as' => 'eula.view',
    'uses' => 'EulaController@getView'
]);

# Push Message Application
Route::group(['prefix' => 'push'], function () {
    Route::get('/', [
        'as' => 'push.index',
        'uses' => 'PushMessageController@index'
    ]);
    Route::get('/create', [
        'as' => 'push.create',
        'uses' => 'PushMessageController@create'
    ]);
    Route::get('/{id}', [
        'as' => 'push.show',
        'uses' => 'PushMessageController@show'
    ]);
    Route::post('/', [
        'as' => 'push.send',
        'uses' => 'PushMessageController@send'
    ]);
    Route::get('{package_id}/count', [
        'as' => 'push.count',
        'uses' => 'PushMessageController@countReceivers'
    ]);
});

# FileBox Application
Route::group(['prefix' => 'file'], function () {
    Route::get('/', [
        'as' => 'filebox.index',
        'uses' => 'FileBoxController@index'
    ]);
    Route::post('/', [
        'as' => 'filebox.store',
        'uses' => 'FileBoxController@store'
    ]);
    Route::delete('/{id}', [
        'as' => 'filebox.destroy',
        'uses' => 'FileBoxController@destroy'
    ]);
    Route::get('/{filename}', [
        'as' => 'filebox.download',
        'uses' => 'FileBoxController@download'
    ]);
});

# Onetime FileBox Application
Route::group(['prefix' => 'onetime-filebox'], function () {
    Route::post('/make', [
        'as' => 'onetime.filebox.make',
        'uses' => 'OnetimeFileboxUrlController@make'
    ]);
    Route::get('/upload/{token}', [
        'https',
        'as' => 'onetime.filebox.upload',
        'uses' => 'OnetimeFileboxUrlController@upload'
    ]);
    Route::post('/upload/{token}', [
        'https',
        'as' => 'onetime.filebox.save',
        'uses' => 'OnetimeFileboxUrlController@save'
    ]);
    Route::get('/download/{token}', [
        'https',
        'as' => 'onetime.filebox.download',
        'uses' => 'OnetimeFileboxUrlController@download'
    ]);
});

# Api Doc Application
Route::group(['prefix' => 'api'], function () {
    Route::get('{id?}', [
        'as' => 'api.index',
        'uses' => 'ApidocController@index'
    ]);
    Route::get('create', [
        'as' => 'api.create',
        'uses' => 'ApidocController@create'
    ]);
    Route::post('/', [
        'as' => 'api.store',
        'uses' => 'ApidocController@store'
    ]);
    Route::get('{id}/edit', [
        'as' => 'api.edit',
        'uses' => 'ApidocController@edit'
    ]);
    Route::put('{id}', [
        'as' => 'api.update',
        'uses' => 'ApidocController@update'
    ]);
});

# Google Play Statistics Application
Route::group(['prefix' => 'googleplay'], function () {
    Route::get('/', [
        'as' => 'googleplay.index',
        'uses' => 'GooglePlaySummaryController@index'
    ]);
    Route::post('fetch', [
        'as' => 'googleplay.fetch',
        'uses' => 'GooglePlaySummaryController@fetch'
    ]);
    Route::post('apps', [
        'as' => 'googleplay.apps.store',
        'uses' => 'GooglePlaySummaryController@storeApp'
    ]);
    Route::get('notify', [
        'as' => 'googleplay.notify',
        'uses' => 'GooglePlaySummaryController@notify'
    ]);
    Route::get('new-notify', [
        'as' => 'googleplay.new.notify',
        'uses' => 'GooglePlaySummaryController@newNotify'
    ]);
    Route::post('send', [
        'as' => 'googleplay.send',
        'uses' => 'GooglePlaySummaryController@send'
    ]);
    Route::post('new-send', [
        'as' => 'googleplay.new.send',
        'uses' => 'GooglePlaySummaryController@newSend'
    ]);
    Route::get('review/{app_id}', [
        'as' => 'googleplay.review.show',
        'uses' => 'GooglePlaySummaryController@getMostRecentReview'
    ]);
    Route::post('review', [
        'as' => 'googleplay.review.store',
        'uses' => 'GooglePlaySummaryController@storeReview'
    ]);
    Route::delete('review/{id}', [
        'as' => 'googleplay.review.destroy',
        'uses' => 'GooglePlaySummaryController@destroyReview'
    ]);
    Route::put('{id}', [
        'as' => 'googleplay.update',
        'uses' => 'GooglePlaySummaryController@update'
    ]);
});

# Marketing Applications
# Mini Pages
Route::get('abc', [
    'as' => 'abc',
    'uses' => 'HomeController@getAbc'
]);
Route::get('airsocket', [
    'as' => 'airsocket',
    'uses' => 'HomeController@getAirsocket'
]);
Route::get('airsocketforhoppin', [
    'as' => 'airsocketforhoppin',
    'uses' => 'HomeController@getAirsocketForHoppin'
]);
Route::get('videoplug', [
    'as' => 'videoplug',
    'uses' => 'HomeController@getVideoplug'
]);

# Url Redirection to Google Play Store
Route::get('util/recommend/{package?}', [
    'as' => 'util.recommend',
    'uses' => 'UtilityController@redirectToPlayStore'
]);

# Share Urls Application
Route::get('util/share', [
    'as' => 'util.share',
    'uses' => 'UtilityController@getLinkToSNS'
]);

# HouseKeeping Application
# Iron MQ (iron.io)
Route::post('queue', function() {
    return Queue::marshal();
});

# TEST APPLICATIONS #

# OVERRIDE FOR LEGACY ROUTINGS - AMS-X RELATED TESTS #
Route::post('tester/response/post', [
    'as' => 'tester.response.post',
    'uses' => 'TesterController@makeResponse'
]);
Route::get('tester/response/get', [
    'as' => 'tester.response.get',
    'uses' => 'TesterController@makeResponse'
]);

# Player/Downloader Test
Route::group(['prefix' => 'test'], function () {
    Route::get('/', [
        'as' => 'test.index',
        'uses' => 'TesterController@index'
    ]);
    Route::get('video', [
        'as' => 'test.video',
        'uses' => 'TesterController@video'
    ]);
    Route::get('bbdemo', [
        'as' => 'test.bbdemo',
        'uses' => 'TesterController@bbdemo'
    ]);
    Route::get('bbdemo2', [
        'as' => 'test.bbdemo2',
        'uses' => 'TesterController@bbdemo2'
    ]);
    Route::get('demo-video', [
        'as' => 'test.demo-video',
        'uses' => 'TesterController@fetchDemoVideos'
    ]);
    Route::get('demo-image', [
        'as' => 'test.demo-image',
        'uses' => 'TesterController@fetchDemoImages'
    ]);
    Route::get('bbdemo2-angular', [
        'as' => 'test.bbdemo2-angular',
        'uses' => 'TesterController@bbdemo2Angular'
    ]);
    Route::get('xdemo', [
        'as' => 'test.xdemo',
        'uses' => 'TesterController@xdemo'
    ]);
    Route::get('xdemo2', [
        'as' => 'test.xdemo2',
        'uses' => 'TesterController@xdemo2'
    ]);
    Route::get('xdemo3', [
        'as' => 'test.xdemo3',
        'uses' => 'TesterController@xdemo3'
    ]);
    Route::get('flickr', [
        'as' => 'test.flickr',
        'uses' => 'TesterController@fetchFlickrImages'
    ]);
    Route::get('xdemo-https', [
        'as' => 'test.xdemo.https',
        'uses' => 'TesterController@xdemo3'
    ]);
    Route::get('web', [
        'as' => 'test.web',
        'uses' => 'TesterController@web'
    ]);
    Route::get('get-random-url', [
        'as' => 'test.get-random-url',
        'uses' => 'TesterController@getRandomWebpageUrl'
    ]);
    Route::get('intent', [
        'as' => 'test.intent',
        'uses' => 'TesterController@intent'
    ]);
    Route::get('download', [
        'as' => 'test.download',
        'uses' => 'TesterController@download'
    ]);
    Route::get('binary', [
        'as' => 'test.binary',
        'uses' => 'TesterController@binary'
    ]);
    Route::get('delayed/{tic}', [
        'as' => 'test.delayed',
        'uses' => 'TesterController@delayed'
    ]);
    Route::get('ams-x/{filename}/{direction?}', [
        'as' => 'test.ams-x',
        'uses' => 'TesterController@amsXTester'
    ]);
    Route::get('request/{method}', [
        'as' => 'test.request',
        'uses' => 'TesterController@httpRequest'
    ]);
    Route::any('response/{method}', [
        'as' => 'test.response',
        'uses' => 'TesterController@makeResponse'
    ]);
    Route::get('logs', [
        'as' => 'test.logs.index',
        'uses' => 'TestLogsController@index'
    ]);
    Route::get('logs/{module}/download', [
        // 'https',
        'as' => 'test.logs.download',
        'uses' => 'TestLogsController@downloadCsv'
    ]);
    Route::get('logs/{id}', [
        'as' => 'test.logs.show',
        'uses' => 'TestLogsController@show'
    ]);
    Route::post('logs/{testName}', [
        'https',
        'as' => 'test.logs.store',
        'uses' => 'TestLogsController@store'
    ]);
    Route::delete('logs/{testName}', [
        'as' => 'test.logs.destroy',
        'uses' => 'TestLogsController@destroy'
    ]);
    Route::get('gcu', [
        'as' => 'test.gcu.index',
        'uses' => 'GcuController@index'
    ]);
    Route::post('gcu', [
        'as' => 'test.gcu.extract',
        'uses' => 'GcuController@extract'
    ]);
    Route::get('cus', [
        'as' => 'test.cutrom-url-scheme',
        'uses' => 'TesterController@cus'
    ]);
});

# Queue Test
// Route::get('queue-test', function(){
//     Queue::push('FileTimeWriter', ['time' => time()]);
// });

# Flickr Image Download
Route::get('flickr-image-download', function() {
    App::make('FlickrImageDownloader')->run();
});
# Flickr Image Size Update
Route::get('flickr-image-size', function() {
    App::make('FlickrImageDownloader')->updateImageSize();
});
