<?php

# Track User activities
Event::listen('history.track', function ($model) {
    $route    = Route::currentRouteName();
    $parts    = explode('.', $route);
    $category = count($parts > 2) ? $parts[0].'.'.$parts[1] : $parts[0];
    $action   = end($parts);

    $field_to_remove = ['id', 'post_id', 'voc_id', 'category_id', 'package_id', 'language', 'url', 'ticker', 'success', 'email', 'created_at', 'updated_at'];
    $data = array_merge(array_except($model->toArray(), $field_to_remove), [
        'user_id' => Auth::user()->id,
        'channel' => $route,
        'ip'      => Request::server('REMOTE_ADDR')
    ]);

    // TODO Should I delete destroyed histories and its related models?
    if ($action == 'update') $history = $model->histories()->update($data);
    else $history = $model->histories()->create($data);

    if ($action == 'store')
    {
        $data = array_merge($history->toArray(), [
            'email' => Auth::user()->email,
            'link'  => get_activity_link($history->trackable_type, $history->trackable_id, $action)
        ]);

        App::make('airplug\Mailers\Mailer')->newPost($data);
    }

    Cache::tags('histories')->flush();
    Tracker::trackEvent($category, $action);
});


# Catch user's last login time and update database
Event::listen('auth.login', function ($user) {
    $now              = new DateTime;
    $user->last_login = $now->format('Y-m-d H:i:s');
    $user->save();
});


# Send activation email
Event::listen('auth.signup', function ($user) {
    $data = [
        'email' => $user->email,
        'link'  => route('user.activate', $user->code)
    ];

    App::make('airplug\Mailers\Mailer')->activateUser($user->email, $data);

    Tracker::trackEvent('user', 'store');
});


# Send feedback in response to VoC
Event::listen('feedback', function ($voc, $feedback) {
    if(! $voc->email) return;

    $data = [
        'subject'  => "Re:[{$voc->package->name}] {$voc->title}",
        'voc'      => $voc->content,
        'feedback' => $feedback->content
    ];

    App::make('airplug\Mailers\Mailer')->newFeedback($voc->email, $data);
});


# Flush cache
Event::listen('cache.flush', function ($tags) {
    if (is_memcached())
    {
        if (is_array($tags)) foreach ($tags as $tag) Cache::tags($tag)->flush();
        else Cache::tags($tags)->flush();
    }
    else Cache::flush();
});


# Notify to the person who made a One-time Filebox URL about the fact
# that the URL has been consumed and New file was created
Event::listen('onetimefileboxurl.consumed', function ($user_id, $file) {
    $to = User::find($user_id)->email;

    $data = [
        'subject'  => 'One-time upload URL has been consumed and New file created.',
        'filename' => $file->filename,
        'size'     => format_filesize($file->size),
        'uploader' => build_file_uploader_string($file),
        'link'     => route('filebox.download', base64_encode($file->filename))
    ];

    App::make('airplug\Mailers\Mailer')->newFilebox($to, $data);
});


# Notify Google Play statistics update
Event::listen('googleplay.update', function ($summary, $cc=[], $body=null) {
    $to = array_merge(['juwonkim@airplug.com'], $cc);
    App::make('airplug\Mailers\Mailer')->notifyGooglePlayUpdate($to, ['summary' => $summary, 'body' => $body]);
});

# Notify Google Play statistics update
Event::listen('googleplay.new.update', function ($summary, $cc=[], $body=null) {
    $to = array_merge(['juwonkim@airplug.com'], $cc);
    App::make('airplug\Mailers\Mailer')->newNotifyGooglePlayUpdate($to, ['summary' => $summary, 'body' => $body]);
});