<?php

/*
|--------------------------------------------------------------------------
| Register The Laravel Class Loader
|--------------------------------------------------------------------------
|
| In addition to using Composer, you may use the Laravel class loader to
| load your controllers and models. This is useful for keeping all of
| your classes in the "global" namespace without Composer updating.
|
*/

ClassLoader::addDirectories(array(

    app_path() . '/commands',
    app_path() . '/controllers',
    app_path() . '/models',
    app_path() . '/database/seeds',

));

/*
|--------------------------------------------------------------------------
| Application Error Logger
|--------------------------------------------------------------------------
|
| Here we will configure the error logger setup for the application which
| is built on top of the wonderful Monolog library. By default we will
| build a basic log file setup which creates a single file for logs.
|
*/

Log::useFiles(storage_path() . '/logs/laravel.log');

/*
|--------------------------------------------------------------------------
| Application Error Handler
|--------------------------------------------------------------------------
|
| Here you may handle any errors that occur in your application, including
| logging them or displaying custom views for specific errors. You may
| even register several error handlers to handle different types of
| exceptions. If nothing is returned, the default error view is
| shown, which includes a detailed stack trace during debug.
|
*/

App::error(function (Exception $exception, $code) {
    Log::error($exception);
});

App::error(function (airplug\Exceptions\UnauthorizedException $exception) {
    return Response::json(['error' => ['code' => 401, 'message' => 'Token does not match']], 401);
});

App::error(function (airplug\Exceptions\ForbiddenException $exception) {
    return Response::json(['error' => ['code' => 403, 'message' => 'Forbidden. SSL request required']], 403);
});

App::error(function (airplug\Exceptions\ModelNotFoundException $exception) {
    return Response::json(['error' => ['code' => 404, 'message' => 'Not Found']], 404);
});

App::error(function (Illuminate\Database\Eloquent\ModelNotFoundException $exception) {
    return Response::view('layouts.notice', [
        'title'       => 'Page Not Found',
        'description' => 'Sorry, the page or resource you are trying to view does not exist.'
    ], 404);
});

App::error(function (airplug\Exceptions\ResourceExpiredException $exception) {
    return Response::view('layouts.notice', [
        'title'       => 'Resource Expired',
        'description' => 'Sorry, the page or resource you are trying to reach is no more valid.'
    ], 403);
});

App::missing(function($exception) {
    return Response::view('layouts.notice', [
        'title'       => 'Page Not Found',
        'description' => 'Sorry, the page or resource you are trying to view does not exist.'
    ], 404);
});

/*
|--------------------------------------------------------------------------
| Maintenance Mode Handler
|--------------------------------------------------------------------------
|
| The "down" Artisan command gives you the ability to put an application
| into maintenance mode. Here, you will define what is displayed back
| to the user if maintenance mode is in effect for the application.
|
*/

App::down(function () {
    return Response::view('layouts.notice', [
        'title'       => 'Be Right Back',
        'description' => 'Sorry, NagScreen application is now in maintenance mode.'
    ], 503);
});

/*
|--------------------------------------------------------------------------
| Require The Filters File
|--------------------------------------------------------------------------
|
| Next we will load the filters file for the application. This gives us
| a nice separate location to store our route and application filter
| definitions instead of putting them all in the main routes file.
|
*/

require app_path() . '/filters.php';


// Import the Events files, where event listeners are located
require app_path() . '/start/events.php';
