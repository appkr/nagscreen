<?php

class Comment extends Eloquent
{

    /**
     * @var string
     */
    protected $table = 'comments';


    /**
     * @var array
     */
    protected $fillable = ['user_id', 'post_id', 'title', 'content'];


    /**
     * @var array
     */
    public $rules = [
        'content' => 'required|min:2'
    ];


    /**
     * @var bool
     */
    protected $softDelete = true;


    /**
     * @var array
     */
    protected $hidden = ['deleted_at'];


    /**
     * Build relationship between Comment and Post model
     */
    public function post()
    {
        return $this->belongsTo('Post');
    }


    /**
     * Build relationship between Comment and User model
     */
    public function user()
    {
        return $this->belongsTo('User');
    }


    public function histories()
    {
        return $this->morphMany('History', 'trackable');
    }

}