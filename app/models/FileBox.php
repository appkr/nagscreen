<?php

class FileBox extends \Eloquent
{

    /**
     * @var string
     */
    protected $table = 'filebox';


    /**
     * @var array
     */
    protected $fillable = ['user_id', 'created_by', 'title', 'filename', 'size'];


    /**
     * Build relationship with User model
     */
    public function user()
    {
        return $this->belongsTo('User');
    }

}