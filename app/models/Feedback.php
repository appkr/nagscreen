<?php

class Feedback extends \Eloquent
{

    /**
     * @var string
     */
    protected $table = 'feedbacks';


    /**
     * @var array
     */
    protected $fillable = ['voc_id', 'user_id', 'content'];


    /**
     * @var array
     */
    public $rules = [
        'voc_id'  => 'required|integer',
        'user_id' => 'required|integer',
        'content' => 'required|min:10'
    ];


    /**
     * @var bool
     */
    protected $softDelete = true;


    /**
     * @var array
     */
    protected $hidden = ['deleted_at'];


    /**
     * @return mixed
     */
    public function vocs()
    {
        return $this->belongsTo('Voc');
    }


    /**
     * @return mixed
     */
    public function user()
    {
        return $this->belongsTo('User');
    }


    /**
     * @return mixed
     */
    public function voc()
    {
        return $this->belongsTo('Voc');
    }


    /**
     * @return mixed
     */
    public function histories()
    {
        return $this->morphMany('History', 'trackable');
    }

}