<?php

class Package extends \Eloquent
{

    /**
     * @var string
     */
    protected $table = 'packages';


    /**
     * @var array
     */
    protected $fillable = ['package', 'name'];


    /**
     * @var array
     */
    public $rules = [
        'package' => 'required|unique:packages|regex:/^[A-z0-9]+\.[A-z0-9]+\.[A-z0-9].+/',
        'name'    => 'required|unique:packages|min:2'
    ];


    /**
     * @var bool
     */
    protected $softDelete = true;


    /**
     * @var array
     */
    protected $hidden = ['created_at', 'updated_at', 'deleted_at'];


    /**
     * @return mixed
     */
    public function posts()
    {
        return $this->hasMany('Post');
    }


    /**
     * @return mixed
     */
    public function pushUsers()
    {
        return $this->hasMany('PushUser');
    }


    /**
     * @return mixed
     */
    public function vocs()
    {
        return $this->hasMany('Voc');
    }


    /**
     *
     */
    public function eulaUsers()
    {
        return $this->hasMany('EulaUser');
    }


    /**
     * @return mixed
     */
    public function comments()
    {
        return $this->hasManyThrough('Comment', 'Post');
    }


    /**
     * @return mixed
     */
    public function attachments()
    {
        return $this->hasManyThrough('Attachment', 'Post');
    }


    /**
     * @return mixed
     */
    public function feedbacks()
    {
        return $this->hasManyThorugh('Feedback', 'Voc');
    }


    /**
     * @return mixed
     */
    public function banner()
    {
        return $this->hasOne('Banner');
    }


    /**
     * @return mixed
     */
    public function google_play_stats()
    {
        return $this->hasMany('GooglePlayStat');
    }


    /**
     * @return mixed
     */
    public function google_play_reviews()
    {
        return $this->hasMany('GooglePlayReview');
    }


    /**
     * @return mixed
     */
    public function google_play_app()
    {
        return $this->hasOne('GooglePlayApp');
    }

}