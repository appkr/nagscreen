<?php

class Category extends \Eloquent
{

    /**
     * @var string
     */
    protected $table = 'categories';


    /**
     * @var array
     */
    protected $fillable = ['name'];


    /**
     * @var array
     */
    public $rules = [
        'name' => 'required|min:2'
    ];


    /**
     * @var bool
     */
    protected $softDelete = true;


    /**
     * @var array
     */
    protected $hidden = ['created_at', 'updated_at', 'deleted_at'];


    /**
     * @return mixed
     */
    public function posts()
    {
        return $this->hasMany('Post');
    }
}