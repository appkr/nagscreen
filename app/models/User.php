<?php

use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableInterface;
use Carbon\Carbon;


class User extends Eloquent implements UserInterface, RemindableInterface
{

    /**
     * @var string
     */
    protected $table = 'users';


    /**
     * @var array
     */
    protected $fillable = ['email', 'password', 'image', 'thumbnail', 'code', 'activated', 'last_login'];


    /**
     * @var array
     */
    public $rules = [
        'email'                 => 'required|email|regex:/.+@airplug\.com$/|unique:users',
        'password'              => 'required|between:6,16|confirmed',
        'password_confirmation' => 'required|between:6,16'
    ];


    /**
     * @var bool
     */
    protected $softDelete = true;


    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'activated', 'code', 'remember_token', 'last_login', 'created_at', 'updated_at', 'deleted_at'];


    /**
     * Build relationship between User and Post model
     */
    public function posts()
    {
        return $this->hasMany('Post');
    }


    /**
     * Build relationship between User and History model
     */
    public function histories()
    {
        return $this->hasMany('History')->take(10)->orderBy('updated_at', 'desc');
    }


    /**
     * Build relationship between User and Comment model
     */
    public function comments()
    {
        return $this->hasMany('Comment');
    }


    /**
     * @return mixed
     */
    public function feedbacks()
    {
        return $this->hasMany('Feedback');
    }


    /**
     * @return mixed
     */
    public function pushmessages()
    {
        return $this->hasMany('PushMessage');
    }


    public function testLogs()
    {
        return $this->hasManay('TestLogs');
    }


    /**
     * Get the unique identifier for the user.
     *
     * @return mixed
     */
    public function getAuthIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Get the password for the user.
     *
     * @return string
     */
    public function getAuthPassword()
    {
        return $this->password;
    }

    /**
     * Get the token value for the "remember me" session.
     *
     * @return string
     */
    public function getRememberToken()
    {
        return $this->remember_token;
    }

    /**
     * Set the token value for the "remember me" session.
     *
     * @param  string $value
     * @return void
     */
    public function setRememberToken($value)
    {
        $this->remember_token = $value;
    }

    /**
     * Get the column name for the "remember me" token.
     *
     * @return string
     */
    public function getRememberTokenName()
    {
        return 'remember_token';
    }

    /**
     * Get the e-mail address where password reminders are sent.
     *
     * @return string
     */
    public function getReminderEmail()
    {
        return $this->email;
    }


    /**
     * @return array
     */
    public function getDates()
    {
        return ['created_at', 'updated_at', 'deleted_at', 'last_login'];
    }

}