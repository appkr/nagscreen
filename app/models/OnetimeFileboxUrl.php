<?php

class OnetimeFileboxUrl extends \Eloquent
{

    /**
     * @var string
     */
    protected $table = 'onetime_filebox_urls';


    /**
     * @var array
     */
    protected $fillable = ['user_id', 'token', 'filename', 'expires_at'];


    /**
     * @var array
     */
    protected $hidden = ['updated_at'];


    /**
     * @var array
     */
    public $rules = [
        'token' => 'required|alpha_dash|min:40'
    ];


    /**
     * @var array
     */
    protected $appends = ['path'];


    /**
     * Build relationship with User model
     */
    public function user()
    {
        return $this->belongsTo('User');
    }


    /**
     * @return array
     */
    public function getDates()
    {
        return ['created_at', 'updated_at', 'expires_at'];
    }


    /**
     * @return string
     */
    public function getPathAttribute()
    {
        return ($this->filename) ? route('onetime.filebox.download', $this->token) : route('onetime.filebox.upload', $this->token);
    }

}