<?php

class GooglePlayReview extends \Eloquent
{

    protected $table = 'google_play_reviews';


    protected $fillable = ['package_id', 'date', 'rating', 'title', 'content', 'reviewer', 'version', 'language', 'device'];


    protected $hidden = ['id', 'package_id'];

    public $timestamps = false;


    public function package()
    {
        return $this->belongsTo('Package');
    }

}