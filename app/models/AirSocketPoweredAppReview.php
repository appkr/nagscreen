<?php

class AirSocketPoweredAppReview extends \Eloquent
{

    protected $table = 'app_reviews';

    protected $fillable = ['app_id', 'device', 'reviewer', 'date', 'title', 'content', 'rating'];

    protected $hidden = ['updated_at'];


    public function app()
    {
        return $this->belongsTo('AirSocketPoweredApp');
    }

}