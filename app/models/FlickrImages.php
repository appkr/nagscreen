<?php

class FlickrImages extends \Eloquent {

    protected $table = 'flickr_images';

    protected $fillable = [
        'flickr_id',
        'owner',
        'title',
        'description',
        'thumbnail',
        'image',
        'thumbnail_size',
        'image_size'
    ];

    protected $hidden = [
        'created_at',
        'updated_at'
    ];

    //public function getBaseUrlAttribute() {
    //    return '//tfrontg1.airplug.co.kr/images/';
    //}

}