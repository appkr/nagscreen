<?php

class Voc extends \Eloquent
{

    /**
     * @var string
     */
    protected $table = 'vocs';


    /**
     * @var array
     */
    protected $fillable = ['package_id', 'title', 'content', 'email'];


    /**
     * @var array
     */
    public $rules = [
        'package_id' => 'required|integer',
        'title'      => 'required|min:2',
        'content'    => 'required|min:2',
        'email'      => 'email'
    ];


    /**
     * @var bool
     */
    protected $softDelete = true;


    /**
     * @var array
     */
    protected $hidden = ['deleted_at'];


    /**
     * @return mixed
     */
    public function package()
    {
        return $this->belongsTo('Package');
    }


    /**
     * @return mixed
     */
    public function feedbacks()
    {
        return $this->hasMany('Feedback')->orderBy('id', 'desc');
    }

    /**
     * @return mixed
     */
    public function histories()
    {
        return $this->morphMany('History', 'trackable');
    }


    /**
     * Filter query
     */
    public function scopeFilter($query, $field, $value)
    {
        if ($field == 'package') {
            $package = Package::wherePackage($value)->first();
            return $query->wherePackageId($package->id);
        }
        else if ($field == 'feedback') {
            return $query->has('feedbacks', '<', (int) $value);
        }
    }

}