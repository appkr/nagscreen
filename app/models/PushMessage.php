<?php

class PushMessage extends \Eloquent
{

    protected $fillable = ['user_id', 'package_id', 'ticker', 'title', 'content', 'success'];

    public $rules = [
        'package_id' => 'required|integer',
        'ticker'     => 'required|min:2',
        'title'      => 'required|min:2',
        'content'    => 'required|between:2,140'
    ];

    /**
     * Build relationship between Post and User model
     */
    public function user()
    {
        return $this->belongsTo('User');
    }


    /**
     * @return mixed
     */
    public function package()
    {
        return $this->belongsTo('Package');
    }


    /**
     * Build relationship with History model
     */
    public function histories()
    {
        return $this->morphMany('History', 'trackable');
    }


    /**
     * Filter query
     */
    public function scopeFilter($query, $field, $value)
    {
        if ($field == 'package') {
            $package = Package::wherePackage($value)->first();
            return ($package) ? $query->wherePackageId($package->id) : $query;
        }
        else if ($field == 'success') {
            return $query->whereNull('success')->orWhere('success', '<', (int) $value);
        }
    }
}