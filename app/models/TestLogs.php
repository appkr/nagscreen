<?php

class TestLogs extends \Eloquent {

    protected $table = 'test_logs';

    protected $fillable = ['module', 'log', 'user_id', 'ip'];

    protected $hidden = ['updated_at'];

    public static $rules = [];

    public function user()
    {
        return $this->belongsTo('User');
    }

    public function modules()
    {
        return self::distinct()->lists('module');
    }

    public function result()
    {
        $log = json_decode($this->log, true);

        if (! isset($log['proxySetting']) || ! isset($log['proxyPresent']) || ! isset($log['communication']))
            return '<i class="fa fa-frown-o text-danger"></i>';

        return ($log['proxySetting'] && $log['proxyPresent'] && $log['communication'])
            ? '<i class="fa fa-smile-o"></i>'
            : '<i class="fa fa-frown-o text-danger"></i>';
    }

}