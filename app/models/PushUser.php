<?php

class PushUser extends \Eloquent
{

    /**
     * @var string
     */
    protected $table = 'push_users';


    /**
     * @var array
     */
    protected $fillable = ['package_id', 'uuid', 'registration_id'];


    /**
     * @var array
     */
    public $rules = [
        'package_id'      => 'required|unique_with:push_users,uuid',
        'uuid'            => 'required|alphanum|min:13|max:16',
        'registration_id' => 'required|alpha_dash|min:162'
    ];


    /**
     * @var bool
     */
    protected $softDelete = true;


    /**
     * @var array
     */
    protected $hidden = ['deleted_at'];


    /**
     * @return mixed
     */
    public function package()
    {
        return $this->belongsTo('Package');
    }

}