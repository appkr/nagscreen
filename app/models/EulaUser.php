<?php

class EulaUser extends Eloquent
{

    /**
     * @var string
     */
    protected $table = 'eula_users';

    /**
     * @var array
     */
    protected $fillable = ['ip', 'eula_version', 'eula_language', 'package_id', 'apppkg', 'agent_version', 'androidID', 'eula_accept'];

    /**
     * @var array
     */
    protected $hidden = ['ip', 'created_at', 'updated_at', 'deleted_at'];


    /**
     * @var array
     */
    public $rules = [
        'eula_version'  => 'integer|exists:posts,id,deleted_at,NULL',
        'eula_language' => 'in:ko,en',
        'package_id'    => 'required|integer',
        'apppkg'        => 'regex:/^[A-z0-9_]+[.][A-z0-9_]+[.][A-z0-9_]+/',
        'agent_version' => 'required|regex:/^[0-9]{1,3}\.[0-9]{1,3}\.[A-z0-9]{1,5}$/',
        'androidID'     => 'required|alpha_num|between:13,16',
        'eula_accept'   => 'required|in:yes,1,true,no,0,false'
    ];


    /**
     * @return mixed
     */
    public function package()
    {
        return $this->belongsTo('Package');
    }

}