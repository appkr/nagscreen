<?php

class GooglePlayApp extends \Eloquent
{

    protected $table = 'google_play_apps';


    protected $fillable = ['package_id', 'platform', 'first_sales_date', 'status', 'icon'];


    public $timestamps = false;


    public function package()
    {
        return $this->belongsTo('Package');
    }

}