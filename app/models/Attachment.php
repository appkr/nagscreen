<?php

class Attachment extends Eloquent
{

    /**
     * @var string
     */
    protected $table = 'attachments';

    /**
     * @var array
     */
    protected $fillable = ['post_id', 'filename', 'size'];

    /**
     * @var bool
     */
    protected $softDelete = true;

    /**
     * @var array
     */
    protected $hidden = ['deleted_at'];


    /**
     * Build relationship between Post and User model
     */
    public function post()
    {
        return $this->belongsTo('Post');
    }


    /**
     * Update attachment foreign key value (which is post_id)
     *
     * @param array $image_field
     * @param int   $post_id
     * @return boolean
     */
    public static function setAttachmentPostIdKey($image_field, $post_id)
    {
        // update post_id of attachment model's
        // which was uploaded with Ajax without post_id
        if (! empty($image_field)) {
            $return = self::whereIn('id', array_flatten($image_field))->update(['post_id' => $post_id]);
        }

        return $return;
    }


    /**
     * @return bool
     */
    public static function deleteOrphanedImages()
    {
        // Delete attachments which don't have post_id.
        // Possibly it was canceled during article compose,
        // so, it is no longer referenced from any article.
        $orphanedAttachments = self::whereNull('post_id')->get();

        if ($orphanedAttachments)
        {
            $savePath = Config::get('setting.image.systemPath');

            foreach ($orphanedAttachments as $attachment) {
                $attachment->forceDelete();

                $path = $savePath . DIRECTORY_SEPARATOR . $attachment->filename;

                if(File::exists($path)) File::delete($path);
            }
        }

        return true;
    }

}