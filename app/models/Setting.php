<?php

class Setting extends Eloquent
{

    /**
     * @var string
     */
    protected $table = 'settings';

    /**
     * @var array
     */
    protected $fillable = ['value'];

}