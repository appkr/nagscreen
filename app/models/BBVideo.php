<?php

class BBVideo extends \Eloquent {

    protected $table = 'bb_videos';

    protected $fillable = ['title', 'author', 'description', 'airplug_id', 'youtube_id', 'duration', 'size', 'locale', 'activated'];

    protected $hidden = ['created_at', 'updated_at', 'activated'];

}