<?php

class AirSocketPoweredApp extends \Eloquent
{

    protected $table = 'apps';

    protected $fillable = ['package', 'name'];
    
    protected $hidden = ['created_at', 'updated_at'];

    public $timestamps = false;


    public function reviews()
    {
        return $this->hasMany('AirSocketPoweredAppReview');
    }

}