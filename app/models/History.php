<?php

use Carbon\Carbon;

class History extends Eloquent
{

    /**
     * @var string
     */
    protected $table = 'histories';


    /**
     * @var array
     */
    protected $guarded = [];


    /**
     * Build relationship with User model
     */
    public function user()
    {
        return $this->belongsTo('User');
    }


    /**
     * Build polymorphic relationship with Trackable Models
     */
    public function trackable()
    {
        return $this->morphTo();
    }


    /**
     * @return mixed
     */
    public function formattedUpdatedAt()
    {
        return ($this->updated_at->diffInDays() > 30)
            ? $this->updated_at->toFormattedDateString()
            : $this->updated_at->diffForHumans();
    }


    /**
     * Fulltext search
     */
    public function scopeSearch($query, $keyword)
    {
        return $query->whereRaw("MATCH(title, content) AGAINST(? IN BOOLEAN MODE)", [$keyword]);
    }

}