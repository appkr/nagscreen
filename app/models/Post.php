<?php

class Post extends Eloquent
{

    /**
     * @var string
     */
    protected $table = 'posts';


    /**
     * @var array
     */
    protected $fillable = ['user_id', 'category_id', 'package_id', 'language', 'title', 'content', 'popup', 'published'];


    /**
     * @var array
     */
    public $rules = [
        'category_id' => 'required|integer',
        'package_id'  => 'required|integer',
        'language'    => 'required',
        'title'       => 'required|min:2',
        'content'     => 'required|min:2'
    ];


    /**
     * @var bool
     */
    protected $softDelete = true;


    /**
     * @var array
     */
    protected $hidden = ['user_id', 'deleted_at', 'popup', 'published'];


    /**
     * @var array
     */
    protected $appends = ['url'];

    /**
     * Build relationship between Post and User model
     */
    public function user()
    {
        return $this->belongsTo('User');
    }


    /**
     * @return mixed
     */
    public function package()
    {
        return $this->belongsTo('Package');
    }


    /**
     * @return mixed
     */
    public function category()
    {
        return $this->belongsTo('Category');
    }


    /**
     * Build relationship between Post and Comment model
     */
    public function comments()
    {
        return $this->hasMany('Comment')->orderBy('id','desc');
    }


    /**
     * Build relationship between Post and Attachment model
     */
    public function attachments()
    {
        return $this->hasMany('Attachment');
    }


    /**
     * Build relationship between Post and History model
     */
    public function histories()
    {
        return $this->morphMany('History', 'trackable');
    }


    /**
     * Check the authenticated user owns a post
     *
     * @return boolean
     */
    public function isOwner()
    {
        return (Auth::check() && Auth::user()->id == $this->user_id) ? true : false;
    }


    /**
     * get url succesor value, to add a arbitrary value to the model
     *
     * @return string
     */
    public function getUrlAttribute()
    {
        return (in_array(Route::currentRouteName(), ['api.post.index', 'api.post.show', 'api.post.popup']))
            ? Config::get('app.url') . '/post/' . $this->id . '/view' : null;
    }


    /**
     * Filter query
     */
    public function scopeFilter($query, $field, $value)
    {
        if ($field == 'package') {
            $package = Package::wherePackage($value)->first();
            return $query->wherePackageId($package->id);
        }
        else if ($field == 'category') {
            $category = Category::whereName($value)->first();
            return $query->whereCategoryId($category->id);
        }
        else {
            return $query->whereLanguage($value);
        }
    }


    /**
     * Filter down 'eula
     */
    public function scopeEula($query)
    {
        return $query->published()->filter('category', 'eula');
    }


    /**
     * Filter published only
     */
    public function scopePublished($query)
    {
        return $query->wherePublished(1);
    }


    /**
     * Filter popup only
     */
    public function scopePopup($query)
    {
        return $query->wherePopup(1);
    }


    /**
     * Filter query against api request
     */
    public function scopeApiFilter($query, array $input)
    {
        if (! empty($input['package_id'])) $query->wherePackageId($input['package_id']);

        if (! empty($input['category_id'])) $query->whereCategoryId($input['category_id']);

        if (! empty($input['language'])) $query->whereLanguage($input['language']);

        return $query;
    }
}