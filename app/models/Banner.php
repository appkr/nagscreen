<?php

class Banner extends \Eloquent
{

    /**
     * @var string
     */
    protected $table = 'banners';


    /**
     * @var array
     */
    protected $fillable = ['package_id', 'launcher_image', 'banner_image', 'excerpt_ko', 'excerpt_en'];


    /**
     * @var array
     */
    public $rules = [
        'package_id'     => 'required|integer|unique:banners',
        'launcher_image' => 'image',
        'banner_image'   => 'image',
        'excerpt_ko'     => 'min:2',
        'excerpt_en'     => 'min:2'
    ];


    /**
     * @var array
     */
    protected $appends = ['launcher_image_url', 'banner_image_url', 'android_url', 'pc_url'];


    /**
     * @return mixed
     */
    public function package()
    {
        return $this->belongsTo('Package');
    }


    /**
     * @return mixed
     */
    public function histories()
    {
        return $this->morphMany('History', 'trackable');
    }


    /**
     * get launcher_image succesor value, to add a arbitrary value to the model
     *
     * @return string
     */
    public function getLauncherImageUrlAttribute()
    {
        return ($this->launcher_image)
            ? Config::get('app.url').'/banners/'.$this->launcher_image
            : null;
    }


    /**
     * @return null|string
     */
    public function getBannerImageUrlAttribute()
    {
        return ($this->banner_image)
            ? Config::get('app.url').'/banners/'.$this->banner_image
            : null;
    }


    /**
     * @return string
     */
    public function getAndroidUrlAttribute()
    {
        return 'market://details?id='.$this->package->package;
    }


    /**
     * @return string
     */
    public function getPcUrlAttribute()
    {
        return 'https://play.google.com/store/apps/details?id='.$this->package->package;
    }

}