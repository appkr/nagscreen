<?php

class Apidoc extends \Eloquent
{

    protected $fillable = ['title', 'content'];


    public $rules = ['title' => 'required|min:2', 'content' => 'required|min:2'];

    /**
     * Fulltext search
     */
    public function scopeSearch($query, $keyword)
    {
        return $query->whereRaw("MATCH(title, content) AGAINST(? IN BOOLEAN MODE)", [$keyword]);
    }


    /**
     * @return mixed
     */
    public function histories()
    {
        return $this->morphMany('History', 'trackable');
    }

}