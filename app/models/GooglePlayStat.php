<?php

class GooglePlayStat extends \Eloquent
{

    protected $table = 'google_play_stats';


    protected $fillable = ['package_id', 'date', 'daily_user_installs', 'daily_user_uninstalls', 'rank_kr'];


    protected $hidden = ['package_id'];


    public $timestamps = false;


    public function package()
    {
        return $this->belongsTo('Package');
    }

}