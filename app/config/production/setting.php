<?php

return [
    'tracker' => [
        'code'   => "ga('create', 'UA-46212331-2', 'airplug.com');",
        'id'     => 'UA-46212331-2',
        'domain' => "airplug.com"
    ],
];