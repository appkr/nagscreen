<?php return [

    // Methods aliases
    //
    // Here you can configure a list of methods to alias and the arguments
    // you want them to take
    //////////////////////////////////////////////////////////////////////

    'photosetsGetList'   => [
        'user_id', 'page', 'per_page'
    ],

    'photosetsGetPhotos' => [
        'photoset_id', 'extras', 'privacy_filter', 'per_page', 'page', 'media'
    ],

    'peopleGetPhotos'    => [
        'user_id', 'safe_search', 'min_upload_date', 'max_upload_date', 'min_taken_date', 'max_taken_date', 'content_type', 'privacy_filter', 'extras', 'per_page', 'page'
    ],

    'collectionsGetTree' => [
        'collection_id', 'user_id'
    ],

];

