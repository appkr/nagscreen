<?php return [

    // API credentials
    //////////////////////////////////////////////////////////////////////

    'api_key'    => 'd401046f072dae687da49de8cb79cf04',
    'api_secret' => 'cef86b3c98788ea3',

    // Cache configuration
    //////////////////////////////////////////////////////////////////////

    'cache'      => [

        // Whether Flickering should cache requests or not
        'cache_requests' => false,

        // The lifetime of a cached request (minutes)
        'lifetime'       => 5,
    ],

];
