<?php

return Cache::remember('setting', 60, function ()
{
    return [
        # Basic project info
        'project'   => [
            'version'     => 1,
            'name'        => 'AirPlug NagScreen',
            'description' => 'Interface to interact with AirPlug app users',
        ],

        # Article images
        'image'     => [
            'systemPath' => public_path() . '/attachments', // attachment storage path
            'webPath'    => 'attachments',
            'width'      => 720,
            'height'     => null
        ],

        # User profile image
        'user'      => [
            'width' => 100,
            'thumb' => 64, //thumbnail width
        ],

        # Available languages list
        'languages' => [
            'en' => 'English',
            'ko' => '한국어'
        ],

        # Email
        'email'     => [
            'to_address' => 'juwonkim@airplug.com',
            'cc_address' => ['hansoo@airplug.com', 'pyo@airplug.com', 'sangyi@airplug.com'],
            'to_name'    => 'Juwon Kim',
        ],

        # SMS
        'sms'       => [
            'send'   => false,
            'from'   => '82234541760',
            'to'     => '821091190979',
            'key'    => '3b372c47',
            'secret' => '24be2f39'
        ],

        # Bitly
        'bitly'     => [
            'user' => 'airplug',
            'key'  => 'R_80fe28e9f2bc3060ec2574ae6f474114'
        ],

        # Push message
        'push'      => [
            'key' => 'AIzaSyAJNSKSzTzeCQRBsuYnkgG1DHE3yhHFgjg', //proj no. 715880485199
        ],

        # Google analytic tracker
        'tracker' => [
            'code'   => "ga('create', 'UA-46212331-2', {'cookieDomain': 'none'});",
            'id'     => 'UA-46212331-2',
            'domain' => 'airplug.com'
        ],
    ];
});