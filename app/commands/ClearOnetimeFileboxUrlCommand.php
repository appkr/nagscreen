<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Carbon\Carbon;

class ClearOnetimeFileboxUrlCommand extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'nagscreen:clear-onetime-filebox-url';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Clear Onetime Filebox Url that is older than specified time frame';

    /**
     * @var OnetimeFileboxUrl
     */
    protected $model;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(OnetimeFileboxUrl $model)
    {
        $this->model = $model;
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $candidates = $this->model->where('expires_at', '<', Carbon::now());

        if (! $count = $candidates->count()) return $this->feedback('Nothing to delete.');

        if (! $candidates->delete()) return $this->feedback('Internal Error. Delete failed.');

        return $this->feedback($count.' entries has been removed.');
    }

    /**
     * Leaves a log and shows a visual feedback
     *
     * @param string $message
     */
    protected function feedback($message)
    {
        $feedback = '['.$this->name.'] '.$message;
        Log::info($feedback);
        return $this->info($feedback);
    }

}
