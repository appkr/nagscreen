<?php

use Illuminate\Console\Command;
use Carbon\Carbon;

class ClearLogs extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'nagscreen:clear-logs';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Clear logs that is older than 1 month';

    /**
     * @var Logs
     */
    protected $model;

    /**
     * @param Logs $model
     */
    public function __construct(\Logs $model)
    {
        $this->model = $model;

        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $candidates = $this->model->where('created_at', '<', Carbon::now()->subMonth());

        if (! $count = $candidates->count())
        {
            return $this->feedback('Nothing to delete.');
        }

        if (! $candidates->delete())
        {
            return $this->feedback('Internal Error. Delete failed.');
        }

        return $this->feedback($count.' entries has been removed.');
    }

    /**
     * Leaves a log and shows a visual feedback
     *
     * @param string $message
     */
    protected function feedback($message)
    {
        $feedback = '['.$this->name.'] '.$message;
        Log::info($feedback);

        return $this->info($feedback);
    }

}
