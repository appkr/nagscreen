<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class FetchGooglePlayDataCommand extends Command {

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'nagscreen:fetch-google-play-data';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fetch Google Play sales, ranks, and reviews data and save to database tables';

    /**
     * @var FetchGooglePlayStatisticsController
     */
    private $controller;

    /**
     * Create a new command instance.
     *
     * @param FetchGooglePlayStatisticsController $controller
     */
    public function __construct(FetchGooglePlayStatisticsController $controller) {
        parent::__construct();
        $this->controller = $controller;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire() {
        $packages = get_active_google_play_packages();

        foreach ($packages as $package) {
            $this->controller->fetchStats($package);
            $this->controller->fetchReviews($package);
        }

        return $this->info('done');
    }

}
