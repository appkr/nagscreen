@extends('layouts.mobile')

@section('content')
    @if ($eula)
        <article>
            {{$eula->content}}
        </article>

        <p class="text-right text-muted">
            <small>Last modified {{$eula->updated_at->diffForHumans()}}</small>
        </p>

    @else
        <acticle>
            <br/>
            <p class="text-center text-danger">Not Found.</p>
            <br/>
        </acticle>
    @endif
@stop