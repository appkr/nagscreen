@extends('layouts.mobile')

@section('content')
    <div class="header-let">
        <h2>
            <a href="{{route('test.index')}}"><small>Test Portal</small></a>  / Intent Test
        </h2>
    </div>

    <hr/>

    <div class="description-let">
        <p class="lead">List of various types of test contents, to see SmartDownloader or Streaming Player receives the thrown intent correctly. The contents are dummy.</p>
    </div>

    <div id="posts-wrapper">
        <article>
            <div class="table-responsive">
                <table class="table table-bordered">
                    <tr class="text-primary">
                        <th class="text-center">Type</th>
                        <th class="text-center">Http</th>
                        <th class="text-center">Https</th>
                    </tr>
            @foreach ($extensions as $extension)
                    <tr>
                        <td>{{ucfirst($extension)}}</td>
                        <td>
                            <a href="/tester/intent/{{$extension}}.{{$extension}}" class='btn btn-default btn-block'>{{$extension}}.{{$extension}}</a>
                        </td>
                        <td>
                            <a href="https://{{Request::server('HTTP_HOST')}}/tester/intent/{{$extension}}.{{$extension}}" class='btn btn-default btn-block'>{{$extension}}.{{$extension}}</a>
                        </td>
                    </tr>
            @endforeach
                </table>
            </div>
        </article>
    </div>
@stop