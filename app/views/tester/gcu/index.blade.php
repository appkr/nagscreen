@extends('layouts.mobile')

@section('style')
    <link href="/css/vendor/google-code-prettify/sunburst.css" rel="stylesheet" />
    <style>
        .form-let{
            width: 95%;
            margin: 1em auto;
        }
    </style>
@stop

@section('content')

    <div class="header-let">
        <h2>
            <small><a href="{{route('test.index')}}">Test Portal</a>  / </small>
            Gcu
        </h2>
    </div>

    <hr/>

    <div class="description-let">
        <p class="lead">Video url extractor. Over 400+ video sites.</p>
        <p>The backend is coined by the notorious <a href="https://github.com/rg3/youtube-dl">youtube-dl</a> command line tool written in python.</p>
    </div>

    <div class="form-let">
        <form action="{{route('test.gcu.extract')}}" method="POST" id="target">
            <input type="hidden" name="_token" value="{{csrf_token()}}"/>
            <div class="form-group">
                <input type="text" name="url" id="url" placeholder="Paste a video page URL here! And then press ENTER!" class="form-control"/>
            </div>
        </form>
    </div>

    <pre class="result-let prettyprint" style="display: none;"></pre>

    <div class="description-let" style="margin-top: 1em;">
        <p class="lead">Current version of youtube-dl library supports...</p>
        <p><small class="extractor-let text-muted" style="margin-top: 1em;">{{$extractors}}</small></p>
    </div>

@stop

@section('script')

    <script>
        (function($) {
            var myForm = $("form#target"),
                resultLet = $("pre.result-let"),
                spinnerHolder = $("h2"),
                loadingIcon = $("<i></i>", {
                    "class": "fa fa-spinner fa-spin"
                }),
                urlInput = $("#url");

            myForm.submit(function(event) {
                event.preventDefault();

                $.ajax({
                    url: myForm.attr('action'),
                    type: "POST",
                    dataType: "json",
                    data: myForm.serialize(),
                    beforeSend: function() {
                        spinnerHolder.append(loadingIcon);
                        urlInput.attr("disabled", "disabled");
                    },
                    success: function(response) {
                        resultLet.text(JSON.stringify(response.data, null, 4));
                        loadingIcon.remove();
                        urlInput.removeAttr("disabled");
                        prettyPrint();
                        resultLet.show();
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        loadingIcon.remove();
                        urlInput.removeAttr("disabled");
                        utility.flash("Error", "Extraction failed !", "danger");
                    }
                });

                return false;
            });
        })(jQuery);
    </script>

    <!-- Google Code Prettify -->
    <script src="/js/vendor/google-code-prettify/prettify.js"></script>
@stop