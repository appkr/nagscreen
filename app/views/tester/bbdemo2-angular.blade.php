@extends('layouts.mobile')

@section('style')

  <style>
  [ng\:cloak], [ng-cloak], [data-ng-cloak], [x-ng-cloak], .ng-cloak, .x-ng-cloak {
    display: none !important;
  }
  #myVideo {
    cursor: pointer;
    width: 100%;
    max-width: 100%;
  }
  .video-let{
    margin: 1em auto;
  }
  </style>

@stop

@section('content')

  <div class="header-let">
    <h3>
      @if (Auth::check())
      <a href="{{route('test.index')}}"><small>Test Portal</small></a> /
      @endif
      <a href="{{route('test.bbdemo2')}}">Demo Videos 2</a>
    </h3>
  </div>

  <hr/>

  <div class="description-let">
    <p class="lead">List of videos provisioned by AirPlug for Aging Test</p>
  </div>

  <div class="row video-let" ng-app="bbDemoApp" ng-controller="VideoController" class="ng-cloak">
    <div class="col-sm-4 video-col">
      <video id="myVideo" poster="@{{items[videoIndex].poster}}" src="@{{items[videoIndex].src}}" title="Click to play/pause video" autoplay controls></video>
    </div>

    <div class="col-sm-8 content-col">
      <h4>
        <a href="@{{items[videoIndex].src}}" id="video-link">@{{items[videoIndex].title}}</a>
      </h4>
      <p class="text-muted">
        <span class="glyphicon glyphicon-time"></span> @{{items[videoIndex].duration}}
        &nbsp;<span class="glyphicon glyphicon-compressed"></span> @{{items[videoIndex].size}}
        &nbsp;<span class="glyphicon glyphicon-hd-video"></span> @{{items[videoIndex].quality}}
      </p>
      <p class="text-muted" id="description">@{{items[videoIndex].description}}</p>
    </div>
  </div>

@stop

@section('script')
  <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.11/angular.js"></script>

  <script>
    (function() {

      var timestamp = {{$timestamp}};

      /** Utility Functions */
      var getYtThumUrl = function(ytId) {
        return "http://i.ytimg.com/vi/" + ytId + "/0.jpg?t=" + timestamp;
      };

      var getApVideoUrl = function(apId) {
        return "http://tfrontg1.airplug.co.kr/media/" + apId + ".mp4?t=" + timestamp;
      };

      var getBitRate = function(size, duration) {
        return numberFormat(Math.round((size * 8) / duration));
      };

      var numberFormat = function(x) {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
      };

      var formatFileSize = function(size) {
        if (isNaN(size)) return 'NaN';

        decr   = 1024;
        step   = 0;
        suffix = ['b', 'KiB', 'MiB', 'GiB'];

        while ((size / decr) > 0.9) {
          size = size / decr;
          step ++;
        }

        return Math.round(size) + suffix[step];
      };

      /** Angular Factory */
      var videosFactory = function($sce) {
        var factory = {};

        factory.getData = function() {
          var videos = {{$videos}},
              items = [];

          for (var i = 0, len = videos.length; i < len; i++) {
            items.push({
              index: i,
              poster: $sce.trustAsResourceUrl(getYtThumUrl(videos[i].youtube_id)),
              src: $sce.trustAsResourceUrl(getApVideoUrl(videos[i].airplug_id)),
              title: videos[i].title,
              duration: videos[i].duration + "sec",
              size: formatFileSize(videos[i].size),
              quality: getBitRate(videos[i].size, videos[i].duration)+ "bps",
              description: videos[i].description
            });
          }

          return items;
        };

        return factory;
      };

      videosFactory.$inject = ["$sce"];

      /** Angular Controller */
      var VideoController = function($scope, videosFactory) {
        $scope.videoIndex = 0;
        $scope.items = [];

        function init() {
          $scope.items = videosFactory.getData();
          $scope.playVideo();
        }

        $scope.playVideo = function() {
          var myVideo = document.getElementById("myVideo");

          myVideo.addEventListener("ended", function() {
            console.log("Video " + $scope.videoIndex + " ended !");
            if ($scope.videoIndex < $scope.items.length) {
              $scope.playVideo();
            }
          });

          myVideo.addEventListener("error", function() {
            console.log("Video " + $scope.videoIndex + " encountered error !");
            if ($scope.videoIndex < $scope.items.length) {
              $scope.playVideo();
            }
          });

          myVideo.addEventListener("click", function() {
            myVideo.paused ? myVideo.play() : myVideo.pause();
          });

          if ($scope.videoIndex < $scope.items.length) {
            $scope.videoIndex ++;
          } else {
            $scope.videoIndex = 0;
          }

          $scope.playVideo();
        };

        init();

      };

      VideoController.$inject = ["$scope", "videosFactory"];

      /** Angular Module */
      angular.module("bbDemoApp", [])
        .factory("videosFactory", videosFactory)
        .controller("VideoController", VideoController);
    })();
  </script>
@stop