@extends('layouts.mobile')

@section('style')

<style>
  #myVideo {
    cursor: pointer;
    width: 100%;
    max-width: 100%;
  }
  .video-let,
  .panel {
    margin: 2.5em auto 0;
  }
  .video-col {
      position: relative;
      padding: 0;
  }
  i.icon {
    display:inline-block;
    width:10px;
    margin-right:10px
  }
  i.fa-spinner {
      font-size: 24px;
  }
  .counter,
  .spinner-holder {
      margin: 0;
      padding: 0.5em 0;
      position: absolute;
  }
  .counter {
      width: 100%;
      top: 0;
      background-color: #000;
      opacity: 0.8;
  }
  .spinner-holder {
      top: 50%;
      left: 50%;
      margin-right: -50%;
      -webkit-transform: translate(-50%, -50%);
      -moz-transform: translate(-50%, -50%);
      -ms-transform: translate(-50%, -50%);
      -o-transform: translate(-50%, -50%);
      transform: translate(-50%, -50%);
  }
  div.js-flash-message {
      display: inline-block;
      margin: auto;
      position: fixed;
      right: 10px;
      bottom: 10px;
      width: 90%;
      /*max-width: 720px;*/
      opacity: .8;
      z-index: 9999;
  }
</style>

@stop

@section('content')

<div class="header-let">
  <h3>
    @if (Auth::check())
      <a href="{{route('test.index')}}"><small>Test Portal</small></a> /
    @endif
    <a href="{{route('test.bbdemo2')}}">Test Videos</a>
  </h3>
</div>

<hr/>

<div class="description-let">
  <p class="lead">Infinite video-show for Aging Test</p>
</div>

<div class="row text-center">
    <button class="btn btn-primary start-test"><i class="fa fa-play icon"></i> START TEST</button>
    <button class="btn btn-default stop-test"><i class="fa fa-stop icon"></i> STOP TEST</button>
</div>

<div class="row">
  <div class="col-sm-12">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title">Test Summary</h3>
      </div>
      <div class="panel-body report-holder">
        <ul class="list-inline">
          <li>This video :</li>
          <li><small class="text-muted">Buffering time (sec)</small> <span class="buffering-time">&nbsp;</span></li>
          <li><small class="text-muted">Accml. buffering time (sec, incl. start/seeking)</small> <span class="accumulated-buffering-time">&nbsp;</span></li>
          <li><small class="text-muted">Buffering count (times, excl. start)</small> <span class="buffering-count">&nbsp;</span></li>
        </ul>
        <ul class="list-inline">
          <li>This test set :</li>
          <li><small class="text-muted">Total buffering time (sec, incl. start/seeking)</small> <span class="total-buffering-time">&nbsp;</span></li>
          <li><small class="text-muted">Total buffering count (times, excl. start)</small> <span class="total-buffering-count">&nbsp;</span></li>
        </ul>
        <ul class="list-inline">
          <li>Test started at : <span class="test-start-time"></span></li>
        </ul>
      </div>
    </div>
  </div>
</div>

<div class="row video-let">
  <div class="col-sm-4 video-col">
    <video id="myVideo" poster="" src="" controls autoplay></video>
    <div class="spinner-holder"></div>
    <h4 class="counter text-center"></h4>
  </div>

  <div class="col-sm-8 content-col">
    <h4>
      @if(Auth::check())
        <a href="" class="video-link"></a>
      @else
        <span href="" class="video-link"></span>
      @endif
    </h4>
    <p class="text-muted">
      <i class="fa fa-clock-o icon"></i> <span class="duration"></span>
      &nbsp;<i class="fa fa-compress icon"></i> <span class="size"></span>
      &nbsp;<i class="fa fa-video-camera icon"></i> <span class="quality"></span>
    </p>
    {{--<p class="text-muted" id="description"></p>--}}
  </div>
</div>

<p class="text-muted"><small>If you want to start from Nth video, pass <code>page</code> as query param. <em>e.g. ?page=200</em></small></p>

@stop

@section('script')
  <script src="/js/DemoVideo.js"></script>
  <script>
    (function() {
      if(navigator.appName.indexOf("Internet Explorer") != -1) {
        alert("Does not support IE. Please use Chrome or FireFox instead !");
        return;
      }

      DemoVideo.init({
          url: "{{route('test.demo-video')}}",
          page: parseInt("{{Input::get("page")}}") || 1
      });
    })();
  </script>
@stop