@extends('layouts.mobile')

@section('style')

    <style>
        .photo-let{
            display: block;
            margin: 1em auto;
            margin-bottom: 2.5em;
        }
        img {
            width: 100%;
            max-width: 100%;
        }
    </style>

@stop

@section('content')

    <div class="header-let">
        <h3>
            <a href="{{route('test.xdemo')}}">Demo Images</a>
            <small>page #{{number_format($photos->getCurrentPage())}}/{{number_format($photos->getLastPage())}}</small>
        </h3>
    </div>

    <hr/>

    <div class="description-let">
        <p class="lead">List of images provided by "Open Source Photography" from Flickr</p>
    </div>

    @if ($pager)
    <form id="form" role="form">
        <div class="input-group">
            <input name="page" id="page" type="text" class="form-control"min="1" max="{{$photos->getLastPage()}}" placeholder="Place the page number ..">
            <span class="input-group-btn">
                <button type="submit" class="btn btn-default"> Go ! </button>
            </span>
        </div>
    </form>
    @endif

    <div class="pager-let">{{$photos->appends(Request::except('page'))->links()}}</div>

    @foreach($photos as $photo)
    <div class="row photo-let">
        <div class="col-sm-4 video-col">
            <img src="{{get_http_addr($photo['url_m'])}}?t={{$timestamp}}"/>
        </div>

        <div class="col-sm-8 content-col">
            <h4>
                {{$photo['title']}}
            </h4>
            <p class="text-muted">
                <span class="glyphicon glyphicon-user"></span> {{$photo['ownername']}} &nbsp;
                <span class="glyphicon glyphicon-calendar"></span> {{time2date($photo['dateadded'])}}
            </p>
            <p class="text-muted">{{strip_tags($photo['description']['_content'])}}</p>
        </div>
    </div>
    @endforeach

    <div class="pager-let">{{$photos->appends(Request::except('page'))->links()}}</div>

@stop

@section('script')
    {{--<script src="/js/vendor/jquery.event.move/jquery.event.move.js"></script>
    <script src="/js/vendor/jquery.event.swipe/jquery.event.swipe.js"></script>
    <script src="/js/vendor/query-string/query-string.js"></script>--}}
    <script>
        (function() {
            /*var context = $("body"),
                currentRoute = "{{route('test.xdemo')}}",
                parsedQueryString = queryString.parse(location.search),
                lastPage = parseInt("{{$photos->getLastPage()}}"),
                moveTo = function(page) {
                    parsedQueryString.page = page;
                    window.location.href = currentRoute + "?" + queryString.stringify(parsedQueryString);
                };

            if (! parsedQueryString.page) parsedQueryString = {page : 1, perPage : 20};

            context.on("swipeleft", function(e) {
                var page = parseInt(parsedQueryString.page) + 1;
                if (page <= lastPage) moveTo(page);
            });

            context.on("swiperight", function(e) {
                var page = parseInt(parsedQueryString.page) - 1;
                if (page > 0) moveTo(page);
            });*/

            $("#form").on("submit", function() {
                var page = $("input[name=page]").val();
                window.location.href = "{{route('test.xdemo')}}?{{$query}}&page=" + page;
            });
        })();
    </script>
@stop