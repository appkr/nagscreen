@extends('layouts.mobile')

@section('style')

    <style>
        .myHtmlVideo,
        .text-to-copy{
            cursor: pointer;
        }
        .video-let{
            margin: 1em auto;
        }
        img {
            width: 100%;
            max-width: 100%;
        }
    </style>

@stop

@section('content')

    <div class="header-let">
        <h3>
            @if (Auth::check())
            <a href="{{route('test.index')}}"><small>Test Portal</small></a> /
            @endif
            <a href="{{route('test.bbdemo')}}">Demo Videos</a>
            <small>page #{{number_format($videos->getCurrentPage())}}/{{number_format($videos->getLastPage())}}</small>
        </h3>
    </div>

    <hr/>

    <div class="description-let">
        <p class="lead">List of videos provisioned by AirPlug for demo purpose</p>
    </div>

    <div class="pager-let">{{$videos->links()}}</div>

    @foreach($videos as $video)

    <div class="row video-let">
        <div class="col-sm-4 video-col">
            @if(Auth::check())
            <video width="100%" poster="{{get_youtube_thumbnail_url($video['youtube_id'])}}?t={{$timestamp}}" class="myHtmlVideo" title="Click to play/pause video">
                <source src="{{get_bb_video_url($video['airplug_id'])}}?t={{$timestamp}}" type="video/mp4" />
            </video>
            @else
            <a href="{{get_bb_video_url($video['airplug_id'])}}">
                <img src="{{get_youtube_thumbnail_url($video['youtube_id'])}}?t={{$timestamp}}"/>
            </a>
            @endif
        </div>

        <div class="col-sm-8 content-col">
            <h4>
                <a href="{{get_bb_video_url($video['airplug_id'])}}">
                    {{$video['title']}}
                </a>
            </h4>
            <p class="text-muted">
                <span class="glyphicon glyphicon-time"></span> {{$video['duration']}} sec
                <span class="glyphicon glyphicon-compressed"></span> {{format_filesize($video['size'])}}
                <span class="glyphicon glyphicon-hd-video"></span> {{get_bb_video_bitrate($video['size'], $video['duration'])}} bps
            </p>
            <p class="text-muted">{{$video['description']}}</p>
        </div>
    </div>
    @endforeach

    <div class="pager-let">{{$videos->links()}}</div>

@stop

@section('script')
    {{--<script src="/js/vendor/jquery.event.move/jquery.event.move.js"></script>
    <script src="/js/vendor/jquery.event.swipe/jquery.event.swipe.js"></script>
    <script src="/js/vendor/query-string/query-string.js"></script>--}}
    <script>
        (function() {
            $(".myHtmlVideo").on("click", function() {
                $(this).get(0).paused ? $(this).get(0).play() : $(this).get(0).pause();
            });
            /*var context = $("body"),
                currentRoute = "{{route('test.bbdemo')}}",
                parsedQueryString = queryString.parse(location.search),
                lastPage = parseInt("{{$videos->getLastPage()}}"),
                moveTo = function(page) {
                    parsedQueryString.page = page;
                    window.location.href = currentRoute + "?" + queryString.stringify(parsedQueryString);
                };

            if (! parsedQueryString.page) parsedQueryString = {page : 1};

            context.on("swipeleft", function(e) {
                var page = parseInt(parsedQueryString.page) + 1;
                if (page <= lastPage) moveTo(page);
            });

            context.on("swiperight", function(e) {
                var page = parseInt(parsedQueryString.page) - 1;
                if (page > 0) moveTo(page);
            });*/
        })();
    </script>
@stop