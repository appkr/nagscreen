@extends('layouts.mobile')

@section('style')
  <style>
    .iframe-holder,
    .panel {
      display: block;
      margin: 2.5em auto 0;
    }
    .iframe-holder {
      position: relative;
      margin-bottom: 2.5em;
    }
    .site-url {
      overflow: hidden;
    }
    i.icon {
      display:inline-block;
      width:10px;
      margin-right:10px
    }
    i.fa-spinner {
      color: #00bc8c;
      font-size: 24px;
    }
    .counter-holder,
    .spinner-holder {
      padding: 0.5em 0;
      position: absolute;
    }
    .counter-holder {
      margin: 0;
      width: 100%;
      top: 0;
      background-color: #000;
      opacity: 0.8;
    }
    .spinner-holder {
      margin: 0 -50% 0 0;
      top: 50%;
      left: 50%;
      -webkit-transform: translate(-50%, -50%);
      -moz-transform: translate(-50%, -50%);
      -ms-transform: translate(-50%, -50%);
      -o-transform: translate(-50%, -50%);
      transform: translate(-50%, -50%);
    }
    div.js-flash-message {
      display: inline-block;
      margin: auto;
      position: fixed;
      right: 10px;
      bottom: 10px;
      width: 90%;
      opacity: .8;
      z-index: 9999;
    }
  </style>
@stop

@section('content')
  <div class="header-let">
    <h3>
      @if (Auth::check()) <a href="{{route('test.index')}}"><small>Test Portal</small></a> / @endif
      <a href="{{route('test.web')}}">Web Walker</a>
    </h3>
  </div>

  <hr/>

  <div class="description-let">
    <p class="lead">
        Infinite web-loading for Aging Test
    </p>
  </div>

  <div class="row text-center">
    <button class="btn btn-primary start-test"><i class="fa fa-play icon"></i> START TEST</button>
    <button class="btn btn-default stop-test"><i class="fa fa-stop icon"></i> STOP TEST</button>
  </div>

  <div class="row">
    <div class="col-sm-12">
      <div class="panel panel-default">
        <div class="panel-heading">
          <h3 class="panel-title">Test Summary</h3>
        </div>
        <div class="panel-body report-holder">
          <ul class="list-inline">
            <li>This site :</li>
            <li>
              <small class="text-muted">Loading time (sec)</small>
              <span class="loading-time">&nbsp;</span>
            </li>
          </ul>
          <ul class="list-inline">
            <li>This test set :</li>
            <li><small class="text-muted">Average loading time (sec)</small> <span class="avg-loading-time">&nbsp;</span></li>
          </ul>
          <ul class="list-inline">
            <li>Test started at : <span class="test-start-time"></span></li>
          </ul>
          <p><small><code>CAVEAT</code> This tool relies on iframe to load a random webpage. Same Origin enforced site can not be displayed.</small></p>
        </div>
      </div>
    </div>
  </div>

  <div class="iframe-holder">
    <iframe src="" frameborder="0" width="100%" height="600" scrolling="no" id="myFrame"></iframe>
    <p class="counter-holder">
      <strong class="counter text-info">&nbsp;</strong>
      <span class="site-url">&nbsp;</span>
    </p>
    <div class="spinner-holder"></div>
  </div>

  <p class="text-muted"><small>To manipulate the speed, pass <code>sleep</code> as query param (default 3000miliseconds). <em>e.g. ?sleep=5000</em></small></p>
  <p class="text-muted"><small>In case of more than 5 consecutive network errors, it automatically restarts the test in 60 seconds.</small></p>

@stop

@section('script')
  <script src="/js/iframeResizer.min.js"></script>
  <script src="/js/Walker.js"></script>
  <script>
    (function() {
      if(navigator.appName.indexOf("Internet Explorer") != -1) {
        alert("Does not support IE. Please use Chrome or FireFox instead !");
        return;
      }

      $('iframe').iFrameResize({
        checkOrigin: false
      });

      Walker.init({
        sleep: parseInt("{{Input::get('sleep')}}") || 3000,
        seed: "https://wordpress.com/fresh/"/*,
        apiEndPoint: "http://localhost:8000/test/get-random-url"*/
      });
    })();
  </script>
@stop