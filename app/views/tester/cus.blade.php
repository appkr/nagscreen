@extends('layouts.mobile')

@section('content')
    <div class="header-let">
        <h2>
            <a href="{{route('test.index')}}"><small>Test Portal</small></a>  / Custom Url Scheme Test
        </h2>
    </div>

    <hr/>

    <div class="description-let">
        <p class="lead">Custom Url Scheme Test</p>
    </div>

    <div id="posts-wrapper">
        <div><a href="airsocket://test" class='btn btn-default btn-block'>Launch Tester(Plain)</a></div><br/>
        <div><a href="intent://test#Intent;scheme=airsocket;action=android.intent.action.VIEW;category=android.intent.category.BROWSABLE;package=com.airplug.service;end" class='btn btn-default btn-block'>Launch Tester(Intent)</a></div><br/>
        <div><a href="airsocket://playvideo?cache=http%3A%2F%2Ftfrontg1.airplug.co.kr%2Fmedia%2FaEasHztgvh8.mp4" class='btn btn-default btn-block'>Launch AirPlayer(Plain)</a></div><br/>
        <div><a href="intent://playvideo?cache=http%3A%2F%2Ftfrontg1.airplug.co.kr%2Fmedia%2FaEasHztgvh8.mp4#Intent;scheme=airsocket;action=android.intent.action.VIEW;category=android.intent.category.BROWSABLE;package=com.airplug.mao.agent;end" class='btn btn-default btn-block'>Launch AirPlayer(Intent)</a></div><br/>
        <div><a href="naversearchapp://default?version=5" class='btn btn-default btn-block'>Launch Naver(Plain)</a></div><br/>
        <div><a href="intent://search?qmenu=voicerecg&version=5#Intent;scheme=naversearchapp;action=android.intent.action.VIEW;category=android.intent.category.BROWSABLE;package=com.nhn.android.search;end" class='btn btn-default btn-block'>Launch Naver VoiceRecorder(Intent)</a></div><br/>

    </div>
@stop