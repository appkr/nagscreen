@extends('layouts.mobile')

@section('content')
    <div class="header-let">
        <h2>AirPlug Test Portal</h2>
    </div>

    <hr/>

    @foreach($tools as $tool)

    <dl>
        @if($tool['type'] == 'url')

        <h4>{{$tool['title']}}</h4>
        <dd>{{$tool['url']}}</dd>
        <dd>{{$tool['secure']}}</dd>
        <dd class="text-muted">{{$tool['description']}}</dd>

        @else

        <h4><a href="{{$tool['url']}}">{{$tool['title']}}</a></h4>
        <dd class="text-muted">{{$tool['description']}}</dd>

        @endif

    </dl>

    @endforeach

    {{--<div class="code-let">
        <h4>How to simulate GET/POST Test</h4>
        <p>When you send a GET or POST test request, you can send querystring or formdata to simulate test environment.</p>

        <dl>
            <dt>json=true</dt>
            <dd class="text-muted">
                If you want a json notation of response...
            </dd>
            <dt>min={int}</dt>
            <dd class="text-muted">
                Minimum sleep time at server. 0~9 in second.
            </dd>
            <dt>max={int}</dt>
            <dd class="text-muted">
                Maximum sleep time at server. 1~10 in second.
            </dd>
            <dt>timestamp={int}</dt>
            <dd class="text-muted">
                For test purpose, you may want to send your request timestamp to see how long it takes from request to server arrival.
                By doing so you should be careful, your terminal system time is not identical to the server's one.
                With the timestamp given by you, the server does nothing but echoing.
            </dd>
        </dl>

        <pre class="prettyprint">
# GET /test/response/get?max=3

Respond to GET request. Request arrived at 1408020297545, server sleep 3000ms, response thrown at 1408020301746
        </pre>

        <pre class="prettyprint">
# POST /test/response/post
# form-data json=1&max=3

{
    "data": {
        "code": 200,
        "message": "Respond to POST request",
        "timestamp": null,
        "received": 1408020518244,
        "sleep": 2000,
        "respond": 1408020521417
    }
}
        </pre>
    </div>--}}

    <div class="code-let">
        <h4>How to call http api to save your test result at the server.</h4>
        <p>
            2 methods, how to tackle it.
            Either way is fine, though later method --sending multiple entries at a batch-- is much safer and preferred.
        </p>
        <p>
            1 entry(url-encoded-form-data):<br/>
            <span class="text-muted">
                Which means you send the log to the server after at each test to save it.
                Presumably it should only be possible through programatically.
                Because it throws test result right away, the application doesn't need to keep the result in the local storage.
                The POST payload should be a url-encoded-form-data.
                But, one downside is that think about the situation when the test fails due to weak network health,
                do you think the log save api request succeed?
            </span>
        </p>
        <p>
            Multi entries(json array):<br/>
            <span class="text-muted">
                You don't throw the logs immediately to the server after the test is done.
                Instead, you can defer sending the test result to the server,
                to throw it in a batch in every certain interval or even after finishing all the test set.
                One downside is that the application has to keep the test result in a local storage to send it later.
                The POST payload should be a json array.
            </span>
        </p>

        <dl>
            <dt>API Endpoint (<strong class="text-warning">https is a must!</strong>)</dt>
            <dd class="text-muted">
                POST /test/logs/{testName}. <br/><br/>
                You can artibrarily make your {testName}. e.g.
                <code>ap-coverage</code>, <code>test-dummy</code>, ... or <code>whatever</code>.
                But don't mix-up different {testName} in a same test project,
                because it will be used as a filter at the log viewer,
                so that you can filter your test result quickly
            </dd>
            <dt>Payload</dt>
            <dd class="text-muted">
                Send your log as an POST body depending on the method you choose.
            </dd>
        </dl>

        <pre class="prettyprint">
# Single log entry at a time(Payload data should be an url-encoded-form-data)

POST /test/logs/ap-coverage
Host: promote.airplug.com
Authorization: Basic anV3b25raW1AYWlycGx1Zy5jb206cGFzc3dvcmQ= # Optional (Nagscreen user id and password)

manufacturer=Samsung&model=ipsum1&operator=SKTelecom&os=16&ssid=T+wifi+zone&bssid=1E:BB:91:6E:6B:58&security_type=EAP&timestamp=1408463903&rtt=410&sleep=0&proxy_setting=1&proxy_present=1&communication=1&message=
        </pre>

        <pre class="prettyprint">
# Multiple log entries at a time(Request header should have a "Content-type: application/json" field, and the payload data should be a json array)

POST /test/logs/ap-coverage
Host: promote.airplug.com
Authorization: Basic anV3b25raW1AYWlycGx1Zy5jb206cGFzc3dvcmQ= # Optional
Content-type: application/json

[
    {
        "timestamp": 1408463903, // test start or finished timestamp
        "method": "http-get", // test method
        "manufacturer": "Samsung",
        "model": "eius1",
        "operator": "SKTelecom",
        "os": 19,
        "ssid": "T wifi zone_secure",
        "bssid": "11:74:94:2A:E7:47",
        "securityType": "EAP",
        "rtt": 315, // round trip time calculated at your application
        "sleep": 0, // server sleep time, which is extracted from server responsed
        "proxySetting": 0, // proxy setting 0|1
        "proxyPresent": 1, // ams-x presence &amp; pass 0|1
        "communication": 1, // get response from server or not 0|1
        "message": "Any message" // string|null
    },
    {
        "timestamp": 1408464487,
        "method": "http-post",
        "manufacturer": "LG",
        "model": "saepe2",
        "operator": "olleh",
        "os": 17,
        "ssid": "soluta",
        "bssid": "62:E3:32:08:01:9B",
        "securityType": "WEP",
        "rtt" : 1363,
        "sleep" : 1000,
        "proxySetting": 1,
        "proxyPresent": 1,
        "communication": 1,
        "message": "Any message"
    }
]
        </pre>

        <pre class="prettyprint">
# return in case of success

{data: true}
        </pre>

    </div>
@stop

@section('script')
    <!-- Google Code Prettify -->
    <script src="https://google-code-prettify.googlecode.com/svn/loader/run_prettify.js?skin=sunburst"></script>
@stop