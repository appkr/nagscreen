@extends('layouts.mobile')

@section('style')
    
    <style>
        .myHtmlVideo,
        .text-to-copy{
            cursor: pointer;
        }
        .video-let{
            margin: 1em auto;
        }
    </style>

@stop

@section('content')

    <div class="header-let">
        <h2>
            <a href="{{route('test.index')}}"><small>Test Portal</small></a>  / 
            <a href="{{route('test.video')}}">BB DEMO Videos</a>
        </h2>
    </div>

    <hr/>

    <div class="description-let">
        <p class="lead">List of AirPlug provisioned test videos that are capable of Blended Boost regardless of the terminal capability.</p>
    </div>

    <div class="pager-let">{{$videos->links()}}</div>

    @foreach($videos as $video)

    <div class="row video-let">
        <div class="col-sm-4 video-col">
            <video width="100%" poster="{{get_youtube_thumbnail_url($video['youtube_id'])}}" class="myHtmlVideo" title="Click to play/pause video">
                <source src="{{get_bb_video_url($video['airplug_id'])}}" type="video/mp4" />
                Your browser does not support html5 video.
                <!-- <img src="{{get_youtube_thumbnail_url($video['youtube_id'])}}" class="img-responsive"> -->
            </video>
        </div>

        <div class="col-sm-8 content-col">
            <h4><a href="{{get_youtube_page_url($video['youtube_id'])}}">{{$video['title']}} <span class="glyphicon glyphicon-new-window" target="_blank"></span></a></h4>
            <p class="text-muted">
                <span class="glyphicon glyphicon-time"></span> {{$video['duration']}} sec
                <span class="glyphicon glyphicon-compressed"></span> {{format_filesize($video['size'])}}
                <span class="glyphicon glyphicon-hd-video"></span> {{get_bb_video_bitrate($video['size'], $video['duration'])}} bps
            </p>
            <p class="text-muted">{{$video['description']}}</p>
            <p class="text-to-copy" data-clipboard-text="{{get_bb_video_url($video['airplug_id'])}}"><span class="glyphicon glyphicon-film"></span> {{get_bb_video_url($video['airplug_id'])}}</p>
            <p>
                <a class="btn btn-default" href="{{get_bb_video_url($video['airplug_id'])}}">Video Link</a>
                <a class="btn btn-default custom-url-button" href="{{get_bb_custom_url($video['airplug_id'])}}" data-custom-url="{{get_bb_custom_url($video['airplug_id'])}}">Custom Url Scheme</a>
            </p>
        </div>
    </div>
    @endforeach

    <div class="pager-let">{{$videos->links()}}</div>

@stop

@section('script')
    <script src="/js/vendor/zeroclipboard/ZeroClipboard.min.js"></script>

    <script>
        (function() {
            var clientText = new ZeroClipboard( $(".text-to-copy"), { 
                moviePath: "/js/vendor/zeroclipboard/ZeroClipboard.swf", 
                debug: false }),
                frame_id = '__check_app__',
                $iframe = $('#' + frame_id),
                userAgent = navigator.userAgent.toLocaleLowerCase();

            $(".myHtmlVideo").on("click", function() {
                $(this).get(0).paused ? $(this).get(0).play() : $(this).get(0).pause();
            });

            $(".custom-url-button").on("click", function() {
                if (! $iframe[0]) $iframe = $('<iframe id="' + frame_id + '" />').hide().appendTo('body');
                if (/(?=.*android)(?=.*chrome)/.test(userAgent)) {
                    window.location.href = $(this).data('custom-url');
                    return;
                }
                $iframe.attr("src", $(this).data('custom-url'));
            });

            clientText.on("aftercopy", function(event) { 
                alert('Url copied !');
            });
        })();
    </script>
@stop