@extends('layouts.mobile')

@section('content')

    <div class="header-let">

        <h2>
            <small><a href="{{route('test.index')}}">Test Portal</a>  / </small>

            @if(isset($filter) && isset($question))

            <small>
                <a href="{{route('test.logs.index')}}" class="my-tooltip" data-toggle="tooltip" data-placement="right" title="Clear Filter">Test Logs</a> /
            </small>
            {{$question}}

            @else

            <a href="{{route('test.logs.index')}}" class="my-tooltip" data-toggle="tooltip" data-placement="right"  title="Clear Filter">Test Logs</a>

            @endif

        </h2>

    </div>

    <hr/>

    <div class="row filter-let">

        <div class="col-md-8 col-xs-12">
            <ul class="list-inline lead">

                @foreach($modules as $module)

                <li>
                    <a href="{{route('test.logs.index')}}?filter=module&question={{$module}}" class="my-tooltip" data-toggle="tooltip" data-placement="top" title="List for this module only">
                        {{$module}}
                    </a>
                    <a href="{{route('test.logs.download', $module)}}">
                        <i class="fa fa-download my-tooltip" data-toggle="tooltip" data-placement="top" title="Export this module to CSV"></i>
                    </a>
                    @if (is_admin())
                    <a href="javascript:undefined" class="delete-hook" data-module-name="{{$module}}">
                        <i class="fa fa-trash-o my-tooltip" data-toggle="tooltip" data-placement="top" title="Delete this module"></i>
                    </a>
                    @endif
                </li>

                @endforeach

            </ul>
        </div>

        <div class="col-md-4 col-xs">
            <form action="{{route('test.logs.index')}}" method="get" role="search">
                <div class="form-group">
                    <input type="hidden" name="filter" value="log">
                    <label class="sr-only" for="question">Search log content: </label>
                    <input type="text" name="question" placeholder="Search log content" class="form-control"/>
                </div>
            </form>
        </div>

    </div>

    <div class="pager-let">{{$testLogs->appends(Request::except('page'))->links()}}</div>

    @if ($numRecord = $testLogs->getTotal())

    <p class="count-let text-center">
        <small class="text-muted">
            <i class="fa fa-tachometer"></i> Total <strong class="text-danger">{{number_format($numRecord)}}</strong> {{Str::plural('record', $numRecord)}}
        </small>
    </p>

    @endif

    <div class="table-responsive">
        <table class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th class="col-md-2 col-sm-2">{{link_for_sort('test.logs.index', 'module', 'Module')}} {{format_active_sort('module')}}</th>
                    <th class="col-md-8 col-sm-8">Log</th>
                    <th class="col-md-2 col-sm-2">{{link_for_sort('test.logs.index', 'created_at', 'Datetime')}} {{format_active_sort('created_at')}}</th>
                </tr>
            </thead>

            <tbody>

            @if($testLogs->count())
                @foreach($testLogs as $log)

                <tr>
                    <td>{{$log->module}}</td>
                    <td><a href="{{route('test.logs.show', $log->id)}}" class="my-popover" data-toggle="popover" data-placement="top" data-content="{{{$log->log}}}">{{$log->log}}</td>
                    <td>{{format_timestring($log->created_at)}}</td>
                </tr>

                @endforeach
            @else

                <tr>
                    <td colspan="3"><p class="text-center">Not Found !</p></td>
                </tr>

            @endif

            </tbody>
        </table>
    </div>

    <div class="pager-let">{{$testLogs->appends(Request::except('page'))->links()}}</div>

@stop

@section('script')

    <script>
        (function() {
            var ua = navigator.userAgent.toLocaleLowerCase();

            if (! (/android/.test(ua))) {
                $(".my-tooltip").tooltip();

                $(".my-popover").popover({
                    trigger: "hover",
                    container: "table"
                });
            }

            $("a.delete-hook").on("click", function(event) {
                // event.preventDefault();
                // event.stopPropagation();

                if(confirm("ARE YOU SURE TO DELETE THIS MODULE ?")) {
                    utility.fetch("/test/logs/" + $(this).data("module-name"), {
                        "_method": "DELETE",
                        "_token": "{{csrf_token()}}"
                    }).done(function(response) {
                        if (response.data) {
                            utility.flash("Success", "Deleted !", "success");
                        } else {
                            utility.flash("Error", response.error.message || "Unknown error !", "danger");
                        }

                        setTimeout(function() {
                            window.location.reload();
                        }, 1000);
                    });
                }
            });
        })();
    </script>

@stop

