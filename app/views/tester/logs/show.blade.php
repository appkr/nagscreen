@extends('layouts.mobile')

@section('content')
    <div class="header-let">
        <h2>
            <small><a href="{{route('test.index')}}">Test Portal</a>  /
            <a href="{{route('test.logs.index')}}" title="Clear filter">Test Logs</a></small> / Details
        </h2>
    </div>

    <hr/>

    <div class="content-col">
        <ul>
    @foreach($testLog as $field => $value)
            <li>{{$field}} : {{$value}}</li>
    @endforeach
        </ul>
    </div>

@stop