@extends('layouts.mobile')

@section('style')

<style>
#myVideo {
  cursor: pointer;
  width: 100%;
  max-width: 100%;
}
.video-let{
  margin: 1em auto;
}
</style>

@stop

@section('content')

<div class="header-let">
  <h3>
    @if (Auth::check())
    <a href="{{route('test.index')}}"><small>Test Portal</small></a> /
    @endif
    <a href="{{route('test.bbdemo2')}}">Demo Videos 2</a>
  </h3>
</div>

<hr/>

<div class="description-let">
  <p class="lead">List of videos provisioned by AirPlug for Aging Test</p>
</div>

<div class="row video-let">
  <div class="col-sm-4 video-col">
    <video id="myVideo" poster="" src="" title="Click to play/pause video" controls autoplay></video>
  </div>

  <div class="col-sm-8 content-col">
    <h4>
      <a href="" id="video-link"></a>
    </h4>
    <p class="text-muted">
      <span class="glyphicon glyphicon-time"></span>
      &nbsp;<span class="glyphicon glyphicon-compressed"></span>
      &nbsp;<span class="glyphicon glyphicon-hd-video"></span>
    </p>
    <p class="text-muted" id="description"></p>
  </div>
</div>

@stop

@section('script')
<script>
(function() {
  var videoIndex = 0,
  videos = {{$videos}},
  timestamp = {{$timestamp}},
  getYtThumUrl = function(ytId) {
    return "http://i.ytimg.com/vi/" + ytId + "/0.jpg?t=" + timestamp;
  },
  getApVideoUrl = function(apId) {
    return "http://tfrontg1.airplug.co.kr/media/" + apId + ".mp4?t=" + timestamp;
  },
  getBitRate = function(size, duration) {
    return numberFormat(Math.round((size * 8) / duration));
  },
  numberFormat = function(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  },
  formatFileSize = function(size) {
    if (isNaN(size)) return 'NaN';

    decr   = 1024;
    step   = 0;
    suffix = ['b', 'KiB', 'MiB', 'GiB'];

    while ((size / decr) > 0.9) {
      size = size / decr;
      step ++;
    }

    return Math.round(size) + suffix[step];
  },
  videoTag = $("video").first(),
  linkTag = $("a#video-link").first(),
  durationTag = $(".glyphicon-time"),
  sizeTag = $(".glyphicon-compressed"),
  qualityTag = $(".glyphicon-hd-video"),
  descriptionTag = $("p#description");

  function playVideo () {
    var item = videos[videoIndex],
    myVideo = document.getElementById("myVideo");

    videoTag.attr("poster", getYtThumUrl(item.youtube_id)).attr("src", getApVideoUrl(item.airplug_id));
    linkTag.attr("href", getApVideoUrl(item.airplug_id));
    linkTag.text(item.title);
    durationTag.text(item.duration + "sec");
    sizeTag.text(formatFileSize(item.size));
    qualityTag.text(getBitRate(item.size, item.duration) + "bps"),
    descriptionTag.text(item.description);

    myVideo.addEventListener("ended", function() {
      console.log("Video " + videoIndex + " ended !");
      if (videoIndex < videos.length) {
        playVideo();
      }
    });

    myVideo.addEventListener("error", function() {
      console.log("Video " + videoIndex + " encountered error !");
      if (videoIndex < videos.length) {
        playVideo();
      }
    });

    myVideo.addEventListener("click", function() {
      myVideo.paused ? myVideo.play() : myVideo.pause();
    });

    if (videoIndex < videos.length) {
      videoIndex ++;
    } else {
      videoIndex = 0;
    }

    myVideo.play();
  }

  playVideo();
})();
</script>
@stop