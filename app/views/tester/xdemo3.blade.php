@extends('layouts.mobile')

@section('style')
  <style>
    .photo-let,
    .panel {
      display: block;
      margin: 2.5em auto 0;
    }
    .image-holder {
      position: relative;
      padding: 0;
    }
    img {
      width: 100%;
      max-width: 100%;
    }
    i.icon {
      display:inline-block;
      width:10px;
      margin-right:10px
    }
    i.fa-spinner {
      font-size: 24px;
    }
    .counter,
    .indicator-holder {
      margin: 0;
      padding: 0.5em 0;
      position: absolute;
    }
    .counter {
      width: 100%;
      top: 0;
      background-color: #000;
      opacity: 0.8;
    }
    .indicator-holder {
      top: 50%;
      left: 50%;
      margin-right: -50%;
      -webkit-transform: translate(-50%, -50%);
      -moz-transform: translate(-50%, -50%);
      -ms-transform: translate(-50%, -50%);
      -o-transform: translate(-50%, -50%);
      transform: translate(-50%, -50%);
    }
    div.js-flash-message {
      display: inline-block;
      margin: auto;
      position: fixed;
      right: 10px;
      bottom: 10px;
      width: 90%;
      opacity: .8;
      z-index: 9999;
    }
  </style>
@stop

@section('content')
  <div class="header-let">
    <h3>
      @if (Auth::check()) <a href="{{route('test.index')}}"><small>Test Portal</small></a> / @endif
      <a href="{{route('test.xdemo3')}}">Test Images</a>
    </h3>
  </div>

  <hr/>

  <div class="description-let">
    <p class="lead">
        Infinite image-show for Aging Test
    </p>
  </div>

  <div class="row text-center">
    <button class="btn btn-primary start-test"><i class="fa fa-play icon"></i> START TEST</button>
    <button class="btn btn-default stop-test"><i class="fa fa-stop icon"></i> STOP TEST</button>
  </div>

  <div class="row">
    <div class="col-sm-12">
      <div class="panel panel-default">
        <div class="panel-heading">
          <h3 class="panel-title">Test Summary</h3>
        </div>
        <div class="panel-body report-holder">
          <ul class="list-inline">
            <li>This image :</li>
            <li><small class="text-muted">Loading time (sec)</small> <span class="loading-time">0</span></li>
            <li><small class="text-muted"><i class="fa fa-compress icon"></i></small> <span id="image-file-size">0</span></li>
            {{--<li><small class="text-muted"><i class="fa fa-tachometer icon"></i></small> <span id="download-speed"></span></li>--}}
          </ul>
          <ul class="list-inline">
            <li>This test set :</li>
            <li><small class="text-muted">Average loading time (sec)</small> <span class="avg-loading-time">0</span></li>
          </ul>
          <ul class="list-inline">
            <li>Test started at : <span class="test-start-time"></span></li>
          </ul>
        </div>
      </div>
    </div>
  </div>

  <div class="row photo-let">
    <div class="col-sm-4 image-holder">
      <img src="" id="image"/>
      <div class="indicator-holder"></div>
      <h4 class="counter text-center"></h4>
    </div>

    <div class="col-sm-8 content-col">
      <h4 id="title"></h4>
    </div>
  </div>

  <p class="text-muted"><small>To manipulate the speed, pass <code>sleep</code> as query param (default 1000miliseconds). <em>e.g. ?sleep=5000</em></small></p>
  <p class="text-muted"><small>If you want to start from Nth image, pass <code>page</code> as query param. <em>e.g. ?page=200</em></small></p>
  <p class="text-muted"><small>In case of more than 5 consecutive network errors, it automatically restarts the test in 60 seconds.</small></p>

@stop

@section('script')
  <script src="/js/DemoImage.js"></script>
  <script>
    (function() {
      if(navigator.appName.indexOf("Internet Explorer") != -1) {
        alert("Does not support IE. Please use Chrome or FireFox instead !");
        return;
      }

      DemoImage.init({
          sleep: getSleepParam(),
          page: parseInt("{{Input::get("page")}}") || 1
      });

      function getSleepParam() {
        var sleep = parseInt("{{Input::get("sleep")}}");

        return (sleep < 1000 || isNaN(sleep)) ? 1000 : sleep;
      }
    })();
  </script>
@stop