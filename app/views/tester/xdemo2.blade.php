@extends('layouts.mobile')

@section('style')
    <link rel="stylesheet" href="/css/vendor/blueimp/blueimp-gallery.min.css">
    <link rel="stylesheet" href="/css/vendor/blueimp/bootstrap-image-gallery.css">
    <link rel="stylesheet" href="/css/vendor/blueimp/demo.css">
@stop

@section('content')
    <div class="header-let">
        <h3>
            <a href="{{route('test.index')}}"><small>Test Portal</small></a> /
            <a href="{{route('test.xdemo2')}}">Demo Images 2</a>
        </h3>
    </div>

    <hr/>

    <div class="description-let">
        <p class="lead">List of images hosted by AirPlug for "Japanese Device Capability" test</p>
    </div>

    <div id="links">
    @foreach($photos as $photo)
        <a href="{{$baseUrl}}{{$photo->image}}?t={{$timestamp}}" title="{{$photo->title or ''}}" data-gallery>
            <img src="{{$baseUrl}}{{$photo->thumbnail}}?t={{$timestamp}}">
        </a>
    @endforeach
    </div>

    <div id="blueimp-gallery" class="blueimp-gallery">
        <!-- The container for the modal slides -->
        <div class="slides"></div>
        <!-- Controls for the borderless lightbox -->
        <h3 class="title"></h3>
        <a class="prev">‹</a>
        <a class="next">›</a>
        <a class="close">×</a>
        <a class="play-pause"></a>
        <ol class="indicator"></ol>
        <!-- The modal dialog, which will be used to wrap the lightbox content -->
        <div class="modal fade">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" aria-hidden="true">&times;</button>
                        <h4 class="modal-title"></h4>
                    </div>
                    <div class="modal-body next"></div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default pull-left prev">
                            <i class="glyphicon glyphicon-chevron-left"></i>
                            Previous
                        </button>
                        <button type="button" class="btn btn-primary next">
                            Next
                            <i class="glyphicon glyphicon-chevron-right"></i>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('script')
    <script src="/js/vendor/blueimp/jquery.blueimp-gallery.min.js"></script>
    <script src="/js/vendor/blueimp/bootstrap-image-gallery.js"></script>

    <script>
        (function() {
            $('#blueimp-gallery')
              .data({
                'useBootstrapModal': false,
                'fullScreen': true,
                'startSlideshow': true,
                'slideshowInterval': 4000
              })
              .toggleClass('blueimp-gallery-controls', true);
        })();
    </script>
@stop