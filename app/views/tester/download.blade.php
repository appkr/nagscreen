@extends('layouts.mobile')

@section('content')
    <div class="header-let">
        <h2>
            <a href="{{route('test.index')}}"><small>Test Portal</small></a>  / Download Test
        </h2>
    </div>

    <hr/>

    <div class="description-let">
        <p class="lead">List of various types of test contents, to see SmartDownloader or AirPlayer/StreamingPlayer is invokable and workable. The contents are real.</p>
    </div>

    <div id="posts-wrapper">
        <article>
            <div class="table-responsive">
                <table class="table table-bordered">
                    <tr class="text-primary">
                        <th class="text-center">Type</th>
                        <th class="text-center">Http</th>
                        <th class="text-center">Https</th>
                    </tr>
            @foreach ($vectors as $type => $vector)
                @if(!empty($vector))
                    <tr>
                        <td>{{ucfirst($type)}}</td>
                        <td>
                            <a href="http://tfrontg1.airplug.co.kr/files/{{$vector}}" class='btn btn-default btn-block'>{{$type}}</a>
                        </td>
                        <td>
                            <a href="https://promote.airplug.com/test-vectors/{{$vector}}" class='btn btn-default btn-block'>{{$type}}</a>
                        </td>
                    </tr>
                @else
                    <tr>
                        <td colspan="3" class="divider">{{ucfirst($type)}}</td>
                    </tr>
                @endif
            @endforeach

                    <tr>
                        <td colspan="3" class="divider">Extra</td>
                    </tr>

            @foreach ($simulation as $type => $vector)
                    <tr>
                        <td>{{ucfirst($type)}}</td>
                        <td colspan="2">
                            <a href="http://{{$vector}}" class='btn btn-default btn-block'>{{$type}}</a>
                        </td>
                    </tr>
            @endforeach
                </table>
            </div>
        </article>
    </div>
@stop