<?php
$presenter = new Illuminate\Pagination\BootstrapPresenter($paginator);

$trans = $environment->getTranslator();
?>

@if ($paginator->getLastPage() > 1)
<?php
$previousPage = ($paginator->getCurrentPage() > 1) ? $paginator->getCurrentPage() - 1 : 1;
$nextPage = ($paginator->getCurrentPage() == $paginator->getLastPage()) ? $paginator->getCurrentPage() : $paginator->getCurrentPage() + 1;
?>
<ul class="pager">
    <li class="previous {{($paginator->getCurrentPage() == 1) ? 'disabled' : ''}}"><a
            href="{{$paginator->getUrl($previousPage)}}">{{$trans->trans('pagination.previous')}}</a></li>
    <li class="next {{($paginator->getCurrentPage() == $paginator->getLastPage()) ? 'disabled' : ''}}"><a
            href="{{$paginator->getUrl($nextPage)}}">{{$trans->trans('pagination.next')}}</a></li>
</ul>
@endif