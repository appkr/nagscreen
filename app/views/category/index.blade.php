@extends('layouts.master')

@section('content')
    <div class="page-header">
        <h4 class="blog-post-title">
            <i class="fa fa-tags"></i> Categories
        </h4>
    </div>

    <div class="table-responsive">
        <table class="table table-bordered table-striped">
            <thead>
            <tr>
                <th>Category name</th>
            </tr>
            </thead>

            <tbody>

            @if($cats->count())
                @foreach($cats as $category)
                    <tr>
                        <td>{{$category->name}}</td>
                    </tr>
                @endforeach
            @else
                <tr>
                    <td><p class="text-center">Not Found !</p></td>
                </tr>
            @endif

            </tbody>
        </table>
    </div>

    @include('category.create')

@stop


