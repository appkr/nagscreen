<div class="page-header">
    <h3 class="text-muted">
        <i class="fa fa-pencil"></i> Generate
    </h3>
</div> <!-- end .page-header-->

{{Form::open(['route' => 'category.store', 'method' => 'post', 'role' => 'form', 'class' => 'form-inline'])}}

    <div class="form-group">
        <label class="sr-only" for="name">Category name</label>
        {{Form::text('name', Input::old('name'), ['class' => 'form-control', 'placeholder' => 'notice, help, ...'])}}
    </div>

    {{Form::submit('GENERATE', ['class' => 'btn btn-primary'])}}
{{Form::close()}}