@extends('layouts.master')

@section('style')
<style>
    .summary-let th, .summary-let td {
        text-align: right;
    }
    .summary-let, .reviews-let, .note {
        margin-bottom: 2em;
    }
</style>
@stop

@section('content')
    <div class="page-header">
        <h4 class="blog-post-title">
            <i class="fa fa-android"></i> Google Play Summary, {{$summary['date']}}
        </h4>
    </div>

    @if(isset($summary['data']) && count($summary['data']) > 0)
        @foreach($summary['data'] as $package => $metric)

        <div class="page-header">
            <h3 class="text-muted">{{$package}}</h3>
        </div>

        <div class="table-responsive summary-let">
            <table class="table">
                <thead>
                <tr>
                    <th>&nbsp;</th>
                    <th>Installed Users <br/><small class="text-muted">(increase %)</small></th>
                    <th>Uninstalled Users <br/><small class="text-muted">(increase %)</small></th>
                    <th>Active Users(*)<br/><small class="text-muted">(retention %)</small></th>
                    <th>Rank (**)<br/><small class="text-muted">(increase)</small></th>
                    <th>Rating/Reviews <br/><small class="text-muted">(increase)</small></th>
                </tr>
                </thead>

                <tbody>
                <tr>
                    <td>{{$metric['today']['date']}}</td>
                    <td>{{$metric['today']['daily_user_installs']}}</td>
                    <td>{{$metric['today']['daily_user_uninstalls']}}</td>
                    <td>{{number_diff($metric['total_user_installs'], $metric['total_user_uninstalls'])}}</td>
                    <td>{{$metric['today']['rank_kr']}}</td>
                    <td>{{round($metric['average_rating'], 1)}}/{{$metric['total_reviews']}}</td>
                </tr>

                <tr style="page-break-inside: avoid;">
                    <td>&nbsp;</td>
                    <td><small class="text-muted">{{increase_rate($metric['today']['daily_user_installs'], $metric['yesterday']['daily_user_installs'])}}</small></td>
                    <td><small class="text-muted">{{increase_rate($metric['today']['daily_user_uninstalls'], $metric['yesterday']['daily_user_uninstalls'])}}</small></td>
                    <td><small class="text-muted">{{retention_ratio($metric['total_user_installs'], $metric['total_user_uninstalls'])}}</small></td>
                    <td><small class="text-muted">{{rank_diff($metric['yesterday']['rank_kr'], $metric['today']['rank_kr'])}}</small></td>
                    <td><small class="text-muted">{{count($metric['today']['reviews']) or ''}}</small></td>
                </tr>

                <tr style="page-break-inside: avoid;">
                    <td>{{$metric['yesterday']['date']}}</td>
                    <td>{{$metric['yesterday']['daily_user_installs']}}</td>
                    <td>{{$metric['yesterday']['daily_user_uninstalls']}}</td>
                    <td>&nbsp;</td>
                    <td>{{$metric['yesterday']['rank_kr'] or ''}}</td>
                    <td>{{$metric['total_reviews'] - count($metric['today']['reviews'])}}</td>
                </tr>
                </tbody>
            </table>
        </div>

            @if (count($metric['today']['reviews']) > 0)
                <reviews class="reviews-let">
                    @foreach ($metric['today']['reviews'] as $review)
                        <div class="media">
                            <div class="media-body">
                                <h4 class="media-heading"><strong>{{$review['reviewer'] or ''}}</strong> <small class="text-muted">{{format_rating($review['rating'])}}</small></h4>
                                <strong>{{$review['title'] or ''}}</strong> {{$review['content'] or ''}}
                                <small class="text-muted">{{$review['date'] or ''}}</small>
                            </div>
                        </div> <!-- end .media (single review) -->

                    @endforeach
                </reviews>
            @endif
        <hr/>

        @endforeach
    @endif

    <p class="note">
        <small class="text-muted">
            (*) Retention = (Total # of Installed User - Total # of Uninstalled User) / Total # of Installed User<br/>
            (**) Rank in 'Apps &gt; Media &amp; Entertainment &gt; Free of Charge' section<br/>
            <em><code>Note.</code> Install/Uninstall count is based on head count, not based on device count. Note that one user can have more than one device.</em>
        </small>
    </p>

    @if(isset($summary['related_reviews']) && count($summary['related_reviews']) > 0)
        @foreach($summary['related_reviews'] as $app => $reviews)
            <div class="page-header">
                <h4 class="text-muted">{{$app}}</h4>
            </div>

            <reviews class="reviews-let">
            @foreach($reviews as $review)
                <div class="media">
                    <div class="media-body">
                        <h4 class="media-heading"><strong>{{$review['reviewer'] or ''}}</strong> <small class="text-muted">{{format_rating($review['rating'])}}</small></h4>
                        <strong>{{$review['title'] or ''}}</strong> {{$review['content'] or ''}}
                        <small class="text-muted">{{$review['date'] or ''}}</small>
                    </div>
                </div> <!-- end .media (single review) -->
            @endforeach
            </reviews>

        @endforeach
    @endif

    <p class="note">
        <small class="text-muted">
            <strong>Data Api</strong> -
            <a href="{{route('api.googleplay.summary')}}">Summary data</a>,
            <a href="{{route('api.googleplay.stat.index')}}">Full install/uninstall statistics</a>,
            <a href="{{route('api.googleplay.review.index')}}">Full reviews list</a>,
            <a href="{{route('api.index', 10)}}">Api document</a>
        </small>
    </p>

    <div class="row">
        <div class="col-sm-4"><hr/></div>
        <div class="col-sm-4"><p class="text-center lead">END OF EMAIL BODY</p></div>
        <div class="col-sm-4"><hr/></div>
    </div>

    {{Form::open(['route' => 'googleplay.send', 'method' => 'post', 'role' => 'form'])}}

        <div class="form-group">
            <label for="to">Mail Receipient's Address (each address should be delimited by comma(,))</label>
            <textarea row="2" name="to" class="form-control">jimmy@airplug.com, hjk@airplug.com, pyo@airplug.com, hansoo@airplug.com, juwonkim@airplug.com, jun@airplug.com, alf123@airplug.com, jsong@airplug.com, linuxs@airplug.com, jmkoo@airplug.com, albertbe@airplug.com</textarea>
        </div>

        <div class="form-group">
            <label for="body">Additional Email Body</label>
            <textarea row="2" name="body" class="form-control"></textarea>
        </div>

        <div class="btn-group pull-right">
            {{Form::submit('SEND EMAIL', ['class' => 'btn btn-danger'])}}
        </div>
    {{Form::close()}}
@stop
