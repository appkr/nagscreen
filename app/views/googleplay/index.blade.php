@extends('layouts.master')

@section('style')
<style>
    .half-let th, .half-let td {
        text-align: right;
    }
    .half-let, .reviews-let, .max-date-let, .apps-let {
        margin-bottom: 1em;
    }
</style>
@stop

@section('content')
    <div class="page-header">
        <h4 class="blog-post-title">
            <i class="fa fa-android"></i> Google Play Summary
        </h4>
    </div>

    <div class="page-header">
        <h3 class="text-muted"><i class="fa fa-calendar-o"></i> Latest Date in NagScreen Database</h3>
    </div> <!-- end .page-header-->

    <div class="max-date-let">
        {{Form::open(['route' => 'googleplay.fetch', 'role' => 'form'])}}
            <p>{{$max_date}}</p>
            <p class="text-right">{{Form::submit('FETCH MANUALLY', ['class' => 'btn btn-primary'])}}</p>
        {{Form::close()}}
    </div>
    
    @if(count($half) > 0)
        <div class="page-header">
            <h3 class="text-muted"><i class="fa fa-chain-broken"></i> Incomplete Records</h3>
        </div> <!-- end .page-header-->

        <p>Blow records are found to be imcomplete status. Please provide <strong class="text-danger">"Unnstalled Users"</strong> value.</p>

        <div class="half-let">
            @foreach($half as $stat)
                {{Form::open(['route' => ['googleplay.update', $stat->id], 'method' => 'put', 'role' => 'form'])}}
                    <div class="form-group">
                        <label for="daily_user_uninstalls">{{$stat->package->package}}, {{$stat->date}}, Installed Users {{$stat->daily_user_installs}}, Rank {{$stat->rank_kr or 'null'}}</label>
                        <input class="form-control" type="text" name="daily_user_uninstalls" id="daily_user_uninstalls" value="{{Input::old('daily_user_uninstalls')}}" placeholder="# of Uninstalled Users"/>
                    </div>
                    <p class="text-right">{{Form::submit('SAVE', ['class' => 'btn btn-primary'])}}</p>
                {{Form::close()}}
            @endforeach
        </div>
    @endif

    <div class="page-header">
        <h3 class="text-muted"><i class="fa fa-pencil"></i> Catch App Reviews</h3>
    </div> <!-- end .page-header-->

    <div class="reviews-let">
        {{Form::open(['route' => 'googleplay.review.store', 'method' => 'post', 'role' => 'form'])}}
            <div class="form-group">
                <label for="app_id">App</label>
                {{Form::select('app_id', $apps, Input::old('app_id'), ['class' => 'form-control', 'id' => 'app_id'])}}
                <div id="recent"></div>
            </div>

            <div class="form-group">
                <label for="date">Date</label>
                <input type="date" name="date" value="{{Input::old('app_id')}}" class="form-control"/>
            </div>

            <div class="form-group">
                <label for="reviewer">Reviewer</label>
                {{Form::text('reviewer', Input::old('reviewer'), ['class' => 'form-control', 'placeholder' => 'Name of Reviewer'])}}
            </div>

            <div class="form-group">
                <label for="rating">Rating</label>
                {{Form::select('rating', [5=>5,4=>4,3=>3,2=>2,1=>1], Input::old('rating'), ['class' => 'form-control'])}}
            </div>

            <div class="form-group">
                <label for="content">Content</label>
                {{Form::textarea('content', Input::old('content'), ['class' => 'form-control', 'placeholder' => 'Review Content', 'rows' => 3])}}
            </div>

            <div class="btn-group pull-right">
                {{Form::submit('SAVE APP REVIEW', ['class' => 'btn btn-primary'])}}
                @if(! count($half)) 
                    <a href="{{route('googleplay.new.notify')}}" class="btn btn-danger">REVIEW &amp; SEND SUMMARY EMAIL</a>
                @endif
            </div>
            
        {{Form::close()}}
    </div>

    <div class="clearfix"></div>

    <div class="page-header">
        <h3 class="text-muted"><i class="fa fa-dropbox"></i> Create Related Apps</h3>
    </div> <!-- end .page-header-->

    <div class="apps-let">
        {{Form::open(['route' => 'googleplay.apps.store', 'role' => 'form', 'class' => 'form-inline'])}}

            <div class="form-group">
                <label class="sr-only" for="package">Package Name</label>
                {{Form::text('package', Input::old('package'), ['class' => 'form-control', 'placeholder' => 'ex) com.acme.app'])}}
            </div>

            <div class="form-group">
                <label class="sr-only" for="name">Package Name</label>
                {{Form::text('name', Input::old('name'), ['class' => 'form-control', 'placeholder' => 'ex) Acme Mobile'])}}
            </div>

            {{Form::submit('CREATE AN APP', ['class' => 'btn btn-primary pull-right'])}}
        {{Form::close()}}

        <p><small class="text-muted"><strong>Apps in the database</strong> - {{implode(', ', $apps)}}</small></p>
    </div>

@stop

@section('script')
    <script>
        (function () {
            $("#app_id").on("change", function () {
                getMostRecentReview();
            });

            function getMostRecentReview()
            {
                $.ajax({url: "googleplay/review/"+$('#app_id').val(), dataType: "json"})
                    .done(function (response) {
                        var review = response.data;
                        
                        $('#recent').html('<small class="text-muted"> <strong>Most recent review</strong> - '+review.date+', Reviewer '+review.reviewer+', Rating '+review.rating+', '+review.content+' <a class="btn btn-default btn-xs" data-review-id="'+review.id+'" id="delete-hook">DELETE</a></small>');

                        $("#delete-hook").on("click", function() {
                            if (confirm("Are you sure to delete ?")) {
                                var $this = $(this);
                                utility.fetch("googleplay/review/"+$this.data("review-id"), { _token: "{{csrf_token()}}", _method: "delete" })
                                    .done(function (response) {
                                        if (response.error) {utility.flash("Error", "Something went wrong! " + response.error.message, "error"); return;}
                                        utility.flash("Success", "Deleted !", "success");
                                        setTimeout(function () {window.location.reload(true);}, 1000);
                                    });
                            }
                        });
                    })
                    .fail(function (response) {
                        $('#recent').html('');
                    });
            }

            getMostRecentReview();
        })();
    </script>
@stop
