@extends('layouts.master')

@section('content')
    <div class="page-header">
        <h4 class="blog-post-title">
            @if(Auth::check())
            <div class="btn-group pull-right">
                <a href="{{route('post.create')}}" class="btn btn-primary"><i class="fa fa-pencil"></i> COMPOSE</a>
            </div>
            @endif
            <i class="fa fa-envelope-o"></i> Nag Messages
            @if(isset($user) && !empty($user))
                <br/>
                <small>composed by "{{$user->email}}"</small>
            @endif
            @if(isset($filter) && !empty($filter))
                <br/>
                <small>filtered by "{{$filter}}"="{{$question}}"</small>
            @endif
        </h4>
    </div>

    <div class="table-responsive">
        <table class="table table-bordered table-striped">
            <thead>
            <tr>
                <th>Category</th>
                <th>Package</th>
                <th>Language</th>
                <th>{{link_for_sort('post.index', 'title', 'Title')}} {{format_active_sort('title')}}</th>
                <th>{{link_for_sort('post.index', 'created_at', 'Datetime')}} {{format_active_sort('created_at')}}</th>
            </tr>
            </thead>

            <tbody>
            @if($posts->count())
                @foreach($posts as $post)
                <tr {{format_draft($post->published)}}>
                    <td>{{$post->category->name}}</td>
                    <td>{{$post->package->name}}</td>
                    <td>{{$post->language}}</td>
                    <td>
                        <a href="{{route('post.show', $post->id)}}" title="{{$post->title}}">{{$post->title}}
                        @if ($post->comments->count() > 0)
                        <small class="badge">{{$post->comments->count()}}</small>
                        @endif
                    </td>
                    <td>{{format_timestring($post->created_at)}}</td>
                </tr>
                @endforeach
            @else
                <tr>
                    <td colspan="5"><p class="text-center">Not Found !</p></td>
                </tr>
            @endif
            </tbody>
        </table>
    </div>
@stop

@section('pagination')
    <div class="text-center">
        {{$posts->appends(Request::except('page'))->links('pagination.slider')}}
    </div>
@stop
