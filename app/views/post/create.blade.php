@extends('layouts.master')

@section('style')
    <!-- Dropzone -->
    <link rel="stylesheet" href="/css/vendor/dropzone/dropzone.css">
@stop

@section('content')
    <div class="page-header">
        <div class="btn-group pull-right">
            <a href="{{route('post.index')}}" class="btn btn-default"><i class="fa fa-list-ul"></i> LIST</a>
        </div>

        <h4 class="blog-post-title">
            <i class="fa fa-envelope-o"></i> Nag Messages
        </h4>
    </div>

    {{ Form::open(['route' => 'post.store', 'method' => 'post', 'role' => 'form', 'id' => 'my-meta-form']) }}

        <div class="form-group">
            <label for="category_id">Category</label>
            {{ Form::select('category_id', $categories, Input::old('category_id'), ['class' => 'form-control']) }}

        </div>

        <div class="form-group">
            <label for="package_id">Package</label>
            {{ Form::select('package_id', $packages, Input::old('package_id'), ['class' => 'form-control']) }}

        </div>

        <div class="form-group">
            <label for="language">Language</label>
            {{ Form::select('language', $languages, Input::old('language'), ['class' => 'form-control']) }}

        </div>

        <div class="form-group">
            <label for="title">Title</label>
            <input type="text" name="title" id="title" class="form-control" value="{{Input::old('title')}}"/>
        </div>

        <div class="form-group">
            <label for="content">Content</label>
            <textarea name="content" id="content" class="form-control" rows="6">{{Input::old('content')}}</textarea>
        </div>

        <div class="checkbox">
            <label>
                <input type="checkbox" name="popup" id="popup"/>
                <span class="text-muted">Designate this as a POPUP.</span>
            </label>
        </div>

        <div class="checkbox">
            <label>
                <input type="checkbox" name="published" id="published"/>
                <strong class="text-danger" id="target-text">CHECK ON TO PUBLISH. As soon as you SAVE, the article will be
                    visible to API users.</strong>
            </label>
        </div>

        <p class="text-center">
        <div class="btn-group pull-right">
            <button type="submit" class="btn btn-primary btn-lg" id="target-button">SAVE ARTICLE</button>
            <a href="{{route('post.create')}}" class="btn btn-default btn-lg">START OVER</a>
        </div>
        </p>

    {{ Form::close() }}

    <br/>
    <div class="clearfix" style="margin-top: 2em;">&nbsp;</div>

    <!-- Form element for Dropzone -->
    <form action="{{route('attachment.store')}}" method="post" enctype="multipart/form-data" id="my-awesome-dropzone" class="dropzone">
        <div class="fallback">
            <input name="file" type="file" multiple/>
        </div>
    </form>
@stop

@section('script')
    <!-- CK Editor library -->
    <script src="/ckeditor/ckeditor.js"></script>

    <!-- Dropzone library -->
    <script src="/js/vendor/dropzone/dropzone.min.js"></script>

    <script>
        (function ($) {
            var editor = CKEDITOR.replace('content', {height: "30em"});

            Dropzone.options.myAwesomeDropzone = {
                maxFilesize: 2, // MB
                acceptedFiles: "image/*",
                dictDefaultMessage: "<div class=\"text-center text-muted\"><h2 class=\"text-muted\">Drop images to upload !</h2><p>(or Click to choose...)</p></div>",
                addRemoveLinks: true,

                init: function () {
                    var imagePath = "http:{{Config::get('app.url')}}/{{Config::get('setting.image.webPath')}}/";

                    this.on("success", function (file, response) {
                        var imgString = '<img src="' + imagePath + response.data.filename + '">',
                            formString = '<input type="hidden" name="images[]" value="' + response.data.id + '">';

                        editor.insertHtml(imgString);
                        $("#my-meta-form").prepend(formString);
                    });
                }
            };

            $("#published").on('click', function () {
                var initiator = $("#published"),
                    targetButton = $("#target-button"),
                    targetText = $("#target-text");

                if (initiator.is(":checked")) {
                    targetButton.removeClass("btn-primary")
                        .addClass("btn-danger")
                        .text("PUBLISH ARTICLE");
                    targetText.removeClass("text-danger")
                        .addClass("text-muted")
                        .text("CHECK OFF TO SAVE IT AS A DRAFT.");
                } else {
                    targetButton.removeClass("btn-danger")
                        .addClass("btn-primary")
                        .text("SAVE ARTICLE");
                    targetText.removeClass("text-muted")
                        .addClass("text-danger")
                        .text("CHECK ON TO PUBLISH. As soon as you SAVE, the article will be visible to API users.");
                }
            });

        })(jQuery);
    </script>
@stop

