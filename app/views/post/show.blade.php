@extends('layouts.master')

@section('content')
    @if ($post)
        <div class="page-header">
            <div class="btn-group pull-right">

                @if ($post->isOwner() || is_admin())
                    <a href="{{route('post.edit', $post->id)}}" class="btn btn-info"><i class="fa fa-cut"></i> EDIT </a>
                @endif
                <a href="{{route('post.index')}}" class="btn btn-default"><i class="fa fa-list-ul"></i> LIST</a>
            </div>

            <h4 class="blog-post-title">
                <i class="fa fa-envelope-o"></i> {{{$post->title}}}

                @if ($post->comments->count())
                    <small class="badge">{{{ $post->comments->count() }}}</small>
                @endif

            </h4>
        </div> <!-- end .page-header (article title) -->

        <div id="posts-wrapper">
            <div class="media">
                <a class="pull-left" href="{{get_gravatar_profile_url($post->user->email)}}">
                    <img class="media-object img-thumbnail" src="{{get_gravatar_url($post->user->email)}}" alt="{{$post->user->email}}">
                </a>

                <div class="media-body">
                    <h4 class="media-heading">
                        <a href="{{get_gravatar_profile_url($post->user->email)}}">{{$post->user->email}}</a>
                    </h4>

                    <small class="text-muted">
                        @if($post->popup)
                        <strong class="text-warning"><i class="fa fa-android"></i> Popup</strong> &nbsp;
                        @endif
                        <i class="fa fa-tags"></i> {{$post->category->name}} &nbsp;
                        <i class="fa fa-dropbox"></i> {{$post->package->name}} &nbsp;
                        <i class="fa fa-globe"></i> {{$post->language}} &nbsp;
                        <i class="fa fa-calendar"></i> {{format_timestring($post->created_at)}} &nbsp;
                    </small>
                </div>
            </div>
            <!-- end .media (single comment) -->

            <article>
                {{$post->content}}
                <p>&nbsp;</p>

                <div class="btn-group pull-right">
                    @if($post->published)
                        <button class="btn btn-xs btn-success" id="publish-status" data-status="0" title="Unpublish this article" {{ ($post->isOwner()) ? '' : 'disabled="disabled"' }}>
                            <i class="fa fa-plus-square"></i> PUBLISHED -> DRAFT
                        </button>
                    @else
                        <button class="btn btn-xs btn-default" id="publish-status" data-status="1" title="Publish this article" {{ ($post->isOwner()) ? '' : 'disabled="disabled"' }}>
                            <i class="fa fa-minus-square"></i> DRAFT -> PUBLISHED
                        </buton>
                    @endif

                    @if ($post->isOwner())
                        <button class="btn btn-default btn-xs" id="delete-hook" title="Delete this article">
                            <i class="fa fa-trash-o"></i> DELETE
                        </button>
                    @endif
                </div>
            </article> <!-- end article -->
        </div> <!-- end #posts-wrapper -->

        @include('comment.index')

    @else
        <div class="page-header">
            <h4 class="blog-post-title">
                <i class="fa fa-desktop"></i> Not Found.
            </h4>
        </div> <!-- end .page-header (article title) -->

        <div id="posts-wrapper">
            <article class="text-center"> Not Found.</article> <!-- end article -->
        </div> <!-- end #posts-wrapper -->
    @endif
@stop


@section('script')
    @if ($post)
        <!-- CK Editor library -->
        <script src="/ckeditor/ckeditor.js"></script>

        <!-- Custom javascript -->
        <script>
            (function ($) {

                @if (Auth::check())
                    // Create CK Editor instance
                    CKEDITOR.replace('content', {toolbar: 'Basic', height: '10em'});
                @endif

                // Instantiate popover
                // $(".revisions").popover({html: true});

                // Dealing AJAX delete request & response
                $("#delete-hook").on("click", function () {
                    if (confirm("You are going to delete this article \n\nAre you sure?")) {
                        $.ajax({
                            url: "{{route('post.destroy', $post->id)}}",
                            type: "POST",
                            data: {
                                _token: "{{csrf_token()}}",
                                _method: "DELETE",
                            },
                            dataType: "json"
                        }).done(function (response) {
                            if (response.data.error) {
                                new PNotify({
                                    title: "Error",
                                    text: "Something went wrong! " + response.data.message,
                                    type: "error"
                                });

                                return;
                            }

                            new PNotify({
                                title: "Success",
                                text: "Deleted !",
                                type: "success"
                            });

                            setTimeout(function () {
                                location = "/post";
                            }, 3000);
                        });
                    }
                });

                // Dealing AJAX update request & response
                $("#publish-status").on("click", function () {
                    var action = $(this).data('status'),
                        confirmString = (action == 0) ? 'UNPUBLISH' : 'PUBLISH';

                    if (confirm("Are you sure to " + confirmString + " this article ?")) {
                        $.ajax({
                            url: "{{route('post.toggle', $post->id)}}",
                            type: "POST",
                            data: {
                                _token: "{{csrf_token()}}",
                                published: action
                            },
                            dataType: "json"
                        }).done(function (response) {
                            if (response.data.error) {
                                new PNotify({
                                    title: "Error",
                                    text: "Something went wrong! " + response.data.message,
                                    type: "error"
                                });

                                return;
                            }

                            new PNotify({
                                title: "Success",
                                text: response.data.message,
                                type: "success"
                            });

                            setTimeout(function () {
                                window.location.reload(true);
                            }, 3000);
                        });
                    }
                });
            })(jQuery);
        </script>
    @endif
@stop



