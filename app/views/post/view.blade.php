@extends('layouts.mobile')

@section('content')
    @if ($post)
        <div class="page-header">
            <h3>
                <i class="fa fa-envelope-o"></i> {{{$post->title}}}
            </h3>
        </div> <!-- end .page-header (article title) -->

        <div id="posts-wrapper">
            <article>
                {{$post->content}}
            </article> <!-- end article -->

            <section id="posts-meta">
                <small class="text-muted pull-right">
                    <i class="fa fa-tags"></i> {{$post->category->name}} &nbsp;
                    <i class="fa fa-calendar"></i> {{$post->created_at->diffForHumans()}} &nbsp;
                </small>
            </section>
        </div> <!-- end #posts-wrapper -->
    @else
        <div class="page-header">
            <h4 class="blog-post-title">
                <i class="fa fa-desktop"></i> Not Found.
            </h4>
        </div> <!-- end .page-header (article title) -->

        <div id="posts-wrapper">
            <article class="text-center"> Not Found.</article> <!-- end article -->
        </div> <!-- end #posts-wrapper -->
    @endif
@stop



