@extends('layouts.master')

@section('content')
    <div class="page-header">
        <h4 class="blog-post-title">
            <i class="fa fa-user"></i> Users
        </h4>
    </div>

    <div class="table-responsive">
        <table class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th>{{link_for_sort('user.index', 'users.email', 'Email')}} {{format_active_sort('users.email')}}</th>
                    <th>Activities</th>
                    <th>{{link_for_sort('user.index', 'last_login', 'Last login')}} {{format_active_sort('last_login')}}</th>
                </tr>
            </thead>

            <tbody>
            @if($users->count())
                @foreach($users as $user)
                <tr {{format_draft($user->activated)}}>
                    <td><a href="{{route('user.profile', $user->id)}}">{{$user->email}}</a></td>
                    <td>{{build_user_activity_count($user)}}</td>
                    <td>{{format_timestring($user->last_login)}}</td>
                </tr>
                @endforeach
            @else
                <tr>
                    <td colspan="3"><p class="text-center">Not Found.</p></td>
                </tr>
            @endif
            </tbody>
        </table>
    </div>
@stop


