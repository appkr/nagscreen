@extends('layouts.master')

@section('style')
    <style>
        .thumbnail:first-child {
            padding: 1em;
        }
    </style>
@stop

@section('content')
    <div class="page-header">
        <h4 class="blog-post-title">
            <i class="fa fa-user"></i> Profile
            @if(isset($user) && !empty($user) && (route('user.index') == URL::previous()))
            <small> - {{$user->email}}</small>
            @endif
        </h4>
    </div>

    <div class="row">
        <div class="col-sm-6 col-md-4">
            <div class="thumbnail">
                <img src="{{get_gravatar_url($user->email, Config::get('setting.user.width'))}}" alt="{{$user->email}}" class="img-circle"/>

                <p>&nbsp;</p>

                <small style="margin-top:1em;">
                    <p><i class="fa fa-picture-o" style="margin-left:1em"></i>
                        <a href="{{get_gravatar_profile_url($user->email)}}" alt="{{$user->email}}"> View Gravatar profile</a></p>

                    <p><i class="fa fa-lock" style="margin-left: 1em"></i>
                        <a href="{{route('user.find')}}" alt="Change password"> Reset password</a></p>

                    <p><i class="fa fa-sign-in" style="margin-left: 1em"></i>
                        Last signin {{format_timestring($user->last_login)}}</p>

                </small>
            </div>
        </div>
        <!-- end .col-sm-6 col-md-4-->

        <div class="col-sm-6 col-md-8 blog-main">
            <div class="page-header">
                <h3 class="text-muted">
                    <i class="fa fa-anchor"></i> 10 Recent Activities
                </h3>
            </div>
            <!-- end .page-header -->

            <ul class="list-unstyled">
            @foreach($user->histories as $history)
                <li style="margin-bottom: 1em;">{{get_alias_from_model_name($history->trackable_type, true)}} {{build_activity_string($history)}}</li>
            @endforeach
            </ul>
        </div> <!-- end .col-sm-6 col-md-8-->
    </div>
@stop