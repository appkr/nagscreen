@extends('layouts.master')

@section('content')
    {{ Form::open(['route' => 'user.store', 'method' => 'post', 'role' => 'form', 'files' => true, 'class'=>'form-signin']) }}

        <input type="hidden" name="url" value="{{ Request::server('HTTP_REFERER') }}">

        <div class="page-header">
            <h4 class="blog-post-title">Sign Up</h4>
            <small class="text-muted">Provide your <strong class="text-danger">@airplug.com</strong> email.</small>
        </div>

        <div class="input-group">
            <span class="input-group-addon"><i class="fa fa-user"></i></span>
            <input type="email" name="email" class="form-control" placeholder="Email address" value="{{Input::old('email')}}" required autofocus/>
        </div>

        <div class="input-group">
            <span class="input-group-addon"><i class="fa fa-lock"></i></span>
            <input type="password" name="password" class="form-control" placeholder="Password" required/>
        </div>

        <div class="input-group">
            <span class="input-group-addon"><i class="fa fa-lock"></i></span>
            <input type="password" name="password_confirmation" class="form-control" placeholder="Confirm password" required/>
        </div>

        <button class="btn btn-lg btn-primary btn-block" type="submit">SIGN UP</button>

    {{ Form::close() }}
@stop

