@extends('layouts.master')

@section('content')
    {{ Form::open(['route' => 'user.remind', 'method' => 'post', 'role' => 'form', 'class'=>'form-signin']) }}

        <h4 class="blog-post-title">Password Remind</h4>
        <p class="text-muted">
            Provide your <strong class="text-danger">@airplug.com</strong> email.<br/>
            And then checkout your email inbox.
        </p>

        <input type="email" name="email" class="form-control" placeholder="Email address" value="{{{ Input::old('email') }}}" required autofocus>

        <button class="btn btn-lg btn-primary btn-block" type="submit">SEND REMINDER</button>

    {{ Form::close() }}
@stop