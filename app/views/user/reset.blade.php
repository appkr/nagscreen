@extends('layouts.master')

@section('content')
    {{ Form::open(['route' => 'user.restore', 'method' => 'post', 'role' => 'form', 'class'=>'form-signin']) }}

        <input type="hidden" name="token" value="{{ $token }}">

        <h4 class="blog-post-title">Password Reset</h4>
        <p class="text-muted">
            Provide your <strong class="text-danger">@airplug.com</strong> email and NEW PASSWORD.
        </p>

        <input type="email" name="email" class="form-control" placeholder="Email address" value="{{Input::old('email')}}" required autofocus>

        <input type="password" name="password" class="form-control" placeholder="Password" required>

        <input type="password" name="password_confirmation" class="form-control" placeholder="Confirm password" required>

        <button class="btn btn-lg btn-primary btn-block" type="submit">RESET PASSWORD</button>

    {{ Form::close() }}
@stop