@extends('layouts.master')

@section('content')
    {{ Form::open(['route' => 'session.store', 'method' => 'post', 'role' => 'form', 'class'=>'form-signin']) }}
        <input type="hidden" name="url" value="{{ Request::server('HTTP_REFERER') }}">

        <div class="page-header">
            <h4 class="blog-post-title">Sign In</h4>
            <small class="text-muted">Sign in using your registered email.</small>
        </div>

        <div class="input-group">
            <span class="input-group-addon"><i class="fa fa-user"></i></span>
            <input type="email" name="email" class="form-control" placeholder="Email address" value="{{Input::old('email')}}" autofocus/>
        </div>

        <div class="input-group">
            <span class="input-group-addon"><i class="fa fa-lock"></i></span>
            <input type="password" name="password" class="form-control" placeholder="Password">
        </div>

        <label class="checkbox">
            <input type="checkbox" name="remember" value="{{Input::old('remember')}}" {{Input::old('remember')}}> Keep me signed in
        </label>

        <button class="btn btn-lg btn-primary btn-block" type="submit">SIGN IN</button>

        <p>&nbsp;</p>
        <p class="text-center">Don't have an account? <a href="{{route('user.create')}}">SIGN UP</a></p>
        <p class="text-center"><a href="{{route('user.find')}}">REMIND PASSWORD</a></p>

    {{ Form::close() }}
@stop

