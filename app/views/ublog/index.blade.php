@extends('layouts.mobile')

@section('style')
    <style>
        table {
            table-layout: fixed;
            word-wrap: break-word;
        }
    </style>
@stop

@section('content')
<?php $project = ['name' => 'NagScreen'];?>
    <table class="table table-bordered table-striped">
        <thead>
            <tr>
                <th>evtTime</th>
                <th>sessionID</th>
                <th>evtCode</th>
                <th>bWifi</th>
                <th>cellId</th>
                <th>ssid</th>
                <th>bssid</th>
                <th>pgName</th>
                <th>appName</th>
                <th>apSrchSTm</th>
                <th>apSrchETm</th>
                <th>apSrchRst</th>
                <th>apList</th>
            </tr>
        </thead>
        <tbody>
            @foreach($data as $item)
            <?php $e = $item->members;?>
            <tr>
                <td>{{strftime("%X", $e->evtTime)}}</td>
                <td>{{$e->sessionID}}</td>
                <td>{{$e->evtCode}}</td>
                <td>{{$e->bWifi}}</td>
                <td>{{$e->cellId}}</td>
                <td>{{isset($e->ssid) ? $e->ssid : null}}</td>
                <td>{{isset($e->bssid) ? $e->bssid : null}}</td>
                <td>{{isset($e->pgName) ? $e->pgName : null}}</td>
                <td>{{isset($e->appName) ? $e->appName : null}}</td>
                <td>{{isset($e->apSrchSTm) ? strftime("%X", $e->apSrchSTm) : null}}</td>
                <td>{{isset($e->apSrchETm) ? strftime("%X", $e->apSrchETm) : null}}</td>
                <td>{{isset($e->apSrchRst) ? $e->apSrchRst : null}}</td>
                <td>{{isset($e->apList) ? $e->apList : null}}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
@stop