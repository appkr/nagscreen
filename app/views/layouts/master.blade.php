<!DOCTYPE html>

<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, , user-scalable=no">
    <title>{{{ $project['name'] }}}</title>

    <!-- Favicon -->
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
    <link rel="icon" href="/favicon.ico" type="image/x-icon">

    <!-- Bootstrap & Font-awewome -->
    <link rel="stylesheet" href="/css/vendor/bootstrap/lumen/bootstrap.min.css">
    <link rel="stylesheet" href="/css/vendor/font-awesome/font-awesome.min.css">

    <!-- PNotify -->
    <link rel="stylesheet" href="/css/vendor/pnotify/pnotify.custom.min.css">

    <!-- Page specific dynamic styles-->
    @yield('style')

    <!-- Method override & custom styling -->
    <link rel="stylesheet" href="/css/mystyle.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="//oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="//oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>

@include('layouts.navigation')

<section class="container">

    <div class="row">

        <div class="col-sm-8 blog-main">

            @yield('content', 'Page Not Found')

            @yield('pagination')

        </div>
        <!-- end .blog-main -->


        <div class="col-sm-3 col-sm-offset-1 blog-sidebar">

            @include('layouts.sidebar')

        </div>
        <!-- end .blog-sidebar -->

    </div>
    <!-- end .row -->

</section>
<!-- end .container(main content) -->


<footer class="container">
    <hr/>
    <p class="pull-right"><a href="#">Back to top</a></p>

    <p>&copy; {{date('Y')}} AirPlug Inc. <a href="{{route('home')}}">{{$project['name']}}</a></p>
</footer>


<!-- jQuery -->
<script src="/js/vendor/jquery/jquery.min.js"></script>

<!-- Bootstrap -->
<script src="/js/vendor/bootstrap/bootstrap.min.js"></script>

<!-- PNotify -->
<script src="/js/vendor/pnotify/pnotify.custom.min.js"></script>

<!-- Site-wide utilities -->
<script src="/js/utility.js"></script>

@include('layouts.flash')

@yield('script')

</body>

</html>