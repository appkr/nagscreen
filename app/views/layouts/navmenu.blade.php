<ul class="nav navbar-nav navbar-right">
    @if(! Auth::check())
        <li><a href="{{route('session.create')}}"><i class="fa fa-sign-in"></i> Sign in</a></li>
        <li><a href="{{route('user.create')}}"><i class="fa fa-certificate"></i> Sign up</a></li>
    @else
        <li>
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <i class="fa fa-folder-open"></i> Tasks <b class="caret"></b>
            </a>
            <ul class="dropdown-menu">
                <li><a href="{{route('home')}}"><i class="fa fa-leaf"></i> Home</a></li>
                <li><a href="{{route('post.index')}}"><i class="fa fa-envelope-o"></i> Nag Messages</a></li>
                <li><a href="{{route('voc.index')}}"><i class="fa fa-frown-o"></i> Voice of Customers</a></li>
                <li><a href="{{route('push.index')}}"><i class="fa fa-bullhorn"></i> Push Message</a></li>
                <li><a href="{{route('filebox.index')}}"><i class="fa fa-paperclip"></i> File Box</a></li>
                <li class="divider"></li>
                <li><a href="{{route('api.index')}}"><i class="fa fa-book"></i> Api Documents</a></li>
                <li class="divider"></li>
                <li><a href="{{route('test.index')}}"><i class="fa fa-stethoscope"></i> Test Portal</a></li>
            </ul>
        </li>
        @if(is_admin())
            <li>
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <span class="glyphicon glyphicon-cog"></span> &nbsp;Admin <b class="caret"></b>
                </a>
                <ul class="dropdown-menu">
                    <!-- <li><a href=""><i class="fa fa-cogs"></i> Setting</a></li> -->
                    <li><a href="{{route('history.index')}}"><i class="fa fa-anchor"></i> Activities</a></li>
                    <li><a href="{{route('package.index')}}"><i class="fa fa-dropbox"></i> Packages</a></li>
                    <li><a href="{{route('category.index')}}"><i class="fa fa-tags"></i> Categories</a></li>
                    <li><a href="{{route('user.index')}}"><i class="fa fa-user"></i> Users</a></li>
                    <li><a href="https://hud.iron.io/mq/aws/projects/536899eca8461a0005000040/queues/nagscreen#overview-pane-tab"><i class="fa fa-list-alt"></i> Queue Workers</a></li>
                    <li><a href="https://www.google.com/analytics/web/?hl=ko#report/visitors-overview/a46212331w82182424p85105117/"><i class="fa fa-bar-chart-o"></i> Analytics</a></li>
                    <li><a href="{{route('googleplay.index')}}"><i class="fa fa-android"></i> GooglePlay Summary</a></li>
                    <li><a href="{{route('speedtest')}}"><i class="fa fa-tachometer"></i> Speed Test</a></li>
                </ul>
            </li>
    @endif
        <li><a href="{{route('user.profile')}}"><i class="fa fa-user"></i> {{Auth::user()->email}}</a></li>
        <li><a href="{{route('session.destroy')}}"><i class="fa fa-sign-out"></i> Sign out</a></li>
    @endif
</ul>