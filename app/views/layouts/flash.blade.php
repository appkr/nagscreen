@if($errors->has())
    <script>
        (function () {
            @foreach($errors->all() as $error)
                new PNotify({
                    title: "Errors",
                    text: "{{{ $error }}}",
                    type: "error"
                });
            @endforeach
        })();
    </script>
@endif

@if(Session::has('error'))
    <script>
        (function () {
            new PNotify({
                title: "Error",
                text: "{{{ Session::get('error') }}}",
                type: 'error'
            });
        })();
    </script>
@endif

@if(Session::has('warning'))
    <script>
        (function () {
            new PNotify({
                title: "Notice",
                text: "{{ Session::get('warning') }}"
            });
        })();
    </script>
@endif

@if(Session::has('success'))
    <script>
        (function () {
            new PNotify({
                title: "Success",
                text: "{{ Session::get('success') }}",
                type: "success"
            });
        })();
    </script>
@endif

@if(Session::has('info'))
    <script>
        (function () {
            new PNotify({
                title: "Info",
                text: "{{ Session::get('info') }}",
                type: "info"
            });
        })();
    </script>
@endif