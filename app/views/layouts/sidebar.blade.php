<div class="sidebar-module sidebar-module-inset">
    <h4><span class="glyphicon glyphicon-copyright-mark"></span> About</h4>
    <ol class="list-unstyled">
        <li class="list-divider"></li>
    </ol>
    <p class="text-muted">{{{ $project['description'] }}}</p>
</div> <!-- end .sidebar-module -->

@if(Auth::check())
<div class="sidebar-module">
    <h4><i class="fa fa-folder-open"></i> Tasks</h4>
    <ol class="list-unstyled">
        <li class="list-divider"></li>
        <li><a href="{{route('post.index')}}"> <i class="fa fa-envelope-o"></i> Nag Messages</a></li>
        <li><a href="{{route('voc.index')}}"> <i class="fa fa-frown-o"></i> Voice of Customers</a></li>
        <li><a href="{{route('push.index')}}"> <i class="fa fa-bullhorn"></i> Push Messages</a></li>
        <li><a href="{{route('filebox.index')}}"> <i class="fa fa-paperclip"></i> File Box</a></li>
        <li class="list-divider"></li>
        <li><a href="{{route('api.index')}}"> <i class="fa fa-book"></i> Api Documents</a></li>
        <li class="list-divider"></li>
        <li><a href="{{route('test.index')}}"><i class="fa fa-stethoscope"></i> Test Portal</a></li>
        @if(is_admin())
        <li class="list-divider"></li>
        <!-- <li><a href=""> <i class="fa fa-cogs"></i> Setting</a></li> -->
        <li><a href="{{route('history.index')}}"> <i class="fa fa-anchor"></i> Activities</a></li>
        <li><a href="{{route('package.index')}}"> <i class="fa fa-dropbox"></i> Packages</a></li>
        <li><a href="{{route('category.index')}}"> <i class="fa fa-tags"></i> Categories</a></li>
        <li><a href="{{route('user.index')}}"><i class="fa fa-user"></i> Users</a></li>
        <li><a href="https://hud.iron.io/mq/aws/projects/536899eca8461a0005000040/queues/nagscreen#overview-pane-tab"><i class="fa fa-list-alt"></i> Queue Workers</a></li>
        <li><a href="https://www.google.com/analytics/web/?hl=ko#report/visitors-overview/a46212331w82182424p85105117/"><i class="fa fa-bar-chart-o"></i> Analytics</a></li>
        <li><a href="{{route('googleplay.index')}}"><i class="fa fa-android"></i> GooglePlay Summary</a></li>
        <li><a href="{{route('speedtest')}}"><i class="fa fa-tachometer"></i> Speed Test</a></li>
        @endif
    </ol>
</div> <!-- end .sidebar-module -->

<div class="sidebar-module">
    <h4><i class="fa fa-filter"></i> Filters</h4>
    <ol class="list-unstyled">
        <li class="list-divider"></li>
    </ol>

    <div class="btn-group">
        <button type="button" class="btn btn-default btn-block dropdown-toggle" data-toggle="dropdown">
            <i class="fa fa-envelope-o"></i> Nag Messages <span class="caret"></span>
        </button>
        <ul class="dropdown-menu" role="menu">
            <li><a href="{{route('post.index')}}">All</a></li>
            <li class="divider"></li>
            <li class="dropdown-header"><i class="fa fa-tags"></i> by Category</li>
            @foreach ($categories as $k => $v)
                <li><a href="{{route('post.index')}}?filter=category&question={{$v}}">{{$v}}</a></li>
            @endforeach
            <li class="divider"></li>
            <li class="dropdown-header"><i class="fa fa-dropbox"></i> by Package</li>
            @foreach ($packages_long as $k => $v)
                <li><a href="{{route('post.index')}}?filter=package&question={{$k}}">{{$v}}</a></li>
            @endforeach
            <li class="divider"></li>
            <li class="dropdown-header"><i class="fa fa-globe"></i> by Language</li>
            @foreach ($languages as $k => $v)
                <li><a href="{{route('post.index')}}?filter=language&question={{$k}}">{{$v}}</a></li>
            @endforeach
        </ul>
    </div>

    <div class="btn-group">
        <button type="button" class="btn btn-default btn-block dropdown-toggle" data-toggle="dropdown">
            <i class="fa fa-frown-o"></i> Voice of Customers <span class="caret"></span>
        </button>
        <ul class="dropdown-menu" role="menu">
            <li><a href="{{route('voc.index')}}">All</a></li>
            <li class="divider"></li>
            <li class="dropdown-header"><i class="fa fa-dropbox"></i> by Package</li>
            @foreach ($packages_long as $k => $v)
                <li><a href="{{route('voc.index')}}?filter=package&question={{$k}}">{{$v}}</a></li>
            @endforeach
            <li class="divider"></li>
            <li><a href="{{route('voc.index')}}?filter=feedback&question=1">Not Answered</a></li>
        </ul>
    </div>

    <div class="btn-group">
        <button type="button" class="btn btn-default btn-block dropdown-toggle" data-toggle="dropdown">
            <i class="fa fa-bullhorn"></i> Push Messages <span class="caret"></span>
        </button>
        <ul class="dropdown-menu" role="menu">
            <li><a href="{{route('push.index')}}">All</a></li>
            <li class="divider"></li>
            <li class="dropdown-header"><i class="fa fa-dropbox"></i> by Package</li>
            @foreach ($packages_long as $k => $v)
                <li><a href="{{route('push.index')}}?filter=package&question={{$k}}">{{$v}}</a></li>
            @endforeach
            <li class="divider"></li>
            <li><a href="{{route('push.index')}}?filter=feedback&question=1">Not Delivered</a></li>
        </ul>
    </div>
</div> <!-- end .sidebar-module -->

<div class="sidebar-module">
    <h4><i class="fa fa-comment"></i> Recent Comments/Feedbacks</h4>
    <ol class="list-unstyled recent-comments-item">
        <li class="list-divider"></li>

        @if($recentComments->count())
            @foreach ($recentComments as $comment)
            <li>
                <p>
                    <a href="{{get_activity_link($comment->trackable_type, $comment->trackable_id)}}">{{str_limit(strip_tags($comment->content), 100)}}</a>
                    <small class="text-muted">by {{$comment->user->email}}</small>
                </p>
            </li>
            @endforeach
        @else
            <li>
                <p>No recent activities.</p>
            </li>
        @endif
    </ol>
</div> <!-- end .sidebar-module -->
@endif

<div class="sidebar-module">
    <h4><i class="fa fa-external-link"></i> Other resources</h4>
    <ol class="list-unstyled recent-comments-item">
        <li class="list-divider"></li>
        <li><p><a href="{{route('abc')}}" target="_blank">ABC minipage</a></p></li>
        <li><p><a href="{{route('airsocket')}}" target="_blank">AirSocket&reg; minipage</a></p></li>
        <li><p><a href="{{route('airsocketforhoppin')}}" target="_blank">AirSocket&reg; for Hoppin minipage</a></p></li>
        <li><p><a href="{{route('videoplug')}}" target="_blank">VideoPlug minipage</a></p></li>
    </ol>
</div> <!-- end .sidebar-module -->