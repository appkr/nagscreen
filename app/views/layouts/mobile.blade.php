<!DOCTYPE html>

<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, , user-scalable=no">
    <title>{{{$project['name']}}}</title>

    <!-- Bootstrap -->
    <link rel="stylesheet" href="/css/vendor/bootstrap/darkly/bootstrap.min.css">
    <link rel="stylesheet" href="/css/vendor/font-awesome/font-awesome.min.css">

    <!-- PNotify -->
    <link rel="stylesheet" href="/css/vendor/pnotify/pnotify.custom.min.css">

    <!-- Custom global style -->
    <link rel="stylesheet" href="/css/mobilestyle.css">

    <!-- Page specific style -->
    @yield('style')
</head>

<body>
    <section class="container">
        @yield('content', 'Page Not Found')
    </section> <!-- end .container(main content) -->

    <footer class="container">
        <hr/>
        <p class="pull-right"><a href="#">Back to top</a></p>

        <p>&copy; {{ date('Y') }} <a href="http://www.airplug.com">AirPlug Inc.</a></p>
    </footer>

    <!-- jQuery -->
    <script src="/js/vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap -->
    <script src="/js/vendor/bootstrap/bootstrap.min.js"></script>

    <!-- PNotify -->
    <script src="/js/vendor/pnotify/pnotify.custom.min.js"></script>

    <!-- Site-wide utilities -->
    <script src="/js/utility.js"></script>

    @include('layouts.flash')

    @yield('script')
</body>

</html>