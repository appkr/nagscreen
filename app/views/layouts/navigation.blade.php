<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">

    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#custom-nav">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a href="{{route('home')}}" class="navbar-brand"><i class="fa fa-leaf"></i> {{$project['name']}}</a>
        </div>
        <!-- end .navbar-header -->

        <div class="collapse navbar-collapse" id="custom-nav">
            <form action="{{route('history.index')}}" method="get" role="search" class="navbar-form navbar-right">
                <div class="form-group">
                    <input type="text" name="keyword" value="{{Input::old('keyword')}}" placeholder="Search NagScreen" class="form-control"/>
                </div>
            </form>

            @include('layouts.navmenu')
        </div> <!-- end #custom-nav -->

    </div> <!-- end .container-fluid -->

</nav> <!-- end nav -->