<!DOCTYPE html>

<html lang="en">
<head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# product: http://ogp.me/ns/product#">
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="{{$data['description']}}"/>
    <meta name="author" content="{{$data['author']}}"/>
    <meta property="og:title" content="{{$data['title']}}"/>
    <meta property="og:site_name" content="{{$data['title']}}"/>
    <meta property="og:description" content="{{$data['description']}}"/>
    <meta property="fb:app_id" content="{{$data['appId']}}"/>
    <meta property="og:type" content="Android Application']"/>
    <meta property="og:url" content="{{$data['likeUrl']}}"/>
    <meta property="og:locale" content="ko_KR"/>
    <meta property="article:author" content="{{$data['author']}}"/>

    <title>{{$data['title']}}</title>

    <!-- Bootstrap core CSS -->
    <link href="/css/vendor/bootstrap/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Custom styles for this template -->
    <link href="/css/mycarousel.css" rel="stylesheet">
    <link href="/css/myminipage.css" rel="stylesheet">

    @yield('style')

</head>
<body>

<div class="container">
    <div id="fb-root"></div>
</div>

<div class="clear-fix"></div>

<!-- put open-hook at a button's class when custom url scheme is ready -->
@yield('content')

<div class="container">
    <footer>
        <p class="pull-right"><a href="#">Back to top</a></p>

        <p>&copy; 2014 <a href="http://www.airplug.com" target="_top">AirPlug Inc.</a></p>
    </footer>
</div>

<!-- Bootstrap core JavaScript -->
<script src="/js/vendor/jquery/jquery.min.js"></script>
<script src="/js/vendor/bootstrap/bootstrap.min.js"></script>

@yield('script')

<script src="/js/open_or_redirect_to_playstore.js"></script>

<script>
    window.fbAsyncInit = function () {
        FB.init({
            appId: "{{$data['appId']}}",
            status: true,
            xfbml: true,
            oauth: true
        });
    };

    (function () {
        var e = document.createElement('script');
        e.async = true;
        e.src = document.location.protocol + '//connect.facebook.net/ko_KR/all.js';
        document.getElementById('fb-root').appendChild(e);
    })();
</script>

</body>
</html>
