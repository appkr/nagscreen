@extends('layouts.master')

@section('content')
    <div class="page-header">
        <div class="btn-group pull-right">
            <a href="{{route('package.index')}}" class="btn btn-default"><i class="fa fa-list-ul"></i> LIST</a>
        </div>
        <h4 class="blog-post-title">
            <i class="fa fa-flag-o"></i> Banner for {{$package->name}}
        </h4>
    </div>

    {{Form::open(['route' => 'banner.store', 'method' => 'post', 'files' => true, 'role' => 'form'])}}
        {{Form::hidden('package_id', $package->id)}}

    <div class="form-group">
        {{Form::label('launcher_image', 'Launcher Image')}}
        {{Form::file('launcher_image', ['class' => 'form-control filestyle', 'accept' => 'image/*'])}}
        <small class="help-block">Launcher image wider than 720px will be resized to 720px width with original aspect ratio.</small>
    </div>

    <div class="form-group">
        {{Form::label('banner_image', 'Banner Image')}}
        {{Form::file('banner_image', ['class' => 'form-control filestyle', 'accept' => 'image/*'])}}
        <small class="help-block">Be consistent with other banners in terms of width and height.</small>
    </div>

    <div class="form-group">
        {{Form::label('excerpt_ko', 'Excerpt (in Korean)')}}
        {{Form::textarea('excerpt_ko', null, ['class' => 'form-control', 'rows' => 2])}}
        <small class="help-block">Describe features or benefits of the app package in short form.</small>
    </div>

    <div class="form-group">
        {{Form::label('excerpt_en', 'Excerpt (in English)')}}
        {{Form::textarea('excerpt_en', null, ['class' => 'form-control', 'rows' => 2])}}
    </div>

    <p class="text-center">
        <div class="btn-group pull-right">
            <button type="submit" class="btn btn-primary btn-lg">SAVE BANNER</button>
            <a href="{{route('banner.create', $package->id)}}" class="btn btn-default btn-lg">START OVER</a>
        </div>
    </p>

    {{ Form::close() }}
@stop

@section('script')
    <script src="/js/vendor/bootstrap/bootstrap-filestyle.min.js"></script>
    <script>
        (function(){
            $(":file").filestyle({buttonText: "Choose Image", input: false});
        })();
    </script>
@stop