<!DOCTYPE html>

<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{{$project['name']}}}</title>

    <!-- Bootstrap -->
    <link rel="stylesheet" href="/css/vendor/bootstrap/yeti/bootstrap.min.css">
    <link rel="stylesheet" href="/css/vendor/font-awesome/font-awesome.min.css">

    <!-- Custom global style -->
    <link rel="stylesheet" href="/css/mobilestyle.css">
</head>

<body>
    <section class="container">
        @foreach($banners as $banner)
        <div class="bannerlet">
            <a href="{{is_android() ? $banner->android_url : $banner->pc_url}}">
                @if ($banner->banner_image_url)
                <img src="{{$banner->banner_image_url}}" class="img-responsive bannerlet-banner-img"/>
                @elseif ($banner->launcher_image_url)
                <img src="{{$banner->launcher_image_url}}" class="pull-left bannerlet-launcher-img" style="width:120px"/>
                @endif
            </a>
            @if (! $banner->banner_image_url && $banner->launcher_image_url && $banner->excerpt_ko)
            <h4 class="bannerlet-title">{{$banner->excerpt_ko}}</h4>
            @endif
        </div>
        @endforeach
    </section> <!-- end .container(main content) -->
</body>

</html>