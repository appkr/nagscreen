@extends('layouts.master')

@section('content')
    <div class="page-header">
        <div class="btn-group pull-right">
            <button class="btn btn-default" id="delete-hook"><i class="fa fa-trash-o"></i> DELETE</button>
            <a href="{{route('package.index')}}" class="btn btn-default"><i class="fa fa-list-ul"></i> LIST</a>
        </div>
        <h4 class="blog-post-title">
            <i class="fa fa-flag-o"></i> Banner @if($banner->package) for {{$banner->package->name}} @endif
        </h4>
    </div>

    {{Form::open(['route' => ['banner.update', $banner->id], 'method' => 'put', 'files' => true, 'role' => 'form'])}}

    <div class="form-group">
        {{Form::label('launcher_image', 'Launcher Image')}}
        @if($banner->launcher_image)
            <p>{{HTML::image($banner->launcher_image_url, 'Launcher image', ['class' => 'img-responsive'])}}</p>
        @endif
        {{Form::file('launcher_image', ['class' => 'form-control filestyle', 'accept' => 'image/*'])}}
        <small class="help-block">Launcher image wider than 720px will be resized to 720px width with original aspect ratio.</small>
    </div>

    <div class="form-group">
        {{Form::label('banner_image', 'Banner Image')}}
        @if($banner->banner_image)
            <p>{{HTML::image($banner->banner_image_url, 'Banner image', ['class' => 'img-responsive'])}}</p>
        @endif
        {{Form::file('banner_image', ['class' => 'form-control filestyle', 'accept' => 'image/*'])}}
        <small class="help-block">Be consistent with other banners in terms of width and height.</small>
    </div>

    <div class="form-group">
        {{Form::label('excerpt_ko', 'Excerpt (in Korean)')}}
        {{Form::textarea('excerpt_ko', $banner->excerpt_ko, ['class' => 'form-control', 'rows' => 2])}}
        <small class="help-block">Describe features or benefits of the app package in short form.</small>
    </div>

    <div class="form-group">
        {{Form::label('excerpt_en', 'Excerpt (in English)')}}
        {{Form::textarea('excerpt_en', $banner->excerpt_en, ['class' => 'form-control', 'rows' => 2])}}
    </div>

    <p class="text-center">
    <div class="btn-group pull-right">
        <button type="submit" class="btn btn-primary btn-lg">UPDATE BANNER</button>
        <a href="{{route('banner.edit', $banner->id)}}" class="btn btn-default btn-lg">START OVER</a>
    </div>
    </p>

    {{ Form::close() }}
@stop

@section('script')
    <script src="/js/vendor/bootstrap/bootstrap-filestyle.min.js"></script>
    <script>
        (function(){
            $(":file").filestyle({buttonText: "Choose Image", input: false});

            // Dealing AJAX delete request & response
            $("#delete-hook").on("click", function () {
                if (confirm("You are going to delete this banner! \n\nAre you sure?")) {
                    $.ajax({
                        url: "{{route('banner.destroy', $banner->id)}}",
                        type: "POST",
                        data: {
                            _token: "{{csrf_token()}}",
                            _method: "DELETE"
                        },
                        dataType: "json"
                    }).done(function (response) {
                        if (response.data.error) {
                            new PNotify({
                                title: "Error",
                                text: "Something went wrong! " + response.data.message,
                                type: "error"
                            });

                            return;
                        }

                        new PNotify({
                            title: "Success",
                            text: "Deleted !",
                            type: "success"
                        });

                        setTimeout(function () {
                            location = "{{route('package.index')}}";
                        }, 3000);
                    });
                }
            });
        })();
    </script>
@stop