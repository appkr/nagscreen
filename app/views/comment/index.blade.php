<div class="page-header">
    <h3 class="text-muted">
        <i class="fa fa-comment"></i> Comments
    </h3>
</div> <!-- end .page-header (comment) -->

@if (Auth::check())
    {{ Form::open(['route' => 'comment.store', 'method' => 'post', 'role' => 'form']) }}
        <input type="hidden" name="post_id" value="{{{ $post->id }}}">

        <div class="form-group">
            <textarea name="content" id="content" class="form-control" rows="3"></textarea>
        </div>

        <button type="submit" class="btn btn-primary btn-lg pull-right">Submit</button>
    {{ Form::close() }}
    <!-- end comment form -->

    <div class="clearfix">&nbsp;</div>
    <p>&nbsp;</p>

@endif

@if ($post->comments->count())
    <comments>
    @foreach ($post->comments as $comment)
        <div class="media">
            <a class="pull-left" href="{{route('user.post.index', $comment->user->email)}}">
                <img class="media-object img-thumbnail" src="{{get_gravatar_url($comment->user->email, Config::get('setting.user.thumb'))}}" alt="{{$comment->user->email}}">
            </a>

            <div class="media-body">
                <h4 class="media-heading">
                    <a href="{{route('user.post.index', $comment->user->email)}}">{{$comment->user->email}}</a>
                </h4>
                {{$comment->content}}
                <small class="text-muted">{{format_timestring($comment->created_at)}}</small>
            </div>
        </div> <!-- end .media (single comment) -->
    @endforeach
    </comments> <!-- end comments list -->

@else
    <p class="text-center">No comment yet! Please become the first one.</p>
@endif

