@extends('layouts.minipage')

@section('content')
    <!-- Carousel -->
    <div id="myCarousel" class="carousel slide">
        <!-- Indicators -->
        <ol class="carousel-indicators">
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1"></li>
            <li data-target="#myCarousel" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner">
            <div class="item active">
                <!-- <img src="" alt="ABC란?"> -->
                <div class="container">
                    <div class="carousel-caption">
                        <h2>ABC는?</h2>

                        <p>ABC = Always Best Connected. 올레 tv mobile의 ON AIR 방송 시청할 때 1) 요금 폭탄의 불안감과 2) 서비스 멈춤/튕김의 불편함을 덜어
                            주는 솔루션입니다.</p>

                        <p><a class="btn btn-lg btn-danger" href="{{$data['playStoreLink']}}" target="_blank"> <span
                                    class="glyphicon glyphicon-cloud-download"></span> 설치하기 </a></p>
                    </div>
                </div>
            </div>
            <div class="item">
                <!-- <img src="" alt="Lte/3G 요금 절약"> -->
                <div class="container">
                    <div class="carousel-caption">
                        <h2>Lte/3G 요금 절약</h2>

                        <p>ABC가 Lte/3G 데이터를 최대 80%(평균 50%)까지 아껴 드려요. 같은 Lte/3G 데이터량으로 5배나 더 긴 시간을 사용할 수 있다는 얘기죠.</p>

                        <p><a class="btn btn-lg btn-danger" href="{{$data['playStoreLink']}}" target="_blank"> <span
                                    class="glyphicon glyphicon-cloud-download"></span> 설치하기 </a></p>
                    </div>
                </div>
            </div>
            <div class="item">
                <!-- <img src="" alt="서비스 품질 향상"> -->
                <div class="container">
                    <div class="carousel-caption">
                        <h2>서비스 품질 향상</h2>

                        <p>ABC를 사용하시면, ON AIR 비디오 시청 시작 못함, 재생 중 멈춤(=버퍼링), 통신 장애로 인한 갑작스런 비디오 종료 등이 눈에 띄게 줄어 들어요. </p>

                        <p><a class="btn btn-lg btn-danger" href="{{$data['playStoreLink']}}" target="_blank"> <span
                                    class="glyphicon glyphicon-cloud-download"></span> 설치하기 </a></p>
                    </div>
                </div>
            </div>
        </div>
        <a class="left carousel-control" href="#myCarousel" data-slide="prev"><span
                class="glyphicon glyphicon-chevron-left"></span></a>
        <a class="right carousel-control" href="#myCarousel" data-slide="next"><span
                class="glyphicon glyphicon-chevron-right"></span></a>
    </div><!-- /.carousel -->

    <div class="container">
        <div class="fb-like" data-href="{{$data['likeUrl']}}" data-layout="button_count" data-action="like"
             data-show-faces="false" data-share="false"></div>
    </div>

    <div class="container marketing"><!-- START THE FEATURETTES -->

        <div class="row featurette">
            <div class="col-sm-7">
                <h3 class="featurette-heading text-danger">Wi-Fi 자동 ON/OFF <br/>
                    <small class="text-muted">Wi-Fi 켰다가 끄는 거 불편하셨죠?</small>
                </h3>
                <p class="lead">ABC는 올레 tv mobile이 동작할 때 스마트폰의 Wi-Fi를 자동으로 켭니다. 올레 tv mobile이 종료되면 원래 설정 상태로 자동 복원합니다. ABC는
                    Wi-Fi를 최대한 이용하면서, 품질 좋은 Wi-Fi가 없을 때만 Lte/3G를 사용함으로써 여러분의 데이터 요금을 아껴드립니다. 이제 손으로 Wi-Fi 켰다 껐다 하지 마세요.</p>
            </div>
            <div class="col-sm-5">
                <img class="featurette-image img-responsive" src="/images/minipage/abc/1_wifi_onoff.png">
            </div>
        </div>

        <hr class="featurette-divider">

        <div class="row featurette">
            <div class="col-sm-7">
                <h3 class="featurette-heading text-danger">최적 네트워크 자동 선택 <br/>
                    <small class="text-muted">Wi-Fi&lt;-&gt;Lte/3G 뭐 쓰지? 뭐 이리 복잡해?</small>
                </h3>
                <p class="lead">Wi-Fi 켜고, 여러 Wi-Fi 중 어떤 것에 붙일까 고민하시죠? '현재 내 위치에서 Wi-Fi가 있을 지?', '또 여러 Wi-Fi 중 어떤 Wi-Fi가 더
                    품질이 좋을지?' 에 대한 답은 여러분들 보다 ABC가 더 잘 알고 있습니다. 통신이 잘되는 Wi-Fi를 자동으로 선택하여 올레 tv mobile ON AIR 서비스의 품질을 높여
                    드립니다. 이제 네트워크 선택에 대한 고민은 ABC에게 맡기세요.</p>
            </div>
            <div class="col-sm-5">
                <img class="featurette-image img-responsive" src="/images/minipage/abc/2_best_network_selection.png">
            </div>
        </div>

        <hr class="featurette-divider">

        <div class="row featurette">
            <div class="col-sm-7">
                <h3 class="featurette-heading text-danger">Wi-Fi와 Lte/3G 동시 사용 <br/>
                    <small class="text-muted">품질 유지하면서 데이터를 한번 더 아껴 드려요.</small>
                </h3>
                <p class="lead">올레 tv mobile의 ON AIR 비디오 즐기는데 필요한 성능이 100인데 Wi-Fi 성능이 90밖에 안된다면, 부족한 10을 Lte/3G에서 빌려와서 보충해
                    드려요. 90이나 되는 성능을 버리고 Lte/3G로 전환해서 100을 전부 쓴다면 완전 아깝죠? ABC는 Wi-Fi를 완전 사용할 수 없을 때만 Lte/3G를 사용한답니다. 지구상에서
                    ABC만이 제공하는 기능입니다.</p>
            </div>
            <div class="col-sm-5">
                <img class="featurette-image img-responsive" src="/images/minipage/abc/3_blended_boost.png">
            </div>
        </div>

        <hr class="featurette-divider">
        <!-- /END THE FEATURETTES -->

    </div><!-- /.container -->
@stop

@section('script')
    <script>
        var airplug = {
            package: "com.airplug.abc.agent"
        }
    </script>
@stop


