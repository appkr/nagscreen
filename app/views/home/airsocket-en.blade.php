@extends('layouts.minipage')

@section('content')
    <!-- Carousel -->
    <div id="myCarousel" class="carousel slide">
        <!-- Indicators -->
        <ol class="carousel-indicators">
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1"></li>
            <li data-target="#myCarousel" data-slide-to="2"></li>
            <li data-target="#myCarousel" data-slide-to="3"></li>
        </ol>
        <div class="carousel-inner">
            <div class="item active">
                <!-- <img src="" alt="AirSocket&reg;이란?"> -->
                <div class="container">
                    <div class="carousel-caption">
                        <h2>AirSocket<sup>&reg;</sup>은?</h2>

                        <p>스마트폰으로 동영상볼 때나 음악들을 때 1) 요금 폭탄의 불안감과 2) 서비스 멈춤/튕김의 불편함 뿐만아니라 3) 네트워크 선택의 피로감을 덜어 주는 솔루션입니다.</p>

                        <p><a class="btn btn-lg btn-info" href="{{$data['playStoreLink']}}" target="_blank"> <span
                                    class="glyphicon glyphicon-cloud-download"></span> 설치하기 </a></p>
                    </div>
                </div>
            </div>
            <div class="item">
                <!-- <img src="" alt="Lte/3G 요금 절약"> -->
                <div class="container">
                    <div class="carousel-caption">
                        <h2>Lte/3G 요금 절약</h2>

                        <p>AirSocket<sup>&reg;</sup>이 Lte/3G 데이터를 최대 80%(평균 50%)까지 아껴 드려요. 같은 Lte/3G 데이터량으로 5배나 더 긴 시간을 사용할
                            수 있다는 얘기죠.</p>

                        <p><a class="btn btn-lg btn-info" href="{{$data['playStoreLink']}}" target="_blank"> <span
                                    class="glyphicon glyphicon-cloud-download"></span> 설치하기 </a></p>
                    </div>
                </div>
            </div>
            <div class="item">
                <!-- <img src="" alt="서비스 품질 향상"> -->
                <div class="container">
                    <div class="carousel-caption">
                        <h2>서비스 품질 향상</h2>

                        <p>AirSocket<sup>&reg;</sup>을 사용하시면, 시작 못함, 재생 중 멈춤(=버퍼링), 통신 장애로 인한 갑작스런 비디오/음악 서비스 종료 등이 눈에 띄게 줄어
                            들어요. </p>

                        <p><a class="btn btn-lg btn-info" href="{{$data['playStoreLink']}}" target="_blank"> <span
                                    class="glyphicon glyphicon-cloud-download"></span> 설치하기 </a></p>
                    </div>
                </div>
            </div>
            <div class="item">
                <!-- <img src="" alt="피곤함 감소"> -->
                <div class="container">
                    <div class="carousel-caption">
                        <h2>피곤함 감소</h2>

                        <p>요금 폭탄이 무서워 Wi-Fi를 껐다 켰다! 기본 제공 데이터량을 넘을까 안절 부절! 어떤 Wi-Fi에 붙여야 할지 갈팡 질팡! 이런 피곤한 일은
                            AirSocket<sup>&reg;</sup>에게 맡기세요.</p>

                        <p><a class="btn btn-lg btn-info" href="{{$data['playStoreLink']}}" target="_blank"> <span
                                    class="glyphicon glyphicon-cloud-download"></span> 설치하기 </a></p>
                    </div>
                </div>
            </div>
        </div>
        <a class="left carousel-control" href="#myCarousel" data-slide="prev"><span
                class="glyphicon glyphicon-chevron-left"></span></a>
        <a class="right carousel-control" href="#myCarousel" data-slide="next"><span
                class="glyphicon glyphicon-chevron-right"></span></a>
    </div><!-- /.carousel -->

    <div class="container">
        <div class="fb-like" data-href="{{$data['likeUrl']}}" data-layout="button_count" data-action="like"
             data-show-faces="false" data-share="false"></div>
    </div>

    <div class="container marketing"><!-- START THE FEATURETTES -->

        <div class="row featurette">
            <div class="col-sm-7">
                <h3 class="featurette-heading text-info">최적 네트워크 자동 선택 <br/>
                    <small class="text-muted">Wi-Fi&lt;-&gt;Lte/3G 뭐 쓰지? 뭐 이리 복잡해?</small>
                </h3>
                <p class="lead">Wi-Fi 켜고, 여러 Wi-Fi 중 어떤 것에 붙일까 고민하시죠? '현재 내 위치에서 Wi-Fi가 있을 지?', '또 여러 Wi-Fi 중 어떤 Wi-Fi가 더
                    품질이 좋을지?' 에 대한 답은 여러분들 보다 AirSocket<sup>&reg;</sup>이 더 잘 알고 있습니다. 통신이 잘되는 Wi-Fi를 자동으로 선택하여 여러분이 사용하시는
                    비디오, 음악 서비스의 품질을 높여 드립니다. AirSocket<sup>&reg;</sup>은 Wi-Fi를 최대한 이용하면서, 품질 좋은 Wi-Fi가 없을 때만 Lte/3G를 사용함으로써
                    여러분의 데이터 요금을 아껴드립니다. 이제 네트워크 선택의 귀찮음은 AirSocket<sup>&reg;</sup>에게 맡기세요.</p>
            </div>
            <div class="col-sm-5">
                <img class="featurette-image img-responsive" src="/images/minipage/airsocket/2_best_network_selection.png">
            </div>
        </div>

        <hr class="featurette-divider">

        <div class="row featurette">
            <div class="col-sm-7">
                <h3 class="featurette-heading text-info">Wi-Fi와 Lte/3G 동시 사용 <br/>
                    <small class="text-muted">품질 유지하면서 데이터를 한번 더 아껴 드려요.</small>
                </h3>
                <p class="lead">비디오 또는 음악을 즐기는데 필요한 성능이 100인데 Wi-Fi 성능이 90밖에 안된다면, 부족한 10을 Lte/3G에서 빌려와서 보충해 드려요. 90이나 되는
                    성능을 버리고 Lte/3G로 전환해서 100을 전부 쓴다면 완전 아깝죠? AirSocket<sup>&reg;</sup>은 Wi-Fi가 완전 사용할 수 없을 때만 Lte/3G를
                    사용한답니다. 지구상에서 AirSocket<sup>&reg;</sup>만이 제공하는 기능입니다.</p>
            </div>
            <div class="col-sm-5">
                <img class="featurette-image img-responsive" src="/images/minipage/airsocket/3_blended_boost.png">
            </div>
        </div>

        <hr class="featurette-divider">

        <div class="row featurette">
            <div class="col-sm-7">
                <h3 class="featurette-heading text-info">Lte/3G 데이터 제한<br/>
                    <small class="text-muted">요금 폭탄 걱정 뚝!</small>
                </h3>
                <p class="lead">Lte나 3G로 비디오 볼 때 불안하시죠? 고화질 비디오(1.3Mbps)를 Lte/3G 데이터로만 100분 동안 시청하면 대략 1 기가바이트를 쓰게 됩니다.
                    Lte62요금제의 경우, 기본 제공 데이터 5 기가바이트로 고화질 비디오 500분 보면 땡! AirSocket<sup>&reg;</sup>의 데이터 사용 제한 설정에서 앱별로 허용할
                    데이터량을 설정하시고 요금폭탄의 불안감으로부터 해방되세요. 아울러, AirSocket<sup>&reg;</sup>은 최근 3개월 동안의 사용 통계를 친절하게 표시해 드립니다.</p>
            </div>
            <div class="col-sm-5">
                <img class="featurette-image img-responsive" src="/images/minipage/airsocket/4_cell_limit.png">
            </div>
        </div>

        <hr class="featurette-divider">

        <div class="row featurette">
            <div class="col-sm-7">
                <h3 class="featurette-heading text-info">마이파이 (MyFi) <br/>
                    <small class="text-muted">마이파이 존에선 Lte/3G만 사용.</small>
                </h3>
                <p class="lead">집이나 사무실에 있는 자주 쓰는 Wi-Fi를 마이파이로 등록할 수 있어요. 마이파이 존에서는 Lte/3G데이터를 전혀 사용하지 않습니다. 비디오/음악을 즐기는 중에
                    마이파이 존을 벗어난 경우 Lte/3G를 전혀 사용하지 않기 때문에 비디오/음악이 멈추게됩니다. 이 때에도 Lte/3G로 계속 이어서 보기를 선택할 수 있는 기능이 곧 출시 예정입니다.
                    마이파이 존에 있을 경우 마이파이 아이콘이, 벗어나면 벗어났다는 아이콘이 재생기 화면에 잠시 동안 표시됩니다.</p>
            </div>
            <div class="col-sm-5">
                <img class="featurette-image img-responsive" src="/images/minipage/airsocket/5_myfi.png">
            </div>
        </div>

        <hr class="featurette-divider">

        <div class="row featurette">
            <div class="col-sm-7">
                <h3 class="featurette-heading text-info">좋은 Wi-Fi 찾기 <br/>
                    <small class="text-muted">쓸만한 Wi-Fi 찾기 힘드시죠?</small>
                </h3>
                <p class="lead">현재 위치에서 가장 좋은 Wi-Fi를 찾은 후 비디오 또는 음악 앱을 시작하세요. AirSocket<sup>&reg;</sup>이 찾은 Wi-Fi의 품질을
                    '좋음'-'나쁨'' 뿐만아니라, 웹, 음악, 비디오 등 어떤 서비스에 적합한 품질인지도 표시해 준답니다. AirSocket<sup>&reg;</sup>이 찾은 Wi-Fi의 품질이 마음에
                    안든다면 다시 한번 버튼을 눌러 다른 Wi-Fi를 찾아 보세요.</p>
            </div>
            <div class="col-sm-5">
                <img class="featurette-image img-responsive" src="/images/minipage/airsocket/6_find_wifi.png">
            </div>
        </div>

        <hr class="featurette-divider">
        <!-- /END THE FEATURETTES -->

    </div><!-- /.container -->
@stop

@section('script')
    <script>
        var airplug = {
            package: "com.airplug.mao.agent"
        }
    </script>
@stop

