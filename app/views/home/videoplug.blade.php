@extends('layouts.minipage')

@section('content')
    <!-- Carousel -->
    <div id="myCarousel" class="carousel slide">
        <!-- Indicators -->
        <ol class="carousel-indicators">
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1"></li>
            <li data-target="#myCarousel" data-slide-to="2"></li>
            <li data-target="#myCarousel" data-slide-to="3"></li>
        </ol>
        <div class="carousel-inner">
            <div class="item active">
                <!-- <img src="" alt="비디오플러그는?"> -->
                <div class="container">
                    <div class="carousel-caption">
                        <h2>비디오플러그는?</h2>

                        <p>무선 네트워크 환경에서 미디어 전송 솔루션을 개발하는 (주)에어플러그에서 개발한 비디오 신디케이션/큐레이션 앱입니다. YouTube, Vimeo, PodCast 등 다양한
                            비디오서비스를 하나의 앱에서 볼 수 있게 해 준답니다.</p>

                        <p><a class="btn btn-lg btn-primary" href="{{$data['playStoreLink']}}" target="_blank"> <span
                                    class="glyphicon glyphicon-cloud-download"></span> 설치하기 </a></p>
                    </div>
                </div>
            </div>
            <div class="item">
                <!-- <img src="" alt="비디오 신디케이션"> -->
                <div class="container">
                    <div class="carousel-caption">
                        <h2>비디오 신디케이션</h2>

                        <p>다양한 비디오 서비스에서 가장 인기가 높은 최신 비디오들만을 엄선해서 다양한 볼거리를 제공합니다. 검색어 하나로 여러 사이트를 한번에 검색하고, 검색결과는 채널로 등록해서
                            항상 최신 업데이트를 확인할 수 있어요.</p>

                        <p><a class="btn btn-lg btn-primary" href="{{$data['playStoreLink']}}" target="_blank"> <span
                                    class="glyphicon glyphicon-cloud-download"></span> 설치하기 </a></p>
                    </div>
                </div>
            </div>
            <div class="item">
                <!-- <img src="" alt="개인화"> -->
                <div class="container">
                    <div class="carousel-caption">
                        <h2>개인화된 비디오 채널</h2>

                        <p>카테고리별, 서비스공급자별, 언어별로 미리 제공되는 채널 외에 통합검색 결과를 채널로 등록해 여러분만의 채널을 만들고 즐길 수 있어요.</p>

                        <p><a class="btn btn-lg btn-primary" href="{{$data['playStoreLink']}}" target="_blank"> <span
                                    class="glyphicon glyphicon-cloud-download"></span> 설치하기 </a></p>
                    </div>
                </div>
            </div>
            <div class="item">
                <!-- <img src="" alt="Tv 다시 보기"> -->
                <div class="container">
                    <div class="carousel-caption">
                        <h2>Tv 다시 보기</h2>

                        <p>각 방송사의 채널을 미리 등록해 두었어요. 지나간 드라마, 다큐, 연예오락 전편을 차곡차곡 챙겨 볼 수 있죠. 비디오플러그의 연속재생 기능이 순서대로 재생시켜 주니 이건
                            뭐~~ 돈 내고 지난 방송 볼 필요 없죠.</p>

                        <p><a class="btn btn-lg btn-primary" href="{{$data['playStoreLink']}}" target="_blank"> <span
                                    class="glyphicon glyphicon-cloud-download"></span> 설치하기 </a></p>
                    </div>
                </div>
            </div>
        </div>
        <a class="left carousel-control" href="#myCarousel" data-slide="prev"><span
                class="glyphicon glyphicon-chevron-left"></span></a>
        <a class="right carousel-control" href="#myCarousel" data-slide="next"><span
                class="glyphicon glyphicon-chevron-right"></span></a>
    </div><!-- /.carousel -->

    <div class="container">
        <div class="fb-like" data-href="{{$data['likeUrl']}}" data-layout="button_count" data-action="like"
             data-show-faces="false" data-share="false"></div>
    </div>

    <div class="container marketing"><!-- START THE FEATURETTES -->

        <div class="row featurette">
            <div class="col-sm-8">
                <h3 class="featurette-heading text-primary"><a href="{{route('airsocket')}}">AirSocket<sup>&reg;</sup></a> 연동 <br/>
                    <small class="text-muted">세상에서 가장 똑똑한 네트워크 접속 앱</small>
                </h3>
                <p class="lead">비디오플러그를 사용할 때, Wi-Fi 우선 사용으로 Lte/3G요금을 아끼는 동시에, 최적의 네트워크 선택으로 비디오 재생 품질을 높여 주는 (주)에어플러그에서
                    개발한 특허 받은 솔루션입니다. AirSocket<sup>&reg;</sup>은 비디오플러그 설정에서 'AirSocket 활성'을 ON하는 순간 Play스토어로 이동하여 설치할 수
                    있습니다. AirSocket<sup>&reg;</sup>에 대한 더 자세한 내용은 <a href="{{route('airsocket')}}">이 링크</a>에서 확인하세요.</p>
            </div>
            <div class="col-sm-4">
                <img class="featurette-image img-responsive" src="/images/minipage/videoplug/1_airsocket.png">
            </div>
        </div>

        <hr class="featurette-divider">

        <div class="row featurette">
            <div class="col-sm-8">
                <h3 class="featurette-heading text-primary">통합검색 <br/>
                    <small class="text-muted">하나의 검색어로 여러 사이트를 한번에</small>
                </h3>
                <p class="lead">원하는 비디오가 현재 사이트에 없을 경우가 있죠? 비디오플러그는 여러 사이트(=서비스)를 통합해서 보여주고 하나의 검색어로 여러 사이트에서 검색한 결과를 표시해
                    줍니다. 통합검색에서 사이트 이름으로 된 탭(Tab)을 선택하시면 되지요. 뿐만 아니라, 검색어를 채널로 등록해 두시면 매번 검색할 필요없어 편리해요. 현재는 YouTube와 Vimeo만
                    지원하지만, 지원 사이트는 점점 늘어날 예정입니다.</p>
            </div>
            <div class="col-sm-4">
                <img class="featurette-image img-responsive" src="/images/minipage/videoplug/2_search.png">
            </div>
        </div>

        <hr class="featurette-divider">

        <div class="row featurette">
            <div class="col-sm-8">
                <h3 class="featurette-heading text-primary">Tv 다시 보기 <br/>
                    <small class="text-muted">MBC, KBS, EBS, CJ...</small>
                </h3>
                <p class="lead">Tv를 다시 볼 수 있는 앱/서비스는 많죠? 그런데, 기업에서 내 놓은 앱은 요금을 지불해야 하고, 개인이 내 놓은 앱은 광고로 도배되어 있거나 중국의 이상한
                    사이트로 넘어가기도 하죠? 비디오플러그는 YouTube에서 제공하는 Tvshows 기능을 이용해서 Tv 다시보기를 시즌/에피소드별로 깔끔하게 정렬해서 보여 드립니다. 비디오플러그의
                    연속재생 기능을 같이 이용하면 50분짜리 Tv 프로그램 한편을 편안하게 보실 수 있답니다. 당연히 무료이고, 광고도 없습니다.</p>
            </div>
            <div class="col-sm-4">
                <img class="featurette-image img-responsive" src="/images/minipage/videoplug/3_tvshows.png">
            </div>
        </div>

        <hr class="featurette-divider">

        <div class="row featurette">
            <div class="col-sm-8">
                <h3 class="featurette-heading text-primary">편리한 네비게이션 <br/>
                    <small class="text-muted">상하 또는 좌우로 스크래치해보세요.</small>
                </h3>
                <p class="lead">비디오 채널들이 표시되는 메인페이지, 메인페이지에서 선택한 하나의 비디오에 관한 페이지, 통합검색, 북마크 등등 비디오플러그의 모든 페이지는 상하좌우 터치가
                    가능합니다. 더군다나, 선택된 하나의 비디오에 대한 상세페에서는 해당 채널의 인접 영상을 제공할 뿐아니라(=비디오탐색기), 관련동영상 목록도 제공하여 편리하게 다른 비디오로 넘어가실 수
                    있어요. 채널설정, 메인페이지/통합검색 상단의 제목 탭도 많아지면 좌우 스크롤이 가능하답니다.</p>
            </div>
            <div class="col-sm-4">
                <img class="featurette-image img-responsive" src="/images/minipage/videoplug/4_navigation.png">
            </div>
        </div>

        <hr class="featurette-divider">

        <div class="row featurette">
            <div class="col-sm-8">
                <h3 class="featurette-heading text-primary">SNS 연동 <br/>
                    <small class="text-muted">괜찮은 비디오를 발견하면 친구와 공유하세요.</small>
                </h3>
                <p class="lead">친구와 나누고 싶은 비디오를 발견하고 영상 주소를 복사해서 붙여넣는 것은 이제 그만. 카톡까지도 연동되어 있답니다. 또, YouTube 계정을 등록해 놓았다면, 다른
                    기기, 앱에서 등록한 나의 즐겨찾기, 구독영상 등등을 비디오플러그의 채널로 가져와 즐길 수 있어요. 아직 YouTube 공식앱에 비해서는 부족한 부분이 많지만, AirSocket연동 등
                    그들이 제공하지 못하는 가치를 제공하고 있습니다.</p>
            </div>
            <div class="col-sm-4">
                <img class="featurette-image img-responsive" src="/images/minipage/videoplug/5_snsshare.png">
            </div>
        </div>

        <hr class="featurette-divider">

        <div class="row featurette">
            <div class="col-sm-6">
                <h3 class="featurette-heading text-primary">편리한 재생기 <br/>
                    <small class="text-muted">특화된 비디오 플레이어</small>
                </h3>
                <p class="lead">제스처 빼고 다른 플레이어가 하는 기본은 다합니다. 하지만, 연속재생 등에서 재생목록의 순서 변경등은 비디오플러그만이 제공하는 기능이에요.</p>
            </div>
            <div class="col-sm-6">
                <img class="featurette-image img-responsive" src="/images/minipage/videoplug/6_player.png">
            </div>
        </div>

        <hr class="featurette-divider">
        <!-- /END THE FEATURETTES -->

    </div><!-- /.container -->
@stop

@section('script')
    <script>
        var airplug = {
            package: "com.airplug.videoplug"
        }
    </script>
@stop


