@extends('layouts.master')

@section('content')

    <script type="text/javascript" src="/ookla/speedtest/swfobject.js?v=2.2"></script>

    <div id="mini-demo" style="display:block; margin:2em auto;">
        Speedtest.net Mini requires at least version 8 of Flash. Please
        <a href="http://get.adobe.com/flashplayer/">update your client</a>.
    </div>

    <script type="text/javascript">
        var flashvars = {
            upload_extension: "php"
        };
        var params = {
            wmode: "transparent",
            quality: "high",
            menu: "false",
            allowScriptAccess: "always"
        };
        var attributes = {};
        swfobject.embedSWF("/ookla/speedtest.swf?v=2.1.8", "mini-demo", "350", "200", "9.0.0", "/ookla/speedtest/expressInstall.swf", flashvars, params, attributes);
    </script>

@stop