@extends('layouts.minipage')

@section('style')
    <style>
        body {
            margin-top: 1em;
            line-height: 1.6em;
        }

        ul li {
            margin-bottom: 0.6em;
        }

        ul .detaillet {
            margin-bottom: 2em;
        }

        ul .listlet {
            margin-bottom: 3em;
        }

        li img {
            margin-left: 1em;
            margin-right: 1em;
            width: 2em;
        }
    </style>
@stop

@section('content')
<!--    <div class="container">-->
<!--        <div class="fb-like" data-href="{{$data['likeUrl']}}" data-layout="button_count" data-action="like"-->
<!--             data-show-faces="false" data-share="false"></div>-->
<!--    </div>-->

    <div class="container">
        <h4 class="text-primary">hoppin에서 AirSocket&reg; 기능을 사용하시면,</h4>
        
        <div>
            <ul class="listlet">
                <li><p class="lead">지하철, 카페, 언제 어디서나 끊김 없이 동영상을 즐기실 수 있습니다.</p></li>
                <ul class="detaillet">
                    <li>미디어 재생 시 Wi-Fi와 3G/LTE를 동시에 사용하여 재생 중 끊김 현상을 방지합니다.</li>
                    <li>자동으로 최적의 Wi-Fi를 찾아주고, Wi-Fi를 최대한 사용하여 3G/LTE 데이터를 절약 해 드립니다.</li>
                    <li>사용 가능한 Wi-Fi가 없거나 속도가 충분하지 않은 경우, 꼭 필요한 만큼만 3G/LTE를 사용하여 재생 중 끊김이 없도록 합니다.</li>
                </ul>
                <li><p class="lead">대용량 컨텐츠를 빠르게 다운로드 받으실 수 있습니다.</p></li>
                <ul class="detaillet">
                    <li>Wi-Fi와 3G/LTE를 동시에 사용하여 가장 빠른 속도로 컨텐츠를 다운로드 합니다.</li>
                    <li>3G/LTE에서도 자동으로 주변의 Wi-Fi를 찾아서 연결하여 다운로드 합니다.</li>
                </ul>
            </ul>
        </div> <!-- featurelet -->

        <h4 class="text-primary">AirSocket® 동작 시에는 아래와 같은 아이콘을 알림창에서 확인하실 수 있습니다.</h4>

        <div>
            <ul class="list-unstyled">
                <li><img src="/images/minipage/airsocketforhoppin/notification_wifi.png"/> Wi-Fi에 연결되어 있습니다.</li>
                <li><img src="/images/minipage/airsocketforhoppin/notification_cell.png"/> 3G/LTE에 연결되어 있습니다.</li>
                <li><img src="/images/minipage/airsocketforhoppin/notification_bb.png"/> Wi-Fi, 3G/LTE를 동시에 사용 중입니다.</li>
            </ul>
        </div> <!--f featurelet -->

        <p class="text-center"><img src="/images/minipage/airsocketforhoppin/1-about-airsocket.png" class="img-responsive"></p>
    </div><!-- container -->
@stop

