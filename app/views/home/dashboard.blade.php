<!DOCTYPE html>

<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{{ $project['name'] }}}</title>

    <!-- Favicon -->
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
    <link rel="icon" href="/favicon.ico" type="image/x-icon">

    <!-- Bootstrap & Font-awewome -->
    <link rel="stylesheet" href="/css/vendor/bootstrap/lumen/bootstrap.min.css">
    <link rel="stylesheet" href="/css/vendor/font-awesome/font-awesome.min.css">

    <!-- PNotify -->
    <link rel="stylesheet" href="/css/vendor/pnotify/pnotify.custom.min.css">

    <!-- Method override & custom styling -->
    <link rel="stylesheet" href="/css/mystyle.css">

    <!-- Base style override -->
    <style>
        body {
            background-color: #fff;
            margin-top: 50px;
        }
    </style>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="//oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="//oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>

    @include('layouts.navigation')

    <div class="jumbotron">
        <div class="container">
            <h2 class="text-center jumbotron-headline">Let's interact with the app users</h2>
            <p class="text-center jumbotron-subhead">Deliver Nag or Push Messages / Capture users opinions and provide Feedbacks</p>
            <p class="text-center jumbotron-subhead text-danger">This is a service only for AirPlug app marketers/developers</p>

            <p class="text-center jumbotron-buttons">
                <a href="{{route('post.index')}}" class="btn btn-primary btn-lg" role="button">NAG MESSAGES &raquo;</a>
                <a href="{{route('api.index')}}" class="btn btn-primary btn-lg" role="button">{{is_mobile() ? 'API DOCS' : 'API DOCUMENTATION'}} &raquo;</a>
            </p>
        </div>
    </div>

    <div class="container">
        <div class="row featurelet">
            <div class="col-sm-4 featurelet-item">
                <h1 class="text-center featurelet-image"><i class="fa fa-envelope-o"></i></h1>
                <h2 class="text-center">{{$count['post']}}</h2>
                <p class="text-center lead">Nag Messages</p>
                <ul class="text-center featurelet-description list-unstyled">
                    <li>Design Nag Messages/Popups at the web console</li>
                    <li>Deliver Nag Messages through data api</li>
                </ul>
                <p class="text-center featurelet-buttons"><a class="btn btn-default" href="{{route('post.index')}}" role="button">View details &raquo;</a></p>
            </div>
            <div class="col-sm-4 featurelet-item">
                <h1 class="text-center featurelet-image"><i class="fa fa-frown-o"></i></h1>
                <h2 class="text-center">{{$count['voc']}}</h2>
                <p class="text-center lead">Voice of Customers</p>
                <ul class="text-center featurelet-description list-unstyled">
                    <li>Capture complaint/improvement opinions from app users</li>
                    <li>Send feedback email from the web console</li>
                </ul>
                <p class="text-center featurelet-buttons"><a class="btn btn-default" href="{{route('voc.index')}}" role="button">View details &raquo;</a></p>
            </div>
            <div class="col-sm-4 featurelet-item">
                <h1 class="text-center featurelet-image"><i class="fa fa-bullhorn"></i></h1>
                <h2 class="text-center">{{$count['pushuser']}}</h2>
                <p class="text-center lead">Push Subscribers</p>
                <ul class="text-center featurelet-description list-unstyled">
                    <li>Create/destroy push subscribers through api call</li>
                    <li>Design and send push message from the web console</li>
                </ul>

                <p class="text-center featurelet-buttons"><a class="btn btn-default" href="{{route('push.index')}}" role="button">View details &raquo;</a></p>
            </div>
        </div> <!-- featurelet -->

        @if ($vocs->count())
        <div class="row">
            <div class="col-sm-4"><hr/></div>
            <div class="col-sm-4"><p class="text-center lead">Most Recent VoCs & Feedback</p></div>
            <div class="col-sm-4"><hr/></div>
        </div>

        <div class="row voclet">
            @foreach($vocs as $voc)
            <div class="col-sm-4 voclet-item">
                <article class="text-center">
                    <p><i class="fa fa-quote-left"></i> {{str_limit($voc->content, 200, '...')}} <i class="fa fa-quote-right"></i></p>
                </article>
                <div class="voclet-meta">
                    @if($voc->email)
                    <a class="pull-left" href="{{get_gravatar_profile_url($voc->email)}}">
                        <img class="img-circle" src="{{get_gravatar_url($voc->email)}}" alt="{{$voc->email}}">
                    </a>
                    @else
                    <a class="pull-left" href="#">
                        <img class="img-circle" src="/images/no-image.png" alt="Unknown user">
                    </a>
                    @endif

                    <h4>
                        @if($voc->email)
                        <a href="{{get_gravatar_profile_url($voc->email)}}">{{$voc->email}}</a>
                        @else
                        <a href="#">Unknown User</a>
                        @endif
                    </h4>

                    <small class="text-muted">
                        <i class="fa fa-dropbox"></i> {{$voc->package->name}} &nbsp;
                        <i class="fa fa-calendar"></i> {{format_timestring($voc->created_at)}} &nbsp;
                    </small>
                </div>

                @if ($voc->feedbacks()->count())
                <article class="voclet-feedback text-center clear-fix">
                    <p class="text-primary"><i class="fa fa-quote-left"></i> {{strip_tags($voc->feedbacks[0]->content)}} <i class="fa fa-quote-right"></i></p>
                </article>
                @endif
            </div>
            @endforeach
        </div> <!-- voclet -->
        @endif
    </div> <!-- container -->

    <footer class="container">
        <hr/>
        <p class="pull-right"><a href="#">Back to top</a></p>

        <p>&copy; {{date('Y')}} AirPlug Inc. <a href="{{route('home')}}">{{$project['name']}}</a></p>
    </footer>


    <!-- jQuery -->
    <script src="/js/vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap -->
    <script src="/js/vendor/bootstrap/bootstrap.min.js"></script>

    <!-- PNotify -->
    <script src="/js/vendor/pnotify/pnotify.custom.min.js"></script>

    @include('layouts.flash')

</body>

</html>