@extends('layouts.master')

@section('content')
    <div class="page-header">
        <h4 class="blog-post-title">
            <i class="fa fa-cogs"></i> Settings
        </h4>
    </div>

    {{ Form::open(['route' => ['setting.update', 1], 'method' => 'PUT', 'role' => 'form']) }}

    @foreach ($setting as $key => $value)
        <div class="form-group">
            <label for="{{$key}}">{{$key}}</label>
            <input type="text" name="{{$key}}" id="{{$key}}" class="form-control" value="{{$value}}"/>
        </div>
    @endforeach

    <p class="text-center">
    <div class="btn-group pull-right">
        <button type="submit" class="btn btn-primary btn-lg">SUBMIT</button>
        <a href="{{route('setting.index')}}" class="btn btn-default btn-lg">RESET</a>
    </div>
    </p>

    {{ Form::close() }}
@stop