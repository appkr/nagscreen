@extends('layouts.master')

@section('content')

    @if ($voc)
        <div class="page-header">
            <div class="btn-group pull-right">
                <a href="{{route('voc.index')}}" class="btn btn-default"><i class="fa fa-list-ul"></i> LIST</a>
            </div>
            <h4 class="blog-post-title">
                <i class="fa fa-frown-o"></i> {{$voc->title}}
            </h4>
        </div> <!-- end .page-header (article title) -->

        <div id="posts-wrapper">
            <div class="media">
                @if($voc->email)
                    <a class="pull-left" href="{{get_gravatar_profile_url($voc->email)}}">
                        <img class="media-object img-thumbnail" src="{{get_gravatar_url($voc->email)}}" alt="{{$voc->email}}">
                    </a>
                @else
                    <a class="pull-left" href="#">
                        <img class="media-object img-thumbnail" src="/images/no-image.png" alt="Unknown user">
                    </a>
                @endif

                <div class="media-body">
                    <h4 class="media-heading">
                        @if($voc->email)
                            <a href="{{get_gravatar_profile_url($voc->email)}}">{{$voc->email}}</a>
                        @else
                            <a href="#">Unknown User</a>
                        @endif
                    </h4>

                    <small class="text-muted">
                        <i class="fa fa-dropbox"></i> {{$voc->package->name}} &nbsp;
                        <i class="fa fa-calendar"></i> {{format_timestring($voc->created_at)}} &nbsp;
                    </small>
                </div>
            </div> <!-- end .media (single comment) -->

            <article>
                {{$voc->content}}
                <p>&nbsp;</p>

                <div class="btn-group pull-right">
                    <button class="btn btn-default btn-xs" id="delete-hook" title="Delete this voc">
                        <i class="fa fa-trash-o"></i> DELETE
                    </button>
                </div>
            </article> <!-- end article -->

        </div> <!-- end #posts-wrapper -->

        @include('feedback.index')
    @else
        <div class="page-header">
            <div class="btn-group pull-right">
                <a href="{{route('voc.index')}}" class="btn btn-default"><i class="fa fa-list-ul"></i> LIST</a>
            </div>
            <h4 class="blog-post-title">
                <i class="fa fa-microphone"></i> Not Found
            </h4>
        </div> <!-- end .page-header (article title) -->

        <div id="posts-wrapper">
            <article class="text-center"> Not Found.</article>
            <!-- end article -->
        </div> <!-- end #posts-wrapper -->
    @endif
@stop


@section('script')
    @if ($voc)
        <!-- CK Editor library -->
        <script src="/ckeditor/ckeditor.js"></script>

        <!-- Custom javascript -->
        <script>
            (function ($) {

                @if (Auth::check())
                    // Create CK Editor instance
                    CKEDITOR.replace('content', {toolbar: 'Basic', height: '10em'});
                @endif

                // Dealing AJAX delete request & response
                $("#delete-hook").on("click", function () {
                    if (confirm("You are going to delete this voc message. \n\nAre you sure?")) {
                        $.ajax({
                            url: "{{route('voc.destroy', $voc->id)}}",
                            type: "POST",
                            data: {
                                _token: "{{csrf_token()}}",
                                _method: "DELETE"
                            },
                            dataType: "json"
                        }).done(function (response) {
                            if (response.data.error) {
                                new PNotify({
                                    title: "Error",
                                    text: "Something went wrong! " + response.data.message,
                                    type: "error"
                                });

                                return;
                            }

                            new PNotify({
                                title: "Success",
                                text: "Deleted !",
                                type: "success"
                            });

                            setTimeout(function () {
                                location = "/voc";
                            }, 3000);
                        });
                    }
                });
            })(jQuery);
        </script>
    @endif

@stop



