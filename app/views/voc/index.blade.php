@extends('layouts.master')

@section('content')
    <div class="page-header">
        <h4 class="blog-post-title">
            <i class="fa fa-frown-o"></i> Voice of Customers
        </h4>
    </div>

    <div class="table-responsive">
        <table class="table table-bordered table-striped">
            <thead>
            <tr>
                <th>Package</th>
                <th>{{link_for_sort('voc.index', 'title', 'Title')}} {{format_active_sort('title')}}</th>
                <th>{{link_for_sort('voc.index', 'email', 'Customer Email')}} {{format_active_sort('email')}}</th>
                <th>{{link_for_sort('voc.index', 'created_at', 'Datetime')}} {{format_active_sort('created_at')}}</th>
            </tr>
            </thead>

            <tbody>

            @if($vocs->count())
                @foreach($vocs as $voc)
                <tr>
                    <td>{{$voc->package->name}}</td>
                    <td>
                        {{link_to_route('voc.show', $voc->title, ['id' => $voc->id], ['title' => $voc->title])}}
                        @if ($voc->feedbacks->count())
                            <small class="badge">{{$voc->feedbacks->count()}}</small>
                        @endif
                    </td>
                    <td>@if($voc->email) {{$voc->email}} @else Unknown User @endif</td>
                    <td>{{format_timestring($voc->created_at)}}</td>
                </tr>
                @endforeach
            @else
                <tr>
                    <td colspan="4"><p class="text-center">Not Found.</p></td>
                </tr>
            @endif

            </tbody>
        </table>
    </div>
@stop

@section('pagination')
    <div class="text-center">
        {{$vocs->appends(Request::except('page'))->links('pagination.slider')}}
    </div>
@stop
