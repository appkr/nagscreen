@extends('layouts.master')

@section('content')
    <div class="page-header">
        <h4 class="blog-post-title">
            @if(isset($keyword))
                <i class="fa fa-search"></i> Search
                @if(isset($keyword) && !empty($keyword))
                    <br/>
                    <small>filtered with keyword "{{$keyword}}"</small>
                @endif
            @else
                <i class="fa fa-anchor"></i> Users Activities
            @endif
        </h4>
    </div>

    <div class="table-responsive">
        <table class="table table-bordered table-striped">
            <thead>
            <tr>
                <th>{{link_for_sort('history.index', 'trackable_type', 'Service')}} {{format_active_sort('trackable_type')}}</th>
                <th>{{link_for_sort('history.index', 'title', 'Activity')}} {{format_active_sort('title')}}</th>
                <th>User</th>
                <th>{{link_for_sort('history.index', 'created_at', 'Datetime')}} {{format_active_sort('created_at')}}</th>
                @if(is_admin())
                    <th>IP addr.</th>
                @endif
            </tr>
            </thead>

            <tbody>
            @if($histories->count())
                @foreach($histories as $history)
                <tr>
                    <td>{{get_alias_from_model_name($history->trackable_type)}}</td>
                    <td>{{build_activity_string($history)}}</td>
                    <td>{{$history->user->email}}</td>
                    <td>{{format_timestring($history->created_at)}}</td>
                    @if(is_admin())
                        <td>{{$history->ip}}</td> <!--TODO google map popover-->
                    @endif
                </tr>
                @endforeach
            @else
                <tr>
                    <td colspan="{{(is_admin() ? 5 : 4)}}"><p class="text-center">Not Found !</p></td>
                </tr>
            @endif
            </tbody>
        </table>
    </div>
@stop

@section('pagination')
    <div class="text-center">
        {{$histories->links('pagination.slider')}}
    </div>
@stop
