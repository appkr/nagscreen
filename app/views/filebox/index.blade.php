@extends('layouts.master')

@section('style')
    <!-- Dropzone -->
    <link rel="stylesheet" href="/css/vendor/dropzone/dropzone.css">
@stop

@section('content')
    <div class="page-header">
        <h4 class="blog-post-title">
            <i class="fa fa-paperclip"></i> FileBox
        </h4>
    </div>

    <!-- Form element for Dropzone -->
    <form action="{{route('filebox.store')}}" method="post" enctype="multipart/form-data" id="my-awesome-dropzone" class="dropzone">
        <div class="fallback">
            <input name="file" type="file" multiple/>
        </div>
    </form>

    <div style="margin-top: 2em;">
        <h3 class="text-muted">
            <i class="fa fa-link"></i> Generate One-time URL for Upload
        </h3>
        <p>Send the generated url to the customer, so that he/she can safely upload a file to this page !</p>
    </div>

    <div class="panel panel-default">
        <div class="panel-body">
            <button class="generate-upload-url-hook btn btn-primary btn-xs" title="GENERATE"><i class="fa fa-bolt"></i> GENERATE ONE-TIME URL FOR UPLOAD</button>
            <div><a id="generated-upload-url" class="text-to-copy" data-clipboard-text="" title="COPY URL"></a></div>
        </div>
    </div>

    <div style="margin-top: 2em;">

        <div class="pull-right">
            <form action="{{route('filebox.index')}}" method="get" role="search">
                <div class="form-group">
                    <input type="text" name="question" value="{{Input::get('question')}}" placeholder="Search FileBox" class="form-control"/>
                </div>
            </form>
        </div>
        <h3 class="text-muted">
            <i class="fa fa-files-o"></i> List of Files
        </h3>
    </div>

    <div class="table-responsive">
        <table class="table table-bordered table-striped">
            <thead>
            <tr>
                <th>{{link_for_sort('filebox.index', 'filename', 'Name')}} {{format_active_sort('filename')}}</th>
                <th>{{link_for_sort('filebox.index', 'size', 'Size')}} {{format_active_sort('size')}}</th>
                <th>User</th>
                <th>{{link_for_sort('filebox.index', 'created_at', 'Datetime')}} {{format_active_sort('created_at')}}</th>
                <th>Action</th>
            </tr>
            </thead>

            <tbody>
            @if($filebox->count())
                @foreach($filebox as $file)
                <tr>
                    <td><a href="{{route('filebox.download', base64_encode($file->filename))}}" title="{{$file->filename}}">{{$file->filename}}</a></td>
                    <td>{{format_filesize($file->size)}}</td>
                    <td>{{build_file_uploader_string($file)}}</td>
                    <td>{{format_timestring($file->created_at)}}</td>
                    <td>
                        <button class="generate-download-url-hook btn btn-default btn-xs" data-filename="{{$file->filename}}" title="Generate One-time URL for Download"><i class="fa fa-bolt"></i></button>
                        <button class="delete-hook btn btn-default btn-xs" data-file-id="{{$file->id}}" title="DELETE"><i class="fa fa-trash-o"></i></button>
                    </td>
                </tr>
                @endforeach
            @else
                <tr>
                    <td colspan="5"><p class="text-center">Not Found !</p></td>
                </tr>
            @endif
            </tbody>
        </table>
    </div>
@stop

@section('pagination')
    <div class="text-center">
        {{$filebox->appends(Request::except('page'))->links('pagination.simple')}}
    </div>
@stop

@section('script')
    <!-- Dropzone library -->
    <script src="/js/vendor/dropzone/dropzone.min.js"></script>

    <!-- ZeroClipboard library -->
    <script src="/js/vendor/zeroclipboard/ZeroClipboard.min.js"></script>

    <script>
        (function () {
            // Dropzone initialization
            Dropzone.options.myAwesomeDropzone = {
                dictDefaultMessage: "<div class=\"text-center text-muted\"><h2 class=\"text-muted\">Drop files to upload !</h2><p>(or Click to choose...)</p></div>",
                maxFilesize: 100,

                init: function () {
                    this.on("error", function (file, response, xhr) {
                        if (response.error) { utility.flash("Error", "Something went wrong! " + response.error, "error"); return; }
                    });

                    this.on("success", function (file, response) {
                        setTimeout(function () { window.location.reload(true); }, 1000);
                        utility.flash("Success", "Uploaded !", "success");
                    });
                }
            };

            // ZeroClipboard instantiation
            var clientText = new ZeroClipboard( $(".text-to-copy"), { moviePath: "/js/vendor/zeroclipboard/ZeroClipboard.swf", debug: false });
            clientText.on("aftercopy", function(event) { utility.flash("Info", "URL has been copied to your clipboard! Now share this URL to external users to Upload resources. The URL is valid for 2 days.", "info"); });

            $(".delete-hook").on("click", function () {
                if (confirm("Are you sure to delete ?")) {
                    utility.fetch("/file/"+$(this).data("file-id"), { _token: "{{csrf_token()}}", _method: "delete" }).done(function (response) {
                        if (response.error) {utility.flash("Error", "Something went wrong! " + response.error.message, "error"); return;}
                        utility.flash("Success", "Deleted !", "success");
                        setTimeout(function () {window.location.reload(true);}, 1000);
                    });
                }
            });

            $(".generate-upload-url-hook").on("click", function () {
                var $this = $(this);
                utility.fetch("{{route('onetime.filebox.make')}}", {_token: "{{csrf_token()}}"}).done(function (response) {
                    if (response.error) { utility.flash("Error", "Something went wrong! " + response.error.message, "error"); return;}

                    var $elem = $("#generated-upload-url"),
                        pathString = response.data.path + ' (This url is valid until ' + response.data.expires_at + ' UTC+09:00)';
                    $elem.text(pathString)
                         .attr('data-clipboard-text', pathString);
                    $this.hide()
                });
            });

            $(".generate-download-url-hook").on("click", function () {
                var $this = $(this);
                utility.fetch("{{route('onetime.filebox.make')}}", {_token: "{{csrf_token()}}", filename: $this.data("filename")}).done(function (response) {
                    if (response.error) { utility.flash("Error", "Something went wrong! " + response.error.message, "error"); return;}
                    var $elem = $("<button/>", {
                        class: "btn btn-default btn-xs text-to-copy",
                        "data-clipboard-text": response.data.path + ' (This url is valid until ' + response.data.expires_at + ' UTC+09:00)',
                        title: "COPY URL",
                        html: '<i class="fa fa-link"></i>'
                    });
                    $this.hide();
                    $this.after($elem);
                    clientText = new ZeroClipboard( $(".text-to-copy"), { moviePath: "/js/vendor/zeroclipboard/ZeroClipboard.swf", debug: false });
                    clientText.on("aftercopy", function(event) { utility.flash("Info", "URL has been copied to your clipboard! Now share this URL to external users to Download resources. The URL is valid for 2 days.", "info"); });
                });
            });
        })();
    </script>
@stop
