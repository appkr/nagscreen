@extends('layouts.mobile')

@section('style')
    <!-- Dropzone -->
    <link rel="stylesheet" href="/css/vendor/dropzone/dropzone.css">
    <style>
        .dropzone { border: 5px dotted #444; min-height: 230px; background-color: #555; color: #FFF; }
        .dropzone .dz-default.dz-message { background-image: none; color: #FFF; }
        @media all and (-webkit-min-device-pixel-ratio: 1.5),(min--moz-device-pixel-ratio: 1.5),(-o-min-device-pixel-ratio: 1.5/1),(min-device-pixel-ratio: 1.5),(min-resolution: 138dpi),(min-resolution: 1.5dppx) { .dropzone .dz-default.dz-message { background-image: none; color: #FFF; } }
        .dropzone .dz-default.dz-message span { display: block; color: #FFF; }
    </style>
@stop

@section('content')
    <div class="page-header">
        <h3 class="blog-post-title">
            <i class="fa fa-paperclip"></i> Welcome to AirPlug's File Upload Service<br/>
            <small>This one-time upload URL will be available until {{$data->expires_at}} UTC+09:00.</small>
        </h3>
    </div>

    <form>
        <div class="form-group">
            <label for="email-input">Your Email Address - <span class="text-danger">BEFORE DROP A FILE</span>, make sure that you provided your email address.</label>
            <input type="email-input" name="email-input" id="email-input" class="form-control" value="{{Input::old('email')}}" placeholer="Your Email Here !"/>
            <span class="help-block">Upon upload, the person who share this url will get a notification saying so.</span>
        </div>
    </form>

    <!-- Form element for Dropzone -->
    <form action="{{route('onetime.filebox.save', $data->token)}}" method="post" enctype="multipart/form-data" id="my-awesome-dropzone" class="dropzone">
        <input type="hidden" name="_token" value="{{csrf_token()}}">
        <input type="hidden" name="user_id" value="{{$data->user_id}}">
        <input type="hidden" name="email" id="email">

        <div class="fallback">
            <input name="file" type="file" multiple/>
        </div>
    </form>

    <div id="generated-download-url"></div>
@stop

@section('script')
    <!-- Dropzone library -->
    <script src="/js/vendor/dropzone/dropzone.min.js"></script>

    <!-- ZeroClipboard library -->
    <script src="/js/vendor/zeroclipboard/ZeroClipboard.min.js"></script>

    <script>
        (function () {
            // Dropzone initialization
            Dropzone.options.myAwesomeDropzone = {
                dictDefaultMessage: "<div class=\"text-center\"><h2>Drop files to upload !</h2><p>(or Click to choose...)</p></div>",
                maxFilesize: 100,

                init: function () {
                    this.on("error", function (file, response, xhr) {
                        if (response.error) {
                            if (response.error) { utility.flash("Error", "Something went wrong! " + response.error, "error"); return; }
                            return;
                        }
                    });

                    this.on("success", function (file, response) {
                        var url = "https:{{Config::get('app.url')}}/file/"+window.btoa(response.data.filename);
                        $("#generated-download-url").append("<a class='text-to-copy' title='COPY URL' data-clipboard-text='"+url+"'>"+url+"</a><br/> Now you can safely close this window.");
                        // ZeroClipboard instantiation
                        var clientText = new ZeroClipboard( $(".text-to-copy"), { moviePath: "/js/vendor/zeroclipboard/ZeroClipboard.swf", debug: false });
                        clientText.on("aftercopy", function(event) { utility.flash("Info", "URL has been copied to your clipboard!", "info"); });
                        // Visual feedback
                        utility.flash("Success", "Uploaded: "+response.data.filename+". And an email has been sent to the person who gave this URL to you. Now you can safely close this window.", "success");
                    });
                }
            };

            $("#email-input").on('keyup', function(){
                $("#email").val($("#email-input").val());
            });
        })();
    </script>
@stop
