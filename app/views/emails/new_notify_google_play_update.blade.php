@extends('layouts.mail')

@section('content')
    <div style="padding-bottom: 9px;margin: 40px 0 20px;border-bottom: 1px solid #eeeeee;">
        <h4 style="line-height: 1.1;color: #666;margin-top: 0;margin-bottom: 5px;font-size: 1.6em;white-space: nowrap;overflow: hidden;text-overflow: ellipsis;">
            <i style="display: inline-block;line-height: 1"></i> Google Play Summary, {{ $summary['date'] }}
        </h4>
    </div>

    @if(isset($summary['data']) && count($summary['data']) > 0)
        @foreach($summary['data'] as $package => $metric)
            <div style="padding-bottom: 9px;margin: 40px 0;">
                <h3 style="line-height: 1.1;color: #999999;margin-top: 0;margin-bottom: 10px;font-size: 24px;"><strong style="font-weight: bold;">{{ $metric['package_name'] }}</strong> <small style="font-size: 65%;line-height: 1;color: #999999;">/ {{ $package }}</small></h3>
            </div>

            <div style="margin-bottom: 2em;">
                <table class="table" style="border-collapse: collapse !important;border-spacing: 0;max-width: 100%;background-color: transparent;table-layout: fixed;word-wrap: break-word;width: 100%;margin-bottom: 0;">
                    <thead>
                    <tr>
                        <th style="padding: 8px;border-color: transparent;white-space: nowrap;overflow: hidden;line-height: 1.4;border-top: 1px solid #dddddd;border-bottom: 2px solid #dddddd;background-color: #fff !important;text-overflow: ellipsis !important;">Period</th>
                        <th style="padding: 8px;border-color: transparent;white-space: nowrap;overflow: hidden;line-height: 1.4;border-top: 1px solid #dddddd;border-bottom: 2px solid #dddddd;background-color: #fff !important;text-overflow: ellipsis !important;">Installed Users <br><small style="font-size: 85%;color: #999999;">(change %)</small></th>
                        <th style="padding: 8px;border-color: transparent;white-space: nowrap;overflow: hidden;line-height: 1.4;border-top: 1px solid #dddddd;border-bottom: 2px solid #dddddd;background-color: #fff !important;text-overflow: ellipsis !important;">Uninstalled Users <br><small style="font-size: 85%;color: #999999;">(change %)</small></th>
                        <th style="padding: 8px;border-color: transparent;white-space: nowrap;overflow: hidden;line-height: 1.4;border-top: 1px solid #dddddd;border-bottom: 2px solid #dddddd;background-color: #fff !important;text-overflow: ellipsis !important;">Active Users(*)<br><small style="font-size: 85%;color: #999999;">(retention %)</small></th>
                        <th style="padding: 8px;border-color: transparent;white-space: nowrap;overflow: hidden;line-height: 1.4;border-top: 1px solid #dddddd;border-bottom: 2px solid #dddddd;background-color: #fff !important;text-overflow: ellipsis !important;">Rank (**)<br><small style="font-size: 85%;color: #999999;"></small></th>
                        <th style="padding: 8px;border-color: transparent;white-space: nowrap;overflow: hidden;line-height: 1.4;border-top: 1px solid #dddddd;border-bottom: 2px solid #dddddd;background-color: #fff !important;text-overflow: ellipsis !important;">Rating/Reviews <br><small style="font-size: 85%;color: #999999;"></small></th>
                    </tr>
                    </thead>

                    <tbody>
                    <tr>
                        <td style="padding: 8px;border-color: transparent;text-align: right;white-space: nowrap;overflow: hidden;line-height: 1.4;vertical-align: top;border-top: 1px solid #dddddd;background-color: #fff !important;text-overflow: ellipsis !important;"><strong style="font-weight: bold;">{{ $summary['date'] }}</strong></td>
                        <td style="padding: 8px;border-color: transparent;text-align: right;white-space: nowrap;overflow: hidden;line-height: 1.4;vertical-align: top;border-top: 1px solid #dddddd;background-color: #fff !important;text-overflow: ellipsis !important;">{{ number_format($metric['today']['install_count']) }} <br><small style="font-size: 85%;">{{ color_rate($metric['today']['install_change'] * 100) }}</small></td>
                        <td style="padding: 8px;border-color: transparent;text-align: right;white-space: nowrap;overflow: hidden;line-height: 1.4;vertical-align: top;border-top: 1px solid #dddddd;background-color: #fff !important;text-overflow: ellipsis !important;">{{ number_format($metric['today']['uninstall_count']) }} <br><small style="font-size: 85%;">{{ color_rate($metric['today']['uninstall_change'] * 100, true) }}</small></td>
                        <td style="padding: 8px;border-color: transparent;text-align: right;white-space: nowrap;overflow: hidden;line-height: 1.4;vertical-align: top;border-top: 1px solid #dddddd;background-color: #fff !important;text-overflow: ellipsis !important;">{{ number_diff($metric['total_user_installs'], $metric['total_user_uninstalls']) }} <br><small style="font-size: 85%;">{{ retention_ratio($metric['total_user_installs'], $metric['total_user_uninstalls']) }}</small></td>
                        <td style="padding: 8px;border-color: transparent;text-align: right;white-space: nowrap;overflow: hidden;line-height: 1.4;vertical-align: top;border-top: 1px solid #dddddd;background-color: #fff !important;text-overflow: ellipsis !important;">{{ $metric['rank_kr'] }}</td>
                        <td style="padding: 8px;border-color: transparent;text-align: right;white-space: nowrap;overflow: hidden;line-height: 1.4;vertical-align: top;border-top: 1px solid #dddddd;background-color: #fff !important;text-overflow: ellipsis !important;">{{ round($metric['average_rating'], 1) }}/{{ $metric['total_reviews'] }}</td>
                    </tr>
                    <tr>
                        <td style="padding: 8px;border-color: transparent;text-align: right;white-space: nowrap;overflow: hidden;line-height: 1.4;vertical-align: top;border-top: 1px solid #dddddd;background-color: #fff !important;text-overflow: ellipsis !important;"><strong style="font-weight: bold;">Last 7 days</strong></td>
                        <td style="padding: 8px;border-color: transparent;text-align: right;white-space: nowrap;overflow: hidden;line-height: 1.4;vertical-align: top;border-top: 1px solid #dddddd;background-color: #fff !important;text-overflow: ellipsis !important;">{{ number_format($metric['last_7days']['install_count']) }} <br><small style="font-size: 85%;">{{ color_rate($metric['last_7days']['install_change'] * 100) }}</small></td>
                        <td style="padding: 8px;border-color: transparent;text-align: right;white-space: nowrap;overflow: hidden;line-height: 1.4;vertical-align: top;border-top: 1px solid #dddddd;background-color: #fff !important;text-overflow: ellipsis !important;">{{ number_format($metric['last_7days']['uninstall_count']) }} <br><small style="font-size: 85%;">{{ color_rate($metric['last_7days']['uninstall_change'] * 100, true) }}</small></td>
                        <td style="padding: 8px;border-color: transparent;text-align: right;white-space: nowrap;overflow: hidden;line-height: 1.4;vertical-align: top;border-top: 1px solid #dddddd;background-color: #fff !important;text-overflow: ellipsis !important;"></td>
                        <td style="padding: 8px;border-color: transparent;text-align: right;white-space: nowrap;overflow: hidden;line-height: 1.4;vertical-align: top;border-top: 1px solid #dddddd;background-color: #fff !important;text-overflow: ellipsis !important;"></td>
                        <td style="padding: 8px;border-color: transparent;text-align: right;white-space: nowrap;overflow: hidden;line-height: 1.4;vertical-align: top;border-top: 1px solid #dddddd;background-color: #fff !important;text-overflow: ellipsis !important;"></td>
                    </tr>
                    <tr>
                        <td style="padding: 8px;border-color: transparent;text-align: right;white-space: nowrap;overflow: hidden;line-height: 1.4;vertical-align: top;border-top: 1px solid #dddddd;background-color: #fff !important;text-overflow: ellipsis !important;"><strong style="font-weight: bold;">Last 30 days</strong></td>
                        <td style="padding: 8px;border-color: transparent;text-align: right;white-space: nowrap;overflow: hidden;line-height: 1.4;vertical-align: top;border-top: 1px solid #dddddd;background-color: #fff !important;text-overflow: ellipsis !important;">{{ number_format($metric['last_30days']['install_count']) }} <br><small style="font-size: 85%;">{{ color_rate($metric['last_30days']['install_change'] * 100) }}</small></td>
                        <td style="padding: 8px;border-color: transparent;text-align: right;white-space: nowrap;overflow: hidden;line-height: 1.4;vertical-align: top;border-top: 1px solid #dddddd;background-color: #fff !important;text-overflow: ellipsis !important;">{{ number_format($metric['last_30days']['uninstall_count']) }} <br><small style="font-size: 85%;">{{ color_rate($metric['last_30days']['uninstall_change'] * 100, true) }}</small></td>
                        <td style="padding: 8px;border-color: transparent;text-align: right;white-space: nowrap;overflow: hidden;line-height: 1.4;vertical-align: top;border-top: 1px solid #dddddd;background-color: #fff !important;text-overflow: ellipsis !important;"></td>
                        <td style="padding: 8px;border-color: transparent;text-align: right;white-space: nowrap;overflow: hidden;line-height: 1.4;vertical-align: top;border-top: 1px solid #dddddd;background-color: #fff !important;text-overflow: ellipsis !important;"></td>
                        <td style="padding: 8px;border-color: transparent;text-align: right;white-space: nowrap;overflow: hidden;line-height: 1.4;vertical-align: top;border-top: 1px solid #dddddd;background-color: #fff !important;text-overflow: ellipsis !important;"></td>
                    </tr>
                    </tbody>
                </table>
            </div>

            @if (count($metric['today']['reviews']) > 0)
                <reviews style="margin-bottom: 1em;">
                    @foreach ($metric['today']['reviews'] as $review)
                        <div style="overflow: hidden;zoom: 1;margin-top: 0;margin-bottom: 2em;">
                            <div style="text-align: left;overflow: hidden;zoom: 1;">
                                <h4 style="text-align: left;line-height: 1.1;color: #333;margin-top: 0;margin-bottom: 10px;font-size: 18px;margin: 0 0 5px;">
                                    <strong style="font-weight: bold;">{{$review['reviewer'] or ''}}</strong>
                                    <small style="font-size: 75%;font-weight: normal;line-height: 1;color: #999999;">{{format_rating($review['rating'])}}</small>
                                </h4>
                                <strong style="font-size: 85%;color: #999999;">{{$review['title'] or ''}}</strong> {{$review['content'] or ''}}
                                <small class="text-muted">{{$review['date'] or ''}}</small>
                            </div>
                        </div> <!-- end .media (single review) -->

                    @endforeach
                </reviews>
            @endif
            <hr/>

        @endforeach
    @endif

    <p style="text-align: left;margin: 0 0 10px;">
        <small class="text-muted" style="text-align: left;font-size: 85%;color: #999999;">
            (*) Retention = (Total # of Installed User - Total # of Uninstalled User) / &Total # of Installed User<br/>
            (**) Rank in 'Apps &gt; Media &amp; Entertainment &gt; Free of Charge' section<br/>
            <em><code>Note.</code> Install/Uninstall count is based on head count, not based on device count. Note that one user can have more than one device.</em>
        </small>
    </p>

    @if(isset($summary['related_reviews']) && count($summary['related_reviews']) > 0)
        @foreach($summary['related_reviews'] as $app => $reviews)
            <div style="padding-bottom: 9px;margin: 40px 0 20px;">
                <h4 style="text-align: left;line-height: 1.1;color: #999999;margin-top: 0;margin-bottom: 10px;font-size: 18px;">{{$app}}</h4>
            </div>

            <reviews style="margin-bottom: 1em;">
                @foreach($reviews as $review)
                    <div style="overflow: hidden;zoom: 1;margin-top: 0;margin-bottom: 2em;">
                        <div style="text-align: left;overflow: hidden;zoom: 1;">
                            <h4 style="text-align: left;line-height: 1.1;color: #333;margin-top: 0;margin-bottom: 10px;font-size: 18px;margin: 0 0 5px;">
                                <strong style="font-weight: bold;">{{$review['reviewer'] or ''}}</strong>
                                <small style="font-size: 75%;font-weight: normal;line-height: 1;color: #999999;">{{format_rating($review['rating'])}}</small>
                            </h4>
                            <strong style="font-weight: bold;">{{$review['title'] or ''}}</strong> {{$review['content'] or ''}}
                            <small style="font-size: 85%;color: #999999;">{{$review['date'] or ''}}</small>
                        </div>
                    </div> <!-- end .media (single review) -->
                @endforeach
            </reviews>

        @endforeach
    @endif

    <p style="text-align: left;margin: 0 0 10px;">
        <small class="text-muted" style="text-align: left;font-size: 85%;color: #999999;">
            <strong style="font-weight: bold;">Data Api</strong> -
            <a href="{{route('api.new.googleplay.summary')}}" style="background: transparent;color: #158cba;text-decoration: underline;">Summary data</a>,
            <a href="{{route('api.googleplay.stat.index')}}" style="background: transparent;color: #158cba;text-decoration: underline;">Full install/uninstall statistics</a>,
            <a href="{{route('api.googleplay.review.index')}}" style="background: transparent;color: #158cba;text-decoration: underline;">Full reviews list</a>,
            <a href="{{route('api.index', 10)}}" style="background: transparent;color: #158cba;text-decoration: underline;">Api document</a>
        </small>
    </p>
@stop