@extends('layouts.mail')

@section('style')
    <style>
        .summary-let th, .summary-let td {
            text-align: right;
        }
        .summary-let, .reviews-let, .note {
            margin-bottom: 2em;
        }
    </style>
@stop

@section('content')
    <div class="page-header" style="padding-bottom: 9px;margin: 40px 0 20px;">
        <h4 class="blog-post-title" style="text-align: left;line-height: 1.1;color: #666;margin-top: 0;margin-bottom: 5px;font-size: 1.6em;white-space: nowrap;overflow: hidden;text-overflow: ellipsis;">
            Google Play Summary, {{$summary['date']}}
        </h4>
    </div>

    @if(isset($body) && ! empty($body))
        <p style="text-align: left;margin: 0 0 10px;">{{$body}}</p>
    @endif

    @if(isset($summary['data']) && count($summary['data']) > 0)
        @foreach($summary['data'] as $package => $metric)
        <div class="page-header" style="padding-bottom: 9px;margin: 40px 0 20px;">
            <h3 class="text-muted" style="text-align: left;line-height: 1.1;color: #999999;margin-top: 0;margin-bottom: 10px;font-size: 24px;">{{$package}}</h3>
        </div>

        <div class="table-responsive summary-let" style="margin-bottom: 1em;">
            <table class="table" style="border-collapse: collapse !important;border-spacing: 0;max-width: 100%;background-color: transparent;table-layout: fixed;word-wrap: break-word;width: 100%;margin-bottom: 0;">
                <thead style="display: table-header-group;">
                <tr style="page-break-inside: avoid;">
                    <th style="padding: 8px;text-align: right;white-space: nowrap;overflow: hidden;line-height: 1.4;vertical-align: bottom;border-top: 1px solid #dddddd;border-bottom: 2px solid #dddddd;background-color: #fff !important;text-overflow: ellipsis !important;">&nbsp;</th>
                    <th style="padding: 8px;text-align: right;white-space: nowrap;overflow: hidden;line-height: 1.4;vertical-align: bottom;border-top: 1px solid #dddddd;border-bottom: 2px solid #dddddd;background-color: #fff !important;text-overflow: ellipsis !important;">Installed Users <br >
                        <small class="text-muted" style="font-size: 85%;color: #999999;">(increase %)</small>
                    </th>
                    <th style="padding: 8px;text-align: right;white-space: nowrap;overflow: hidden;line-height: 1.4;vertical-align: bottom;border-top: 1px solid #dddddd;border-bottom: 2px solid #dddddd;background-color: #fff !important;text-overflow: ellipsis !important;">Uninstalled Users <br >
                        <small class="text-muted" style="font-size: 85%;color: #999999;">(increase %)</small>
                    </th>
                    <th style="padding: 8px;text-align: right;white-space: nowrap;overflow: hidden;line-height: 1.4;vertical-align: bottom;border-top: 1px solid #dddddd;border-bottom: 2px solid #dddddd;background-color: #fff !important;text-overflow: ellipsis !important;">Active Users (*)<br >
                        <small class="text-muted" style="font-size: 85%;color: #999999;">(retention %)</small>
                    </th>
                    <th style="padding: 8px;text-align: right;white-space: nowrap;overflow: hidden;line-height: 1.4;vertical-align: bottom;border-top: 1px solid #dddddd;border-bottom: 2px solid #dddddd;background-color: #fff !important;text-overflow: ellipsis !important;">Rank (**)<br >
                        <small class="text-muted" style="font-size: 85%;color: #999999;">(increase)</small>
                    </th>
                    <th style="padding: 8px;text-align: right;white-space: nowrap;overflow: hidden;line-height: 1.4;vertical-align: bottom;border-top: 1px solid #dddddd;border-bottom: 2px solid #dddddd;background-color: #fff !important;text-overflow: ellipsis !important;">Rating/Reviews <br >
                        <small class="text-muted" style="font-size: 85%;color: #999999;">(increase)</small>
                    </th>
                </tr>
                </thead>

                <tbody >
                <tr style="page-break-inside: avoid;">
                    <td style="padding: 8px;text-align: right;white-space: nowrap;overflow: hidden;line-height: 1.4;vertical-align: top;border-top: 1px solid #dddddd;background-color: #fff !important;text-overflow: ellipsis !important;">{{$metric['today']['date']}}</td>
                    <td style="padding: 8px;text-align: right;white-space: nowrap;overflow: hidden;line-height: 1.4;vertical-align: top;border-top: 1px solid #dddddd;background-color: #fff !important;text-overflow: ellipsis !important;">{{$metric['today']['daily_user_installs']}}</td>
                    <td style="padding: 8px;text-align: right;white-space: nowrap;overflow: hidden;line-height: 1.4;vertical-align: top;border-top: 1px solid #dddddd;background-color: #fff !important;text-overflow: ellipsis !important;">{{$metric['today']['daily_user_uninstalls']}}</td>
                    <td style="padding: 8px;text-align: right;white-space: nowrap;overflow: hidden;line-height: 1.4;vertical-align: top;border-top: 1px solid #dddddd;background-color: #fff !important;text-overflow: ellipsis !important;">{{number_diff($metric['total_user_installs'], $metric['total_user_uninstalls'])}}</td>
                    <td style="padding: 8px;text-align: right;white-space: nowrap;overflow: hidden;line-height: 1.4;vertical-align: top;border-top: 1px solid #dddddd;background-color: #fff !important;text-overflow: ellipsis !important;">{{$metric['today']['rank_kr']}}</td>
                    <td style="padding: 8px;text-align: right;white-space: nowrap;overflow: hidden;line-height: 1.4;vertical-align: top;border-top: 1px solid #dddddd;background-color: #fff !important;text-overflow: ellipsis !important;">{{round($metric['average_rating'], 1)}}/{{$metric['total_reviews']}}</td>
                </tr>

                <tr style="page-break-inside: avoid;">
                    <td style="padding: 8px;text-align: right;white-space: nowrap;overflow: hidden;line-height: 1.4;vertical-align: top;border-top: 1px solid #dddddd;background-color: #fff !important;text-overflow: ellipsis !important;">&nbsp;</td>
                    <td style="padding: 8px;text-align: right;white-space: nowrap;overflow: hidden;line-height: 1.4;vertical-align: top;border-top: 1px solid #dddddd;background-color: #fff !important;text-overflow: ellipsis !important;">
                        <small class="text-muted" style="font-size: 85%;color: #999999;"><span style="color: red;">{{increase_rate($metric['today']['daily_user_installs'], $metric['yesterday']['daily_user_installs'])}}</span></small>
                    </td>
                    <td style="padding: 8px;text-align: right;white-space: nowrap;overflow: hidden;line-height: 1.4;vertical-align: top;border-top: 1px solid #dddddd;background-color: #fff !important;text-overflow: ellipsis !important;">
                        <small class="text-muted" style="font-size: 85%;color: #999999;"><span style="color: red;">{{increase_rate($metric['today']['daily_user_uninstalls'], $metric['yesterday']['daily_user_uninstalls'])}}</span></small>
                    </td>
                    <td style="padding: 8px;text-align: right;white-space: nowrap;overflow: hidden;line-height: 1.4;vertical-align: top;border-top: 1px solid #dddddd;background-color: #fff !important;text-overflow: ellipsis !important;">
                        <small class="text-muted" style="font-size: 85%;color: #999999;">{{retention_ratio($metric['total_user_installs'], $metric['total_user_uninstalls'])}}</small>
                    </td>
                    <td style="padding: 8px;text-align: right;white-space: nowrap;overflow: hidden;line-height: 1.4;vertical-align: top;border-top: 1px solid #dddddd;background-color: #fff !important;text-overflow: ellipsis !important;">
                        <small class="text-muted" style="font-size: 85%;color: #999999;"><span style="color: green;">{{rank_diff($metric['yesterday']['rank_kr'], $metric['today']['rank_kr'])}}</span></small>
                    </td>
                    <td style="padding: 8px;text-align: right;white-space: nowrap;overflow: hidden;line-height: 1.4;vertical-align: top;border-top: 1px solid #dddddd;background-color: #fff !important;text-overflow: ellipsis !important;">
                        <small class="text-muted" style="font-size: 85%;color: #999999;">{{count($metric['today']['reviews']) or ''}}</small>
                    </td>
                </tr>

                <tr style="page-break-inside: avoid;">
                    <td style="padding: 8px;text-align: right;white-space: nowrap;overflow: hidden;line-height: 1.4;vertical-align: top;border-top: 1px solid #dddddd;background-color: #fff !important;text-overflow: ellipsis !important;">{{$metric['yesterday']['date']}}</td>
                    <td style="padding: 8px;text-align: right;white-space: nowrap;overflow: hidden;line-height: 1.4;vertical-align: top;border-top: 1px solid #dddddd;background-color: #fff !important;text-overflow: ellipsis !important;">{{$metric['yesterday']['daily_user_installs']}}</td>
                    <td style="padding: 8px;text-align: right;white-space: nowrap;overflow: hidden;line-height: 1.4;vertical-align: top;border-top: 1px solid #dddddd;background-color: #fff !important;text-overflow: ellipsis !important;">{{$metric['yesterday']['daily_user_uninstalls']}}</td>
                    <td style="padding: 8px;text-align: right;white-space: nowrap;overflow: hidden;line-height: 1.4;vertical-align: top;border-top: 1px solid #dddddd;background-color: #fff !important;text-overflow: ellipsis !important;">&nbsp;</td>
                    <td style="padding: 8px;text-align: right;white-space: nowrap;overflow: hidden;line-height: 1.4;vertical-align: top;border-top: 1px solid #dddddd;background-color: #fff !important;text-overflow: ellipsis !important;">{{$metric['yesterday']['rank_kr'] or ''}}</td>
                    <td style="padding: 8px;text-align: right;white-space: nowrap;overflow: hidden;line-height: 1.4;vertical-align: top;border-top: 1px solid #dddddd;background-color: #fff !important;text-overflow: ellipsis !important;">{{$metric['total_reviews'] - count($metric['today']['reviews'])}}</td>
                </tr>
                </tbody>
            </table>
        </div>

            @if (count($metric['today']['reviews']) > 0)
            <reviews class="reviews-let" style="margin-bottom: 1em;">
                @foreach ($metric['today']['reviews'] as $review)
                <div class="media" style="overflow: hidden;zoom: 1;margin-top: 0;margin-bottom: 2em;">
                    <div class="media-body" style="text-align: left;overflow: hidden;zoom: 1;">
                        <h4 class="media-heading" style="text-align: left;line-height: 1.1;color: #333;margin-top: 0;margin-bottom: 10px;font-size: 18px;margin: 0 0 5px;">
                            <strong style="font-weight: bold;">{{$review['reviewer'] or ''}}</strong>
                            <small class="text-muted" style="font-size: 75%;font-weight: normal;line-height: 1;color: #999999;">{{format_rating($review['rating'])}}</small>
                        </h4>

                        <strong style="font-weight: bold;">{{$review['title'] or ''}}</strong> {{$review['content'] or ''}}
                        <small class="text-muted" style="font-size: 85%;color: #999999;">{{$review['date'] or ''}}</small>
                    </div>
                </div>
                @endforeach
            </reviews>
            @endif

            <!-- <hr style="height: 0;margin-top: 20px;margin-bottom: 20px;border: 0;border-top: 1px solid #eeeeee;"> -->

        @endforeach
    @endif

    <p style="text-align: left;margin: 0 0 10px;">
        <small class="text-muted" style="text-align: left;font-size: 85%;color: #999999;">
            (*) Retention = (Total # of Installed User - Total # of Uninstalled User) / &Total # of Installed User<br/>
            (**) Rank in 'Apps &gt; Media &amp; Entertainment &gt; Free of Charge' section<br/>
            <em><code>Note.</code> Install/Uninstall count is based on head count, not based on device count. Note that one user can have more than one device.</em>
        </small>
    </p>

    @if(isset($summary['related_reviews']) && count($summary['related_reviews']) > 0)
        @foreach($summary['related_reviews'] as $app => $reviews)
            <div class="page-header" style="padding-bottom: 9px;margin: 40px 0 20px;">
                <h4 class="text-muted" style="text-align: left;line-height: 1.1;color: #999999;margin-top: 0;margin-bottom: 10px;font-size: 18px;">{{$app}}</h4>
            </div>

            <reviews class="reviews-let" style="margin-bottom: 1em;">
            @foreach($reviews as $review)
                <div class="media" style="overflow: hidden;zoom: 1;margin-top: 0;margin-bottom: 2em;">
                    <div class="media-body" style="text-align: left;overflow: hidden;zoom: 1;">
                        <h4 class="media-heading" style="text-align: left;line-height: 1.1;color: #333;margin-top: 0;margin-bottom: 10px;font-size: 18px;margin: 0 0 5px;"><strong style="font-weight: bold;">{{$review['reviewer'] or ''}}</strong>
                            <small class="text-muted" style="font-size: 75%;font-weight: normal;line-height: 1;color: #999999;">{{format_rating($review['rating'])}}</small>
                        </h4>
                        <strong style="font-weight: bold;">{{$review['title'] or ''}}</strong> {{$review['content'] or ''}}
                        <small class="text-muted" style="font-size: 85%;color: #999999;">{{$review['date'] or ''}}</small>
                    </div>
                </div>
            @endforeach
            </reviews>

        @endforeach
    @endif

    <p style="text-align: left;margin: 0 0 10px;">
        <small class="text-muted" style="text-align: left;font-size: 85%;color: #999999;">
            <strong style="font-weight: bold;">Data Api</strong> -
            <a href="{{route('api.googleplay.summary')}}" style="background: transparent;color: #158cba;text-decoration: underline;">Summary data</a>,
            <a href="{{route('api.googleplay.stat.index')}}" style="background: transparent;color: #158cba;text-decoration: underline;">Full install/uninstall statistics</a>,
            <a href="{{route('api.googleplay.review.index')}}" style="background: transparent;color: #158cba;text-decoration: underline;">Full reviews list</a>,
            <a href="{{route('api.index', 10)}}" style="background: transparent;color: #158cba;text-decoration: underline;">Api document</a>
        </small>
    </p>
@stop