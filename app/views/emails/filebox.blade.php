@extends('layouts.mail')


@section('content')

<!-- EMAIL CONTAINER // -->
<!--
    The table "emailBody" is the email's container.
    Its width can be set to 100% for a color band
    that spans the width of the page.
-->
<table border="0" cellpadding="0" cellspacing="0" width="600" id="emailBody">


    <!-- MODULE ROW // -->
    <!--
        To move or duplicate any of the design patterns
        in this email, simply move or copy the entire
        MODULE ROW section for each content block.
    -->
    <tr>
        <td align="center" valign="top">
            <!-- CENTERING TABLE // -->
            <!--
                The centering table keeps the content
                tables centered in the emailBody table,
                in case its width is set to 100%.
            -->
            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td align="center" valign="top">
                        <!-- FLEXIBLE CONTAINER // -->
                        <!--
                            The flexible container has a set width
                            that gets overridden by the media query.
                            Most content tables within can then be
                            given 100% widths.
                        -->
                        <table border="0" cellpadding="0" cellspacing="0" width="600" class="flexibleContainer">
                            <tr>
                                <td align="center" valign="top" width="600" class="flexibleContainerCell">


                                    <!-- CONTENT TABLE // -->
                                    <!--
                                        The content table is the first element
                                        that's entirely separate from the structural
                                        framework of the email.
                                    -->
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                        <tr>
                                            <td align="center" valign="middle" class="textContent">
                                                <h1>{{$subject}}</h1>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="middle" class="textContent">
                                                <p style="margin-left:2em"><a href="{{$link}}">{{$filename}} ({{$size}})</a> by {{$uploader}}</p>
                                            </td>
                                        </tr>
                                    </table>
                                    <!-- // CONTENT TABLE -->


                                </td>
                            </tr>
                        </table>
                        <!-- // FLEXIBLE CONTAINER -->
                    </td>
                </tr>
            </table>
            <!-- // CENTERING TABLE -->
        </td>
    </tr>
    <!-- // MODULE ROW -->


    <!-- MODULE ROW // -->
    <tr>
        <td align="center" valign="top">
            <!-- CENTERING TABLE // -->
            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td align="center" valign="top">
                        <!-- FLEXIBLE CONTAINER // -->
                        <table border="0" cellpadding="0" cellspacing="0" width="600" class="flexibleContainer">
                            <tr>
                                <td align="center" valign="top" width="600" class="flexibleContainerCell bottomShim">


                                    <!-- CONTENT TABLE // -->
                                    <!--
                                        The emailButton table's width can be changed
                                        to affect the look of the button. To make the
                                        button width dependent on the text inside, leave
                                        the width blank. When a button is placed in a column,
                                        it's helpful to set the width to 100%.
                                    -->
                                    <!-- <table border="0" cellpadding="0" cellspacing="0" width="260" class="emailButton">
                                        <tr>
                                            <td align="center" valign="middle" class="buttonContent">
                                                <a href="#" target="_blank">Button</a>
                                            </td>
                                        </tr>
                                    </table> -->
                                    <!-- // CONTENT TABLE -->


                                </td>
                            </tr>
                        </table>
                        <!-- // FLEXIBLE CONTAINER -->
                    </td>
                </tr>
            </table>
            <!-- // CENTERING TABLE -->
        </td>
    </tr>
    <!-- // MODULE ROW -->


</table>
<!-- // EMAIL CONTAINER -->

@stop