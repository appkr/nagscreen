@extends('layouts.master')

@section('content')
    <div class="page-header">
        <h4 class="blog-post-title">
            <i class="fa fa-dropbox"></i> Packages
        </h4>
    </div>

    <div class="table-responsive">
        <table class="table table-bordered table-striped">
            <thead>
            <tr>
                <th>Package</th>
                <th>Name</th>
            </tr>
            </thead>

            <tbody>

            @if($packages->count())
                @foreach($packages as $package)
                <tr>
                    <td>{{$package->package}}</td>
                    <td>
                        @if(! is_null($package->banner))
                            <a href="{{route('banner.edit', $package->banner->id)}}">{{$package->name}}</a>
                            {{build_package_additional_info_string($package)}}
                        @else
                            <a href="{{route('banner.create', $package->id)}}">{{$package->name}}</a>
                        @endif
                    </td>
                </tr>
                @endforeach
            @else
                <tr>
                    <td colspan="2"><p class="text-center">Not Found.</p></td>
                </tr>
            @endif
            </tbody>
        </table>
    </div>

    @include('package.create')

@stop