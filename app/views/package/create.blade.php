<div class="page-header">
    <h3 class="text-muted">
        <i class="fa fa-pencil"></i> Genarate
    </h3>
</div> <!-- end .page-header-->

{{Form::open(['route' => 'package.store', 'method' => 'post', 'files' => 'true', 'role' => 'form', 'class' => 'form-inline'])}}
    <div class="form-group">
        <label class="sr-only" for="package">Package</label>
        {{Form::text('package', Input::old('package'), ['class' => 'form-control', 'placeholder' => 'com.airplug.package'])}}
    </div>

    <div class="form-group">
        <label class="sr-only" for="name">Name</label>
        {{Form::text('name', Input::old('name'), ['class' => 'form-control', 'placeholder' => 'ExamplePackage'])}}
    </div>

    {{Form::submit('GENERATE', ['class' => 'btn btn-primary'])}}
{{Form::close()}}