@extends('apidoc.master')

@section('content')

{{ Form::open(['route' => ['api.update', $apidoc->id], 'method' => 'put', 'role' => 'form']) }}

    <div class="form-group">
        <label for="title">Title</label>
        <input type="text" name="title" id="title" class="form-control" value="{{$apidoc->title}}"/>
    </div>

    <div class="form-group">
        <label for="content">Content</label>
        <textarea name="content" id="content" class="form-control">{{$apidoc->content}}</textarea>
    </div>

    <p class="text-center">
    <div class="btn-group pull-right">
        <button type="submit" class="btn btn-primary" id="target-button">UPDATE DOCUMENT</button>
        <a href="{{route('api.edit', $apidoc->id)}}" class="btn btn-default">START OVER</a>
    </div>
    </p>

{{ Form::close() }}

@stop


@section('script')

<!-- CK Editor library -->
<script src="/ckeditor/ckeditor.js"></script>

<script>
    (function () {
        CKEDITOR.replace('content', {height: "30em"});
    })();
</script>

@stop

