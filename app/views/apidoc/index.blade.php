@extends('apidoc.master')

@section('content')
    <article>
        @include('apidoc.sidebar')
    </article>

    @if($apidoc)
    <article>

        <h1 class="blog-post-title">
            <div class="pull-right hidden-xs"><small><a href="{{route('api.edit', $apidoc->id)}}">(Edit)</a></small></div>
            {{$apidoc->title}}
        </h1>
        <hr/>
        <div class="blog-main">{{$apidoc->content}}</div>
    </article>
    @else
    <article>Not Found.</article>
    @endif
@stop

@section('script')
    <!-- Google Code Prettify -->
    <script src="https://google-code-prettify.googlecode.com/svn/loader/run_prettify.js?skin=sunburst"></script>
@stop
