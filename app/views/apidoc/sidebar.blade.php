@if (isset($search) && ! is_null($search))
    <h1 class="blog-post-title">API 검색 결과</h1>
    <hr/>
    <div class="blog-main">
        <ul>
            @foreach($search->data as $data)
                <li>
                    {{link_to_route('api.index', $data['title'], ['id' => $data['id']])}}
                    @if($data['type'] == 'content')
                        <br/><small class="text-muted">{{$data['content']}}</small>
                    @endif
                </li>
            @endforeach
        </ul>
    </div>
@endif

@if (count($documents) > 0)
    <div class="blog-main">
        <h1 class="blog-post-title">
            <div class="pull-right hidden-xs"><small><a href="{{route('api.create')}}">(New API Document)</a></small></div>
            API 목록
        </h1>
        <hr/>
        <ul>
            @foreach($documents as $id => $title)
                <li>{{link_to_route('api.index', $title, ['id' => $id])}}</li>
            @endforeach
        </ul>
    </div>
@endif