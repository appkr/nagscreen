<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">

    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#custom-nav">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a href="{{route('api.index')}}" class="navbar-brand"><i class="fa fa-leaf"></i> <strong>API Documentation</strong></a>
        </div><!-- end .navbar-header -->

        <div class="collapse navbar-collapse" id="custom-nav">
            <form action="{{route('api.index')}}" method="get" role="search" class="navbar-form navbar-right">
                <div class="form-group">
                    <input type="text" name="keyword" value="{{Input::get('keyword')}}" placeholder="Search API" class="form-control" id="keyword"/>
                </div>
            </form>

            @include('layouts.navmenu')
        </div>

    </div> <!-- end .container-fluid -->

</nav> <!-- end nav -->