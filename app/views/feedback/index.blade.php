<div class="page-header">
    <h3 class="text-muted">
        <i class="fa fa-comments"></i> Feedbacks
    </h3>
</div> <!-- end .page-header (comment) -->

@if (Auth::check())
    {{ Form::open(['route' => 'feedback.store', 'method' => 'post', 'role' => 'form']) }}
    <input type="hidden" name="voc_id" value="{{$voc->id}}">

    <div class="form-group">
        <textarea name="content" id="content" class="form-control" rows="3"></textarea>
    </div>

    <div class="checkbox">
        <label>
            <input type="checkbox" name="email" checked="checked"/>
            <strong class="text-danger">Email this feedback to the customer.</strong>
        </label>
    </div>

    <button type="submit" class="btn btn-primary btn-lg pull-right">Submit</button>

    {{ Form::close() }}

    <div class="clearfix">&nbsp;</div>
    <p>&nbsp;</p>
@endif

@if ($voc->feedbacks->count())
    <comments>
        @foreach ($voc->feedbacks as $feedback)
            <div class="media">
                <a class="pull-left" href="{{route('user.post.index', $feedback->user->email)}}">
                    <img class="media-object img-thumbnail" src="{{get_gravatar_url($feedback->user->email, Config::get('setting.user.thumb'))}}" alt="{{$feedback->user->email}}">
                </a>

                <div class="media-body">
                    <h4 class="media-heading">
                        <a href="{{route('user.post.index', $feedback->user->email)}}">{{$feedback->user->email}}</a>
                    </h4>

                    {{$feedback->content}}
                    <small class="text-muted">{{format_timestring($feedback->created_at)}}</small>
                </div>
            </div> <!-- end .media (single feedback) -->

        @endforeach
    </comments> <!-- end comments list -->
@else
    <p class="text-center">No feedback yet! Please become the first one.</p>
@endif

