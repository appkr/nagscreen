@extends('layouts.master')

@section('content')

    @if ($push_message)

        <div class="page-header">
            <div class="btn-group pull-right">
                <a href="{{route('push.index')}}" class="btn btn-default"><i class="fa fa-list-ul"></i> LIST</a>
            </div>
            <h4 class="blog-post-title">
                <i class="fa fa-bullhorn"></i> {{$push_message->title}}
            </h4>
        </div> <!-- end .page-header (article title) -->

        <div id="posts-wrapper">
            <div class="media">
                <a href="#" class="pull-left">
                    <img class="media-object img-thumbnail" src="{{get_gravatar_url($push_message->user->email)}}" alt="{{$push_message->user->email}}">
                </a>

                <div class="media-body">
                    <h4 class="media-heading">
                        <a href="#">{{$push_message->user->email}}</a>
                    </h4>

                    <small class="text-muted">
                        @if ($push_message->success > 0)
                            <strong class="text-success"><span class="glyphicon glyphicon-thumbs-up"></span> success count {{$push_message->success}} &nbsp;</strong>
                        @else
                            <strong class="text-danger"><span class="glyphicon glyphicon-thumbs-down"></span> delivery failed &nbsp;</strong>
                        @endif
                        <span class="glyphicon glyphicon-tag"></span> {{$push_message->package->name}} &nbsp;
                        <span class="glyphicon glyphicon-calendar"></span> {{format_timestring($push_message->created_at)}}
                    </small>
                </div>
            </div>
            <!-- end .media (single comment) -->

            <article>
                {{$push_message->content}}
                <p>&nbsp;</p>
            </article>
            <!-- end article -->

        </div> <!-- end #posts-wrapper -->

    @else

        <div class="page-header">
            <div class="btn-group pull-right">
                <a href="{{route('push.index')}}" class="btn btn-default btn-lg"><i class="fa fa-list-ul"></i> LIST</a>
            </div>
            <h4 class="blog-post-title">
                <i class="fa fa-bullhorn"></i> Not Found
            </h4>
        </div> <!-- end .page-header (article title) -->

        <div id="posts-wrapper">
            <article class="text-center"> Not Found.</article> <!-- end article -->
        </div> <!-- end #posts-wrapper -->

    @endif

@stop


