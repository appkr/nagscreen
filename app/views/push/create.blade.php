@extends('layouts.master')

@section('content')
    <div class="page-header">
        <div class="btn-group pull-right">
            <a href="{{route('push.index')}}" class="btn btn-default btn-lg"><span class="glyphicon glyphicon-list"></span> LIST</a>
        </div>
        <h4 class="blog-post-title">
            <span class="glyphicon glyphicon-bullhorn"></span>
            Send a Push Message
        </h4>
    </div>

    {{ Form::open(['route' => 'push.send', 'method' => 'post', 'role' => 'form']) }}

    <div class="form-group">
        <label for="package_id">Package</label>
        {{Form::select('package_id', $packages, Input::old('package_id'), ['class' => 'form-control', 'onchange' =>'javascript:getReceiverCount();', 'id' => 'package_id'])}}
        <span class="help-block text-muted">Package you want to target. <span class="text-danger">"Global" means all packages. SO BE CAREFUL!</span></span>
        <strong><span class="help-block text-danger" id="receiver-count"></span></strong>
    </div>

    <div class="form-group">
        <label for="ticker">Ticker</label>
        <input type="text" name="ticker" id="ticker" class="form-control" value="{{Input::old('ticker')}}"/>
        <span class="help-block text-muted">Message at phone status bar.</span>
    </div>

    <div class="form-group">
        <label for="title">Title</label>
        <input type="text" name="title" id="title" class="form-control" value="{{Input::old('title')}}"/>
        <span class="help-block text-muted">Title of the message when notification area was pulled down.</span>
    </div>

    <div class="form-group">
        <label for="content">Content (Message)</label>
        <textarea name="content" id="content" class="form-control" rows="2">{{Input::old('content')}}</textarea>
    </div>

    <p class="text-center">
    <div class="btn-group pull-right">
        <button type="submit" class="btn btn-danger btn-lg">SEND MESSAGE</button>
        <a href="{{route('push.index')}}" class="btn btn-default btn-lg">START OVER</a>
    </div>
    </p>

    {{ Form::close() }}
@stop

@section('script')
<script>
        function fetch(url) {
            return $.ajax({
                url: url,
                dataType: "json"
            }).promise();
        }

        function getReceiverCount() {
            var package_id = $('#package_id').val(),
                url = "{{Config::get('app.url')}}/push/" + package_id + '/count';

            fetch(url).then(function (Data) {
                render(Data);
            });
        }

        function render(Data) {
            var $elem = $('#receiver-count');

            $elem.html(Data.data + " receivers");
        }

        getReceiverCount();
    </script>
@stop


