@extends('layouts.master')

@section('content')
    <div class="page-header">
        <h4 class="blog-post-title">
            <div class="btn-group pull-right">
                <a href="{{route('push.create')}}" class="btn btn-primary"><i class="fa fa-pencil"></i> COMPOSE</a>
            </div>
            <i class="fa fa-bullhorn"></i> Push Messages
        </h4>
    </div>

    <div class="table-responsive">
        <table class="table table-bordered table-striped">
            <thead>
            <tr>
                <th>Package</th>
                <th>{{link_for_sort('push.index', 'title', 'Title')}} {{format_active_sort('title')}}</th>
                <th>User</th>
                <th>{{link_for_sort('push.index', 'created_at', 'Datetime')}} {{format_active_sort('created_at')}}</th>
            </tr>
            </thead>

            <tbody>
            @if($push_messages->count())
                @foreach($push_messages as $push_message)
                <tr>
                    <td>{{$push_message->package->name}}</td>
                    <td>
                        <a href="{{route('push.show', $push_message->id)}}" title="{{$push_message->title}}">{{$push_message->title}}
                        @if ($push_message->success > 0)
                        <small class="badge">{{$push_message->success}}</small>
                        @endif
                    </td>
                    <td>{{$push_message->user->email}}</td>
                    <td>{{format_timestring($push_message->created_at)}}</td>
                </tr>
            @endforeach
            @else
                <tr>
                    <td colspan="4"><p class="text-center">Not Found !</p></td>
                </tr>
            @endif
            </tbody>
        </table>
    </div>
@stop


@section('pagination')
    <div class="text-center">
        {{$push_messages->appends(Request::except('page'))->links('pagination.slider')}}
    </div>
@stop
