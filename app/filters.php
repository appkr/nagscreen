<?php

/*
|--------------------------------------------------------------------------
| Application & Route Filters
|--------------------------------------------------------------------------
|
| Below you will find the "before" and "after" events for the application
| which may be used to do any work before or after a request into your
| application. Here you may also register your custom route filters.
|
*/

App::before(function ($request) {
    //
});

App::after(function ($request, $response) {
    // Tracker::trackPageView('/'.Request::path(), Route::currentRouteName());
});

Route::filter('auth', function () {
    if (Auth::guest()) return Redirect::guest('user/signin');
});

Route::filter('auth.basic', function () {
    return Auth::basic();
});

Route::filter('guest', function () {
    if (Auth::check()) return Redirect::home();
});

Route::filter('admin', function () {
    $user = Auth::user();
    if (! $user || (! is_admin())) {
        return Redirect::home()->with('error', 'Access denied. Login first to access the restricted realm !');
    }
});

Route::filter('csrf', function () {
    if (Session::token() != Input::get('_token')) {
        throw new Illuminate\Session\TokenMismatchException;
    }
});

Route::filter('apicsrf', function () {
    if (Session::token() != Input::get('_token')) {
        throw new \airplug\Exceptions\UnauthorizedException;
    }
});

Route::filter('force.ssl', function () {
    if (! Request::secure()) {
        throw new \airplug\Exceptions\ForbiddenException;
//        return Redirect::secure(Request::getRequestUri());
    }
});