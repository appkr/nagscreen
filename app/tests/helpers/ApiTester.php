<?php

use Faker\Factory as Faker;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

abstract class ApiTester extends TestCase
{

    /**
     * @var Faker\Generator
     */
    protected $fake;

    /**
     *
     */
    public function __construct()
    {
        $this->fake = Faker::create();
    }

    /**
     *
     */
    public function setUp()
    {
        parent::setUp();

        $this->app['artisan']->call('migrate');
        $this->app['artisan']->call('db:seed');
        $this->session(['_token' => md5(microtime())]);

        // Route::enableFilters();
    }
}