<?php

trait Factory
{

    /**
     * @var integer
     */
    protected $times = 1;


    /**
     * Setters
     *
     * @param  integer $count
     * @return $this
     */
    public function times($count)
    {
        $this->times = $count;

        return $this;
    }


    /**
     * Make a record in the database
     *
     * @param  string $type Model name
     * @param  array  $fields
     * @return void
     */
    protected function make($type, $fields = [])
    {
        while ($this->times --) {
            $stub = array_merge($this->getStub(), $fields);

            $type::create($stub);
        }
    }


    /**
     * Get json response
     *
     * @param  string $uri
     * @param string  $method
     * @param array   $parameters
     * @param array   $headers The server parameters (HTTP headers are referenced with a HTTP_ prefix as PHP does)
     * @return object
     */
    protected function getJson($uri, $method = 'GET', $parameters = [], $headers = [])
    {
        $parameters = array_merge($parameters, ['_token' => csrf_token()]);

        $server = array_merge($headers, ['HTTP_AUTHORIZATION' => 'Basic ' . base64_encode('com.airplug.mao.agent:com.airplug.mao.agent')]);

        return json_decode($this->call($method, $uri, $parameters, [], $server)->getContent());
    }


    /**
     * Helper function for multiple "assertObjectHasAttribute"
     *
     * @return void
     */
    protected function assertObjectHasAttributes()
    {
        $args   = func_get_args();
        $object = array_shift($args);

        foreach ($args as $attribute) {
            $this->assertObjectHasAttribute($attribute, $object);
        }
    }

    // abstract protected function getStub();

    /**
     * Abstract get
     *
     * @return [type] [description]
     */
    protected function getStub()
    {
        throw new BadMethodCallException('Create your own getStub method to declare your fields.');
    }
}