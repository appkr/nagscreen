<?php

class VocApiTest extends ApiTester
{

    use Factory;

    /** Original */
    // Event::fire('foo', array('name' => 'Dayle'));
    /** Mocking Facades */
    // Event::shouldReceive('fire')->once()->with('foo', array('name' => 'Dayle'));
    // $this->call('GET', '/');

    /** @test */
    public function it_fetches_vocs()
    {
        // arrange
        $this->times(2)->make('Voc');

        // act
        $vocs = $this->getJson('voc/api');

        // assert
        $this->assertResponseOk();
    }


    /** @test */
    public function it_fetches_a_single_voc()
    {
        $this->make('Voc');

        $voc = $this->getJson('voc/api/1')->data;

        $this->assertResponseOk();
        $this->assertObjectHasAttributes($voc, 'package', 'title', 'content', 'email');
    }


    /** @test */
    public function it_404s_if_a_voc_is_not_found()
    {
        $object = $this->getJson('voc/api/1000');

        $this->assertResponseStatus(404);
    }


    /** @teset */
    public function it_throws_a_401_if_a_new_voc_request_has_invalid_credential()
    {
        $object = $this->getJson('voc/api', 'POST', exchange_package_name($this->getStub()), ['HTTP_AUTHORIZATION' => null]);

        $this->assertResponseStatus(401);
        $this->assertObjectHasAttribute('error', $object);
    }


    /** @teset */
    public function it_throws_a_401_if_a_new_voc_request_has_invalid_csrf_token()
    {
        $object = $this->getJson('voc/api', 'POST', ['_token' => null]);

        $this->assertResponseStatus(401);
        $this->assertObjectHasAttribute('error', $object);
    }


    /** @test */
    public function it_throws_a_422_if_a_new_voc_request_fails_validation()
    {
        $object = $this->getJson('voc/api', 'POST');

        $this->assertResponseStatus(422);
        $this->assertObjectHasAttribute('error', $object);
    }


    /** @test */
//    public function it_creates_a_new_voc_given_valid_parameters()
//    {
//        $voc = $this->getJson('voc/api', 'POST', exchange_package_name($this->getStub()))->data;
//
//        $this->assertResponseStatus(201);
//        $this->assertObjectHasAttributes($voc, 'package', 'title', 'content', 'email');
//    }


    /**
     * prepare test Stub
     *
     * @return array
     */
    protected function getStub($fields = [])
    {
        $package_ids = Package::lists('id');
        $package_id  = $this->fake->randomElement($package_ids);
        $package     = Package::findOrFail($package_id)->package;

        return array_merge([
            'package_id' => $package_id,
            'title'      => 'test ' . $this->fake->sentence(4),
            'content'    => 'test ' . $this->fake->paragraph(2),
            'email'      => $this->fake->email()
        ], $fields);
    }

}