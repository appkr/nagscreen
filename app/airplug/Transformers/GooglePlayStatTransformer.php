<?php namespace airplug\Transformers;

class GooglePlayStatTransformer extends Transformer
{

    /**
     * Transform single data
     *
     * @param array $stat
     * @return array
     */
    public function transform(array $stat)
    {
        return [
            'package'               => $stat['package']['package'],
            'date'                  => $stat['date'],
            'daily_user_installs'   => $stat['daily_user_installs'] ? (int) $stat['daily_user_installs'] : 0,
            'daily_user_uninstalls' => $stat['daily_user_uninstalls'] ? (int) $stat['daily_user_uninstalls'] : 0,
            'rank_kr'               => $stat['rank_kr'] ? (int) $stat['rank_kr'] : null
        ];
    }

}