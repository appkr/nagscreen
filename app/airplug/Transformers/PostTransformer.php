<?php namespace airplug\Transformers;

class PostTransformer extends Transformer
{

    /**
     * Transform single data
     *
     * @param  array $item
     * @return array
     */
    public function transform(array $post)
    {
        return [
            'id'             => (int) $post['id'],
            'category'       => $post['category']['name'],
            'package'        => $post['package']['package'],
            'language'       => $post['language'],
            'title'          => $post['title'],
            'content_plain'  => preg_replace('/&#?[a-z0-9]{2,8};/i',' ',strip_tags($post['content'])),
            'content_simple' => strip_tags($post['content'], '<br><p>'),
            'content'        => $post['content'],
            'email'          => $post['user']['email'],
            'url'            => $post['url'],
            'created_at'     => (string) $post['created_at'],
            'updated_at'     => (string) $post['updated_at']
        ];
    }

}