<?php namespace airplug\Transformers;

class NewGooglePlayReviewTransformer extends Transformer
{

    /**
     * Transform single data
     *
     * @param array $review
     * @return array
     */
    public function transform(array $review)
    {
        return [
            'reviewer' => $review['reviewer'] ? : null,
            'rating'   => (int) $review['rating'] ? : null,
            'title'    => $review['title'] ? : null,
            'content'  => $review['content'] ? : null,
            'language' => $review['language'] ? : null,
            'version'  => $review['version'] ? : null,
            'device'   => $review['device'] ? : null
        ];
    }

}