<?php namespace airplug\Transformers;

class BannerTransformer extends Transformer
{

    /**
     * Transform single data
     *
     * @param  array $item
     * @return array
     */
    public function transform(array $banner)
    {
        return [
            'id'                 => (int) $banner['id'],
            'package'            => $banner['package']['package'],
            'title'              => $banner['package']['name'],
            'excerpt_ko'         => $banner['excerpt_ko'] ? $banner['excerpt_ko'] : null,
            'excerpt_en'         => $banner['excerpt_en'] ? $banner['excerpt_en'] : null,
            'launcher_image_url' => $banner['launcher_image'] ? \Config::get('app.url').'/banners/'.$banner['launcher_image'] : null,
            'banner_image_url'   => $banner['banner_image'] ? \Config::get('app.url').'/banners/'.$banner['banner_image'] : null,
            'android_url'        => $banner['android_url'],
            'pc_url'             => $banner['pc_url']
        ];
    }

}