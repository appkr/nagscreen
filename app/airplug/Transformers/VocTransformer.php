<?php namespace airplug\Transformers;

class VocTransformer extends Transformer
{

    /**
     * Transform single data
     *
     * @param  array $item
     * @return array
     */
    public function transform(array $voc)
    {
        return [
            'id'         => (int) $voc['id'],
            'package'    => $voc['package']['package'],
            'title'      => $voc['title'],
            'content'    => $voc['content'],
            'email'      => $voc['email'],
            'created_at' => (string) $voc['created_at'],
            'updated_at' => (string) $voc['updated_at']
        ];
    }

}