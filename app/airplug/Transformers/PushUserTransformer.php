<?php namespace airplug\Transformers;

class PushUserTransformer extends Transformer
{

    /**
     * Transform single data
     *
     * @param  array $pushUser
     * @return array
     */
    public function transform(array $pushUser)
    {
        return [
            'id'              => (int) $pushUser['id'],
            'package'         => $pushUser['package']['package'],
            'uuid'            => $pushUser['uuid'],
            'registration_id' => $pushUser['registration_id'],
            'created_at'      => (string) $pushUser['created_at'],
            'updated_at'      => (string) $pushUser['updated_at']
        ];
    }

}