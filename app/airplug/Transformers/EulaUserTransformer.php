<?php namespace airplug\Transformers;

class EulaUserTransformer extends Transformer
{

    /**
     * Transform single data
     *
     * @param array $eulaUser
     * @return array
     */
    public function transform(array $eulaUser)
    {
        return [
            'id'            => (int) $eulaUser['id'],
            'eula_version'  => (int) $eulaUser['eula_version'],
            'eula_language' => $eulaUser['eula_language'],
            'package'       => $eulaUser['package']['package'],
            'apppkg'        => $eulaUser['apppkg'],
            'agent_version' => $eulaUser['agent_version'],
            'androidID'     => $eulaUser['androidID'],
            'eula_accept'   => $eulaUser['eula_accept']
        ];
    }

}