<?php namespace airplug\Transformers;

class GooglePlayReviewTransformer extends Transformer
{

    /**
     * Transform single data
     *
     * @param array $review
     * @return array
     */
    public function transform(array $review)
    {
        return [
            'package'  => $review['package']['package'],
            'date'     => $review['date'] ? : null,
            'reviewer' => $review['reviewer'] ? : null,
            'rating'   => (int) $review['rating'] ? : null,
            'title'    => $review['title'] ? : null,
            'content'  => $review['content'] ? : null,
            'language' => $review['language'] ? : null,
            'version'  => $review['version'] ? : null,
            'device'   => $review['device'] ? : null
        ];
    }

}