<?php namespace airplug\Transformers;

abstract class Transformer
{

    /**
     * Transform collection of data
     *
     * @param  array $items
     * @return array
     */
    public function transformCollection(array $items)
    {
        return array_map([$this, 'transform'], $items);
    }

    /**
     * Calculate GooglePlayStat data and transform it
     *
     * @param array $items
     * @return array
     */
    public function calculateAndTransformCollection(array $items) {
        $data = array_map([$this, 'calculate'], $items);
        $data[0]['install_change'] = $this->getGrowthRate($data[0]['install_count'], $data[1]['install_count']);
        $data[0]['uninstall_change'] = $this->getGrowthRate($data[0]['uninstall_count'], $data[1]['uninstall_count']);

        return $data[0];
    }


    /**
     * Transform single data
     *
     * @param  array $item
     * @return array
     */
    public abstract function transform(array $item);

    /**
     * @return callable
     */
    private function calculate($item) {
        $res = [
            'install_count'   => 0,
            'uninstall_count' => 0
        ];

        foreach ($item as $entry) {
            $res['install_count'] += $entry['daily_user_installs'];
            $res['uninstall_count'] += $entry['daily_user_uninstalls'];
        }

        return $res;
    }

    /**
     * @param $a
     * @param $b
     * @return float
     */
    private function getGrowthRate($a, $b) {
        return round(($a - $b) / $a, 2);
    }

}