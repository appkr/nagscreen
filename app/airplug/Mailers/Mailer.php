<?php namespace airplug\Mailers;

class Mailer {

    /**
     * Base abstract mailer
     *
     * @param            $to
     * @param            $subject
     * @param            $view
     * @param array      $data
     * @param array|null $cc
     * @return mixed
     */
    public function send($to, $subject, $view, $data = [], $cc = []) {
        $method = \App::environment('production') ? 'queue' : 'send';
        return \Mail::$method($view, $data, function ($message) use ($to, $subject, $cc) {
            $message->to($to);
            if (! empty($cc)) $message->cc($cc);
            $message->subject('[' . \Config::get('setting.project.name') . "] {$subject}");
        });
    }

    /**
     * Notify administrators when there is a new entry registered in the site
     *
     * @param array $data
     * @return mixed
     */
    public function newPost($data = []) {
        return $this->send(\Config::get('setting.email.to_address'), 'New Entry Created.', 'emails.post', $data, \Config::get('setting.email.cc_address'));
    }

    /**
     * Send mail nagging activation of the newly registered user account
     *
     * @param $to
     * @param $data
     * @return mixed
     */
    public function activateUser($to, $data) {
        return $this->send($to, 'Activate your account.', 'emails.auth.activate', $data);
    }

    /**
     * Send mail to the external app user when there is a new feedback is created
     *
     * @param $to
     * @param $data
     * @return mixed
     */
    public function newFeedback($to, $data) {
        return $this->send($to, $data['subject'], 'emails.feedback', $data);
    }

    /**
     * Send mail to airplug member who generate one-file file upload url and hand it over the this file uploader
     *
     * @param $to
     * @param $data
     * @return mixed
     */
    public function newFilebox($to, $data) {
        return $this->send($to, 'One-time upload URL has been consumed and New file created.', 'emails.filebox', $data);
    }

    /**
     * Send Daily Google Play Update mail
     *
     * @param $to
     * @param $summary
     * @internal param $data
     * @return mixed
     */
    public function notifyGooglePlayUpdate($to, $summary) {
        return $this->send($to, 'Google Play Daily Updates, ' . $summary['summary']['date'], 'emails.notify_google_play_update', $summary);
    }

    /**
     * Send Daily Google Play Update mail
     *
     * @param $to
     * @param $summary
     * @internal param $data
     * @return mixed
     */
    public function newNotifyGooglePlayUpdate($to, $summary) {
        return $this->send($to, 'Google Play Daily Updates, ' . $summary['summary']['date'], 'emails.new_notify_google_play_update', $summary);
    }

} 