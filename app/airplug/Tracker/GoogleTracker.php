<?php namespace airplug\Tracker;

use UnitedPrototype\GoogleAnalytics;

class GoogleTracker {

    /**
     * @var \UnitedPrototype\GoogleAnalytics\Tracker
     */
    private $tracker;

    /**
     * @var \UnitedPrototype\GoogleAnalytics\Visitor
     */
    private $visitor;

    /**
     * @var \UnitedPrototype\GoogleAnalytics\Session
     */
    private $session;

    public function __construct($resolution = '1024x768')
    {
        $this->tracker = new GoogleAnalytics\Tracker('UA-46212331-2', 'airplug.com');

        $this->visitor = new GoogleAnalytics\Visitor();
        $this->visitor->setIpAddress(isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : null);
        $this->visitor->setUserAgent(isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : null);
        $this->visitor->setScreenResolution($resolution);

        $this->session = new GoogleAnalytics\Session();
    }

    /**
     * @param        $identifier
     * @param        $description
     * @param string $resolution
     */
    public function trackPageView($identifier, $description)
    {
        // Assemble Page information
        $page = new GoogleAnalytics\Page($identifier);
        $page->setTitle($description);

        // Track page view
        $this->tracker->trackPageview($page, $this->session, $this->visitor);
    }

    /**
     * @param string  $category
     * @param string  $action
     * @param string  $label
     * @param integer $value
     * @param bool    $noninteraction
     */
    public function trackEvent($category, $action, $label = null, $value = null, $noninteraction = false)
    {
        // Prepare Event object
        $event = new GoogleAnalytics\Event($category, $action, $label, $value, $noninteraction);

        // Track event
        $this->tracker->trackEvent($event, $this->session, $this->visitor);
    }

    /**
     * @param      $network
     * @param      $action
     * @param null $target
     * @param null $pageIdentifier
     */
    public function trackSocial($network, $action, $target = null, $pageIdentifier = null)
    {
        // Assemble Page information
        $page = new GoogleAnalytics\Page($pageIdentifier);

        // Define Social Interaction
        $socialInteraction = new GoogleAnalytics\SocialInteraction($network, $action, $target);

        // Track Social Interaction
        $this->tracker->trackSocial($socialInteraction, $page, $this->session, $this->visitor);
    }
} 