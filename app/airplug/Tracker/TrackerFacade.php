<?php namespace airplug\Tracker;

use Illuminate\Support\Facades\Facade;

class TrackerFacade extends Facade
{

    public static function getFacadeAccessor()
    {
        return 'tracker';
    }

}