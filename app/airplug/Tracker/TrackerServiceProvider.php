<?php namespace airplug\Tracker;

use airplug\Tracker\GoogleTracker;
use Illuminate\Support\ServiceProvider;

class TrackerServiceProvider extends ServiceProvider
{

    public function register()
    {
        $this->app->bind('tracker', function () {
            return new GoogleTracker();
        });
    }
}