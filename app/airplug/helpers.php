<?php

/**
 * Build array of ids from the given unanimous Eloquent collection
 *
 * @param $collection
 * @return array
 */
function make_id_array_from_eloqent_collection($collection) {
    $id_array = [];

    foreach ($collection as $model) {
        $id_array[] = $model->id;
    }

    return $id_array;
}

/**
 * Resize and save image file
 *
 * @param \Symfony\Component\HttpFoundation\File\UploadedFile $file
 * @param                                                     $path
 * @param string                                              $prefix
 * @param int                                                 $width
 * @return array|bool
 */
function save_image(Symfony\Component\HttpFoundation\File\UploadedFile $file, $path, $prefix = 'img', $width = 720) {
    $file_name = $prefix . '_' . time() . '_' . $file->getClientOriginalName();
    $save_path = $path . '/' . $file_name;

    $image = Image::make($file->getRealPath());

    if ($image->width() > $width) {
        $image->resize($width, null, function ($constraint) {
            $constraint->aspectRatio();
        });
    }

    if ($image->save($save_path)) return ['name' => $file_name, 'size' => $file->getSize()];

    return false;
}

/**
 * Build pretty date format
 *
 * @param \Carbon\Carbon $timestring
 * @param int            $interval
 * @return string
 */
function format_timestring(Carbon\Carbon $timestring = null, $interval = 30) {
    if (! is_a($timestring, 'Carbon\Carbon')) return '<span class="text-muted">null</span>';

    return ($timestring->diffInDays() > $interval)
        ? $timestring->toFormattedDateString()
        : $timestring->diffForHumans();
}

/**
 * Build format for Not Published Nag Messages(=Post)
 *
 * @param $published
 * @return null|string
 */
function format_draft($published) {
    return (! $published) ? 'class="draft"' : null;
}

/**
 * Build format for column name which is active for sorting
 *
 * @param $column
 * @return null|string
 */
function format_active_sort($column) {
    if (! Input::get('sort') && ($column == 'created_at' || $column == 'last_login')) {
        return '<i class="fa fa-sort-alpha-desc"></i>';
    }

    $glyphicon = (Input::get('order') == 'asc')
        ? '<i class="fa fa-sort-alpha-asc"></i>'
        : '<i class="fa fa-sort-alpha-desc"></i>';

    return (Input::get('sort') == $column) ? $glyphicon : null;
}

/**
 * Build a link for sorting to work
 *
 * @param $route
 * @param $column
 * @param $text
 * @return string
 */
function link_for_sort($route, $column, $text) {
    $order = (Input::get('order') == 'asc') ? 'desc' : 'asc';

    return link_to_route($route, $text, array_merge(
        Input::except(['page', 'sort', 'order']),
        ['sort' => $column, 'order' => $order]
    ));
}

/**
 * Prepare input array against Post Page call
 *
 * @param array $input
 * @return array
 */
function prepare_input_for_post(array $input) {
    $input = set_limit_param($input, 20);
    $input = set_sort_param($input, 'id');
    $input = set_order_param($input, 'desc');
    $input = set_filter_param($input);
    $input = set_question_param($input);

    return $input;
}

/**
 * Prepare input array against Post Api call
 *
 * @param array $input
 * @return array
 */
function prepare_input_for_post_api_call(array $input) {
    $input = exchange_package_for_package_id($input);
    $input = exchange_category_for_category_id($input);
    $input = set_limit_param($input, 10);
    $input = set_order_param($input, 'desc');
    $input = set_interval_param($input, 30);

    return $input;
}

/**
 * Prepare input array against VoC Api call
 *
 * @param array $input
 * @return array
 */
function prepare_input_for_voc_api_call(array $input) {
    $input = exchange_package_for_package_id($input);
    $input = set_limit_param($input, 25);
    $input = set_order_param($input, 'desc');

    return $input;
}

/**
 * Prepare input array against EulaUser Api|Eula Page call
 *
 * @param array $input
 * @return array
 */
function prepare_input_for_eula(array $input) {
    return exchange_package_for_package_id($input);
}

/**
 * Prepare input array against History Page call
 *
 * @param array $input
 * @return array
 */
function prepare_input_for_history(array $input) {
    $input = set_keyword_param($input);
    $input = set_limit_param($input, 50);
    $input = set_sort_param($input, 'id');
    $input = set_order_param($input, 'desc');

    return $input;
}

/**
 * Prepare input array for user index page
 *
 * @param array $input
 * @return array
 */
function prepare_input_for_user(array $input) {
    $input = set_sort_param($input, 'last_login');
    $input = set_order_param($input, 'desc');

    return $input;
}

/**
 * Prepare input array for banner index page
 *
 * @param array $input
 * @return array
 */
function prepare_input_for_banner(array $input) {
    $input = set_order_param($input, 'desc');
    $input = set_limit_param($input, 5);

    return $input;
}

/**
 * Prepare input array against Filebox page
 *
 * @param array $input
 * @return array
 */
function prepare_input_for_filebox(array $input) {
    $input = set_sort_param($input, 'id');
    $input = set_order_param($input, 'desc');
    $input = set_question_param($input);

    return $input;
}

/**
 * Prepare input array for GooglePlayApi call
 *
 * @param array $input
 * @return array
 */
function prepare_input_for_google_play_api(array $input) {
    $input = exchange_package_for_package_id($input);
    $input = set_order_param($input, 'desc');
    $input = set_limit_param($input, 30);

    return $input;
}

/**
 * @param array $input
 * @return array
 */
function abandon_null_input(array $input) {
    foreach ($input as $key => $value) {
        if (is_null($value)) unset($input[$key]);
    }

    return $input;
}

/**
 * @param array $input
 * @return array
 * @throws airplug\Exceptions\ModelNotFoundException
 */
function exchange_package_for_package_id(array $input) {
    if (isset($input['package'])) {
        if (! $package = Package::wherePackage($input['package'])->first()) throw new airplug\Exceptions\ModelNotFoundException;

        return exchange_array_item($input, ['package_id' => $package->id], 'package');
    }

    return $input;
}

/**
 * @param array $input
 * @return array
 * @throws airplug\Exceptions\ModelNotFoundException
 */
function exchange_category_for_category_id(array $input) {
    if (isset($input['category'])) {
        if (! $category = Category::whereName($input['category'])->first()) throw new airplug\Exceptions\ModelNotFoundException;

        return exchange_array_item($input, ['category_id' => $category->id], 'category');
    }

    return $input;
}

/**
 * @param array $input
 * @param int   $limit
 * @return array
 */
function set_limit_param(array $input, $limit = 10) {
    $input['limit'] = (isset($input['limit']) && $input['limit'] < $limit) ? $input['limit'] : $limit;

    return $input;
}

/**
 * @param array  $input
 * @param string $sort
 * @return array
 */
function set_sort_param(array $input, $sort = 'id') {
    $input['sort'] = (isset($input['sort'])) ? $input['sort'] : $sort;

    return $input;
}

/**
 * @param array  $input
 * @param string $order
 * @return array
 */
function set_order_param(array $input, $order = 'desc') {
    $input['order'] = (isset($input['order'])) ? $input['order'] : $order;

    return $input;
}

/**
 * @param array $input
 * @return array
 */
function set_filter_param(array $input) {
    $input['filter'] = (isset($input['filter'])) ? $input['filter'] : '';

    return $input;
}

/**
 * @param array $input
 * @return array
 */
function set_question_param(array $input) {
    $input['question'] = (isset($input['question'])) ? $input['question'] : '';

    return $input;
}

/**
 * @param array $input
 * @return array
 */
function set_keyword_param(array $input) {
    $input['keyword'] = (isset($input['keyword'])) ? $input['keyword'] : '';

    return $input;
}

/**
 * @param array $input
 * @param int   $interval
 * @return array
 */
function set_interval_param(array $input, $interval = 30) {
    $input['interval'] = (isset($input['interval'])) ? $input['interval'] : $interval;

    return $input;
}

/**
 * Inject $append_item in exchange for an item which has array index of $remove_key
 *
 * @param  array $input
 * @param array  $append_item
 * @param string $remove_key
 * @return array
 */
function exchange_array_item(array $input, array $append_item = [], $remove_key = '') {
    if (! empty($append_item)) {
        $input = array_merge($input, $append_item);

        return array_flip(array_filter(array_flip($input), function ($item) use ($remove_key) {
            return (! empty($remove_key)) ? $item != $remove_key : true;
        }));
    }

    return $input;
}

/**
 * @param array $input
 * @return array
 */
function append_user_id(array $input) {
    if (Auth::check()) {
        return array_merge($input, ['user_id' => Auth::user()->id]);
    }

    return $input;
}

/**
 * @param array $input
 * @return array
 */
function transform_checkbox_value(array $input) {
    return array_merge($input, [
        'popup'     => isset($input['popup']) ? 1 : null,
        'published' => isset($input['published']) ? 1 : null
    ]);
}

/**
 * Add 0 to the left
 *
 * @param  array $input
 * @return array
 */
function lpad_uuid(array $input) {
    $uuid = null;

    if (isset($input['uuid'])) {
        $uuid = $input['uuid'];
    } elseif (isset($input['androidID'])) $uuid = $input['androidID'];

    if (is_null($uuid)) return $input;

    $len = strlen($uuid);

    if ($len < 16) {
        if ($len == 15) {
            $uuid = '0' . $uuid;
        } elseif ($len == 14) $uuid = '00' . $uuid;
        elseif ($len == 13) $uuid = '000' . $uuid;

        $input['uuid'] = $uuid;
        $input['androidID'] = $uuid;
    }

    return $input;
}

/**
 * Find passed $key from the $search collection's title property and format them
 *
 * @param null|Illuminate\Database\Eloquent\Collection $search
 * @param null                                         $key
 * @return null|stdClass
 */
function highlight_search_match($search = null, $key = null) {
    if (! is_null($search)) {
        $object = (object) ['data' => []];

        foreach ($search as $item) {
            $start = strpos(strip_tags($item->title), $key);

            if ($start !== false) {
                $found = substr($item->title, $start, 60);
                $object->data[] = [
                    'type'  => 'title',
                    'id'    => $item->id,
                    'title' => $item->title
                ];
            } else {
                $start = strpos(strip_tags($item->content), $key);

                $found = substr($item->content, $start, 60);
                $object->data[] = [
                    'type'    => 'content',
                    'id'      => (int) $item->id,
                    'title'   => $item->title,
                    'content' => str_replace($key, "<strong class=\"text-danger\">{$key}</strong>", strip_tags($found))
                ];
            }
        }

        $search = $object;
    }

    return $search;
}

/**
 * Build html string from passed $activity
 *
 * @param History $activity
 * @return string
 */
function build_activity_string(History $activity) {
    $parts = explode('.', $activity->channel);
    $action = end($parts);

    $glyphicon = get_glyphicon($action);
    $link = get_activity_link($activity->trackable_type, $activity->trackable_id, $action);
    $title = isset($activity->title)
        ? $activity->title : strip_tags($activity->content);

    return <<<EOT
    <i class="fa {$glyphicon}"></i>
    <a href="{$link}" title="{$title}">{$title}</a>
EOT;
}

/**
 * Build html string for given $user
 *
 * @param User $user
 * @return string
 */
function build_user_activity_count(User $user) {
    $string = ($user->posts()->count()) ? "<i class=\"fa fa-envelope-o\"></i> {$user->posts()->count()} " : '';
    $string .= ($user->comments()->count()) ? "<i class=\"fa fa-comment\"></i> {$user->comments()->count()} " : '';
    $string .= ($user->feedbacks()->count()) ? "<i class=\"fa fa-comments\"></i> {$user->feedbacks()->count()} " : '';
    $string .= ($user->pushmessages()->count()) ? "<i class=\"fa fa-bullhorn\"></i> {$user->pushmessages()->count()} " : '';

    return empty($string) ? 'null' : $string;
}

/**
 * @param Package $package
 * @return string
 */
function build_package_additional_info_string(Package $package) {
    $string = null;

    if (! is_null($package->banner)) {
        $string .= ($package->banner->launcher_image) ? '<i class="fa fa-rocket" title="Launcher image"></i> ' : null;
        $string .= ($package->banner->banner_image) ? '<i class="fa fa-picture-o" title="Banner image"></i> ' : null;
    }

    return $string;
}

/**
 * @param      $model_name
 * @param bool $compact
 * @return string
 */
function get_alias_from_model_name($model_name, $compact = false) {
    if ($compact) {
        if ($model_name == 'Post') {
            return '<i class="fa fa-envelope-o"></i>';
        } else {
            if ($model_name == 'Comment') {
                return '<i class="fa fa-comment"></i>';
            } else {
                if ($model_name == 'Voc') {
                    return '<i class="fa fa-frown-o"></i>';
                } else {
                    if ($model_name == 'Feedback') {
                        return '<i class="fa fa-comments"></i>';
                    } else {
                        if ($model_name == 'PushMessage') {
                            return '<i class="fa fa-bullhorn"></i>';
                        } else {
                            if ($model_name == 'Apidoc') {
                                return '<i class="fa fa-book"></i>';
                            } else '<i class="fa fa-question"></i>';
                        }
                    }
                }
            }
        }
    } else {
        if ($model_name == 'Post') {
            return '<i class="fa fa-envelope-o"></i> Nag Messages';
        } else {
            if ($model_name == 'Comment') {
                return '<i class="fa fa-comment"></i> Comments to Nags';
            } else {
                if ($model_name == 'Voc') {
                    return '<i class="fa fa-frown-o"></i> Voice of Customers';
                } else {
                    if ($model_name == 'Feedback') {
                        return '<i class="fa fa-comments"></i> Feedbacks to VoC';
                    } else {
                        if ($model_name == 'PushMessage') {
                            return '<i class="fa fa-bullhorn"></i> Push Messages';
                        } else {
                            if ($model_name == 'Apidoc') {
                                return '<i class="fa fa-book"></i> Api Documents';
                            } else '<i class="fa fa-question"></i> Unknown';
                        }
                    }
                }
            }
        }
    }
}

/**
 * Calculate correct link to resource
 *
 * @param $morph_model
 * @param $morph_id
 * @param $action
 * @return string
 */
function get_activity_link($morph_model, $morph_id, $action = null) {
    if ($morph_model == 'Feedback') {
        $voc_id = Feedback::findOrFail($morph_id)->voc->id;

        return ($action != 'destroy') ? route('voc.show', $voc_id) : '#';
    } else {
        if ($morph_model == 'Comment') {
            $post_id = Comment::findOrFail($morph_id)->post->id;

            return ($action != 'destroy') ? route('post.show', $post_id) : '#';
        } else {
            if ($morph_model == 'Post') {
                return ($action != 'destroy') ? route('post.show', $morph_id) : '#';
            } else {
                if ($morph_model == 'Apidoc') {
                    return ($action != 'destroy') ? route('api.index', $morph_id) : '#';
                } else {
                    if ($morph_model == 'Voc') {
                        return ($action != 'destroy') ? route('voc.show', $morph_id) : '#';
                    } else {
                        if ($morph_model == 'PushMessage') {
                            return ($action != 'destroy') ? route('push.show', $morph_id) : '#';
                        } else if ($morph_model == 'Banner') return ($action != 'destroy') ? route('banner.edit', $morph_id) : '#';
                    }
                }
            }
        }
    }

    return '#';
}

/**
 * Calculate correct glyphicon
 *
 * @param $action
 * @return string
 */
function get_glyphicon($action) {
    if ($action == 'store' || $action == 'send') {
        return 'fa-pencil';
    } else {
        if ($action == 'update') {
            return 'fa-cut';
        } else if ($action == 'destroy') return 'fa-trash-o';
    }

    return 'fa-question';
}

/**
 * Get gravata image url
 *
 * @param  string  $email
 * @param  integer $size
 * @return string
 */
function get_gravatar_url($email, $size = 60) {
    return 'http://www.gravatar.com/avatar/' . md5($email) . '?s=' . $size;
}

/**
 * Get gravata profile page url
 *
 * @param  string $email
 * @return string
 */
function get_gravatar_profile_url($email) {
    return 'http://www.gravatar.com/' . md5($email);
}

/**
 * @param Filebox $filebox
 * @return string
 */
function build_file_uploader_string(Filebox $filebox) {
    $email = is_null($filebox->created_by) ? $filebox->user->email : $filebox->created_by;
    $link = get_gravatar_profile_url($email);

    return "<a href=\"{$link}\" title=\"{$email}\">{$email}</a>";
}

/**
 * Evaluate user to decide s/he is an administrator
 *
 * @return boolean
 */
function is_admin() {
    $email = Auth::user()->email;

    $admins = array_merge(['aerosocket@gmail.com', 'juwonkim@airplug.com'], Config::get('setting.email.cc_address'));

    return (in_array($email, $admins)) ? true : false;
}

/**
 * Make redirect url to which a user has to see after signin
 *
 * @param  string $url
 * @return string
 */
function get_location_after_signin($url = null) {
    $path = parse_url($url, PHP_URL_PATH);

    return (in_array($path, ['user/signin']))
        ? '/' : $url;
}

/**
 * Flatten a multi-dimensional associative array with @.
 *
 * @param  array  $array
 * @param  string $prepend
 * @return array
 */
function array_stringify($array, $prepend = '') {
    $results = [];

    foreach ($array as $key => $value) {
        if (is_array($value)) {
            $results = array_merge($results, array_stringify($value, $prepend . $key . '@'));
        } else {
            $results[$prepend . $key] = $value;
        }
    }

    return $results;
}

/**
 * Set an array item to a given value using "@" notation.
 * If no key is given to the method, the entire array will be replaced.
 *
 * @param  array  $array
 * @param  string $key
 * @param  mixed  $value
 * @return array
 */
function array_restore(&$array, $key, $value) {
    if (is_null($key)) return $array = $value;

    $keys = explode('@', $key);

    while (count($keys) > 1) {
        $key = array_shift($keys);

        // If the key doesn't exist at this depth, we will just create an empty array
        // to hold the next value, allowing us to create the arrays to hold final
        // values at the correct depth. Then we'll keep digging into the array.
        if (! isset($array[$key]) || ! is_array($array[$key])) {
            $array[$key] = [];
        }

        $array =& $array[$key];
    }

    $array[array_shift($keys)] = $value;

    return $array;
}

/**
 * Check variable is set and not empty
 *
 * @param  mixed $var
 * @return boolean
 */
function is($var) {
    return (isset($var) && ! empty($var));
}

/**
 * Check UserAgent for android
 *
 * @return boolean
 */
function is_android() {
    return (isset($_SERVER['HTTP_USER_AGENT']) && strpos($_SERVER['HTTP_USER_AGENT'], 'Android')) ? true : false;
}

/**
 * Check UserAgent is mobile
 *
 * @return bool
 */
function is_mobile() {
    return (isset($_SERVER['HTTP_USER_AGENT']) && (strpos($_SERVER['HTTP_USER_AGENT'], 'Android') || strpos($_SERVER['HTTP_USER_AGENT'], 'iPhone'))) ? true : false;
}

/**
 * Check Memcached presence
 *
 * @return bool
 */
function is_memcached() {
    return App::environment('production') ? true : false;
}

/**
 * Calculate human readable file size string
 *
 * @param $filesize
 * @return string
 */
function format_filesize($filesize) {
    if (! is_numeric($filesize)) return 'NaN';

    $decr = 1024;
    $step = 0;
    $suffix = ['b', 'KiB', 'MiB', 'GiB'];

    while (($filesize / $decr) > 0.9) {
        $filesize = $filesize / $decr;
        $step ++;
    }

    return round($filesize, 2) . $suffix[$step];
}

/**
 * parse Accept-Language HTTP header and get Brower's language preference
 *
 * @return array
 */
function get_browser_language() {
    $pattern = '/^(?P<primarytag>[a-zA-Z]{2,8})' . '(?:-(?P<subtag>[a-zA-Z]{2,8}))?(?:(?:;q=)' . '(?P<quantifier>\d\.\d))?$/';
    $languages = [];

    foreach (explode(',', $_SERVER['HTTP_ACCEPT_LANGUAGE']) as $language) {
        $splits = [];
        $languages[] = preg_match($pattern, $language, $splits) ? $splits : null;
    }

    return $languages;
}

/**
 * Helper function for quick check of the user's browser language
 *
 * @return bool
 */
function is_korean_browser() {
    return get_browser_language()[0]['primarytag'] == 'ko';
}

/**
 * Get active Google Play package list
 *
 * @param bool $with_package_id Set it true if you need [package=>package_id] type
 * @return array
 */
function get_active_google_play_packages($with_package_id = false) {
    $packages = GooglePlayApp::with('package')->whereStatus(1)->get();
    $list = [];

    foreach ($packages as $package) {
        if (! $with_package_id) {
            $list[] = $package->package->package;
        } else $list[$package->package->package] = (int) $package->package_id;
    }

    return $list;
}

/**
 * Calculate diff between two numbers
 *
 * @param int $a
 * @param int $b
 * @return string
 */
function number_diff($a, $b) {
    return number_format($a - $b);
}

/**
 * Calculate increase rate between two numbers and format it
 *
 * @param int $a
 * @param int $b
 * @return string
 */
function increase_rate($a, $b) {
    if (! $a || ! $b) return null;
    $product = round(($a - $b) / $a * 100);

    return ($product > 0) ? '<span style="color:green">+' . $product . '%</span>' : '<span style="color:red">' . $product . '%</span>';
}

/**
 * Format red or green depending on positive or negative
 *
 * @param $number
 * @return string
 */
function color_rate($number, $reverse = false) {
    if ($reverse) {
        return ($number > 0) ? '<span style="color:red">+' . round($number) . '%</span>' : '<span style="color:green">' . round($number) . '%</span>';
    }
    return ($number > 0) ? '<span style="color:green">+' . round($number) . '%</span>' : '<span style="color:red">' . round($number) . '%</span>';
}

/**
 * Calculate retention ratio between two numbers and format it
 *
 * @param int $a
 * @param int $b
 * @return string
 */
function retention_ratio($a, $b) {
    if (! $a || ! $b) return null;
    $diff = $a - $b;
    $product = round(($diff) / $a * 100);

    return $product . '%<br/>(' . number_format($diff) . '/' . number_format($a) . ')';
}

/**
 * Calculate increase in Google Play ranking and format it
 *
 * @param int $a
 * @param int $b
 * @return null|string
 */
function rank_diff($a, $b) {
    if (! $a || ! $b) return null;
    $product = $a - $b;

    return ($product > 0) ? '<span style="color:green">+' . $product . '</span>' : '<span style="color:red">' . $product . '</span>';
}

/**
 * Build a star rating string
 *
 * @param int $score
 * @return string
 */
function format_rating($score) {
    switch (round($score)) {
        case 0:
            return '☆☆☆☆☆';
            break;
        case 1:
            return '★☆☆☆☆';
            break;
        case 2:
            return '★★☆☆☆';
            break;
        case 3:
            return '★★★☆☆';
            break;
        case 4:
            return '★★★★☆';
            break;
        case 5:
            return '★★★★★';
            break;
    }
}

/**
 * Get the thumbnail ulr of Youtube based on passed $youtube_id argument
 *
 * @param string $youtube_id
 * @return string
 */
function get_youtube_thumbnail_url($youtube_id) {
    return "http://i.ytimg.com/vi/{$youtube_id}/0.jpg";
}

/**
 * Get AirPlug seasoned video's url
 * The video was downloaded from YouTube and serviced from AirPlug server
 * The video is capable of BB regardless of UE's BB capability
 * The url is redirect to JP,US,EU server depending on the UE's location
 *
 * @param $airplug_id
 * @return string
 */
function get_bb_video_url($airplug_id) {
    return "http://tfrontg1.airplug.co.kr/media/{$airplug_id}.mp4";
}

/**
 * Calculate and format bitrate of the given video
 *
 * @param int $size
 * @param int $duration
 * @return string
 */
function get_bb_video_bitrate($size, $duration) {
    return number_format(round(($size * 8) / $duration));
}

/**
 * Get the original YouTube video page url
 *
 * @param string $youtube_id
 * @return string
 */
function get_youtube_page_url($youtube_id) {
    return "http://www.youtube.com/watch?v={$youtube_id}";
}

/**
 * Calculate the video url having custom url scheme
 *
 * @param string $airplug_id
 * @return string
 */
function get_bb_custom_url($airplug_id) {
    return "airsocket://playvideo?url=" . urlencode(get_bb_video_url($airplug_id));
}

/**
 * Get the xml url of the given video
 *
 * @param $airplug_id
 * @return string
 */
function get_bb_xml_url($airplug_id) {
    return "http://vpg.airplug.com/vp/feeds/{$airplug_id}.xml";
}

/**
 * Convert and format given timestamp to human readable time string
 *
 * @param int $timestamp
 * @return string
 */
function time2date($timestamp) {
    return \Carbon\Carbon::createFromTimestamp($timestamp)->diffForHumans();
}

/**
 * Replace http to https
 *
 * @param $addr
 * @return mixed
 */
function get_http_addr($addr) {
    return str_replace("https://", "http://", $addr);
}
