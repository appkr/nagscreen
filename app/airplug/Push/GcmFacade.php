<?php namespace airplug\Push;

use Illuminate\Support\Facades\Facade;

class GcmFacade extends Facade
{

    public static function getFacadeAccessor()
    {
        return 'gcm';
    }

}