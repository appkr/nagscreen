<?php namespace airplug\Push;

use Guzzle\Service\Client;
use Illuminate\Foundation\AliasLoader;
use Illuminate\Support\ServiceProvider;
use Config;

class GcmServiceProvider extends ServiceProvider
{

    public function register()
    {
        $this->app->bind('gcm', function () {
            return new GcmAPI(new Client());
        });
    }


    // public function boot()
    // {
    //     $this->package('airplug/Push');

    //     AliasLoader::getInstance()->alias(
    //         'Gcm',
    //         'airplug\Push\GcmAPI'
    //     );
    // }

}