<?php namespace airplug\Push;

use Guzzle\Service\Client;
use Package;
use PushUser;
use Config;
use Redirect;

class GcmAPI
{

    /**
     * @var \Guzzle\Service\Client
     */
    protected $client;

    /**
     * @param Client $client
     */
    public function __construct(Client $client)
    {
        $this->client = $client;
    }


    /**
     * method for push queue
     */
    public function fire($job, $data)
    {
        $this->send($data['input'], $data['retry_receivers']);
    }


    /**
     * @param array $input
     * @param array $retry_receivers
     * @return mixed
     * @throws Exception
     */
    public function send(array $input, $retry_receivers = [])
    {
        static $success_count = 0;

        $package_name = Package::findOrFail($input['package_id'])->pluck('name');

        $receivers = (empty($retry_receivers))
            ? $this->getReceivers($input['package_id'])
            : $retry_receivers;

        $payload = [
            'collapse_key'     => $package_name,
            'delay_while_idle' => true,
            'data'             => [
                'ticker'  => $input['ticker'],
                'title'   => $input['title'],
                'message' => $input['content']
            ],
            'dry_run'          => true
        ];

        // Instantiate and Ready Http POST request to the Google server
        $request = $this->client->createRequest('POST', 'https://android.googleapis.com/gcm/send');
        $request->setHeader('Authorization', 'key=' . Config::get('setting.push.key'));
        $request->setHeader('Content-type', 'application/json');

        if (! empty($receivers)) 
        {
            foreach (array_chunk($receivers, 1000) as $index => $chunked_receivers) 
            {
                $payload['registration_ids'] = $chunked_receivers;

                $request->setBody(json_encode($payload));

                $response = $this->client->send($request);

                $response = $response->json();

                if (! $response) throw new Exception ('Null returned', 500);

                if ($response['failure'] > 0 || $response['canonical_ids'] > 0) 
                {
                    $this->retry($input, $chunked_receivers, $response);
                }

                $success_count += $response['success'];
            }
        }

        return $success_count;
    }


    /**
     * @param $input
     * @param $original_receivers
     * @param $response
     */
    protected function retry($input, $original_receivers, $response)
    {
        // TODO To test whether it works, as it has never been hit before

        $ii        = 0;
        $receivers = [];

        while (isset($response['results'][$ii])) 
        {

            if (isset($response['results'][$ii]['error'])) 
            {
                if ($response['results'][$ii]['error'] == 'Unavailable') 
                {
                    // Prepare to retry send by building new receivers array
                    $receivers[] = $original_receivers[$ii];
                }
                else if (in_array($response['results'][$ii]['error'], ['NotRegistered', 'InvalidRegistration'])) 
                {
                    // Delete corresponding 'registration_id' in the table
                    $pushuser = PushUser::where('registration_id', '=', $original_receivers[$ii]);
                    if ($pushuser) $pushuser->delete();
                }
            }

            if (isset($response['results'][$ii]['registration_id'])) 
            {
                // Update corresponding 'registration_id' in the table
                $pushuser = PushUser::where('registration_id', '=', $original_receivers[$ii]);

                if ($pushuser) {
                    $pushuser->update([
                        'registration_id' => $response['results'][$ii]['registration_id']
                    ]);
                }

                $receivers[] = $response['results'][$ii]['registration_id'];
            }

            $ii ++;
        } // end while

        if (! empty($receivers)) 
        {
            for ($retry_count = 0; $retry_count < 2; $retry_count ++) 
            {
                $this->send($input, $receivers);
                sleep(1);
            }
        }
    }


    /**
     * @param $package_id
     * @return mixed
     */
    protected function getReceivers($package_id)
    {
        return \DB::table('push_users')->whereNotNull('registration_id')->where('package_id', '=', $package_id)->lists('registration_id');
    }
}